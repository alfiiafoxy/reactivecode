-- CREATE table users(
-- 	id serial primary key,
-- 	first_name character varying (30) not null,
-- 	last_name character varying (30) not null,
-- 	email character varying(100) not null,
-- 	password character varying (30) not null,
-- 	phone character varying (20) not null,
-- 	keywords character varying(50) null,
-- 	date_of_birth character varying (20) not null,
-- 	role character varying (15) not null
-- );

-- Create table active_users (
-- email character varying(100) not null,
-- token UUID not null,
-- role character varying (15) not null
-- );

CREATE TABLE "user" (
	id serial NOT NULL,
	first_name text NOT NULL,
	last_name text NOT NULL,
	email text NOT NULL,
	password text NOT NULL,
	birth_date DATE NOT NULL,
	user_role TEXT NOT NULL,
	token TEXT NOT NULL,
	avatar bytea null,
	phone character varying (20) not null,
	keywords character varying(50) null,
	about TEXT NULL,
	rate_per_hour integer NULL,
	CONSTRAINT user_pk PRIMARY KEY (id)
);


CREATE TABLE "groups" (
	id serial NOT NULL,
	name TEXT NOT NULL,
	level TEXT NOT NULL,
	teacher_id integer NOT NULL,
	city TEXT NOT NULL,
	CONSTRAINT groups_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE "groups_detail" (
	id serial NOT NULL,
	group_id integer NOT NULL,
	student_id integer NOT NULL,
	CONSTRAINT groups_detail_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE "lessons" (
	id serial NOT NULL,
	group_id integer NOT NULL,
	start_time TIMESTAMP NOT NULL,
	finish_time TIMESTAMP NOT NULL,
	date TIMESTAMP NOT NULL,
	lesson_type TEXT NOT NULL,
	CONSTRAINT lessons_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE "attendance" (
	id serial NOT NULL,
	lesson_id integer NOT NULL,
	student_id integer NOT NULL,
	attendance boolean NOT NULL,
	CONSTRAINT attendance_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);

ALTER TABLE "groups" ADD CONSTRAINT groups_fk FOREIGN KEY (teacher_id) REFERENCES "user"(id);
ALTER TABLE "groups_detail" ADD CONSTRAINT groups_detail_fk_group FOREIGN KEY (group_id) REFERENCES "groups"(id);
ALTER TABLE "groups_detail" ADD CONSTRAINT groups_detail_fk_user FOREIGN KEY (student_id) REFERENCES "user"(id);
ALTER TABLE "lessons" ADD CONSTRAINT lessons_fk_groups FOREIGN KEY (group_id) REFERENCES "groups"(id);
ALTER TABLE attendance ADD CONSTRAINT attendance_fk_lessons FOREIGN KEY (lesson_id) REFERENCES "lessons"(id);

///Groups
INSERT INTO "groups" (name, level, teacher_id, city)
	VALUES ('FE', 'beginner', 1, 'Kharkov');
////groups_details
    INSERT INTO public.groups_detail(
    	group_id, student_id)
    	VALUES (1, 2);
////lessons

        INSERT INTO public.lessons(
        	group_id, start_time, finish_time, lesson_type, date)
        	VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Webinar', CURRENT_TIMESTAMP);
///attendance


INSERT INTO attendance ( lesson_id, student_id, attendance) VALUES (19,1,true)

user
INSERT INTO "user" (first_name, last_name, email, password, birth_date, user_role, token, phone, keywords)
                    VALUES ('Paras', 'Kasha', 'mumu@gmail.com', 'qqqqqq', CURRENT_TIMESTAMP,
                    'teacher', 'qqq', '+380958287778', 'qqqqqq');

INSERT INTO "user" (first_name, last_name, email, password, birth_date, user_role, token, phone, keywords)
                    VALUES ('Maur', 'Patak', 'qqq@qqq.qqq', 'qqqqqq', CURRENT_TIMESTAMP,
                    'student', 'qqq', '+380956487778', 'qqqqqq');

                    INSERT INTO "user" (first_name, last_name, email, password, birth_date, user_role, token, phone, keywords)
                    VALUES ('Papa', 'Rimskiy', 'qqq@qqq.qqq', 'qqqqqq', CURRENT_TIMESTAMP,
                    'admin', 'qqq', '+380956487778', 'qqqqqq');

select students.user_id, students.firstname, students.lastname, students.age, students.city, students.groups_id
  from students inner join groups on students.groups_id = groups.groups_id
  where groups.teacher_id = ${userId}
     order by students.user_id

INSERT INTO "attendance"() WHERE ( IN (SELECT field1 FROM targetTable))


	ALTER TABLE "lessons"
ADD lesson_type text;

ALTER TABLE "lessons"
UPDATE start_time TIMESTAMPZ NOT NULL,
	finish_time TIMESTAMPZ NOT NULL,
	date TIMESTAMPZ NOT NULL;
	alter table "lessons" alter column columnName type timestamp with time zone;


	ALTER TABLE "lessons" ALTER COLUMN start_time SET DATA TYPE timestamp;
	ALTER TABLE "lessons" ALTER COLUMN finish_time SET DATA TYPE timestamp;
	ALTER TABLE "lessons" ALTER COLUMN date SET DATA TYPE timestamp;

INSERT INTO public."user"(
	first_name, last_name, email, password, birth_date, user_role, phone, token)
	VALUES
	('Lerka', 'Pomogaeva', 'admin01@gmail.com', 'admin', '01.08.1990', 'admin', '+380971510221',1),
	('Alfia', 'Chenyshova', 'admin02@gmail.com', 'admin', '01.08.1990', 'admin', '+380971510221',1),
	('Maria', 'Osadcha', 'admin03@gmail.com', 'admin', '01.08.1990',  'admin', '+380971510221',1),
	('Anton', 'Koshelev', 'admin04@gmail.com', 'admin', '01.08.1990', 'admin', '+380971510221',1),
	('Denis', 'Tkachenko', 'pickupmaster007@gmail.com', 'admin', '01.08.1990','admin', '+380971510221',1),
	('Roman', 'Tretiakov', 'admin06@gmail.com', 'admin', '01.08.1990', 'admin', '+380971510221',1),
	('Vlad', 'ZayaNuPogodi', 'admin07@gmail.com', 'admin', '01.08.1990','admin', '+380971510221',1),
	('Maxim', 'Shvachko', 'admin08@gmail.com', 'admin', '01.08.1990','admin', '+380971510221',1),
	('Alex', 'Sanin', 'admin09@gmail.com', 'qwerty', '01.08.1990', 'admin', '+380971510221',1),
	('Donald', 'Trump', 'donald@gmail.com', 'qwerty', '01.08.1990', 'teacher', '+380971510221',1),
	('Mahatma', 'Gandi', 'gandi@gmail.com', 'qwerty', '01.08.1990', 'teacher', '+380971510221',1),
	('Dart', 'Veider', 'veider@gmail.com', 'qwerty', '01.08.1990', 'teacher', '+380971510221',1),
	('Bruce', 'Lee', 'lee@gmail.com', 'qwerty', '01.08.1990','teacher', '+380971510221',1),
	('Bruce', 'Wayne', 'wayne@gmail.com', 'qwerty', '01.08.1990','teacher', '+380971510221',1),
	('Donald', 'Trump', 'donald@gmail.com', 'qwerty', '01.08.1990', 'teacher', '+380971510221',1),
	('Mahatma', 'Gandi', 'gandi@gmail.com', 'qwerty', '01.08.1990', 'teacher', '+380971510221',1),
	('Dart', 'Veider', 'veider@gmail.com', 'qwerty', '01.08.1990', 'teacher', '+380971510221',1),
	('Bruce', 'Lee', 'lee@gmail.com', 'qwerty', '01.08.1990','teacher', '+380971510221',1),
	('Bruce', 'Wayne', 'wayne@gmail.com', 'qwerty', '01.08.1990','teacher', '+380971510221',1),
	('Barak', 'Obama', 'donald@gmail.com', 'qwerty', '01.08.1990', 'teacher', '+380971510221',1),
	('Vlad', 'Shpitalniy', 'student09@gmail.com', 'qwerty', '01.08.1990', 'student', '+380971510221',1),
	('Bogdan', 'Pryvalov', 'student02@gmail.com', 'qwerty', '01.08.1990', 'student', '+380971510221',1),
	('Bogdan', 'Sorobei', 'student03@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Kristina', 'Tokar', 'student04@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Nikolay', 'Yusov', 'student05@gmail.com', 'qwerty', '01.08.1990', 'student', '+380971510221',1),
	('Borya', 'Andrzhevskiy', 'student07@gmail.com', 'qwerty', '01.08.1990', 'student', '+380971510221',1),
	('Ivan', 'Yuriev', 'student08@gmail.com', 'qwerty', '01.08.1990', 'student', '+380971510221',1),
	('Andrei', 'Belichenko', 'student10@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Aleksandr', 'Grytsenko', 'student11@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Diana', 'Cherednyk', 'student12@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Eleonora', 'Adonkina', 'student13@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Marina', 'Herasimova', 'student14@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Andrey', 'Franko', 'student15@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Evgeniy', 'Belenihin', 'student16@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Daria', 'Semeney', 'student17@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Dina', 'Laptiy', 'student18@gmail.com', 'qwerty', '01.08.1990','student', '+380971510221',1),
	('Dmitriy', 'Medvedev', 'student19@gmail.com', 'qwerty', '01.08.1990','teacher', '+380971510221',1),
	('Aleksandr', 'Nasliedov', 'student14@gmail.com', 'qwerty', '01.08.1990','teacher', '+380971510221',1);
