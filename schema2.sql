CREATE TABLE "user" (
	"id" serial NOT NULL,
	"first_name" TEXT NOT NULL,
	"last_name" TEXT NOT NULL,
	"email" TEXT NOT NULL,
	"password" TEXT NOT NULL,
	"birth_date" DATE NOT NULL,
	"user_role" TEXT NOT NULL,
	"token" TEXT NOT NULL,
	"avatar" bytea NOT NULL,
	"about" TEXT NOT NULL,
	CONSTRAINT "user_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "groups" (
	"id" serial NOT NULL,
	"name" TEXT NOT NULL,
	"level" TEXT NOT NULL,
	"teacher_id" TEXT NOT NULL,
	"city" TEXT NOT NULL,
	CONSTRAINT "groups_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "groups_detail" (
	"id" serial NOT NULL,
	"group_id" TEXT NOT NULL,
	"student_id" TEXT NOT NULL,
	CONSTRAINT "groups_detail_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "lessons" (
	"id" serial NOT NULL,
	"group_id" TEXT NOT NULL,
	"start_time" TIMESTAMP NOT NULL,
	"finish_time" TIMESTAMP NOT NULL,
	"data" TIMESTAMP NOT NULL,
	CONSTRAINT "lessons_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "attendance" (
	"id" serial NOT NULL,
	"lesson_id" TEXT NOT NULL,
	"student_id" TEXT NOT NULL,
	"attendance" BOOLEAN NOT NULL,
	CONSTRAINT "attendance_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);




ALTER TABLE "groups" ADD CONSTRAINT "groups_fk0" FOREIGN KEY ("teacher_id") REFERENCES "user"("id");

ALTER TABLE "groups_detail" ADD CONSTRAINT "groups_detail_fk0" FOREIGN KEY ("group_id") REFERENCES "groups"("id");
ALTER TABLE "groups_detail" ADD CONSTRAINT "groups_detail_fk1" FOREIGN KEY ("student_id") REFERENCES "user"("id");

ALTER TABLE "lessons" ADD CONSTRAINT "lessons_fk0" FOREIGN KEY ("group_id") REFERENCES "groups"("id");

ALTER TABLE "attendance" ADD CONSTRAINT "attendance_fk0" FOREIGN KEY ("lesson_id") REFERENCES "lessons"("id");
ALTER TABLE "attendance" ADD CONSTRAINT "attendance_fk1" FOREIGN KEY ("student_id") REFERENCES "user"("id");
