import { createSelector } from 'reselect';

export const getConfig = state => state.config;
export const getTheme = state => state.theme;
export const getAuthBtnState = state => state.config.isDisabledButtonAuth;
export const getTranslate = state => state.translates.currentDictionary;
export const getScheduleCaption = state => state.config.scheduleData;
export const getMyGroupCaption = state => state.config.myGroupData;
export const getAdminStatistic = state => state.config.adminStatisticData;
export const getModals = state => state.modal;
export const getlocale = state => state.translates.locale;
export const getSchedule = state => state.schedule.schedule;
export const getfirstSelectWeekDay = state => state.schedule.firstSelectWeekDay;
export const getlastSelectWeekDay = state => state.schedule.lastSelectWeekDay;
export const getfirstSelectWeekDayMilliseconds = state => state.schedule.firstSelectWeekDayMilliseconds;
export const getlastSelectWeekDayMilliseconds = state => state.schedule.lastSelectWeekDayMilliseconds;
export const getToken = state => state.account.token;
export const getIsMyAccount = state => state.account.isMyAccount;
export const getcontentName = state => state.account.contentName; // TODO camelCase
export const getStudents = state => state.students;
export const isChangeButton = state => state.attendance.isChangeButton;
export const getSelectedGroup = state => state.groups.selectedGroup;
export const getGroups = state => state.groups.groups;
export const getCurrentLesson = state => state.schedule.currentLesson;
export const getCurrentStudent = state => state.groups.currentStudent;
export const getUserRole = state => state.account.userRole;
export const getTeacherData = state => state.account;
export const getCurrentInput = state => state.dashboard.currentInputUsers;
export const getAttendance = state => state.attendance.attendance;
export const getStudentsAll = state => state.students.allStudents;
export const getUsers = state => state.admin.users;
export const getAccountButton = state => state.accountButton.isAccountButton;
export const getTeachers = createSelector(getUsers, users => {
 return users.filter(user => user.user_role === 'teacher');
});
export const getAllStudents = createSelector(getUsers, users => {
  return users.filter(user => user.user_role === 'student');
});
export const getAddModalData = state => state.modal.addGroupModal.data;
export const getConfirmModalData = state => state.modal.confirmModal.data;
export const getSelectedTeacher = state => state.groups.selectedTeacher;
export const getCurrentTeacher = createSelector(getGroups, getSelectedGroup,
  (groups, selectedGroup) => {
    let name = '';
    groups.forEach(group => {
      if (group.id === selectedGroup) {
        name = `${group.first_name} ${group.last_name}`;
      }
    });
    return name;
  });
export const getDaysOfLessons = state => state.attendance.daysOfLessons;
export const getSalary = state => state.admin.teacherSalary;
export const getUserData = state => state.account.userData;
export const getTotalSalary = state => state.admin.totalStatistic;
export const getDateFrom = state => state.admin.dateFrom;
export const getDateTo = state => state.admin.dateTo;