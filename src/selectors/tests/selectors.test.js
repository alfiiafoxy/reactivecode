import { getConfig, getTheme, getTranslate, getAuthBtnState, getScheduleCaption, getMyGroupCaption, getModals, getSchedule } from '../selectors.js';

describe('testing selector getConfig', () => {
    it('getConfig', () => {
        const state = {
            config: {},
        };
        const actual = getConfig(state);
        assert.deepEqual(actual, state.config);
    });
});

describe('testing selector getTheme', () => {
    it('getTheme', () => {
        const state = {
            theme: {},
        };
        const actual = getTheme(state);
        assert.deepEqual(actual, state.theme);
    });
});

describe('testing selector getTranslates', () => {
    it('getTranslates', () => {
        const state = {
            translates: {
                currentDictionary: {},
            },
        };
        const actual = getTranslate(state);
        assert.deepEqual(actual, state.translates.currentDictionary);
    });
});

describe('testing selector getAuthBtnState', () => {
    it('getAuthBtnState', () => {
        const state = {
            config: {
                isDisabledButtonAuth: {},
            },
        };
        const actual = getAuthBtnState(state);
        assert.deepEqual(actual, state.config.isDisabledButtonAuth);
    });
});

describe('testing selector getScheduleCaption', () => {
    it('getScheduleCaption', () => {
        const state = {
            config: {
                scheduleData: {
                    caption: 'caption',
                },
            },
        };
        const actual = getScheduleCaption(state);
        assert.deepEqual(actual, state.config.scheduleData);
    });
});

describe('testing selector getMyGroupCaption', () => {
    it('getMyGroupCaption', () => {
        const state = {
            config: {
                myGroupData: {
                    caption: 'caption',
                },
            },
        };
        const actual = getMyGroupCaption(state);
        assert.deepEqual(actual, state.config.myGroupData);
    });
});

describe('testing selector getModals', () => {
    it('getModals', () => {
        const state = {
            modal: {
            },
        };
        const actual = getModals(state);
        assert.deepEqual(actual, state.modal);
    });
});
describe('testing selector getSchedule', () => {
    it('getSchedule', () => {
        const state = {
            schedule: 25,
        };
        const actual = getSchedule(state);
        assert.deepEqual(actual, state.schedule);
    });
});
