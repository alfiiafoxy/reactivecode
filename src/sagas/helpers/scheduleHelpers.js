import moment from 'moment';
import { sortByDataStartTime, sortByDateCallback } from '../attendensSaga';

export const createDayOfLesson = (date, group_id, id) => {
  return { date: moment(date).format('DD.MM.YYYY'), group_id: +group_id, id };
};

export const createLessonAttendance = (attendance_id, lesson_id, date) => {
  return { attendance: false, attendance_id, lesson_id, start_time: moment(date).format('DD.MM.YYYY') };
};

export const calculateAttendanceStatistic = (attendance, groupId) => {
  let result = { ...attendance };

  result[groupId].forEach(el => {
    let statistic = 0;
    el.lessons.forEach(item => {
      if (item.attendance) {
        statistic++;
      }
    });
    el.statistic = (100 * statistic / el.lessons.length).toFixed(1);
  });

  return result;
};

export const addAttendanceInGroup = (groupId, attendance, newAttendance, lessonId, date) => {
  let result = { ...attendance };
  if (!result[groupId]) {
    result[groupId] = newAttendance.map(newAtten => createLessonAttendance(newAtten.id, lessonId, date));
  }
  result[groupId].forEach(attend => {
    newAttendance.forEach(newAtten => {
      if (attend.student_id === newAtten.student_id) {
        let createAtten = createLessonAttendance(newAtten.id, lessonId, date);
        attend.lessons.push(createAtten);
        attend.lessons = attend.lessons.sort(sortByDataStartTime);
      }
    });
  });

  result = calculateAttendanceStatistic(result, groupId);

  return result;
};

export const createStudentInAttendance = (lessons, studentId, studentFirstName, studentLastName) => {
  const name = `${studentFirstName} ${studentLastName}`;
  return { lessons: [lessons], statistic: '0.0', student_id: studentId, student_name: name };
};

export const createAttendance = (groupId, students, newAttendance, lessonId, date) => {
  const attendanceArray = [...newAttendance];
  let result = [];

  if (Array.isArray(students[groupId])) {
    for (let key of students[groupId]) {
      for (let newAtten of attendanceArray) {
        if (key.id === newAtten.student_id) {
          let createAtten = createLessonAttendance(newAtten.id, lessonId, date);
          let studentAttendance = createStudentInAttendance(createAtten, key.id, key.first_name, key.last_name);
          result.push(studentAttendance);
        }
      }
    }
    return { [groupId]: result };
  }
};

export const createLessonsDay = (newLesson, groupId) => {
  let lessons = [];
  let result = [];
  lessons.push(newLesson);
  result.push(lessons);
  return { [groupId]: result };
};

export const sortDaysOfLessons = (daysOfLessons, newLesson, groupId) => {
  let result = { ...daysOfLessons };
  if (result[groupId]) {
    result[groupId][0].push(newLesson);
    result[groupId][0] = result[groupId][0].sort(sortByDateCallback);
  } else {
    let tempArray = [];
    tempArray.push(newLesson);
    result[groupId] = [];
    result[groupId][0] = tempArray;
  }
  return result;
};

export const deleteFromLessonsDay = (daysOfLessons, groupId, deleteLessonId) => {
  let result = { ...daysOfLessons };
  let temp = [];
  if (daysOfLessons) {
    const newDaysOfLesson = result[groupId][0].filter(lesson => lesson.id !== deleteLessonId);
    if (!newDaysOfLesson.length) {
      return null;
    }
    temp.push(newDaysOfLesson);
    result[groupId] = temp;
    return result;
  }
};

export const deleteAttendance = (attendance, groupId, deleteLessonId) => {
  if (attendance === null) {
    return null;
  }
  let isEmptyLessons = [];
  let result = { ...attendance };
  result[groupId].forEach(attend => {
    if (attend.lessons.length) {
      let newLessons = attend.lessons.filter(el => el.lesson_id !== deleteLessonId);
      isEmptyLessons = [...isEmptyLessons, newLessons];
      attend.lessons = newLessons;
    }
  });

  if (isEmptyLessons.every(el => !el.length)) {
    return null;
  }

  result = calculateAttendanceStatistic(result, groupId);

  return result;
};