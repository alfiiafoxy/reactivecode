import moment from 'moment';

export const sortLessonByDay = (data, groups) => {
  const result = [];
  let elOfArray = {};
  let lessons = [];
  let currentDate = data[0].date;
  elOfArray.date = currentDate;
  elOfArray.day = data[0].day;

  for (let i = 0; i < data.length; i++) {
    if (currentDate === data[i].date) {
      for (let k = 0; k < groups.length; k++) {
        if (groups[k].id === data[i].group_id) {
          lessons.push({
            id: data[i].id,
            timeStart: moment(data[i].start_time).utcOffset(0).format('HH:mm'),
            timeEnd: moment(data[i].finish_time).utcOffset(0).format('HH:mm'),
            type: data[i].lesson_type,
            groupsId: data[i].group_id,
            groupsName: groups[k].name,
          });
        }
      }
    } else {
      currentDate = data[i].date;
      elOfArray.lessons = sortLessonByTime(lessons);
      result.push(elOfArray);
      elOfArray = {};
      lessons = [];
      elOfArray.date = currentDate;
      elOfArray.day = data[i].day;
      for (let k = 0; k < groups.length; k++) {
        if (groups[k].id === data[i].group_id) {
          lessons.push({
            id: data[i].id,
            timeStart: moment(data[i].start_time).utcOffset(0).format('HH:mm'),
            timeEnd: moment(data[i].finish_time).utcOffset(0).format('HH:mm'),
            type: data[i].lesson_type,
            groupsId: data[i].group_id,
            groupsName: groups[k].name,
          });
        }
      }
    }
  }
  elOfArray.lessons = sortLessonByTime(lessons);
  result.push(elOfArray);
  return result;
};

export const sortLessonByTime = data => {
  return data.sort((a, b) => {
    let startA = a.timeStart.replace(':', '');
    let startB = b.timeStart.replace(':', '');
    if (+startA > +startB) {
      return 1;
    }
    if (+startA < +startB) {
      return -1;
    }
    return 0;
  });
};

export const convertToDate = data => {
  return data.map(el => {
    if (el.date) {
      el.day = moment(el.date).format('dddd');
      el.date = moment(el.date).format('DD.MM.YYYY');
    }
    return el;
  });
};

export const addLessonsSort = (lessons, addedLesson) => {
  let result = [...lessons];
  let currentItem = 0;
  const lessonWithDate = convertToDate([addedLesson]);
  lessonWithDate[0].dataOfLesson = lessonWithDate[0].dataOfLesson.split('-').reverse().join('.');
  let elOfArray = {};
  elOfArray.date = lessonWithDate[0].dataOfLesson;
  elOfArray.day = moment(addedLesson.startTime).format('dddd');

  let lesson = {
    id: lessonWithDate[0].id,
    timeStart: moment(lessonWithDate[0].startTime).format('HH:mm'),
    timeEnd: moment(lessonWithDate[0].endTime).format('HH:mm'),
    type: lessonWithDate[0].typeOfLesson,
    groupsId: lessonWithDate[0].groupsId,
    groupsName: addedLesson.groupsName,
  };

  for (let i = 0; i < result.length; i++) {
    if (!result[i]) {
      continue;
    }
    if (result[i].date === lessonWithDate[0].dataOfLesson) {
      result[i].lessons.push(lesson);
      currentItem = i;
      break;
    } else if (i === lessons.length - 1) {
      elOfArray.lessons = [lesson];
      result.push(elOfArray);
      break;
    }
  }
  if (!result.length) {
    elOfArray.lessons = [lesson];
    return [elOfArray];
  }
  result[currentItem].lessons = sortLessonByTime(result[currentItem].lessons);
  sortDate(result);
  return result;
};

export const sortDate = data => {
    let result = [];
    result.push(data.sort((a, b) => {
      let startA = invert(a.date);
      let startB = invert(b.date);
      if (+startA > +startB) {
        return 1;
      }
      if (+startA < +startB) {
        return -1;
      }
      return 0;
    }));
    return result;
  };

function invert(date) {
  return date.split('.').reverse().join('');
}

// function sortLessonByGroupId(data, groups) { // we can use this function later
//   const result = {};
//   let group = [];
//   const useGroups = [];
//   let currentGroup = data[0].group_id;
//
//   for (let i = 0; i < data.length; i++) {
//     if (useGroups.includes(data[i].group_id)) {
//       continue;
//     }
//       currentGroup = data[i].group_id;
//       for (let j = 0; j < data.length; j++) {
//         if (currentGroup === data[j].group_id) {
//           group.push(data[j]);
//         }
//       }
//       result[data[i].group_id] = sortLessonByDay(group);
//       group = [];
//       useGroups.push(currentGroup);
//     }
//   return result;
// }
