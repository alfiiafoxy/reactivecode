import { apply, call, put, select, takeEvery } from '@redux-saga/core/effects';
import actionTypes from '../constans/actionTypes';
import rout from '../../server/constants/rout';
import * as request from '../utils/request/request';
import { getToken, getTranslate } from '../selectors/selectors';
import * as validReg from '../utils/validation/validationReg';
import { notificationError, notificationSuccess } from '../utils/libsHelpers/tostify';

export function* watchAccountAction() {
  yield takeEvery(actionTypes.GET_USER_DATA, getUserData);
  yield takeEvery(actionTypes.SEND_UPDATE_USER_DATA_REQ, sendUpdateData);
}

function* getUserData() {
  try {
    let token = yield select(getToken);
    if (!token) {
      token = yield apply(localStorage, localStorage.getItem, ['token']);
    }
    const response = yield call(request.getRequestSender, rout.getUserData, token);
    const payload = response.data;
    yield response.data && put({ type: actionTypes.SET_USER_DATA, payload });
  } catch (err) {
    console.error(err);
  }
}

function* sendUpdateData(action) {
  if (!action || !action.payload) {
    return;
  }
  const message = yield select(getTranslate);
  const isValid = yield call(checkCorrectUpdateData, action.payload.data, action.payload.regErrors, message);

  yield call(sendRequestAxios, isValid, action, message);
}

function* sendRequestAxios(isValid, action, dictionary) {
  if (isValid) {
    try {
      const response = yield call(request.postRequestSender, rout.updateUserData, action.payload.data, action.payload.token);
      if (response.data === 'exist') {
        yield call(notificationError, `${dictionary.errorMessage.emailAlreadyExist}`);
      } else {
        yield put({ type: actionTypes.CHANGE_ACCOUNT_BUTTON });
        yield call(notificationSuccess, `${dictionary.message.updateDataSuccess}`);
      }
    } catch (err) {
      yield call(notificationError, `${dictionary.errorMessage.serverError} ${err}`);
    }
    const payload = action.payload;
    yield put({ type: actionTypes.SET_USER_DATA, payload });
  } else {
    return;
  }
}

function checkCorrectUpdateData(data, regErrors, message) {
  let isValid = true;
  if (!validReg.namesFieldValid(data.first_name)) {
    regErrors.errorAccountName.innerText = message.errorMessage.firstNameError;
    isValid = false;
  } else if (!validReg.namesFieldValid(data.last_name)) {
    regErrors.errorAccountSurname.innerText = message.errorMessage.lastNameError;
    isValid = false;
  } else if (!validReg.validEmail(data.email)) {
    regErrors.errorAccountEmail.innerText = message.errorMessage.emailError;
    isValid = false;
  } else if (!validReg.validPhone(data.phone)) {
    regErrors.errorAccountPhone.innerText = message.errorMessage.phoneError;
    isValid = false;
  }
  return isValid;
}