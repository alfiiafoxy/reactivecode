import { all, fork } from 'redux-saga/effects';
import { watchAuthRequest } from './authSaga.js';
import { watchRegistrRequest } from './requestSage.js';
import { watchDisableInputs } from './dashboardSaga.js';
import { watchChangeTheme } from '../managers/themeManager/saga.js';
import { watchChangeModal } from '../managers/modalManager/saga.js';
import { watchChangeLocale } from '../managers/localesManager/saga.js';
import { watchGetGroupsRequest } from './groupsSaga.js';
import { watchButtonClick } from './mainSaga.js';
import { watchSchedule } from './scheduleSaga.js';
import { watchStudents } from './studentsSaga.js';
import { watchAttendance } from './attendensSaga.js';
import { watchAdmin } from './adminSaga.js';
import { watchAccountAction } from './accountSaga.js';

const sagas = [
    watchAuthRequest,
    watchChangeTheme,
    watchChangeModal,
    watchChangeLocale,
    watchDisableInputs,
    watchAccountAction,
    watchRegistrRequest,
    watchSchedule,
    watchChangeModal,
    watchButtonClick,
    watchGetGroupsRequest,
    watchStudents,
    watchAttendance,
    watchAdmin,
];

export default function* rootSaga() {
    yield all(sagas.map(fork));
}
