import { call, put, select, takeEvery, takeLatest } from '@redux-saga/core/effects';
import moment from 'moment';
import actionTypes from '../constans/actionTypes.js';
import {
  getToken,
  getTranslate,
  getUserRole,
  getSelectedGroup,
  getGroups,
  getUserData,
  getUsers,
  getAttendance,
  getStudentsAll,
  getSalary,
  getTotalSalary,
} from '../selectors/selectors.js';
import * as request from '../utils/request/request.js';
import * as rout from '../../server/constants/rout.js';
import { deleteAttendanceFromStore, deleteStudentFromStore } from './groupsSaga.js';
import * as validReg from '../utils/validation/validationReg';
import { changeToTimestampForStatistic, sumTwoHours, exportExcel } from '../utils/helpers/getDataHelpers.js';
import { checkIfDateValid } from '../utils/validation/validationHelpers.js';
import { notificationError, notificationSuccess } from '../utils/libsHelpers/tostify.js';

export function* watchAdmin() {
  yield takeEvery(actionTypes.GET_ALL_USERS, getUsersReq);
  yield takeEvery(actionTypes.UPDATE_USER, updateUser);
  yield takeEvery(actionTypes.GET_TEACHER_SALARY, getTeacherSalary);
  yield takeEvery(actionTypes.DELETE_TEACHER_FROM_GROUP, deleteTeacherFromGroup);
  yield takeEvery(actionTypes.DELETE_USER, deleteUser);
  yield takeLatest(actionTypes.DOWNLOAD_EXCEL, downloadExcel);
}

export function* getUsersReq() {
  let payload = [];
  try {
    let token = yield select(getToken);
    let role = yield select(getUserRole);
    if (role === 'student') {
      return;
    } else {
      const response = yield call(request.getRequestSender, rout.getAllUsersData, token);
      payload = yield call(sortUsers, response.data.data);
      yield put({ type: actionTypes.SET_USERS, payload });
    }
  } catch (err) {
    console.error(err);
  }
}

export function* updateUser(action) {
  const role = yield select(getUserRole);
  if (role !== 'admin') {
    return;
  } else {
    const message = yield select(getTranslate);
    const isValid = yield call(checkCorrectData, action.payload, action.payload.regErrors, message);
    yield call(setUpdateUser, action.payload, isValid, message);
  }
}

export function* setUpdateUser(data, isValid, dictionary) {
  if (isValid) {
    try {
      const token = yield select(getToken);
      const role = yield select(getUserRole);
      if (role !== 'admin') {
        return;
      } else {
        const response = yield call(request.postRequestSender, rout.updateCurrentUser, data, token);
        if (response.data === 'exist') {
          yield call(notificationError, `${dictionary.errorMessage.emailAlreadyExist}`);
        } else {
          yield call(notificationSuccess, `${dictionary.message.updateDataSuccess}`);
          yield put({ type: actionTypes.CHANGE_CURRENT_INPUT_USERS, payload: null });
        }
      }//TODO FIX ADD REDUCER
    } catch (err) {
      yield call(notificationError, `${err}`);
    }
  } else {
    return;
  }
}

export function* checkCorrectData(data, regErrors, message) {
  let isValid = true;
  if (!validReg.namesFieldValid(data.firstName)) {
    yield call(notificationError, `${message.errorMessage.firstNameError}`);
    isValid = false;
  } else if (!validReg.namesFieldValid(data.lastName)) {
    yield call(notificationError, `${message.errorMessage.lastNameError}`);
    isValid = false;
  } else if (!validReg.validEmail(data.email)) {
    yield call(notificationError, `${message.errorMessage.emailError}`);
    isValid = false;
  } else if (!validReg.validRatePerHour(data.ratePerHour)) {
    yield call(notificationError, `${message.errorMessage.ratePerHourError}`);
    isValid = false;
  } else if (!validReg.validPhone(data.phone)) {
    yield call(notificationError, `${message.errorMessage.phoneError}`);
    isValid = false;
  } else if (!validReg.validData(data.birthDate)) {
    yield call(notificationError, `${message.errorMessage.dateOfBirthError}`);
    isValid = false;
  }
  return isValid;
}

export function* getTeacherSalary(action) {
  const { dateTo, dateFrom, getFrom } = action.payload;
  const isValid = yield call(checkIfDateValid, dateTo, dateFrom);
  const dictionary = yield select(getTranslate);
  let data = null;
  if (isValid) {
    if (getFrom === 'statistic') {
      data = yield call(changeToTimestampForStatistic, action.payload);
    } else {
      data = { dateTo, dateFrom };
    }
    try {
      const token = yield select(getToken);
      const role = yield select(getUserRole);
      if (role === 'student') {
        return;
      } else {
        const response = yield call(request.postRequestSender, rout.getTeacherSalary, data, token);
        const { attendance, statistic } = response.data;

        const sumAttendance = yield call(editAttendance, attendance);
        const salary = yield call(sortStatisticForRender, statistic, sumAttendance);
        const total = yield call(getTotalStatistic, salary, role);

        yield put({ type: actionTypes.PUT_SALARY_IN_STORE, payload: { salary, total } });
        if (getFrom === 'statistic') {
          yield call(notificationSuccess, `${dictionary.message.statisticGetSuccessful}`);
        }
      }
    } catch (err) {
      if (getFrom === 'statistic') {
        yield call(notificationError, `${dictionary.errorMessage.getStatisticError}`);
      }
    }
  } else {
    yield call(notificationError, `${dictionary.errorMessage.timeDiapasonIncorrect}`);
  }
}

function sortStatisticForRender(statistic, attendance) {
  let result = [];
  let lastIndex = statistic.length - 1;
  let duration = 0;
  let salary = 0;
  let teacherAttend = '';
  let currentTeacher = statistic[0].teacherId;

  for (let i = 0; i < statistic.length; i++) {
    if (statistic[i].teacherId === currentTeacher) {
      for (let k = 0; k < attendance.length; k++) {
        if (statistic[i].teacherId === attendance[k].teacherId) {
          teacherAttend = attendance[k].attendance;
        }
      }
      duration += statistic[i].duration;
      salary += statistic[i].salary;
    } else {
      duration = moment.utc(moment.duration(duration, 'seconds').asMilliseconds()).format('HH:mm');
      salary = salary.toFixed(2);
      result.push({
        teacherId: currentTeacher,
        name: statistic[i - 1].first_name,
        surname: statistic[i - 1].last_name,
        city: statistic[i - 1].city,
        duration,
        salary,
        teacherAttend,
      });
      currentTeacher = statistic[i].teacherId;
      if (i !== statistic.length - 1) {
        duration = 0;
        salary = 0;
      }
      if (i <= statistic.length - 1) {
        duration = 0;
        salary = 0;
        for (let k = 0; k < attendance.length; k++) {
          if (statistic[i].teacherId === attendance[k].teacherId) {
            teacherAttend = attendance[k].attendance;
          }
        }
        duration += statistic[i].duration;
        salary += statistic[i].salary;
      }
    }
  }

  duration = moment.utc(moment.duration(duration, 'seconds').asMilliseconds()).format('HH:mm');
  salary = salary.toFixed(2);
  result.push({
    teacherId: statistic[lastIndex].teacherId,
    name: statistic[lastIndex].first_name,
    surname: statistic[lastIndex].last_name,
    city: statistic[lastIndex].city,
    duration,
    salary,
    teacherAttend,
  });

  return result;
}

function editAttendance(attendance) {
  let result = [];
  let currentTeacher = attendance[0].teacherId;
  let count = 0;
  let sumAttendance = 0;
  for (let i = 0; i < attendance.length; i++) {
    if (currentTeacher === attendance[i].teacherId && attendance[i] !== null) {
      sumAttendance += attendance[i].attendance;
      count++;
    } else {
      let attendRes = sumAttendance / count;
      count = 0;
      sumAttendance = 0;
      result.push({ teacherId: currentTeacher, attendance: attendRes.toFixed(2) });
      currentTeacher = attendance[i].teacherId;
      sumAttendance += attendance[i].attendance;
      count++;
    }
  }
  let attendRes = sumAttendance / count;
  result.push({ teacherId: attendance[attendance.length - 1].teacherId, attendance: attendRes.toFixed(2) });

  return result;
}

function getTotalStatistic(statistic, role) {
  if (role === 'teacher') {
    return [{
      sumAttendance: statistic[0].teacherAttend,
      sumHours: statistic[0].duration,
      sumSalary: statistic[0].salary,
    }];
  }
  let sumHours = '0';
  let sumAttendance = 0;
  if (statistic.length === 1) {
    sumHours = statistic[0].duration;
  } else {
    sumHours = statistic.reduce((prev, curr) => sumTwoHours(prev.duration, curr.duration));
  }

  let sumSalary = 0;
  let count = 0;
  statistic.forEach((el, i) => {
    sumAttendance += +el.teacherAttend;
    sumSalary += +el.salary;
    count = i + 1;
  });
  sumAttendance /= count;
  sumSalary = sumSalary.toFixed(2);
  return [{ sumHours, sumSalary, sumAttendance }];
}

export function* deleteTeacherFromGroup() {
  try {
    const token = yield select(getToken);
    const role = yield select(getUserRole);
    const groupId = yield select(getSelectedGroup);
    if (role !== 'admin') {
      return;
    } else {
      const adminId = yield call(request.postRequestSender, rout.deleteTeacherFromGroup, { groupId }, token);
      const userData = yield select(getUserData);
      const groups = yield select(getGroups);
      const payload = yield call(setOvnerOfGroupToAdmin, groupId, userData, groups, adminId.data.userId);
      yield put({ type: actionTypes.SET_GROUPS, payload });
      yield put({
        type: actionTypes.TOGGLE_MODAL_WINDOW,
        payload: { type: 'confirmModal', data: { callbackValue: null, data: null } },
      });
    }
  } catch (err) {
    console.error(err);
  }
}

export function* deleteUser(action) {
  try {
    let token = yield select(getToken);
    let role = yield select(getUserRole);
    if (role !== 'admin') {
      return;
    } else {
      yield call(request.postRequestSender, rout.deleteUser, action.payload, token);
      if (action.payload.user_role !== 'student') {
        const groups = yield select(getGroups);
        const groupId = yield select(getSelectedGroup);
        const userData = yield select(getUserData);
        const allUsers = yield select(getUsers);
        const newAllUsers = yield call(deleteUserFromAdminReducer, allUsers, action.payload.user_id);
        const newGroups = yield call(setOvnerOfGroupToAdmin, groupId, userData, groups, action.payload.user_id);
        yield newGroups && put({ type: actionTypes.SET_GROUPS, payload: newGroups });
        yield newAllUsers && put({ type: actionTypes.SET_USERS, payload: newAllUsers });
      } else {
        const allUsers = yield select(getUsers);
        const students = yield select(getStudentsAll);
        const attendance = yield select(getAttendance);
        const selectedGroup = yield select(getSelectedGroup);
        const newAllUsers = yield call(deleteUserFromAdminReducer, allUsers, action.payload.user_id);
        const newStudents = yield call(deleteStudentFromStore, action.payload.user_id, students, selectedGroup);
        const newAttendance = yield call(deleteAttendanceFromStore, action.payload.user_id, attendance, selectedGroup);
        yield newAllUsers && put({ type: actionTypes.SET_USERS, payload: newAllUsers });
        yield newStudents && put({ type: actionTypes.PUT_STUDENTS_IN_STORE, payload: newStudents });
        yield newAttendance && put({ type: actionTypes.PUT_ATTENDANCE_IN_STORE, payload: newAttendance });
        yield put({
          type: actionTypes.TOGGLE_MODAL_WINDOW,
          payload: { type: 'confirmModal', data: { callbackValue: null, data: null } },
        });
      }
    }
  } catch (err) {
    console.error(err);
  }
}

export function* downloadExcel(action) {
  let salaries = yield select(getSalary);
  let totalStatistic = yield select(getTotalSalary);
  const dictionary = yield select(getTranslate);

  if (salaries.length) {
    const { dateFrom, dateTo } = action.payload;
    const headers = [
      dictionary.resources.number,
      dictionary.resources.name,
      dictionary.resources.surname,
      dictionary.resources.city,
      dictionary.resources.adminHours,
      dictionary.resources.adminSalary,
      dictionary.resources.attendance,
      dateFrom,
      dateTo,
    ];
    const removeIdFromTotalSalary = salaries.map((item, index) => {
      item.teacherId = index + 1;
      return item;
    });
    const totalSalary = removeIdFromTotalSalary.map(item => Object.values(item));
    const totalValues = totalStatistic.map(item => Object.values(item));

    const removeInnerMassive = totalValues[0];
    const emptySpaceForCaption = ['Total', '', '', ''];
    const captionForTotal = [...emptySpaceForCaption, ...removeInnerMassive];
    const totalReport = [...totalSalary, captionForTotal];

    const fileName = `Salaries_${dateFrom}_${dateTo}`;
    const sheetName = `Total Statistic`;
    yield call(exportExcel, headers, totalReport, fileName, sheetName);
  } else {
    yield call(notificationError, `${dictionary.errorMessage.getReportFirst}`);
  }
}

function sortUsers(data) {
  const { users, group_detail } = data;
  users.map((user) => {
    user.groups = [];
    user.birth_date = moment(user.birth_date).format('DD.MM.YYYY');
  });
  if (group_detail.length) {
    group_detail.map((group) => {
      users.map((user) => {
        if (user.user_id === group.student_id) {
          user.groups.push(group.group_name);
        }
      });
    });
  }
  return users;
}

function setOvnerOfGroupToAdmin(groupID, userData, groups, adminId) {
  return groups.map((group) => {
    if (groupID === group.id) {
      group.first_name = userData.first_name;
      group.last_name = userData.last_name;
      group.teacher_id = adminId;
    }
    return group;
  });
}

function deleteUserFromAdminReducer(allUsers, user_id) {
  return allUsers.filter((user) => user.user_id !== user_id);
}
