import { put, takeEvery, apply, select, call } from '@redux-saga/core/effects';
import actionTypes from '../constans/actionTypes';
import { postRequestSender } from '../utils/request/request.js';
import * as rout from '../../server/constants/rout.js';
import { getToken } from '../selectors/selectors';

export function* watchButtonClick() {
  yield takeEvery(actionTypes.CLICK_HEADER_BUTTON, getActionOnClick);
  yield takeEvery(actionTypes.CHECK_TOKEN, checkToken);
}

export function* getActionOnClick(action) {
  if (!action || !action.payload) {
    return;
  }

  const { name, history } = action.payload;

  if (name === 'myAccount') {
    yield put({ type: actionTypes.CHANGE_STATE_ISMYACCOUNT });
  }
  if (name === 'back') {
    yield put({ type: actionTypes.CHANGE_STATE_ISMYACCOUNT });
  }
  if (name === 'settings') {
    yield put({ type: actionTypes.TOGGLE_MODAL_WINDOW, payload: { type: action.payload.name, data: null } });
  }
  if (name === 'logOut') {
    const token = yield select(getToken);
    yield call(postRequestSender, rout.userLogout, {}, token);
    yield apply(localStorage, localStorage.clear, []);

    yield put({ type: actionTypes.LOG_OUT });
    yield put({ type: actionTypes.SET_USERS, payload: [] });
    yield put({ type: actionTypes.CLEAR_ATTENDANCE_STORE });
    yield put({ type: actionTypes.SET_GROUPS, payload: [] });
    yield put({ type: actionTypes.PUT_SCHEDULE_IN_STORE, payload: [] });
    yield put({ type: actionTypes.PUT_STUDENTS_IN_STORE, payload: {} });
    yield put({ type: actionTypes.PUT_SALARY_IN_STORE, payload: { salary: [], total: [] } });
    yield apply(history, history.push, ['/']);
  }
}

export function* checkToken() {
  const token = yield apply(localStorage, localStorage.getItem, ['token']);
  const userRole = yield apply(localStorage, localStorage.getItem, ['userRole']);
  yield token && put({ type: actionTypes.LOG_IN, payload: { token, userRole } });
}
