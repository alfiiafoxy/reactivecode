import axios from 'axios';
import { takeEvery, put, apply, call, select, delay } from '@redux-saga/core/effects';
import serverConstants from '../../server/constants/constants.js';
import rout from '../../server/constants/rout.js';
import actionTypes from '../constans/actionTypes.js';
import { getTranslate } from '../selectors/selectors.js';
import * as validReg from '../utils/validation/validationReg.js';
import { notificationSuccess, notificationError } from '../utils/libsHelpers/tostify.js';

export function* watchAuthRequest() {
  yield takeEvery(actionTypes.SEND_AUTH_REQ, sendAuthData);
}

export function* sendAuthData(action) {
  if (!action || !action.payload) {
    return;
  }
  const { email, password } = action.payload.data;
  const data = { email, password };
  const url = `${serverConstants.serverPath}${rout.auth}`;
  const headers = { 'Content-Type': 'application/json; charset=UTF-8' };
  yield put({ type: actionTypes.TOGGLE_DISABLE_BUTTON_AUTH });
  const dictionary = yield select(getTranslate);
  const isValid = yield call(checkCorrectRequestData, action.payload.data, action.payload.authErrors, dictionary);
  if (isValid) {
    try {
      const response = yield apply(axios, axios.post, [url, data, headers]);
      yield response && call(signInSuccess, response, action.payload.history);
    } catch (err) { // TODO FIX Male function error
      yield call(notificationError, dictionary.errorMessage.emailNotExist);
    }
  }
  yield put({ type: actionTypes.TOGGLE_DISABLE_BUTTON_AUTH });
}

export function* signInSuccess(response, history) {
  if (!response || !response.data) {
    return;
  }
  const token = response.data.token; //TODO FIX
  const userRole = response.data.user_role;
  yield apply(localStorage, localStorage.setItem, ['token', token]);
  yield apply(localStorage, localStorage.setItem, ['userRole', userRole]);
  const dictionary = yield select(getTranslate);
  yield call(notificationSuccess, dictionary.message.authSuccess);
  yield delay(1000);
  yield apply(history, history.push, ['/main']);//TODO add test
}

export function checkCorrectRequestData(data, authErrors, message) {
  let isValid = true;
  if (!validReg.validEmail(data.email)) {
    authErrors.emailError.innerText = message.errorMessage.emailError;
    isValid = false;
  } else if (!validReg.validPassword(data.password)) {
    authErrors.passwordError.innerText = message.errorMessage.passwordError;
    isValid = false;
  }
  return isValid;
}