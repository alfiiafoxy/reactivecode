import { takeEvery, put, call, select } from '@redux-saga/core/effects';
import actionTypes from '../../constans/actionTypes.js';
import { watchAuthRequest, sendAuthData, signInSuccess, checkCorrectRequestData } from '../authSaga.js';
import { getTranslate } from '../../selectors/selectors';

describe('Testing watchAuthRequest: ', () => {
  const generator = watchAuthRequest();

  it('watchAuthRequest takeEvery SEND_AUTH_REQ', () => {
    const actual = generator.next();

    assert.deepEqual(
      actual.value,
      takeEvery('SEND_AUTH_REQ', sendAuthData),
    );
  });
});

describe('Testing sendAuthData: ', () => {
  const action = {
    type: 'SEND_REG_REQ',
    payload: {
      data: {
        email: 'shvachkor@ukr.net',
        password: 'qqqqqq',
      },
      authErrors: {
        emailError: 'emailError',
        passwordError: 'passwordError',
      },
    },
  };

  const generator = sendAuthData(action);

  it('sendAuthData call TOGGLE_DISABLE_BUTTON_AUTH', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      put({ type: actionTypes.TOGGLE_DISABLE_BUTTON_AUTH }));
  });

  it('sendAuthData call select(getTranslate)', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      select(getTranslate));
  });

  it('sendAuthData call checkCorrectRequestData', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      call(checkCorrectRequestData, action.payload.data, action.payload.authErrors, undefined),
    );
  });

  it('sendAuthData call TOGGLE_DISABLE_BUTTON_AUTH', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      put({ type: actionTypes.TOGGLE_DISABLE_BUTTON_AUTH }));
  });

  it('signUpSuccess is done', () => {
    const actual = generator.next();
    assert.isTrue(actual.done, 'checkCorrectRequestData is done');
  });
});

describe('Testing signInSuccess: ', () => {
  const history = [1, 2, 3];
  const response = { data: { token: 'Success' }, history };
  const generator = signInSuccess(response);

  it('Testing put into accountReducer ', () => {
    const actual = generator.next();
    const expected = {
      type: actionTypes.LOG_IN,
      payload: response.data.token,
    };

    assert.deepEqual(
      actual.value,
      put(expected),
    );
  });
});

describe('Testing checkCorrectRequestData statusText is OK ', () => {
  const data = {
    email: 'shvachkor@ukr.net',
    password: 'qqqqqq',
  };

  const refErrors = {
    emailError: 'emailError',
    passwordError: 'passwordError',
  };

  const message = {
    errorMessage: {
      emailError: 'emailError',
      passwordError: 'passwordError',
    },
  };

  const isValid = checkCorrectRequestData(data, refErrors, message);

  it('signUpSuccess is done', () => {
    assert.isTrue(isValid, 'checkCorrectRequestData true');
  });
});