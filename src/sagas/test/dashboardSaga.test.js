import { takeEvery, put } from '@redux-saga/core/effects';
import { watchDisableInputs, switchStateInputs } from '../dashboardSaga.js';
import actionTypes from '../../constans/actionTypes';

describe('Testing watchDisableInputs: ', () => {
  const generator = watchDisableInputs();

  it('watchDisableInputs takeEvery TOGGLE_DISABLE_INPUT', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      takeEvery('TOGGLE_DISABLE_INPUT', switchStateInputs),
    );
  });
});

describe('Testing switchStateInputs: ', () => {
  const action = {
    type: 'TOGGLE_DISABLE_INPUT',
    payload: {},
  };

  const generator = switchStateInputs(action);

  it('switchStateInputs put SWITCH_STATE_INPUTS', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      put({ type: actionTypes.SWITCH_STATE_INPUTS }));
  });
});