import { takeEvery, select } from '@redux-saga/core/effects';

import { getSchedule, getTranslate } from '../../selectors/selectors';
import { changeScheduleWeek, watchSchedule } from '../scheduleSaga';

describe('Testing watchSchedule: ', () => {
  const generator = watchSchedule();

  it('watchSchedule takeEvery SET_SCHEDULE_WEEK', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      takeEvery('SET_SCHEDULE_WEEK', changeScheduleWeek),
    );
  });
});

describe('Testing changeScheduleWeek: ', () => {
  const action = {
    type: 'SEND_REG_REQ',
    payload: 1,
  };

  const generator = changeScheduleWeek(action);

  it('changeScheduleWeek  select(getSchedule)', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      select(getSchedule));
  });
});
