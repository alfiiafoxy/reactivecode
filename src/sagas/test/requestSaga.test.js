import { takeEvery, put, call, select } from '@redux-saga/core/effects';
import { watchRegistrRequest, registrSendRequest, signUpSuccess, checkCorrectRequestData } from '../../sagas/requestSage.js';
import { getTranslate } from '../../selectors/selectors.js';
import { sendRequestAxios } from '../../sagas/requestSage';

describe('Testing watchRegistrRequest: ', () => {
  const generator = watchRegistrRequest();

  it('watchRegistrRequest takeEvery SEND_REG_REQ', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      takeEvery('SEND_REG_REQ', registrSendRequest),
    );
  });
});

describe('Testing registrSendRequest: ', () => {
  const action = {
    type: 'SEND_REG_REQ',
    payload: {
      data: {
        firstName: 'Mdhdh',
        lastName: 'Mfhdfh',
        email: 'shvachkor@ukr.net',
        password: 'qqqqqq',
        repeatPassword: 'qqqqqq',
        phone: '+380958287778',
        keywords: 'dfghdfhdfh',
        dateOfBirth: '12.12.1212',
        refErrors: 'refErrors',
      },
      refErrors: {
        firstNameError: 'firstNameError',
        lastNameError: 'lastNameError',
        emailError: 'emailError',
        passwordError: 'passwordError',
        repeatPasswordError: 'repeatPasswordError',
        phoneError: 'phoneError',
        dateOfBirthError: 'dateOfBirthError',
        keywordsError: 'keywordsError',
      },
    },
  };
  const isValid = undefined;
  const generator = registrSendRequest(action);

  it('sendAuthData call select(getTranslate)', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      select(getTranslate));
  });

  it('sendRequestAxios put TOGGLE_DISABLE_BUTTON_REG', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      put({ type: 'TOGGLE_DISABLE_BUTTON_REG' }),
    );
  });

  it('registrSendRequest call checkCorrectRequestData', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      call(checkCorrectRequestData, action.payload.data, action.payload.regErrors, undefined),
    );
  });

  it('registrSendRequest call sendRequestAxios', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      call(sendRequestAxios, isValid, action));
  });

  it('registrSendRequest done', () => {
    const actual = generator.next();
    assert.isTrue(actual.done, 'registrSendRequest is done');
  });
});

describe('Testing sendRequestAxios', () => {
  const action = {
    data: {
      firstName: 'Mdhdh',
      lastName: 'Mfhdfh',
      email: 'shvachkor@ukr.net',
      password: 'qqqqqq',
      repeatPassword: 'qqqqqq',
      phone: '+380958287778',
      keywords: 'dfghdfhdfh',
      dateOfBirth: '12.12.1212',
      refErrors: 'refErrors',
    },
    refErrors: {
      firstNameError: 'firstNameError',
      lastNameError: 'lastNameError',
      emailError: 'emailError',
      passwordError: 'passwordError',
      repeatPasswordError: 'repeatPasswordError',
      phoneError: 'phoneError',
      dateOfBirthError: 'dateOfBirthError',
      keywordsError: 'keywordsError',
    },
  };
  const isValid = true;

  const generator = sendRequestAxios({ isValid, action });

  it('sendRequestAxios put TOGGLE_DISABLE_BUTTON_REG', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      put({ type: 'TOGGLE_DISABLE_BUTTON_REG' }),
    );
  });

  it('sendRequestAxios put TOGGLE_DISABLE_BUTTON_REG', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      put({ type: 'TOGGLE_DISABLE_BUTTON_REG' }),
    );
  });
});

describe('Testing signUpSuccess', () => {
  const response = {
    data: {
      token: 'data',
    },
    statusText: 'OK',
  };

  const generator = signUpSuccess(response);
  const token = response.data.token;

  it('signUpSuccess put LOG_IN action', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      put({ type: 'LOG_IN', payload: token }),
    );
  });
});

describe('Testing checkCorrectRequestData statusText is OK ', () => {
  const data = {
    firstName: 'Mdhdh',
    lastName: 'Mfhdfh',
    email: 'shvachkor@ukr.net',
    password: 'qqqqqq',
    repeatPassword: 'qqqqqq',
    phone: '+380958287778',
    keywords: 'dfghdfhdfh',
    dateOfBirth: '12.12.1212',
    refErrors: 'refErrors',
  };
  const regErrors = {
    firstNameError: 'firstNameError',
    lastNameError: 'lastNameError',
    emailError: 'emailError',
    passwordError: 'passwordError',
    repeatPasswordError: 'repeatPasswordError',
    phoneError: 'phoneError',
    dateOfBirthError: 'dateOfBirthError',
    keywordsError: 'keywordsError',
  };

  const message = {
    errorMessage: {
      firstNameError: 'firstNameError',
      lastNameError: 'lastNameError',
      emailError: 'emailError',
      passwordError: 'passwordError',
      repeatPasswordError: 'repeatPasswordError',
      phoneError: 'phoneError',
      dateOfBirthError: 'dateOfBirthError',
      keywordsError: 'keywordsError',
    },
  };

  it('checkCorrectRequestData ', () => {
    const actual = checkCorrectRequestData(data, regErrors, message);
    assert.isTrue(
      actual,
     'checkCorrectRequestData is true'
    );
  });
});