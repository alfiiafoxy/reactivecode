import { call, put, select, takeEvery, takeLatest } from '@redux-saga/core/effects';
import actionTypes from '../constans/actionTypes.js';
import { getRequestSender, postRequestSender } from '../utils/request/request';
import * as rout from '../../server/constants/rout';
import { getToken } from '../selectors/selectors.js';
import moment from 'moment';
import { getAllStudents, getSelectedGroup, getStudentsAll, getUsers, getGroups } from '../selectors/selectors';

export function* watchStudents() {
  yield takeEvery(actionTypes.GET_ALL_STUDENTS, getStudentsReq);
  yield takeLatest(actionTypes.ADD_STUDENT_IN_GROUP, addStudentInDb);
}

export function* getStudentsReq() {
  const token = yield select(getToken);
  try {
    const response = yield call(getRequestSender, rout.getStudents, token);
    if (response.data.length) {
      const payload = yield call(sortStudentsByGroup, response.data);
      yield put({ type: actionTypes.PUT_STUDENTS_IN_STORE, payload });
    }
  } catch (err) {
    console.error(err);
  }
}

export function sortStudentsByGroup(data) {
  const result = {};
  let group = [];
  const useGroups = [];
  let currentGroup = data[0].group_id;
  for (let i = 0; i < data.length; i++) {
    data[i].birth_date = moment(data[i].birth_date).format('DD.MM.YYYY');
    if (useGroups.includes(data[i].group_id)) {
      continue;
    }
    currentGroup = data[i].group_id;
    for (let j = 0; j < data.length; j++) {
      if (currentGroup === data[j].group_id) {
        group.push(data[j]);
      }
    }
    result[data[i].group_id] = group;
    group = [];
    useGroups.push(currentGroup);
  }
  return result;
}

const findStudentById = (data, id, groupId) => {
  const user = data.find(el => el.user_id === id);
  return {
    birth_date: user.birth_date,
    email: user.email,
    first_name: user.first_name,
    group_id: groupId,
    id,
    last_name: user.last_name,
    phone: user.phone,
  };
};

const addStudentInGroup = (data, student, groupId) => {
  if (Object.keys(data).length === 0 && data.constructor === Object) {
    data[groupId] = [student];
  } else {
    for (let key in data) {
      if (data.hasOwnProperty(key)) {
        if (data[key][0].group_id === groupId) {
          data[key].push(student);
        }
      }
    }
  }
  return { ...data };
};

export function* addStudentInDb(action) {
  const token = yield select(getToken);
  const selectedGroup = yield select(getSelectedGroup);
  const data = { groupId: selectedGroup, studentId: action.payload };
  try {
    yield call(postRequestSender, rout.addStudentInGroup, data, token);
    const allStudentsInGroups = yield select(getStudentsAll);
    const allStudents = yield select(getAllStudents);
    const currentStudent = yield call(findStudentById, allStudents, action.payload, selectedGroup);
    const payload = yield call(addStudentInGroup, allStudentsInGroups, currentStudent, selectedGroup);
    const allGroups = yield select(getGroups);
    const users = yield select(getUsers);
    const payloadForAdminReducer = yield call(addStudentToGroupInAdminReducer, users, data, allGroups);
    yield put({ type: actionTypes.PUT_STUDENTS_IN_STORE, payload });
    yield put({ type: actionTypes.SET_USERS, payload: payloadForAdminReducer });
  } catch (e) {
    console.error(e);
  }
}

export function addStudentToGroupInAdminReducer(users, data, allGroups) {
  const currentGroup = allGroups.filter(group => group.id === data.groupId);
  return users.map((user) => user.user_id === data.studentId ? { ...user, groups: [...user.groups, currentGroup[0].name] } : user);
}