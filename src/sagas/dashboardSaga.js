import { takeEvery, put, apply } from '@redux-saga/core/effects';
import actionTypes from '../constans/actionTypes.js';

export function* watchDisableInputs() {
  yield takeEvery(actionTypes.TOGGLE_DISABLE_INPUT, switchStateInputs);
  yield takeEvery(actionTypes.CHANGE_NAME_CONTENT, changeNameContent);
}

export function* switchStateInputs() {
  yield put({ type: actionTypes.SWITCH_STATE_INPUTS });
}

export function* changeNameContent(action) {
  yield apply(localStorage, localStorage.setItem, ['contentName', action.payload]);
  yield put({ type: actionTypes.SWITCH_STATE_INPUTS, payload: action.payload });
}