import moment from 'moment';
import { apply, call, put, select, takeEvery, takeLatest } from '@redux-saga/core/effects';
import actionTypes from '../constans/actionTypes.js';
import { getRequestSender, postRequestSender } from '../utils/request/request.js';
import * as rout from '../../server/constants/rout.js';
import {
  getAttendance,
  getDaysOfLessons,
  getGroups,
  getSchedule,
  getStudentsAll,
  getToken,
  getUserRole,
  getTranslate,
} from '../selectors/selectors.js';
import { changeDataToTimestamp, changeTimeToTimestamp } from '../utils/helpers/getDataHelpers.js';
import { addLessonsSort, convertToDate, sortLessonByDay } from './helpers/sagaHelpers.js';
import { mockLessons } from '../modules/mainPage/components/content/components/scheduleMain/components/schedule/mockLessonsData';
import { notificationError, notificationSuccess } from '../utils/libsHelpers/tostify.js';
import { validationLesson } from '../utils/validation/validationLesson.js';
import {
  addAttendanceInGroup,
  createAttendance,
  createDayOfLesson,
  createLessonsDay,
  sortDaysOfLessons,
  deleteFromLessonsDay,
  deleteAttendance,
} from './helpers/scheduleHelpers.js';

export function* watchSchedule() {
  yield takeEvery(actionTypes.SET_SCHEDULE_WEEK, changeScheduleWeek);
  yield takeEvery(actionTypes.GET_SCHEDULE, getScheduleReq);
  yield takeLatest(actionTypes.ADD_LESSON, addLesson);
  yield takeEvery(actionTypes.DELETE_LESSON, deleteLessonRequest);
  yield takeEvery(actionTypes.UPDATE_LESSON, updateLessonRequest);
  yield takeEvery(actionTypes.GET_PREV_WEEK, getLastWeek);
  yield takeEvery(actionTypes.GET_NEXT_WEEK, getNextWeek);
  yield takeEvery(actionTypes.SET_DATE_GROUP_UI_FROM, setGroupUiDateFrom);
  yield takeEvery(actionTypes.SET_DATE_GROUP_UI_TO, setGroupUiDataTo);
}

export function* changeScheduleWeek(action) {
  const schedule = yield select(getSchedule);
  const payload = schedule.currentWeekCount + action.payload;
  if (payload > mockLessons.month.length - 1 || payload < 0) {
    return false;
  }
  yield put({ type: actionTypes.CHANGE_SCHEDULE_WEEK, payload });
}

export function* getScheduleReq() { //TODO spinner load
  let token = yield select(getToken);
  if (!token) {
    token = yield apply(localStorage, localStorage.getItem, ['token']);
  }
  try {
    const response = yield call(getRequestSender, rout.schedule, token);
    if (response.data.data) {
      const data = yield call(convertToDate, response.data.data);
      const groups = yield select(getGroups);
      let payload = [];
      if (response.data.data.length) {
        payload = yield call(sortLessonByDay, data, groups);
      }
      yield put({ type: actionTypes.PUT_SCHEDULE_IN_STORE, payload });
    }
  } catch (err) {
    console.error(err);
  }
}

export function* addLesson(action) {
  let token = yield select(getToken);
  const role = yield select(getUserRole);
  const students = yield select(getStudentsAll);
  const lessons = yield select(getSchedule);
  const dictionary = yield select(getTranslate);

  const { groupName, clearFunc } = action.payload;

  if (role === 'student') {
    return;
  }
  if (!validationLesson(action.payload.data, role) || !checkIfGroupHasStudent(action.payload.data, students)) {
    yield call(notificationError, action.payload.message.err);
    return;
  }
  if (!token) {
    token = yield apply(localStorage, localStorage.getItem, ['token']);
  }
  try {
    const changesDate = yield call(changeDataToTimestamp, action.payload.data);
    const response = yield call(postRequestSender, rout.createLesson, { ...changesDate, token });

    yield call(clearFunc);

    const { lessonId } = response.data.data;
    const payload = yield call(addLessonsSort, lessons, { ...changesDate, id: lessonId, groupsName: groupName });
    yield put({ type: actionTypes.ADD_NEW_LESSON, payload });

    yield call(addAttendance, action, response);
    yield call(notificationSuccess, action.payload.message.success);
  } catch (e) {
    yield call(notificationError, `${dictionary.errorMessage.createLessonServerError}`);
  }
}

function checkIfGroupHasStudent(data, students) {
  let result = false;
  const { groupsId } = data;
  if (students[groupsId]) {
    result = Boolean(students[groupsId].length);
  }
  return result;
}

function* addAttendance(action, response) {
  const daysOfLessons = yield select(getDaysOfLessons);
  const attendance = yield select(getAttendance);
  const students = yield select(getStudentsAll);
  let sortedDaysOfLesson = null;
  let sortedNewAttendance = null;
  const { dataOfLesson, groupsId } = action.payload.data;
  const { lessonId, attendanceId } = response.data.data;

  const newDayOfLesson = yield call(createDayOfLesson, dataOfLesson, groupsId, lessonId);

  if (attendance && attendance[groupsId]) {
    sortedDaysOfLesson = yield call(sortDaysOfLessons, daysOfLessons, newDayOfLesson, groupsId);
    sortedNewAttendance = yield call(addAttendanceInGroup, groupsId, attendance, attendanceId, lessonId, dataOfLesson);
  } else {
    sortedDaysOfLesson = yield call(createLessonsDay, newDayOfLesson, groupsId);
    sortedNewAttendance = yield call(createAttendance, groupsId, students, attendanceId, lessonId, dataOfLesson);
  }
  yield put({ type: actionTypes.PUT_ATTENDANCE_IN_STORE, payload: sortedNewAttendance });
  yield put({ type: actionTypes.PUT_DAYS_OF_LESSONS, payload: sortedDaysOfLesson });
}

function* deleteLessonRequest(action) {
  if (!action || !action.payload) {
    return;
  }
  const role = yield select(getUserRole);
  if (role === 'student') {
    return;
  }
  const token = yield select(getToken);
  try {
    const { lesson_id, groupId } = action.payload;
    const response = yield call(postRequestSender, rout.deleteLesson, action.payload, token);
    yield response && put({ type: actionTypes.CHANGE_CURRENT_LESSON, payload: null });
    yield call(deleteAttendanceFromUi, groupId, lesson_id);

    const schedule = yield select(getSchedule);
    const newSchedule = yield call(putDeleteLesson, action.payload, schedule);
    yield newSchedule && put({ type: actionTypes.PUT_SCHEDULE_IN_STORE, payload: newSchedule });
    yield put({ type: actionTypes.TOGGLE_MODAL_WINDOW, payload: { type: 'confirmModal', data: { callbackValue: null, data: null } } });
  } catch (err) {
    console.error(err);
  }
}

function* deleteAttendanceFromUi(groupId, lessonId) {
  const daysOfLesson = yield select(getDaysOfLessons);
  const attendance = yield select(getAttendance);

  const newDaysOfLessons = yield call(deleteFromLessonsDay, daysOfLesson, groupId, lessonId);
  const newAttendance = yield call(deleteAttendance, attendance, groupId, lessonId);

  yield put({ type: actionTypes.PUT_DAYS_OF_LESSONS, payload: newDaysOfLessons });
  yield put({ type: actionTypes.PUT_ATTENDANCE_IN_STORE, payload: newAttendance });
}

function* updateLessonRequest(action) {
  if (!action || !action.payload) {
    return;
  }

  const token = yield select(getToken);
  const role = yield select(getUserRole);
  const schedule = yield select(getSchedule);
  const dictionary = yield select(getTranslate);
  if (role === 'student') {
    return;
  }
  if (!validationLesson(action.payload.data, role)) {
    yield call(notificationError, dictionary.errorMessage.changeLessonError);
    return;
  }
  try {
    const changesDate = yield call(changeTimeToTimestamp, action.payload);
    const response = yield call(postRequestSender, rout.setLessonData, changesDate, token);
    const newSchedule = yield call(putUpdateLesson, action.payload, schedule);
    yield newSchedule && put({ type: actionTypes.PUT_SCHEDULE_IN_STORE, payload: newSchedule });
    yield response && put({ type: actionTypes.CHANGE_CURRENT_LESSON, payload: null });
    yield call(notificationSuccess, dictionary.message.changeLessonSuccess);
  } catch (err) {
    yield call(notificationError, dictionary.errorMessage.changeLessonServerError);
  }
}

export function putDeleteLesson(updateLesson, schedule) {
  schedule.map(day => {
    if (!day) {
      return;
    }
    day.lessons = day.lessons.filter(lesson => lesson.id !== updateLesson.lesson_id);
    if (day.lessons.length === 0) {
      return;
    }
    return day;
  });
  const newSchedule = schedule.filter(day => day.lessons.length !== 0);
  return newSchedule;
}

export function putUpdateLesson(updateLesson, schedule) {
  schedule.map(day => {
    day.lessons.map(lesson => {
      if (!day.lessons) {
        return;
      }
      if (lesson.id === updateLesson.lesson_id) {
        lesson.type = updateLesson.lesson_type;
        lesson.timeStart = updateLesson.data.startTime;
        lesson.timeEnd = updateLesson.data.endTime;
        return lesson;
      }
      return lesson;
    });
    return day;
  });
}

function* getLastWeek(action) {
  if (!action.payload) {
    const payloadLastWeek = {
      firstSelectWeekDay: 1,
      lastSelectWeekDay: 7,
      firstSelectWeekDayMilliseconds: moment(moment().day(1).format('YYYY-MM-DD'), 'YYYY-MM-DD').format('X'),
      lastSelectWeekDayMilliseconds: moment(moment().day(7).format('YYYY-MM-DD'), 'YYYY-MM-DD').format('X'),
    };
    yield put({ type: actionTypes.SET_PREV_WEEK, payload: payloadLastWeek });
    return;
  }
  const payloadLastWeek = {
    firstSelectWeekDay: action.payload.firstSelectWeekDay - 7,
    lastSelectWeekDay: action.payload.lastSelectWeekDay - 7,
    firstSelectWeekDayMilliseconds: moment(moment().day(action.payload.firstSelectWeekDay - 7).format('YYYY-MM-DD'), 'YYYY-MM-DD').format('X'),
    lastSelectWeekDayMilliseconds: moment(moment().day(action.payload.lastSelectWeekDay - 7).format('YYYY-MM-DD'), 'YYYY-MM-DD').format('X'),
  };
  yield put({ type: actionTypes.SET_PREV_WEEK, payload: payloadLastWeek });
}

function* getNextWeek(action) {
  const payloadLastWeek = {
    firstSelectWeekDay: action.payload.firstSelectWeekDay + 7,
    lastSelectWeekDay: action.payload.lastSelectWeekDay + 7,
    firstSelectWeekDayMilliseconds: moment(moment().day(action.payload.firstSelectWeekDay + 7).format('YYYY-MM-DD'), 'YYYY-MM-DD').format('X'),
    lastSelectWeekDayMilliseconds: moment(moment().day(action.payload.lastSelectWeekDay + 7).format('YYYY-MM-DD'), 'YYYY-MM-DD').format('X'),
  };
  yield put({ type: actionTypes.SET_NEXT_WEEK, payload: payloadLastWeek });
}

function* setGroupUiDateFrom(action) {
  const firstSelectWeekDayMilliseconds = moment(action.payload, 'YYYY-MM-DD').format('X');
  yield put({ type: actionTypes.SET_DATE_GROUP_UI_FROM_SAGA, payload: firstSelectWeekDayMilliseconds });
}

function* setGroupUiDataTo(action) {
  const lastSelectWeekDayMilliseconds = moment(action.payload, 'YYYY-MM-DD').format('X');
  yield put({ type: actionTypes.SET_DATE_GROUP_UI_TO_SAGA, payload: lastSelectWeekDayMilliseconds });
}