import { put, takeEvery, call, select, delay } from '@redux-saga/core/effects';
import actionTypes from '../constans/actionTypes.js';
import { getTranslate } from '../selectors/selectors.js';
import * as request from '../utils/request/request.js';
import * as rout from '../../server/constants/rout.js';
import * as validReg from '../utils/validation/validationReg.js';
import { notificationSuccess, notificationError } from '../utils/libsHelpers/tostify.js';

export function* watchRegistrRequest() {
  yield takeEvery(actionTypes.SEND_REG_REQ, registrSendRequest);
}

export function* registrSendRequest(action) {
  if (!action || !action.payload) {
    return;
  }
  const message = yield select(getTranslate);
  yield put({ type: actionTypes.TOGGLE_DISABLE_BUTTON_REG });
  const isValid = yield call(checkCorrectRequestData, action.payload.data, action.payload.regErrors, message);
  yield call(sendRequestAxios, isValid, action);
}

export function* sendRequestAxios(isValid, action) {
  if (isValid) {
    const dictionary = yield select(getTranslate);
    try {
      const response = yield call(request.postRequestSender, rout.reg, action.payload.data, 'new user');
      if (response.data === 'exist') {
        yield call(notificationError, `${dictionary.errorMessage.emailAlreadyExist}`);
      } else {
        yield call(notificationSuccess, `${dictionary.message.registrationSuccess}`);
        yield response && call(signUpSuccess, response, action.payload.history);
      }
    } catch (err) {
      yield call(notificationError, `${dictionary.errorMessage.serverError} ${err}`);
    }
  }
  yield put({ type: actionTypes.TOGGLE_DISABLE_BUTTON_REG });
}

export function* signUpSuccess(response, history) {
  if (!response || !response.data) {
    return;
  }
  const token = response.data.token; //TODO FIX
  const userRole = response.data.userRole; //TODO FIX
  yield localStorage.setItem('token', token);
  yield localStorage.setItem('userRole', userRole);
  yield delay(2000);
  yield history.push('/main');
}

export function checkCorrectRequestData(data, regErrors, message) {
  let isValid = true;
  if (!validReg.namesFieldValid(data.firstName)) {
    regErrors.firstNameError.innerText = message.errorMessage.firstNameError;
    isValid = false;
  } else if (!validReg.namesFieldValid(data.lastName)) {
    regErrors.lastNameError.innerText = message.errorMessage.lastNameError;
    isValid = false;
  } else if (!validReg.validEmail(data.email)) {
    regErrors.emailError.innerText = message.errorMessage.emailError;
    isValid = false;
  } else if (!validReg.validPassword(data.password)) {
    regErrors.passwordError.innerText = message.errorMessage.passwordError;
    isValid = false;
  } else if (!validReg.checkValidPassword(data.password, data.repeatPassword)) {
    regErrors.repeatPasswordError.innerText = message.errorMessage.repeatPasswordError;
    isValid = false;
  } else if (!validReg.validPhone(data.phone)) {
    regErrors.phoneError.innerText = message.errorMessage.phoneError;
    isValid = false;
  } else if (!validReg.validData(data.dateOfBirth)) {
    regErrors.dateOfBirthError.innerText = message.errorMessage.dateOfBirthError;
    isValid = false;
  } else if (!validReg.validKeyword(data.keywords)) {
    regErrors.keywordsError.innerText = message.errorMessage.keywordsError;
    isValid = false;
  }
  return isValid;
}
