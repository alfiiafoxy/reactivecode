import { takeEvery, put, select, call, apply, takeLatest } from '@redux-saga/core/effects';
import actionTypes from '../constans/actionTypes.js';
import { getRequestSender, postRequestSender } from '../utils/request/request.js';
import * as rout from '../../server/constants/rout.js';
import { getToken, getAttendance, getSelectedGroup, getTranslate } from '../selectors/selectors.js';
import { convertToDate } from './helpers/sagaHelpers.js';
import { notificationError } from '../utils/libsHelpers/tostify.js';
import moment from 'moment';

export function* watchAttendance() {
  yield takeEvery(actionTypes.GET_ATTENDANCE, getAttendanceReq);
  yield takeLatest(actionTypes.SET_ATTENDANCE, setAttendance);
}

export function* getAttendanceReq() { //TODO spinner load
  let token = yield select(getToken);
  const dictionary = yield select(getTranslate);
  if (!token) {
    token = yield apply(localStorage, localStorage.getItem, ['token']);
  }
  try {
    const response = yield call(getRequestSender, rout.getAttendance, token);
    if (response.data) {
      const { attendance, date } = response.data;
      if (date.length && attendance.length) {
        const sortedLessonsDateByGroup = yield call(sortLessonsDateByGroup, date);
        const payload = yield call(sortAttendance, attendance, date);
        yield put({ type: actionTypes.PUT_DAYS_OF_LESSONS, payload: sortedLessonsDateByGroup });
        yield put({ type: actionTypes.PUT_ATTENDANCE_IN_STORE, payload });
      }
    }
  } catch (err) {
    yield call(notificationError, `${dictionary.errorMessage.attendanceSetError}`);
  }
}

export function* setAttendance(action) {
  let token = yield select(getToken);
  const selectedGroup = yield select(getSelectedGroup);
  const attendance = yield select(getAttendance);

  if (!token) {
    token = yield apply(localStorage, localStorage.getItem, ['token']);
  }
  const { attendance_id, studentId, attendancePayload } = action.payload;
  try {
    yield call(postRequestSender, rout.setAttendance, action.payload, token);
    const payload = yield call(changeAttendanceInStore, studentId, attendance_id, attendance, selectedGroup, attendancePayload);
    yield put({ type: actionTypes.PUT_ATTENDANCE_IN_STORE, payload });
  } catch (err) {
    console.error(err);
  }
}

export function changeAttendanceInStore(studentId, attendanceId, attendance, groupId, attendancePayload) {
  const result = { ...attendance };
  let statistic = 0;

  result[groupId].forEach(el => {
    if (el.student_id === studentId) {
      el.lessons.forEach(lesson => {
        if (lesson.attendance_id === attendanceId) {
          lesson.attendance = attendancePayload;
          el.lessons.forEach(item => {
            if (item.attendance) {
              statistic++;
            }
          });
        }
      });
      el.statistic = (100 * statistic / el.lessons.length).toFixed(1);
    }
  });

  return result;
}

function invert(date) {
  return date.split('.').reverse().join('');
}

export const sortByDateCallback = (a, b) => {
  let startA = invert(a.date);
  let startB = invert(b.date);
  if (+startA > +startB) {
    return 1;
  }
  if (+startA < +startB) {
    return -1;
  }
  return 0;
};

export const sortDate = data => {
  let result = [];
  for (let key in data) {
    if (data.hasOwnProperty(key)) {
      result = [];
      result.push(data[key].sort(sortByDateCallback));
      data[key] = result;
    }
  }
  return data;
};

export const sortByDataStartTime = (a, b) => {
  let startA = invert(a.start_time);
  let startB = invert(b.start_time);
  if (+startA > +startB) {
    return 1;
  }
  if (+startA < +startB) {
    return -1;
  }
  return 0;
};

export function sortLessonsDateByGroup(lessonsDate) {
  if (lessonsDate) {
    const dateNormal = convertToDate(lessonsDate);
    let result = {};
    let groupDate = {};
    let currentGroup = dateNormal[0].group_id;
    groupDate[currentGroup] = [];

    for (let i = 0; i < dateNormal.length; i++) {
      if (dateNormal[i].group_id === currentGroup) {
        groupDate[currentGroup].push(dateNormal[i]);
      } else {
        result = { ...result, ...groupDate };
        groupDate = {};
        currentGroup = dateNormal[i].group_id;
        groupDate[currentGroup] = [];
        groupDate[currentGroup].push(dateNormal[i]);
      }
    }
    result = { ...result, ...groupDate };
    return sortDate(result);
  } else {
    return [];
  }
}

export function sortAttendance(data) {
  if (data) {
    const result = {};
    let group = [];
    const useGroups = [];
    let currentGroup = data[0].group_id;

    for (let i = 0; i < data.length; i++) {
      if (useGroups.includes(data[i].group_id)) {
        continue;
      }
      currentGroup = data[i].group_id;
      for (let j = 0; j < data.length; j++) {
        if (currentGroup === data[j].group_id) {
          group.push(data[j]);
        }
      }
      result[data[i].group_id] = sortAttendanceByStudent(group);
      group = [];
      useGroups.push(currentGroup);
    }
    return result;
  } else {
    return {};
  }
}

function sortAttendanceByStudent(data) {
  const result = [];
  let student = { lessons: [] };
  const useStudents = [];
  let currentStudent = data[0].student_id;
  for (let i = 0; i < data.length; i++) {
    if (useStudents.includes(data[i].student_id)) {
      continue;
    }
    currentStudent = data[i].student_id;
    for (let j = 0; j < data.length; j++) {
      if (currentStudent === data[j].student_id) {
        student.lessons.push({
          lesson_id: data[j].lesson_id,
          attendance_id: data[j].attendance_id,
          attendance: data[j].attendance,
          start_time: data[j].start_time,
        });
      }
    }
    student.student_id = data[i].student_id;
    student.student_name = `${data[i].first_name} ${data[i].last_name}`;
    result.push(sortAttendanceByLessonTime(student));
    student = { lessons: [] };
    useStudents.push(currentStudent);
  }
  return result;
}

function sortAttendanceByLessonTime(studentData) {
  studentData.lessons.sort(function (a, b) {
    if (a.start_time > b.start_time) {
      return 1;
    }
    if (a.start_time < b.start_time) {
      return -1;
    }
    return 0;
  });
  studentData.lessons.map(el => {
    if (el.start_time) {
      el.start_time = moment(el.start_time).format('DD.MM.YYYY');
    }
    return el;
  });
  let statistic = 0;
  studentData.lessons.map(el => {
    if (el.attendance) {
      statistic++;
    }
    return el;
  });
  studentData.statistic = (100 * statistic / studentData.lessons.length).toFixed(1);
  return studentData;
}

