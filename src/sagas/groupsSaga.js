import { put, takeEvery, call, select, apply, takeLatest } from '@redux-saga/core/effects';
import actionTypes from '../constans/actionTypes.js';
import * as request from '../utils/request/request.js';
import * as rout from '../../server/constants/rout.js';
import { getToken,
  getSelectedTeacher,
  getTeachers,
  getTranslate,
  getGroups,
  getStudentsAll,
  getSelectedGroup,
  getUsers,
  getUserRole,
  getAttendance,
  getDaysOfLessons,
  getSchedule,
} from '../selectors/selectors.js';
import { notificationSuccess, notificationError } from '../utils/libsHelpers/tostify.js';
import { validAddGroup } from '../utils/validation/validationReg.js';
import { postRequestSender } from '../utils/request/request';

export function* watchGetGroupsRequest() {
  yield takeEvery(actionTypes.GET_GROUPS_REQ, getGroupsRequest);
  yield takeLatest(actionTypes.CREATE_GROUP, sendRequestCreateGroup);
  yield takeEvery(actionTypes.SET_SELECTED_TEACHER, changeSelectedTeacher);
  yield takeEvery(actionTypes.SET_SELECTED_GROUP, changeGroup);
  yield takeEvery(actionTypes.ADD_NEW_TEACHER_IN_GROUP, addNewTeacherInGroup);
  yield takeEvery(actionTypes.DELETE_STUDENT, deleteStudentRequest);
  yield takeEvery(actionTypes.DELETE_GROUP, deleteGroup);
}

export function* getGroupsRequest() { //TODO spinner load
  try {
    let token = yield select(getToken);
    if (!token) {
      token = yield apply(localStorage, localStorage.getItem, ['token']);
    }
    const response = yield call(request.getRequestSender, rout.groups, token);
    yield response.data && put({ type: actionTypes.SET_GROUPS, payload: response.data });
    yield response.data && response.data[0] && put({ type: actionTypes.SET_SELECTED_GROUP, payload: response.data[0].id });
  } catch (err) {
    console.error(err);
  }
}

function getTeacherName(data, id) {
  return data.find(el => el.user_id === id);
}

function* changeSelectedTeacher(action) {
  const allTeachers = yield select(getTeachers);
  const dictionary = yield select(getTranslate);
  const teacherName = yield call(getTeacherName, allTeachers, action.payload);
  const { first_name, last_name } = teacherName;
  yield put({ type: actionTypes.PUT_SELECTED_TEACHER, payload: action.payload });
  yield call(notificationSuccess, `${first_name} ${last_name} ${dictionary.message.teacherSelected}`);
}

export function* sendRequestCreateGroup(action) {
  const token = yield select(getToken);
  const selectedTeacher = yield select(getSelectedTeacher);
  const selectedTeacherName = yield select(getTeachers);
  const dictionary = yield select(getTranslate);
  if (!selectedTeacher) {
    yield call(notificationError, `${dictionary.errorMessage.pleaseSelectTeacher}`);
    return;
  }
  const thisTeacherData = getDataObject(selectedTeacherName, selectedTeacher);
  const data = { ...thisTeacherData, selectedTeacher, ...action.payload };
  const isValid = validAddGroup(data.name);
  if (isValid) {
    try {
      const response = yield call(request.postRequestSender, rout.createGroup, data, token);
      const payload = getPayloadTypeGroup(data, response.data.id);
      yield put({ type: actionTypes.ADD_NEW_GROUP, payload });
      yield call(notificationSuccess, `${dictionary.message.groupWasCreated}`);
    } catch (e) {
      yield call(notificationError, `${dictionary.errorMessage.cantCreateGroup}`);
    }
  } else {
    yield call(notificationError, `${dictionary.errorMessage.groupNameIncorrect}`);
  }
}

function getDataObject(arrayData, selectId) {
  let result;

  for (let i = 0; i < arrayData.length; i++) {
    if (arrayData[i].user_id === selectId) {
      result = { firstName: arrayData[i].first_name, lastName: arrayData[i].last_name };
    }
  }

  return result;
}

function getPayloadTypeGroup(data, groupId) {
  return {
    id: groupId,
    name: data.name,
    city: data.city,
    level: data.level,
    last_name: data.lastName,
    first_name: data.firstName,
    teacher_id: data.selectedTeacher,
  };
}

function findTeacher(groups, users, groupId) {
  const teacherId = groups.find(el => el.id === groupId ? el.teacher_id : []);
  if (typeof teacherId === 'object') {
    if (users.length) {
      return ``;
    }
  }
  return '';
}

function* changeGroup(action) {
  const groups = yield select(getGroups);
  const users = yield select(getUsers);
  const groupId = yield select(getSelectedGroup);
  const payload = yield call(findTeacher, groups, users, groupId);
  yield put({ type: actionTypes.PUT_TEACHER_NAME, payload });
  yield put({ type: actionTypes.PUT_SELECTED_GROUP, payload: action.payload });
}

function* addNewTeacherInGroup(action) {
  const selectedGroup = yield select(getSelectedGroup);
  const token = yield select(getToken);
  const groups = yield select(getGroups);
  const dictionary = yield select(getTranslate);
  const teachers = yield select(getTeachers);
  try {
    const response = yield call(request.postRequestSender, rout.updateTeacher, { selectedGroup, id: action.payload }, token);
    const payload = yield call(changeTeacherInStore, teachers, groups, selectedGroup, action.payload);
    yield response && put({ type: actionTypes.SET_GROUPS, payload });
    yield call(notificationSuccess, dictionary.message.teacherWasChanged);
  } catch (e) {
    yield call(notificationError, dictionary.errorMessage.teacherNotChanged);
  }
}

function changeTeacherInStore(teachers, groups, selectedGroup, teacherId) {
  if (groups.length) {
    const teacherName = teachers.find(teacher => teacher.user_id === teacherId);
    return groups.map(group => {
      if (group.id === selectedGroup) {
        group.teacher_id = +teacherId;
        group.first_name = teacherName.first_name;
        group.last_name = teacherName.last_name;
        return group;
      }
      return group;
    });
  }
  return [];
}

function* deleteStudentRequest(action) {
  if (!action || !action.payload) {
    return;
  }
  const role = yield select(getUserRole);
  if (role === 'student') {
    return;
  }
  const token = yield select(getToken);
  const attendance = yield select(getAttendance);
  try {
    const { user_id } = action.payload;
    const response = yield call(postRequestSender, rout.deleteStudent, action.payload, token);
    yield response && put({ type: actionTypes.CHANGE_CURRENT_STUDENT, payload: null });
    const students = yield select(getStudentsAll);
    const selectedGroup = yield select(getSelectedGroup);
    const newStudents = yield call(deleteStudentFromStore, user_id, students, selectedGroup);
    const newAttendance = yield call(deleteAttendanceFromStore, user_id, attendance, selectedGroup);
    yield newStudents && put({ type: actionTypes.PUT_STUDENTS_IN_STORE, payload: newStudents });
    yield newAttendance && put({ type: actionTypes.PUT_ATTENDANCE_IN_STORE, payload: newAttendance });
    yield put({ type: actionTypes.TOGGLE_MODAL_WINDOW, payload: { type: 'confirmModal', data: { callbackValue: null, data: null } } });
  } catch (err) {
    console.error(err);
  }
}

export function deleteAttendanceFromStore(deleteStudent, attendance, selectedGroup) {
  const result = { ...attendance };
  if (typeof result === 'object' && attendance && attendance[selectedGroup]) {
    const tempArr = attendance[selectedGroup].filter(el => el.student_id !== deleteStudent);
    result[selectedGroup] = tempArr;
  }
  return result;
}

export function deleteStudentFromStore(deleteStudent, students, selectedGroup) {
  const result = { ...students };
  if (typeof result === 'object' && students && students[selectedGroup]) {
    const tempArr = students[selectedGroup].filter(el => el.id !== deleteStudent);
    result[selectedGroup] = tempArr;
  }
  return result;
}

function* deleteGroup() {
  const role = yield select(getUserRole);
  if (role !== 'admin') {
    return;
  }
  const token = yield select(getToken);
  const group_id = yield select(getSelectedGroup);
  try {
    yield call(postRequestSender, rout.deleteGroup, { group_id: group_id }, token);

    const attendance = yield select(getAttendance);
    const newAttendance = yield call(deleteAttendanceByGroupId, attendance, group_id);
    yield newAttendance && put({ type: actionTypes.PUT_ATTENDANCE_IN_STORE, payload: newAttendance });

    const daysOfLessons = yield select(getDaysOfLessons);
    const newDaysOfLessons = yield call(deleteDaysOfLessonsByGroupId, daysOfLessons, group_id);
    yield newDaysOfLessons && put({ type: actionTypes.PUT_DAYS_OF_LESSONS, payload: newDaysOfLessons });

    const schedule = yield select(getSchedule);
    const newSchedule = yield call(deleteLessonsInScheduleByGroupId, schedule, group_id);
    yield newSchedule && put({ type: actionTypes.PUT_SCHEDULE_IN_STORE, payload: newSchedule });

    const studentsAll = yield select(getStudentsAll);
    const newStudentsAll = yield call(deleteStudentInStudentsAllByGroupId, studentsAll, group_id);
    yield newStudentsAll && put({ type: actionTypes.PUT_STUDENTS_IN_STORE, payload: newStudentsAll });

    const groups = yield select(getGroups);
    const newGroups = yield call(deleteGroupInInGroupReducerByGroupId, groups, group_id);
    yield newGroups && put({ type: actionTypes.SET_GROUPS, payload: newGroups });

    const adminUsers = yield select(getUsers);
    const newAdminUsers = yield call(deleteGroupInInAdminReducerByGroupId, adminUsers, group_id, groups);
    yield newAdminUsers && put({ type: actionTypes.SET_USERS, payload: newAdminUsers });

    yield put({ type: actionTypes.SET_SELECTED_GROUP, payload: newGroups[0].id });
    yield put({ type: actionTypes.TOGGLE_MODAL_WINDOW, payload: { type: 'confirmModal', data: { callbackValue: null, data: null } } });
  } catch (err) {
    console.error(err);
  }
}

export function deleteAttendanceByGroupId(attendance, selectedGroup) {
  const result = { ...attendance };
  if (typeof result === 'object' && attendance && attendance[selectedGroup]) {
    delete result[selectedGroup.toString()];
  }
  return result;
}

export function deleteDaysOfLessonsByGroupId(daysOfLessons, selectedGroup) {
  const result = { ...daysOfLessons };
  if (typeof result === 'object' && daysOfLessons && daysOfLessons[selectedGroup]) {
    delete result[selectedGroup.toString()];
  }
  return result;
}

export function deleteLessonsInScheduleByGroupId(schedule, selectedGroup) {
  const result = [...schedule];
  result.map((day) => {
    day.lessons = day.lessons.filter((lesson) => lesson.groupsId !== selectedGroup);
    return day;
  });
  return result.filter((day) => day.lessons.length > 0);
}

export function deleteStudentInStudentsAllByGroupId(studentsAll, selectedGroup) {
  const result = { ...studentsAll };
  if (typeof result === 'object' && studentsAll && studentsAll[selectedGroup]) {
    delete result[selectedGroup.toString()];
  }
  return result;
}

export function deleteGroupInInGroupReducerByGroupId(groups, selectedGroup) {
  const result = [...groups];
  if (groups) {
    return result.filter(group => {
      return group.id !== selectedGroup;
    });
  }
  return result;
}

export function deleteGroupInInAdminReducerByGroupId(adminUsers, selectedGroup, groups) {
  const result = [...adminUsers];
  const deleteGroupData = groups.filter(group => group.id === selectedGroup);
  if (adminUsers) {
    return result.map(user => {
      if (user.groups.includes(deleteGroupData[0].name)) {
        user.groups = [];
      }
      return user;
    });
  }
}