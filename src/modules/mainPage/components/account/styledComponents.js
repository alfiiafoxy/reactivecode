import styled from 'styled-components';

export const AccountWrapper = styled.div`
    width: 400px;
    margin-top: 20px;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    background: ${props => props.bgColor ? props.bgColor : '#ffffff'};
    border: ${props => props.borderStyle ? props.borderStyle : 'transparent'};
`;

export const AccountContentContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border: ${props => props.borderStyle ? props.borderStyle : 'transparent'};
`;

export const AccountTitleWrapper = styled.div`
    margin: 15px;
 `;

export const AccountBtnWrapper = styled.div`
    width:130px;
    margin-bottom:10px;
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

export const WrapperInp = styled.div`
    min-height: 35px;
    width: auto;
    justify-content: center;
    align-items: center;
`;