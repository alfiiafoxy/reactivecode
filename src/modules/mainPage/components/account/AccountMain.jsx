import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import {
    WrapperInp,
    AccountWrapper,
    AccountBtnWrapper,
    AccountTitleWrapper,
    AccountContentContainer } from './styledComponents.js';
import CustomLabel from '../../../../libs/customLabel/CustomLabel.jsx';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';
import AuthRegBlock from '../../../../libs/AuthRegBlock/AuthRegBlock.jsx';
import * as helpers from '../../../../utils/helpers/getDataHelpers';
import * as validationHelper from '../../../../utils/validation/validationHelpers';

const AccountPage = props => {
    const themeContext = useContext(ThemeContext);
    const accountColors = themeContext.currentTheme.accountPage;
    const {
        config,
        dictionary,
        teacherData,
        isAccountButton,
        changeAccountButton,
        updateUserData,
    } = props;

    const refAccountPage = {
        first_name: React.createRef(),
        email: React.createRef(),
        phone: React.createRef(),
        last_name: React.createRef(),
    };

    const accountErrors = {
        errorAccountName: React.createRef(),
        errorAccountEmail: React.createRef(),
        errorAccountPhone: React.createRef(),
        errorAccountSurname: React.createRef(),
    };

    const getTextInput = input => {
        const { first_name, last_name, email, phone } = teacherData.userData;
        switch (input) {
            case 'first_name' :
                return first_name;
            case 'last_name' :
                return last_name;
            case 'email' :
                return email;
            default :
                return phone;
        }
    };

    const sendRequestUpdate = () => {
        updateUserData({ data: helpers.getDataFromInputs(refAccountPage), regErrors: helpers.getRefOfElement(accountErrors), token: teacherData.token });
    };

    const clearErrorFields = data => {
        data.current.innerText = null;
    };

    const renderInputsAccount = () => {
        return (
            config.accountInputs.map(item => {
                return (
                    <AuthRegBlock
                        key={item.idInput}
                        htmlFor={item.idInput}
                        idInput={item.idInput}
                        inputRef={refAccountPage[item.idInput]}
                        labelName={dictionary.resources[item.resourcesKey]}
                        inputText={getTextInput(item.idInput)}
                        inputType={item.inputType}
                        isDisabled={isAccountButton}
                        focusColor={accountColors.mainTextColor}
                        inputCursor={'text'}
                        borderRadius={0}
                        fontSizeLabel={'18px'}
                        requiredInput={'required'}
                        backgroundColor={accountColors.inputBgColor}
                        placeholderText={dictionary.placeholders[item.placeholderKey]}
                        onFocusCallback={() => clearErrorFields(accountErrors[item.errorMessage])}
                        dataAttributeInput={config.dataAttributes.dataAttributeInputAuth[item.idInput]}
                        backgroundFocusColor={accountColors.inputBgColorFocus}
                        textColorLabel={accountColors.secondaryText}
                        colorInput={accountColors.mainTextColor}
                        textColor={accountColors.mainTextErrorColor}
                        textAlign={'center'}
                        marginBlock={'7px 0'}
                        errorRef={accountErrors[item.errorMessage]}
                        onKeyDownCallback={ event => validationHelper[item.onKeyDownCallback](event,
                          accountErrors[item.errorMessage].current, dictionary.errorMessage[item.errorMessage])}
                        dataAttributeError={config.dataAttributes.dataAttributeAccountError}
                    />
                );
            })
        );
    };

    const renderButtonsAccount = () => {
                return (
                    <CustomButton
                        key={isAccountButton ? 'accountChange' : 'accountSave'}
                        fontSize={20}
                        borderRadius={15}
                        fontWeight={'normal'}
                        textTransform={'none'}
                        isDisabled={false}//TODO
                        onClickCallback={ isAccountButton ? changeAccountButton : sendRequestUpdate }
                        backgroundColor={accountColors.btnBgColor}
                        border={'1px solid' + accountColors.borderBtm}
                        buttonName={isAccountButton ? dictionary.resources.accountChange : dictionary.resources.accountSave}
                        btnMargin={5}
                        dataAttribute={isAccountButton ? 'at-profile-change' : 'at-profile-save'}
                    />
                );
    };

    return (
        <AccountWrapper
            bgColor={accountColors.formBgColor}
            borderStyle={'1px solid' + accountColors.borderColor}
        >
            <AccountContentContainer>
                <AccountTitleWrapper>
                    <CustomLabel
                        fontSize={'40px'}
                        margin={'5px'}
                        labelName={dictionary.resources.profile}
                        textColor={accountColors.mainTextColor}
                        dataAttributeLabel={''} //TODO
                    />
                </AccountTitleWrapper>
                <WrapperInp>
                    {renderInputsAccount()}
                </WrapperInp>
                <AccountBtnWrapper>
                    {renderButtonsAccount()}
                </AccountBtnWrapper>
            </AccountContentContainer>
        </AccountWrapper>
    );
};

AccountPage.propTypes = {
    config: PropTypes.object,
    dictionary: PropTypes.object,
    teacherData: PropTypes.object,
    history: PropTypes.object,
    isAccountButton: PropTypes.bool,
    changeAccountButton: PropTypes.func,
    updateUserData: PropTypes.func,
};

export default React.memo(AccountPage);
