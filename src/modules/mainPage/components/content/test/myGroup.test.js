import {
  MyGroupWrapper,
  MyGroupTableWrapper,
  MyGroupContentTable,
  MyGroupContentRow,
  GroupElement,
  GroupSpanElement,
  defaultStyles,
} from '../components/myGroup/styledComponents.js';

describe('MyGroupWrapper styled components', () => {
  const props = {
    borderGroupWrapper: 'borderGroupWrapper',
    groupWrapperBgColor: 'groupWrapperBgColor',
  };

  it('MyGroupWrapper should have correct styles when all props were transferred', () => {
    const component = getTreeSC(<MyGroupWrapper {...props}/>);

    expect(component).toHaveStyleRule('border', 'borderGroupWrapper');
    expect(component).toHaveStyleRule('background', 'groupWrapperBgColor');
  });

  it('MyGroupWrapper should have default styles when all props weren\'t transferred', () => {
    const component = getTreeSC(<MyGroupWrapper/>);

    expect(component).toHaveStyleRule('border', defaultStyles.borderGroupWrapperDefault);
    expect(component).toHaveStyleRule('background', defaultStyles.groupWrapperBgColorDefault);
  });
});

describe('MyGroupTableWrapper styled components', () => {
  const props = {
    groupCaptionWrapperBgColor: 'groupCaptionWrapperBgColor',
    borderGroupCaptionWrapper: 'borderGroupCaptionWrapper',
  };

  it('MyGroupTableWrapper should have correct styles when all props were transferred', () => {
    const component = getTreeSC(<MyGroupTableWrapper {...props}/>);

    expect(component).toHaveStyleRule('background', 'groupCaptionWrapperBgColor');
    expect(component).toHaveStyleRule('border', 'borderGroupCaptionWrapper');
  });

  it('MyGroupTableWrapper should have default styles when all props weren\'t transferred', () => {
    const component = getTreeSC(<MyGroupTableWrapper/>);

    expect(component).toHaveStyleRule('border', defaultStyles.borderGroupCaptionWrapperDefault);
    expect(component).toHaveStyleRule('background', defaultStyles.groupCaptionWrapperBgColorDefault);
  });
});

describe('MyGroupContentTable styled components', () => {
  const props = {
    groupContentWrapper: 'groupContentWrapper',
    borderContentGroupWrapper: 'borderContentGroupWrapper',
  };

  it('MyGroupContentTable should have correct styles when all props were transferred', () => {
    const component = getTreeSC(<MyGroupContentTable {...props}/>);

    expect(component).toHaveStyleRule('background', 'groupContentWrapper');
    expect(component).toHaveStyleRule('border', 'borderContentGroupWrapper');
  });

  it('MyGroupContentTable should have default styles when all props weren\'t transferred', () => {
    const component = getTreeSC(<MyGroupContentTable/>);

    expect(component).toHaveStyleRule('background', defaultStyles.groupContentWrapperDefault);
    expect(component).toHaveStyleRule('border', defaultStyles.borderContentGroupWrapperDefault);
  });
});

describe('MyGroupContentRow styled components', () => {
  const props = {
    bgGroupContentRow: 'bgGroupContentRow',
    borderGroupRow: 'borderGroupRow',
  };

  it('MyGroupContentTable should have correct styles when all props were transferred', () => {
    const component = getTreeSC(<MyGroupContentRow {...props}/>);

    expect(component).toHaveStyleRule('background', 'bgGroupContentRow');
    expect(component).toHaveStyleRule('border', 'borderGroupRow');
  });

  it('MyGroupContentTable should have default styles when all props weren\'t transferred', () => {
    const component = getTreeSC(<MyGroupContentRow/>);

    expect(component).toHaveStyleRule('background', defaultStyles.bgGroupContentRowDefault);
    expect(component).toHaveStyleRule('border', defaultStyles.borderGroupRowDefault);
  });
});

describe('GroupElement styled components', () => {
  const props = {
    groupElement: 'groupElement',
    widthGroupElem: 'widthGroupElem',
    borderSchElem: 'borderSchElem',
    borderGroupElem: 'borderGroupElem',
    borderRightGroupElem: 'borderRightGroupElem',
  };

  it('GroupElement should have correct styles when all props were transferred', () => {
    const component = getTreeSC(<GroupElement {...props}/>);

    expect(component).toHaveStyleRule('width', 'widthGroupElem');
    expect(component).toHaveStyleRule('background', 'groupElement');
    expect(component).toHaveStyleRule('border', 'borderGroupElem');
    expect(component).toHaveStyleRule('border-right', 'borderRightGroupElem');
  });

  it('GroupElement should have default styles when all props weren\'t transferred', () => {
    const component = getTreeSC(<GroupElement/>);

    expect(component).toHaveStyleRule('width', defaultStyles.widthGroupElemDefault);
    expect(component).toHaveStyleRule('background', defaultStyles.groupElementBgDefault);
    expect(component).toHaveStyleRule('border', defaultStyles.borderGroupElemDefault);
    expect(component).toHaveStyleRule('border-right', defaultStyles.borderRightGroupElemDefault);
  });
});

describe('GroupSpanElement styled components', () => {
  const props = {
    groupElementBg: 'groupElementBg',
    borderGroupElem: 'borderGroupElem',
    colorGroupElem: 'colorGroupElem',
  };

  it('GroupSpanElement should have correct styles when all props were transferred', () => {
    const component = getTreeSC(<GroupSpanElement {...props}/>);

    expect(component).toHaveStyleRule('background', 'groupElementBg');
    expect(component).toHaveStyleRule('border', 'borderGroupElem');
    expect(component).toHaveStyleRule('color', 'colorGroupElem');
  });

  it('GroupSpanElement should have default styles when all props weren\'t transferred', () => {
    const component = getTreeSC(<GroupSpanElement/>);

    expect(component).toHaveStyleRule('background', defaultStyles.groupElementBgDefault);
    expect(component).toHaveStyleRule('border', defaultStyles.borderGroupSpanDefault);
    expect(component).toHaveStyleRule('color', defaultStyles.colorGroupElemDefault);
  });
});
