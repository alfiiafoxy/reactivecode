import {
    ScheduleWrapper,
    ScheduleCaptionWrapper,
    ScheduleContentWrapper,
    SchElementColumn,
    SchElement,
    SchSpanElement,
    SchContentRow,
    defaultStyles,
    SchGroupContainer,
    SchGroupElement,
    SchButtonWrapper } from '../components/scheduleMain/components/schedule/styledComponents.js';

describe('ScheduleWrapper styled components', () => {
    const props = {
        schWrapperBgColor: 'schWrapperBgColor',
    };

    it('ScheduleWrapper should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<ScheduleWrapper {...props}/>);

        expect(component).toHaveStyleRule('background', 'schWrapperBgColor');
    });

    it('ScheduleWrapper should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<ScheduleWrapper/>);

        expect(component).toHaveStyleRule('background', defaultStyles.schWrapperBgColorDefault);
    });
});

describe('ScheduleCaptionWrapper styled components', () => {
    const props = {
        schCaptionWrapperBgColor: 'schCaptionWrapperBgColor',
        borderSchCaptionWrapper: 'borderSchCaptionWrapper',
    };

    it('ScheduleWrapper should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<ScheduleCaptionWrapper {...props}/>);

        expect(component).toHaveStyleRule('background', 'schCaptionWrapperBgColor');
    });

    it('ScheduleWrapper should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<ScheduleCaptionWrapper/>);

        expect(component).toHaveStyleRule('background', defaultStyles.schCaptionWrapperBgColorDefault);
    });
});

describe('SchContentRow styled components', () => {
    const props = {
        borderSchRow: 'borderSchRow',
    };

    it('SchContentRow should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<SchContentRow {...props}/>);

        expect(component).toHaveStyleRule('border', 'borderSchRow');
    });

    it('SchContentRow should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<SchContentRow/>);

        expect(component).toHaveStyleRule('border', defaultStyles.borderSchRowDefault);
    });
});

describe('SchElementColumn styled components', () => {
    const props = {
        marginSchColumn: 'marginSchColumn',

    };

    it('SchElementColumn should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<SchElementColumn {...props}/>);

        expect(component).toHaveStyleRule('margin', 'marginSchColumn');
    });

    it('SchElementColumn should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<SchElementColumn/>);

        expect(component).toHaveStyleRule('margin', defaultStyles.marginSchColumnDefault);
    });
});

describe('SchElement styled components', () => {
    const props = {
        shElement: 'shElement',
        widthScgElem: 'widthScgElem',
        borderSchElem: 'borderSchElem',
        borderRightSchElem: 'borderRightSchElem',
    };

    it('SchElement should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<SchElement {...props}/>);

        expect(component).toHaveStyleRule('width', 'widthScgElem');
        expect(component).toHaveStyleRule('background', 'shElement');
        expect(component).toHaveStyleRule('border', 'borderSchElem');
        expect(component).toHaveStyleRule('border-right', 'borderRightSchElem');
    });

    it('SchElement should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<SchElement/>);

        expect(component).toHaveStyleRule('width', defaultStyles.widthScgElemDefault);
        expect(component).toHaveStyleRule('background', defaultStyles.shElementBgDefault);
        expect(component).toHaveStyleRule('border', defaultStyles.borderSchElemDefault);
        expect(component).toHaveStyleRule('border-right', defaultStyles.borderRightSchElemDefault);
    });
});

describe('SchSpanElement styled components', () => {
    const props = {
        shElementBg: 'shElementBg',
        borderSchElem: 'borderSchElem',
        colorSchElem: 'colorSchElem',
    };

    it('SchSpanElement should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<SchSpanElement {...props}/>);

        expect(component).toHaveStyleRule('background', 'shElementBg');
        expect(component).toHaveStyleRule('border', 'borderSchElem');
        expect(component).toHaveStyleRule('color', 'colorSchElem');
    });

    it('SchSpanElement should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<SchSpanElement/>);

        expect(component).toHaveStyleRule('background', defaultStyles.shElementBgDefault);
        expect(component).toHaveStyleRule('border', defaultStyles.borderSchSpanDefault);
        expect(component).toHaveStyleRule('color', defaultStyles.colorSchElemDefault);
    });
});

describe('SchGroupContainer styled components', () => {
    const props = {
        schGroupContainerBg: 'schGroupContainerBg',
    };

    it('SchSpanElement should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<SchGroupContainer {...props}/>);

        expect(component).toHaveStyleRule('background', 'schGroupContainerBg');
    });

    it('SchSpanElement should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<SchGroupContainer/>);

        expect(component).toHaveStyleRule('background', defaultStyles.schGroupContainerBgDefault);
    });
});

describe('SchGroupElement styled components', () => {
    const props = {
        widthSchGroupElem: 'widthSchGroupElem',
        schGroupElementBg: 'schGroupElementBg',
    };

    it('SchGroupElement should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<SchGroupElement {...props}/>);

        expect(component).toHaveStyleRule('width', 'widthSchGroupElem');
        expect(component).toHaveStyleRule('background', 'schGroupElementBg');
    });

    it('SchGroupElement should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<SchGroupElement/>);

        expect(component).toHaveStyleRule('width', defaultStyles.widthSchGroupElemDefault);
        expect(component).toHaveStyleRule('background', defaultStyles.schGroupElementBgDefault);
    });
});