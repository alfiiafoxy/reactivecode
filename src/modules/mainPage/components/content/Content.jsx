import React from 'react';
import PropTypes from 'prop-types';
import MyGroup from './components/myGroup/MyGroup.jsx';
import Attendance from './components/attendance/Attendance.jsx';
import { ContentWrapper } from './styledComponents.js';
import ScheduleMain from '@/mainPage/components/content/components/scheduleMain/ScheduleMain.jsx';
import Users from '@/mainPage/components/content/components/users/Users.jsx';
import AdminStatistic from '@/mainPage/components/content/components/adminStatistic/AdminStatistic.jsx';
import Statistic from './components/statistic/Statistic.jsx';

const Content = props => {
  const {
    schedule,
    attendance,
    changeScheduleWeek,
    config, dictionary,
    contentName,
    isChangeButton,
    changeButton,
    addLesson,
    currentLesson,
    selectedGroup,
    userRole,
    groupsData,
    currentInputUsers,
    changeInputUser,
    users,
    statistic,
    toggleModalWindow,
    adminStatisticData,
    adminStatistic,
    teacherName,
    setSelectedGroup,
    daysOfLessons,
    firstSelectWeekDay,
    lastSelectWeekDay,
    firstSelectWeekDayMilliseconds,
    lastSelectWeekDayMilliseconds,
    teacherSalary,
    getSalary,
    totalSalary,
    teachers,
  } = props;

  const scheduleMain = <ScheduleMain
    firstSelectWeekDay={firstSelectWeekDay}
    lastSelectWeekDay={lastSelectWeekDay}
    firstSelectWeekDayMilliseconds={firstSelectWeekDayMilliseconds}
    lastSelectWeekDayMilliseconds={lastSelectWeekDayMilliseconds}
    history={history}
    config={config}
    dictionary={dictionary}
    schedule={schedule}
    changeScheduleWeek={changeScheduleWeek}
    addLesson={addLesson}
    currentLesson={currentLesson}
    userRole={userRole}
    selectedGroup={selectedGroup}
    groupsData={groupsData}
    teacherName={teacherName}
    setSelectedGroup={setSelectedGroup}
    teachers={teachers}
    toggleModalWindow={toggleModalWindow}
  />;

  const attendanceMain = <Attendance
    firstSelectWeekDay={firstSelectWeekDay}
    lastSelectWeekDay={lastSelectWeekDay}
    firstSelectWeekDayMilliseconds={firstSelectWeekDayMilliseconds}
    lastSelectWeekDayMilliseconds={lastSelectWeekDayMilliseconds}
    config={config}
    dictionary={dictionary}
    schedule={schedule}
    attendance={attendance}
    selectedGroup={selectedGroup}
    changeScheduleWeek={changeScheduleWeek}
    isChangeButton={isChangeButton}
    changeButton={changeButton}
    userRole={userRole}
    groupsData={groupsData}
    daysOfLessons={daysOfLessons}
  />;

  const groupsMain = <MyGroup
    config={config}
    userRole={userRole}
    groupsData={groupsData}
    selectedGroup={selectedGroup}
    toggleModalWindow={toggleModalWindow}
    attendance={attendance}
    teacherName={teacherName}
  />;

  const usersMain = <Users
    config={config}
    users={users}
    dictionary={dictionary}
    schedule={schedule}
    attendance={attendance}
    changeScheduleWeek={changeScheduleWeek}
    isChangeButton={isChangeButton}
    changeButton={changeButton}
    userRole={userRole}
    groupsData={groupsData}
    currentInputUsers={currentInputUsers}
    changeInputUser={changeInputUser}
    toggleModalWindow={toggleModalWindow}
  />;

  const teachersStatistic = <Statistic
    history={history}
    config={config}
    dictionary={dictionary}
    statistic={statistic}
    changeSheduleWeek={changeScheduleWeek}
    groupsData={groupsData}
    getSalary={getSalary}
  />;
  const adminStatisticMain = <AdminStatistic
    config={config}
    dictionary={dictionary}
    adminStatisticData={adminStatisticData}
    adminStatistic={adminStatistic}
    teacherSalary={teacherSalary}
    getSalary={getSalary}
    totalSalary={totalSalary}
    groupsData={groupsData}
    userRole={userRole}
  />;

  const getRenderMainContent = name => {
    switch (name) {
      case 'schedule' :
        return scheduleMain;
      case 'mySchedule' :
        return scheduleMain;
      case 'groups' :
        return groupsMain;
      case 'myGroup' :
        return groupsMain;
      case 'attendance' :
        return attendanceMain;
      case 'users' :
        return usersMain;
      case 'statistic' :
        return teachersStatistic;
      case 'teachersStatistic' :
        return adminStatisticMain;
      default :
        return scheduleMain;
    }
  };

  return (
    <ContentWrapper>
      {getRenderMainContent(contentName)}
    </ContentWrapper>
  );
};

Content.propTypes = {
  contentName: PropTypes.string,
  userRole: PropTypes.string,
  schedule: PropTypes.array,
  changeScheduleWeek: PropTypes.func,
  addLesson: PropTypes.func,
  config: PropTypes.object,
  dictionary: PropTypes.object,
  attendance: PropTypes.object,
  isChangeButton: PropTypes.bool,
  changeButton: PropTypes.func,
  currentLesson: PropTypes.number,
  selectedGroup: PropTypes.number,
  groupsData: PropTypes.array,
  currentInputUsers: PropTypes.number,
  changeInputUser: PropTypes.func,
  students: PropTypes.object,
  users: PropTypes.array,
  statistic: PropTypes.object,
  toggleModalWindow: PropTypes.func,
  adminStatisticData: PropTypes.object,
  adminStatistic: PropTypes.object,
  teacherName: PropTypes.string,
  firstSelectWeekDayMilliseconds: PropTypes.string,
  lastSelectWeekDayMilliseconds: PropTypes.string,
  setSelectedGroup: PropTypes.func,
  daysOfLessons: PropTypes.object,
  firstSelectWeekDay: PropTypes.number,
  lastSelectWeekDay: PropTypes.number,
  teacherSalary: PropTypes.array,
  getSalary: PropTypes.func,
  totalSalary: PropTypes.array,
  teachers: PropTypes.array,
};

export default React.memo(Content);
