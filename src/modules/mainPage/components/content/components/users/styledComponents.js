import styled from 'styled-components';

export const Table = styled.div`
  display: flex;
  flex-direction: column;
  width: 90%;
  box-sizing: border-box;
`;

export const CaptionSection = styled.div`
  display: flex;
  height: 50px;
  font-size: 16px;
  box-sizing: border-box;
  text-align: center;
  border: ${props => props.captionSectionBorder ? props.captionSectionBorder : null};
  background-color: ${props => props.captionSectionBg ? props.captionSectionBg : null};
`;

export const Caption = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.widthSize}%;
  box-sizing: border-box;
`;

export const SpinnerContainer = styled.div`
  width:100%;
  height: 95%;
  display: flex;
  justify-content: center;
  align-content: center;
`;

export const TableBody = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
  margin-right: ${props => props.marginMinus ? props.marginMinus : '0'};
`;

export const UserRow = styled.div`
  width: 100%;
  display: flex;
  min-height: 40px;  
  box-sizing: border-box;
  border-right: ${props => props.borderUserRow ? props.borderUserRow : null};
  border-left: ${props => props.borderUserRow ? props.borderUserRow : null};
  border-bottom: ${props => props.borderUserRow ? props.borderUserRow : null};
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

export const UserItem = styled.div`
  display: flex;
  width: ${props => props.widthSize}%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  flex-direction: column;
  background-repeat: no-repeat;
  background-position: center;
  background-size: 19px;
`;

export const Header = styled.div`
  display: flex;
  width: 80%;
  height: 80px;
  justify-content: space-between;
  align-items: center;
`;

export const UserWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const ButtonWrapper = styled.div`   
  display: flex;  
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  width: ${props => props.btnWrapperWidth ? props.btnWrapperWidth : null};
  height: ${props => props.btnWrapperHeight ? props.btnWrapperHeight : null};
`;

export const ButtonContainer = styled.div`
  width: ${props => props.widthSize}%;
  box-sizing: border-box;
  display: flex;   
  justify-content: space-around; 
  align-items: center;
`;

export const SearchSection = styled.div`
  display: flex;
  flex-direction: row;
  box-sizing: border-box;
`;

export const SearchBox = styled.div`
  display: flex;
  width: ${props => props.widthSize}%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  flex-direction: column;
  background-repeat: no-repeat;
  background-position: center;
  background-size: 19px;
  border-bottom: 1px solid black;
`;
