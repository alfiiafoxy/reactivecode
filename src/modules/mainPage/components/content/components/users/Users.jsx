import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Scrollbar from 'react-scrollbars-custom';
import { ThemeContext } from 'styled-components';
import {
  Table,
  Header,
  Caption,
  TableBody,
  UserRow,
  UserItem,
  CaptionSection,
  ButtonContainer,
  ButtonWrapper,
  UserWrapper,
  SearchSection,
  SearchBox,
  SpinnerContainer,
} from './styledComponents';
import CustomDropdown from '../../../../../../libs/CustomDropdown/CustomDropdown.jsx';
import actionTypes from '../../../../../../constans/actionTypes.js';
import CustomButton from '../../../../../../libs/customButton/CustomButton.jsx';
import CustomInput from '../../../../../../libs/customInput/CustomInput.jsx';
import { MainPageContext } from '@/mainPage/MainPage.jsx';
import { usersFilterInputs } from '../../../../../../config/filterInputs.js';
import { getResultSearch } from '../../../../../../utils/helpers/filterHelpers.js';
import { getDataFromInputs } from '../../../../../../utils/helpers/getDataHelpers.js';
import LoaderSpinner from '../../../../../../libs/Loader/Loader';

const Users = props => {
  const themeContext = useContext(ThemeContext);
  const usersColor = themeContext.currentTheme.mainPage;
  const contextData = useContext(MainPageContext);
  const { dictionary, config } = contextData;
  const dispatch = useDispatch();
  const updateUser = useCallback(
    value => dispatch({ type: actionTypes.UPDATE_USER, payload: value })
      [dispatch],
  );

  const toggleModalWindowUserComponent = value => {
    toggleModalWindow({ type: value.type, data: value.data });
  };

  function searchUser(data) {
    const { lastName, firstName, email, phone, role, groupForUser, birthday, salaryPerHour } = data;
    let updateUsers = users.filter(item =>
      getResultSearch(lastName, item.last_name)
      && getResultSearch(firstName, item.first_name)
      && getResultSearch(email, item.email)
      && getResultSearch(phone, item.phone)
      && getResultSearch(role, item.user_role)
      && getResultSearch(salaryPerHour, item.rate_per_hour)
      && getResultSearch(groupForUser, item.groups)
      && getResultSearch(birthday, item.birth_date),
    );
    setFilteredUsers(updateUsers);
  }

  const {
    currentInputUsers,
    changeInputUser,
    users,
    toggleModalWindow,
  } = props;

  const getRow = (value, index, userId) => {
    const userRole = value.childNodes[index].childNodes[1].childNodes[0].value;
    const firstName = value.childNodes[index].childNodes[2].childNodes[0].value;
    const lastName = value.childNodes[index].childNodes[3].childNodes[0].value;
    const ratePerHour = value.childNodes[index].childNodes[5].childNodes[0].value;
    const phone = value.childNodes[index].childNodes[6].childNodes[0].value;
    const birthDate = value.childNodes[index].childNodes[7].childNodes[0].value;
    const email = value.childNodes[index].childNodes[8].childNodes[0].value;
    updateUser({ userId, userRole, firstName, lastName, ratePerHour, phone, birthDate, email });
  };

  const filterRef = {
    lastName: React.createRef(),
    firstName: React.createRef(),
    email: React.createRef(),
    phone: React.createRef(),
    role: React.createRef(),
    groupForUser: React.createRef(),
    birthday: React.createRef(),
    salaryPerHour: React.createRef(),
  };

  const handleSearch = () => {
    searchUser(getDataFromInputs(filterRef));
  };

  const [rowNumber, setRowNumber] = useState(0);
  const [filteredUsers, setFilteredUsers] = useState(users);
  useEffect(() => {
    setFilteredUsers(users);
  }, [users]);
  useEffect(() => {
    setRowNumber(users.length);
  }, []); // TODO fix unnecessary rerender

  return (
    <UserWrapper>
      <Header/>
      <Table>
        <CaptionSection
          captionSectionBg={usersColor.scheduleCaptionBg}
          captionSectionBorder={usersColor.borderColor}
        >
          <Caption widthSize={5} children={'#'}/>
          <Caption widthSize={7} children={dictionary.resources.tabUserCaptionRole}/>
          <Caption widthSize={10} children={dictionary.resources.tabUserCaptionName}/>
          <Caption widthSize={10} children={dictionary.resources.tabUserCaptionSurname}/>
          <Caption widthSize={10} children={dictionary.resources.tabUserCaptionGroup}/>
          <Caption widthSize={6} children={dictionary.resources.tabUserCaptionSalary}/>
          <Caption widthSize={10} children={dictionary.resources.tabUserCaptionPhone}/>
          <Caption widthSize={10} children={dictionary.resources.tabUserCaptionBirthDate}/>
          <Caption widthSize={22} children={dictionary.resources.tabUserCaptionEmail}/>
          <Caption widthSize={14} children={dictionary.resources.tabUserCaptionButtons}/>
        </CaptionSection>
        <SearchSection>
          {usersFilterInputs && usersFilterInputs.length ? usersFilterInputs.map((item) => {
              return (
                <SearchBox widthSize={item.width} key={item.inputKey}>
                  <CustomInput
                    inputRef={filterRef[item.refName]}
                    inputType={item.inputType}
                    isDisabled={item.isDisabled}
                    placeholderText={dictionary.placeholders[item.placeholder]}
                    inputCallback={handleSearch}
                    textAlignInput={'center'}
                  />
                </SearchBox>);
            },
          ) : null}
        </SearchSection>
        <TableBody
          data-at={'at-admin-users-table'}
          marginMinus={rowNumber > 12 ? '-10px' : '0'}
        >
          <Scrollbar
            style={{ height: 500, boxSizing: 'border-box', marginRight: '-20px' }}
          >
            {filteredUsers.length ? filteredUsers.map((user, index) => {
              return (
                <UserRow key={user.user_id} borderUserRow={`1px solid ${usersColor.borderColor}`}>
                  <UserItem widthSize={5}>{index + 1}</UserItem>
                  <UserItem widthSize={7}>
                    <CustomDropdown
                      backgroundColor={'none'}
                      isDisabled={user.user_id !== currentInputUsers}
                      content={config.usersRoleDropdown}
                      selectedValue={user.user_role}
                      focusColor={usersColor.dropDownSelectTextColor}
                      borderColorInp={user.user_id !== currentInputUsers ? 'none' : '2px solid' + usersColor.borderColorInp}
                      dataAttributeDropdown={config.dataAttributes.dataAttributeUsers.selectRole}
                    />
                  </UserItem>
                  <UserItem widthSize={10}>
                    <CustomInput
                      backgroundColor={'none'}
                      inputText={user.first_name}
                      isDisabled={user.user_id !== currentInputUsers}
                      dataAttributeInput={''}
                      inputType={'text'}
                      borderColorInp={user.user_id !== currentInputUsers ? 'none' : '2px solid' + usersColor.borderColorInp}
                      textAlignInput={'center'}
                    />
                  </UserItem>
                  <UserItem widthSize={10}>
                    <CustomInput
                      backgroundColor={'none'}
                      inputText={user.last_name}
                      isDisabled={user.user_id !== currentInputUsers}
                      dataAttributeInput={''}
                      inputType={'text'}
                      borderColorInp={user.user_id !== currentInputUsers ? 'none' : '2px solid' + usersColor.borderColorInp}
                      textAlignInput={'center'}
                    />
                  </UserItem>
                  <UserItem widthSize={10}>
                    {user.groups ? user.groups.map((groupName) => {
                      return (
                        <CustomInput
                          key={Math.random()}
                          backgroundColor={'none'}
                          inputText={groupName}
                          isDisabled={true}
                          dataAttributeInput={''}
                          inputType={'text'}
                          borderColorInp={'none'}
                          textAlignInput={'center'}
                        />);
                    }) : null
                    }
                  </UserItem>
                  <UserItem widthSize={6}>
                    <CustomInput
                      backgroundColor={'none'}
                      inputText={user.rate_per_hour ? user.rate_per_hour : 0}
                      isDisabled={user.user_id !== currentInputUsers}
                      dataAttributeInput={''}
                      inputType={'text'}
                      borderColorInp={user.user_id !== currentInputUsers ? 'none' : '2px solid' + usersColor.borderColorInp}
                      textAlignInput={'center'}
                    />
                  </UserItem>
                  <UserItem widthSize={10}>
                    <CustomInput
                      backgroundColor={'none'}
                      inputText={user.phone}
                      isDisabled={user.user_id !== currentInputUsers}
                      dataAttributeInput={''}
                      inputType={'text'}
                      borderColorInp={user.user_id !== currentInputUsers ? 'none' : '1px solid' + usersColor.borderColorInp}
                      textAlignInput={'center'}
                    />
                  </UserItem>
                  <UserItem widthSize={10}>
                    <CustomInput
                      backgroundColor={'none'}
                      inputText={user.birth_date}
                      isDisabled={user.user_id !== currentInputUsers}
                      dataAttributeInput={''}
                      inputType={'text'}
                      borderColorInp={user.user_id !== currentInputUsers ? 'none' : '2px solid' + usersColor.borderColorInp}
                      textAlignInput={'center'}
                    />
                  </UserItem>
                  <UserItem widthSize={22}>
                    <CustomInput
                      backgroundColor={'none'}
                      inputText={user.email}
                      isDisabled={user.user_id !== currentInputUsers}
                      dataAttributeInput={''}
                      inputType={'email'}
                      borderColorInp={user.user_id !== currentInputUsers ? 'none' : '2px solid' + usersColor.borderColorInp}
                      textAlignInput={'center'}
                    />
                  </UserItem>
                  <ButtonContainer widthSize={14}>
                    {user.user_id === currentInputUsers ?
                      <React.Fragment>
                        <ButtonWrapper
                          btnWrapperWidth={'24px'}
                          btnWrapperHeight={'24px'}
                        >
                          <CustomButton
                            backgroundImage={config.images.ok}
                            dataAttribute={config.dataAttributes.dataAttributeUsers.buttonOkEdit}
                            onClickCallback={event => getRow(event.target.parentNode.parentNode.parentNode.parentNode, index, user.user_id)}
                          />
                        </ButtonWrapper>
                        <ButtonWrapper
                          btnWrapperWidth={'24px'}
                          btnWrapperHeight={'24px'}
                        >
                          <CustomButton
                            backgroundImage={config.images.cancel}
                            dataAttribute={config.dataAttributes.dataAttributeUsers.buttonCancel}
                            onClickCallback={() => changeInputUser(0)}
                          />
                        </ButtonWrapper>
                      </React.Fragment> :
                      <React.Fragment>
                        <ButtonWrapper
                          btnWrapperWidth={'24px'}
                          btnWrapperHeight={'24px'}
                        >
                          <CustomButton
                            backgroundImage={config.images.edit}
                            dataAttribute={config.dataAttributes.dataAttributeUsers.buttonEdit}
                            onClickCallback={() => changeInputUser(user.user_id)}
                          />
                        </ButtonWrapper>
                        <ButtonWrapper
                          btnWrapperWidth={'24px'}
                          btnWrapperHeight={'24px'}
                        >
                          <CustomButton
                            backgroundImage={config.images.delete}
                            dataAttribute={config.dataAttributes.dataAttributeUsers.buttonDeleteUser}
                            onClickCallback={() => toggleModalWindowUserComponent({
                              type: 'confirmModal',
                              data: {
                                callbackValue: 'deleteUser',
                                attribute: { user_id: user.user_id, user_role: user.user_role },
                              },
                            })}
                          />
                        </ButtonWrapper>
                      </React.Fragment>
                    }
                  </ButtonContainer>
                </UserRow>);
            }) : <SpinnerContainer>
              <LoaderSpinner
                loaderType={'Rings'}
                loaderColor={usersColor.lightText}
                loaderHeight={80}
                loaderWidth={80}
              />
            </SpinnerContainer>
            }
          </Scrollbar>
        </TableBody>
      </Table>
    </UserWrapper>
  );
};

Users.propTypes = {
  attendance: PropTypes.object,
  config: PropTypes.object,
  dictionary: PropTypes.object,
  isChangeButton: PropTypes.bool,
  changeButton: PropTypes.func,
  currentInputUsers: PropTypes.number,
  changeInputUser: PropTypes.func,
  users: PropTypes.array,
  toggleModalWindow: PropTypes.func,
};

export default React.memo(Users);
