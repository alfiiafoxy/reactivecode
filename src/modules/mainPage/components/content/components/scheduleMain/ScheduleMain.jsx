import React from 'react';
import PropTypes from 'prop-types';
import Schedule from './components/schedule/Schedule.jsx';
import ControlPanel from './components/controlPanel/ControlPanel.jsx';
import { ControlPanelContainer, SchedulePanelContainer } from './styledComponents.js';

const ScheduleMain = (props) => {
  const {
    schedule,
    changeScheduleWeek,
    config,
    dictionary,
    history,
    addLesson,
    currentLesson,
    groupsData,
    userRole,
    teacherName,
    setSelectedGroup,
    firstSelectWeekDay,
    lastSelectWeekDay,
    firstSelectWeekDayMilliseconds,
    lastSelectWeekDayMilliseconds,
    teachers,
    toggleModalWindow,
  } = props;

  return (
    <React.Fragment>
      {userRole !== 'student' ?
        < ControlPanelContainer>
          < ControlPanel
            history={history}
            config={config}
            dictionary={dictionary}
            addLesson={addLesson}
            groupsData={groupsData}
          />
        </ControlPanelContainer> : null}
      <SchedulePanelContainer
        schPanelContainer={schedule.length === 0 ? 'flex-start' : 'center'}
      >
        <Schedule
          firstSelectWeekDayMilliseconds={firstSelectWeekDayMilliseconds}
          lastSelectWeekDayMilliseconds={lastSelectWeekDayMilliseconds}
          firstSelectWeekDay={firstSelectWeekDay}
          lastSelectWeekDay={lastSelectWeekDay}
          schedule={schedule}
          changeScheduleWeek={changeScheduleWeek}
          currentLesson={currentLesson}
          userRole={userRole}
          teacherName={teacherName}
          setSelectedGroup={setSelectedGroup}
          groupsData={groupsData}
          teachers={teachers}
          toggleModalWindow={toggleModalWindow}
        />
      </SchedulePanelContainer>
    </React.Fragment>
  );
};

ScheduleMain.propTypes = {
  contentName: PropTypes.string,
  schedule: PropTypes.array,
  changeScheduleWeek: PropTypes.func,
  addLesson: PropTypes.func,
  config: PropTypes.object,
  dictionary: PropTypes.object,
  history: PropTypes.object,
  currentLesson: PropTypes.number,
  groupsData: PropTypes.array,
  userRole: PropTypes.string,
  teacherName: PropTypes.string,
  firstSelectWeekDayMilliseconds: PropTypes.string,
  lastSelectWeekDayMilliseconds: PropTypes.string,
  setSelectedGroup: PropTypes.func,
  firstSelectWeekDay: PropTypes.number,
  lastSelectWeekDay: PropTypes.number,
  teachers: PropTypes.array,
  toggleModalWindow: PropTypes.func,
};

export default React.memo(ScheduleMain);
