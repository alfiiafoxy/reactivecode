import React, { useContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import {
  WrapperInp,
  ControlPanelWrapper,
  ControlPanelBtnWrapper,
  ControlPanelTitleWrapper,
  ControlPanelContentContainer,
} from './styledComponents.js';
import ContrPanelBlock from '../../../../../../../../libs/ContrPanelBlock/ConrolPanelBlock.jsx';
import CustomLabel from '../../../../../../../../libs/customLabel/CustomLabel.jsx';
import CustomButton from '../../../../../../../../libs/customButton/CustomButton.jsx';
import DropdownBlock from '../../../../../../../../libs/DropdownBlock/DropdownBlock.jsx';
import { getDataFromInputs, clearInputs } from '../../../../../../../../utils/helpers/getDataHelpers.js';

const ControlPanel = props => {
  const themeContext = useContext(ThemeContext);
  const contrPanelColors = themeContext.currentTheme.controlPanel;
  const { config, dictionary, addLesson, groupsData } = props;

  const refControlPanel = {
    groupsId: React.createRef(),
    dataOfLesson: React.createRef(),
    startTime: React.createRef(),
    endTime: React.createRef(),
    typeOfLesson: React.createRef(),
  };

  const [groupName, setGroupName] = useState('');

  const changeGroupName = () => {
      const select = refControlPanel.groupsId.current;
      const name = select.options[select.selectedIndex].innerText;
      setGroupName(name);
    };

  useEffect(() => {
    const name = groupsData.length ? groupsData[0].name : '';
    setGroupName(name);
  }, [groupsData]);

  const renderDropdownControlPanel = () => {
    return (
      config.controlPanelDropdown.map(item => {
        return (
          <DropdownBlock
            key={item.idDropdown}
            textColorLabel={contrPanelColors.secondaryText}
            labelName={dictionary.resources[item.resourcesKey]}
            htmlFor={item.idDropdown}
            idDropdown={item.idDropdown}
            dropdownRef={refControlPanel[item.dropdownRef]}
            dataAttributeDropdown={config.dataAttributes.dataAttribiteControlPanelDropdown[item.idDropdown]}
            backgroundColor={contrPanelColors.inputBgColor}
            borderRadius={'none'}
            backgroundFocusColor={contrPanelColors.inputBgColorFocus}
            focusColor={contrPanelColors.mainTextColor}
            color={contrPanelColors.mainTextColor}
            textColor={'transparent'}
            onChangeCallback={changeGroupName}
            content={item.idDropdown === 'typeOfLesson' ? item.content : groupsData}
          />
        );
      })
    );
  };

  const renderInputsContrPanel = () => {
    return (
      config.controlPanelInputs.map(item => {
        return (
          <ContrPanelBlock
            key={item.idInput}
            textColorLabel={contrPanelColors.secondaryText}
            labelName={dictionary.resources[item.resourcesKey]}
            htmlFor={item.idInput}
            idInput={item.idInput}
            inputRef={refControlPanel[item.idInput]}
            inputType={item.inputType}
            requiredInput={'required'}
            placeholderText={dictionary.placeholders[item.placeholderKey]}
            isDisabled={false}
            dataAttributeInput={config.dataAttributes.dataAttributeControlPanelInput[item.idInput]}
            backgroundColor={contrPanelColors.inputBgColor}
            borderRadius={'none'}
            backgroundFocusColor={contrPanelColors.inputBgColorFocus}
            focusColor={contrPanelColors.mainTextColor}
            inputCursor={'pointer'}
            colorInput={contrPanelColors.mainTextColor}
            errorMessage={''} //TODO need add error logic
            textColor={'transparent'}
          />
        );
      })
    );
  };

  const addLessonCallback = () => addLesson({ data: getDataFromInputs(refControlPanel), groupName, clearFunc: clearLessonCallback, message: { err: dictionary.errorMessage.lessonError, success: dictionary.message.lessonSuccess } });

  const clearLessonCallback = () => {
    const data = {
      dataOfLesson: refControlPanel.dataOfLesson,
      startTime: refControlPanel.startTime,
      endTime: refControlPanel.endTime,
    };
    clearInputs(data);
  };

  const renderButtonsContrPanel = () => {
    return (
      config.controlPanelButton.map(item => {
        return (
          <CustomButton
            key={item.isButton}
            fontSize={18}
            borderRadius={4}
            dataAttribute={config.dataAttributes.dataAttributeControlPanelButton[item.isButton]}
            textTransform={'none'}
            isDisabled={false}
            onClickCallback={item.onClickCallback === 'createLessonHandler' ? addLessonCallback : clearLessonCallback}
            backgroundColor={item.resourcesKey === 'add' ? contrPanelColors.btnMainBgColor : contrPanelColors.btnBgColor}
            border={`1px solid ${contrPanelColors.borderColor}`}
            buttonName={dictionary.resources[item.resourcesKey]}
            btnMargin={5}
            fontWeight={'none'}
            textColor={ item.resourcesKey === 'add' ? contrPanelColors.btnMainTextColor : contrPanelColors.btnSecondaryTextColor}
          />
        );
      })
    );
  };

  return (
    <ControlPanelWrapper
      bgColor={contrPanelColors.formBgColor}
    >
      <ControlPanelContentContainer>
        <ControlPanelTitleWrapper>
          <CustomLabel
            fontSize={'40px'}
            margin={'5px'}
            labelName={dictionary.resources.controlPanel}
            textColor={contrPanelColors.mainTextColor}
            dataAttributeLabel={''} //TODO
          />
        </ControlPanelTitleWrapper>
        <WrapperInp>
          {renderDropdownControlPanel()}
          {renderInputsContrPanel()}
        </WrapperInp>
        <ControlPanelBtnWrapper>
          {renderButtonsContrPanel()}
        </ControlPanelBtnWrapper>
      </ControlPanelContentContainer>
    </ControlPanelWrapper>
  );
};

ControlPanel.propTypes = {
  config: PropTypes.object,
  dictionary: PropTypes.object,
  history: PropTypes.object,
  addLesson: PropTypes.func,
  groupsData: PropTypes.array,
};

export default React.memo(ControlPanel);
