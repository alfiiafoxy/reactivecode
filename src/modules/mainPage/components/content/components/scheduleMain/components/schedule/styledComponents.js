import styled from 'styled-components';

const widthScgElemDefault = '16%';
const borderSchElemDefault = 'none';
const borderSchSpanDefault = 'none';
const colorSchElemDefault = '#000000';
const bgSchContentRow = 'transparent';
const bgSchContentRowDefault = '74,136,204,0.2';
const widthSchGroupElemDefault = '35%';
const shElementBgDefault = 'transparent';
const marginSchColumnDefault = '5px 3px';
const borderContentSchWrapperDefault = 'none';
const schContentWrapperDefault = 'transparent';
const schWrapperBgColorDefault = 'transparent';
const schGroupElementBgDefault = 'transparent';
const borderSchRowDefault = 'none';
const schGroupContainerBgDefault = 'transparent';
const schCaptionWrapperBgColorDefault = 'transparent';
const borderRightSchElemDefault = '1px solid #000000';
const borderSchCaptionWrapperDefault = '1px solid #000000';
const schButtonWrapperBgDefault = 'transparent';
const schButtonWrapperBrRightDefault = '2px solid #000000';
const schButtonWrapperBrTopDefault = '2px solid #000000';
const schButtonWrapperRotateDefault = '-135deg';
const schButtonWrapperTopDefault = '0';
const schButtonWrapperLeftDefault = '0';
const flexSchElDefault = ' ';
const alignItemSchDefault = 'center';
const paddingSchEllDefault = '0';
const paddingSchContRowDefault = '0px';
const subRowElHeightDefault = '100%';
const fontSizeSchElemDefault = '16px';
const schCaptionBorderBgColorDefault = 'none';

export const ScheduleContainerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 950px;
  box-sizing: border-box;
`;

export const ScheduleWrapper = styled.div`
  width:100%;
  height: 95%;
  display: flex;
  flex-direction: column; 
  align-content: center;  
  background: ${props => props.schWrapperBgColor ? props.schWrapperBgColor : schWrapperBgColorDefault};
`;

export const SpinnerContainer = styled.div`
  width:100%;
  height: 95%;
  display: flex;
  justify-content: center;
  align-content: center;
`;

export const ScheduleCaptionWrapper = styled.div`
  display: flex;
  height: 50px;
  font-size: 16px;
  font-weight: 700;
  box-sizing: border-box;
  border: ${props => props.schCaptionBorder ? props.schCaptionBorder : schCaptionBorderBgColorDefault};
  background: ${props => props.schCaptionWrapperBgColor ? props.schCaptionWrapperBgColor : schCaptionWrapperBgColorDefault};
`;

export const ScheduleContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
  font-weight: 500;
  margin-right: ${props => props.marginMinus ? props.marginMinus : '0'};;
`;

export const SchContentRow = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 20%;
  display: flex;
  border-right: ${props => props.borderSchRow ? props.borderSchRow : borderSchRowDefault};
  border-left: ${props => props.borderSchRow ? props.borderSchRow : borderSchRowDefault};
  border-bottom: ${props => props.borderSchRow ? props.borderSchRow : borderSchRowDefault};
  padding: ${props => props.paddingSchContRow ? props.paddingSchContRow : paddingSchContRowDefault};;

  :nth-child(odd) {
    background-color: rgba(${props => props.bgSchContentRow ? props.bgSchContentRow : bgSchContentRowDefault});
  }
`;

export const SchElementColumn = styled.div`
  width: 35%;
  display: flex;
  flex-direction: column;
  margin: ${props => props.marginSchColumn ? props.marginSchColumn : marginSchColumnDefault};
`;

export const SchElement = styled.div`
  width: ${props => props.widthScgElem ? props.widthScgElem : widthScgElemDefault};
  box-sizing: border-box;
  display: flex;
  flex: ${props => props.flexSchEl ? props.flexSchEl : flexSchElDefault};
  flex-direction: column;
  justify-content: space-around;
  align-items: ${props => props.alignItemSch ? props.alignItemSch : alignItemSchDefault};
  text-align: center;
  background: ${props => props.shElement ? props.shElement : shElementBgDefault}; 
  padding: ${props => props.paddingSchEll ? props.paddingSchEll : paddingSchEllDefault};
  
  :last-child {
    border-right: none;
  }
`;

export const SchSpanElement = styled.span`
  width: 100%;
  margin: 5px 0;
  font-weight: normal;
  background: ${props => props.shElementBg ? props.shElementBg : shElementBgDefault}; 
  border: ${props => props.borderSchElem ? props.borderSchElem : borderSchSpanDefault};
  color: ${props => props.colorSchElem ? props.colorSchElem : colorSchElemDefault};
  font-size: ${props => props.fontSizeSchElem ? props.fontSizeSchElem : fontSizeSchElemDefault};;
`;

export const SchGroupContainer = styled.div`
  width: 950px;
  height: 50px;
  margin-bottom: 10px;
  background: ${props => props.schGroupContainerBg ? props.schGroupContainerBg : schGroupContainerBgDefault}; 
  display: flex;
  justify-content: space-between;
`;

export const SchGroupElement = styled.div`
  width: ${props => props.widthSchGroupElem ? props.widthSchGroupElem : null};
  height: 100%;
  margin: 5px 0;
  background: ${props => props.schGroupElementBg ? props.schGroupElementBg : schGroupElementBgDefault}; 
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const SchButtonWrapper = styled.div`
  width: 20px;
  height: 20px;
  box-sizing: border-box;
`;

export const SchSubRow = styled.div`
  display: flex;
  flex: 1;
  height: 100%;
  box-sizing: border-box;
`;

export const SchSubRowElement = styled.div`
  width: ${props => props.subRowElWidth ? props.subRowElWidth : null};
  height: ${props => props.subRowElHeight ? props.subRowElHeight : null};;
  display: flex;
  justify-content: space-around;
  align-items: center;
  box-sizing: border-box;
  min-height: 45px;
  
  :last-child {
    border-right: none;
  }
`;

export const SchSubRowElementCont = styled.div`
  width: ${props => props.subRowElContWidth ? props.subRowElContWidth : null};
  height: ${props => props.subRowElContElHeight ? props.subRowElContElHeight : null};
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`;
export const DropdownContainer = styled.div`
  width: ${props => props.dropdownContainerWidth ? props.dropdownContainerWidth : null};
  height: ${props => props.dropdownContainerHeight ? props.dropdownContainerHeight : null};
  display: flex;
  justify-content: space-around;
  align-items: center;
  box-sizing: border-box;
`;

export const DropdownElement = styled.div`
  width: ${props => props.dropdownElementWidth ? props.dropdownElementWidth : '100%'};
  height: ${props => props.dropdownElementHeight ? props.dropdownElementHeight : '100%'};
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`;

export const defaultStyles = {
  bgSchContentRow,
  shElementBgDefault,
  widthScgElemDefault,
  colorSchElemDefault,
  borderSchRowDefault,
  borderSchElemDefault,
  borderSchSpanDefault,
  marginSchColumnDefault,
  bgSchContentRowDefault,
  schContentWrapperDefault,
  schWrapperBgColorDefault,
  widthSchGroupElemDefault,
  schButtonWrapperBgDefault,
  schGroupElementBgDefault,
  borderRightSchElemDefault,
  schGroupContainerBgDefault,
  schButtonWrapperTopDefault,
  schButtonWrapperLeftDefault,
  schButtonWrapperBrTopDefault,
  schButtonWrapperRotateDefault,
  borderContentSchWrapperDefault,
  schButtonWrapperBrRightDefault,
  borderSchCaptionWrapperDefault,
  schCaptionWrapperBgColorDefault,
  subRowElHeightDefault,
};

