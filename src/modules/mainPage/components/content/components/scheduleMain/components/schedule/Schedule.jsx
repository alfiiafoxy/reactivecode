import React, { useContext, useCallback, useState, useEffect } from 'react';
import moment from 'moment';
import actionTypes from '../../../../../../../../constans/actionTypes';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Scrollbar from 'react-scrollbars-custom';
import { ThemeContext } from 'styled-components';
import { MainPageContext } from '@/mainPage/MainPage.jsx';
import {
  ScheduleCaptionWrapper,
  ScheduleContentWrapper,
  SchElement,
  SchSpanElement,
  SchContentRow,
  SchGroupContainer,
  SchGroupElement,
  ScheduleContainerWrapper,
  SchSubRow,
  SchSubRowElement,
  SchSubRowElementCont,
  SpinnerContainer,
  DropdownContainer,
  DropdownElement,
} from './styledComponents.js';
import CustomInput from '../../../../../../../../libs/customInput/CustomInput.jsx';
import CustomDropdown from '../../../../../../../../libs/CustomDropdown/CustomDropdown.jsx';
import CustomButton from '../../../../../../../../libs/customButton/CustomButton.jsx';
import LoaderSpinner from '../../../../../../../../libs/Loader/Loader.jsx';
import FilterDateBlock from '../../../../../../../../libs/FilterDateBlock/FilterDateBlock.jsx';
import { filterScheduleByCity, filterScheduleByTeacherId } from '../../../../../../../../utils/helpers/filterHelpers';

const Schedule = props => {
  const contextData = useContext(MainPageContext);
  const { scheduleData, dictionary, config } = contextData;
  const themeContext = useContext(ThemeContext);
  const mainColors = themeContext.currentTheme.mainPage;
  const scheduleColors = themeContext.currentTheme.schedulePage;

  const {
    schedule,
    currentLesson,
    teacherName,
    groupsData,
    userRole,
    firstSelectWeekDay,
    lastSelectWeekDay,
    firstSelectWeekDayMilliseconds,
    lastSelectWeekDayMilliseconds,
    teachers,
    toggleModalWindow,
  } = props;

  const dispatch = useDispatch();

  const changeCurrentLesson = useCallback(
    value => dispatch({ type: actionTypes.CHANGE_CURRENT_LESSON, payload: value }),
    [dispatch],
  );

  const toggleModalWindowSchedule = value => {
    toggleModalWindow({ type: value.type, data: value.data });
  };

  const [rowNumber, setRowNumber] = useState(0);

  useEffect(() => {
    setRowNumber(schedule.length);
  }, []);

  const filterDropdownRef = {
    teacher: React.createRef(),
    city: React.createRef(),
  };

  const editLesson = useCallback(
    value => dispatch({ type: actionTypes.UPDATE_LESSON, payload: value }),
    [dispatch],
  );

  const getRow = (value, lesson_id) => {
    const startTime = value.childNodes[1].childNodes[0].childNodes[0].value;
    const endTime = value.childNodes[2].childNodes[0].childNodes[0].value;
    const lesson_type = value.childNodes[3].childNodes[0].childNodes[0].value;
    const dataOfLesson = value.parentNode.parentNode.childNodes[1].childNodes[0].childNodes[0];
    editLesson({ lesson_id, lesson_type: lesson_type, data: { endTime: endTime, startTime: startTime, dataOfLesson: dataOfLesson } });
  };

  const getSelectCheckMilliseconds = date => {
    const dateSecondsLesson = moment(date, 'DD.MM.YYYY').format('X');
    return dateSecondsLesson >= firstSelectWeekDayMilliseconds && dateSecondsLesson <= lastSelectWeekDayMilliseconds;
  };

  function getTeachersData() {
    let newTeachers = teachers.map(teacher => {
      return {
        id: teacher.user_id,
        name: `${teacher.first_name} ${teacher.last_name}`,
      };
    });
    newTeachers.unshift({ id: 0, name: `All Teachers` });
    return newTeachers;
  }

  function filterSchedule() {
    if (!schedule.length || userRole !== 'admin') {
      setFilteredLessons(schedule);
      return;
    }
    let newSchedule = filterScheduleByCity(schedule, filterDropdownRef.city, groupsData);
    newSchedule = filterScheduleByTeacherId(newSchedule, filterDropdownRef.teacher, groupsData);
    setFilteredLessons(newSchedule);
  }

  const [filteredLessons, setFilteredLessons] = useState(schedule);
  useEffect(() => {
    filterSchedule();
  }, [schedule]);

  const renderControlGroupBlock = () => {
    return (
      <SchGroupContainer>
        <SchGroupElement widthSchGroupElem={'35%'}>
          <SchSpanElement
            children={groupsData.length ? `${dictionary.resources.teacher}: ${teacherName}` : null}
            colorSchElem={scheduleColors.mainTextColor}
            fontSizeSchElem={'20px'}
          />
        </SchGroupElement>
        <SchGroupElement widthSchGroupElem={'35%'}>
          {userRole === 'admin' ?
            <DropdownContainer dropdownContainerWidth={'300px'} dropdownContainerHeight={'30px'}>
              <DropdownElement dropdownElementWidth={'100px'} dropdownElementHeight={'100%'}>
                <CustomDropdown
                  dropdownRef={filterDropdownRef.city}
                  idDropdown={'city'}
                  dataAttributeDropdown={config.dataAttributes.dataAttributesAddModal.groupCity}
                  selectedValue={config.cityFilterDropdown[0].name}
                  content={config.cityFilterDropdown}
                  onChangeCallback={filterSchedule}
                />
              </DropdownElement>
              <DropdownElement dropdownElementWidth={'140px'} dropdownElementHeight={'100%'}>
                {teachers ? <CustomDropdown
                  dropdownRef={filterDropdownRef.teacher}
                  idDropdown={'city'}
                  dataAttributeDropdown={config.dataAttributes.dataAttributesAddModal.groupCity}
                  content={getTeachersData()}
                  onChangeCallback={filterSchedule}
                /> : null}
              </DropdownElement>
            </DropdownContainer>
            : null}
        </SchGroupElement>
        <SchGroupElement>
          <FilterDateBlock
            config={config}
            firstSelectWeekDay={firstSelectWeekDay}
            lastSelectWeekDay={lastSelectWeekDay}
            colorInput={scheduleColors.mainTextColor}
            errorMessage={''} //TODO need add error logic
            textColor={'transparent'}
          />
        </SchGroupElement>
      </SchGroupContainer>
    );
  };

  return (
    <React.Fragment>
      {renderControlGroupBlock()}
      <ScheduleContainerWrapper
        data-at={'at-schedule-table'}
      >
        <ScheduleCaptionWrapper schCaptionWrapperBgColor={mainColors.scheduleCaptionBg} schCaptionBorder={`1px solid ${mainColors.borderColor}`}>
          {scheduleData.scheduleCaption.map((item, i) => {
            if (userRole === 'student' && item.resourcesKey === 'controlButtons') {
              return;
            }
            return (
              <SchElement widthScgElem={i < 2 ? '10%' : userRole === 'student' ? '20%' : '16%'} key={item.idCaption}>
                <SchSpanElement
                  children={dictionary.resources[item.resourcesKey]}
                />
              </SchElement>
            );
          })}
        </ScheduleCaptionWrapper>
        <ScheduleContentWrapper
          marginMinus={rowNumber > 8 ? '-10px' : '0'}
        >
          <Scrollbar
            style={{ height: 450, boxSizing: 'border-box', marginRight: '-20px' }}
          >
            {filteredLessons.length ? filteredLessons.map(item => {
              if (!item) {
                return;
              }
              if (!getSelectCheckMilliseconds(item.date)) {
                return;
              }
              return (
                <SchContentRow key={item.date}
                               borderSchRow={`1px solid ${mainColors.borderColor}`}
                >
                  <SchElement widthScgElem={'10%'}>
                    <SchSpanElement children={item.day}/>
                  </SchElement>
                  <SchElement widthScgElem={'10%'}>
                    <SchSpanElement children={item.date}/>
                  </SchElement>
                  <SchElement
                    flexSchEl={1}
                    alignItemSch={'none'}
                  >
                    {item.lessons.map(el => {
                      return (
                        <SchSubRow
                          key={el.id}
                          schSubRowPadd={'5px 0'}
                        >
                          <SchSubRowElement subRowElWidth={userRole === 'student' ? '25%' : '20%'}>
                            <SchSubRowElementCont
                              subRowElContWidth={'100%'}
                              subRowElContElHeight={'100%'}
                            >
                              <CustomInput
                                inputText={el.groupsName}
                                isDisabled={true}
                                dataAttributeInput={''}
                                inputType={'text'}
                                borderColorInp={'none'}
                                backgroundColor={ 'none'}
                                textAlignInput={'center'}
                              />
                            </SchSubRowElementCont>
                          </SchSubRowElement>
                          <SchSubRowElement subRowElWidth={userRole === 'student' ? '25%' : '20%'}>
                            <SchSubRowElementCont
                              subRowElContWidth={'100%'}
                              subRowElContElHeight={'100%'}
                            >
                              <CustomInput
                                inputText={el.timeStart}
                                isDisabled={el.id !== currentLesson}
                                dataAttributeInput={''}
                                inputType={'time'}
                                borderColorInp={el.id !== currentLesson ? 'none' : '2px solid' + mainColors.borderColorInp}
                                backgroundColor={'none'}
                                textAlignInput={'center'}
                              />
                            </SchSubRowElementCont>
                          </SchSubRowElement>
                          <SchSubRowElement subRowElWidth={userRole === 'student' ? '25%' : '20%'}>
                            <SchSubRowElementCont
                              subRowElContWidth={'100%'}
                              subRowElContElHeight={'100%'}
                            >
                              <CustomInput
                                inputText={el.timeEnd}
                                isDisabled={el.id !== currentLesson}
                                dataAttributeInput={''}
                                inputType={'time'}
                                borderColorInp={el.id !== currentLesson ? 'none' : '2px solid' + mainColors.borderColorInp}
                                backgroundColor={'none'}
                                textAlignInput={'center'}
                              />
                            </SchSubRowElementCont>
                          </SchSubRowElement>
                          <SchSubRowElement subRowElWidth={userRole === 'student' ? '25%' : '20%'}>
                            <SchSubRowElementCont
                              subRowElContWidth={'50%'}
                              subRowElContElHeight={'100%'}
                            >
                              <CustomDropdown
                                isDisabled={el.id !== currentLesson}
                                selectedValue={el.type}
                                content={config.controlPanelDropdown[0].content}
                                borderColorInp={el.id !== currentLesson ? 'none' : '2px solid' + mainColors.borderColorInp}
                                backgroundColor={'none'}
                              />
                            </SchSubRowElementCont>
                          </SchSubRowElement>
                          {userRole === 'student' ? null : <SchSubRowElement subRowElWidth={'20%'}>
                            {el.id === currentLesson ?
                              <React.Fragment>
                                <SchSubRowElementCont
                                  subRowElContWidth={'24px'}
                                  subRowElContElHeight={'24px'}
                                >
                                  <CustomButton
                                    backgroundImage={config.images.ok}
                                    dataAttribute={''}
                                    onClickCallback={event => getRow(event.target.parentNode.parentNode.parentNode, el.id)}
                                  />
                                </SchSubRowElementCont>
                                <SchSubRowElementCont
                                  subRowElContWidth={'24px'}
                                  subRowElContElHeight={'24px'}
                                >
                                  <CustomButton
                                    backgroundImage={config.images.cancel}
                                    dataAttribute={''}
                                    onClickCallback={() => changeCurrentLesson(0)}
                                  />
                                </SchSubRowElementCont>
                              </React.Fragment> :
                              <React.Fragment>
                                <SchSubRowElementCont
                                  subRowElContWidth={'24px'}
                                  subRowElContElHeight={'24px'}
                                >
                                  <CustomButton
                                    backgroundImage={config.images.edit}
                                    dataAttribute={''}
                                    onClickCallback={() => changeCurrentLesson(el.id)}
                                  />
                                </SchSubRowElementCont>
                                <SchSubRowElementCont
                                  subRowElContWidth={'24px'}
                                  subRowElContElHeight={'24px'}
                                >
                                  <CustomButton
                                    backgroundImage={config.images.delete}
                                    dataAttribute={''}
                                    onClickCallback={() => toggleModalWindowSchedule({
                                      type: 'confirmModal',
                                      data: {
                                        callbackValue: 'deleteLesson',
                                        attribute: { lesson_id: el.id, groupId: el.groupsId },
                                    },
                                    })}
                                  />
                                </SchSubRowElementCont>
                              </React.Fragment>
                            }
                          </SchSubRowElement>}
                        </SchSubRow>
                      );
                    })}
                  </SchElement>
                </SchContentRow>
              );
            }) : <SpinnerContainer>
              <LoaderSpinner
                loaderType={'Rings'}
                loaderColor={mainColors.lightText}
                loaderHeight={80}
                loaderWidth={80}
              />
            </SpinnerContainer>
            }
          </Scrollbar>
        </ScheduleContentWrapper>
      </ScheduleContainerWrapper>
    </React.Fragment>
  );
};

Schedule.propTypes = {
  changeScheduleWeek: PropTypes.func,
  schedule: PropTypes.array,
  currentLesson: PropTypes.number,
  groupsData: PropTypes.array,
  teacherName: PropTypes.string,
  firstSelectWeekDayMilliseconds: PropTypes.string,
  lastSelectWeekDayMilliseconds: PropTypes.string,
  userRole: PropTypes.string,
  setSelectedGroup: PropTypes.func,
  firstSelectWeekDay: PropTypes.number,
  lastSelectWeekDay: PropTypes.number,
  teachers: PropTypes.array,
  toggleModalWindow: PropTypes.func,
};

export default React.memo(Schedule);
