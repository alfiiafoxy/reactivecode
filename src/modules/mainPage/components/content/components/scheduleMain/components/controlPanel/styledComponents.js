import styled from 'styled-components';

export const ControlPanelWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: start;
    background: ${props => props.bgColor ? props.bgColor : '#ffffff'};
    border: ${props => props.borderStyle ? props.borderStyle : 'transparent'};
`;

export const ControlPanelContentContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
    border: ${props => props.borderStyle ? props.borderStyle : 'transparent'};
`;

export const ControlPanelTitleWrapper = styled.div`
    margin: 15px;
 `;

export const ControlPanelBtnWrapper = styled.div`
    width:130px;
    margin-bottom:10px;
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

export const WrapperInp = styled.div`
    min-height: 35px;
    width: auto;
    justify-content: center;
    align-items: center;
`;
