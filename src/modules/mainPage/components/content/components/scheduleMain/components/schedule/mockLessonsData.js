 // [
 //  {
 //    date: '18.05.20',
 //    day: 'Monday',
 //    lessons: [
 //      {
 //        id: 1
 //        groupsName: 'name',
 //        timeStart: '08:30',
 //        timeEnd: '08:30',
 //        type: 'Lection',
 //        duration: 1, TODO
 //        groupsId: 1,
 //
 //      },
 //    ],
 //  },
export const mockLessons = {
  month: [
    [
      {
        date: '18.05.20',
        name: 'Monday',
        timeStart: [
          '08:30',
          '12:30',
          '08:30',
        ],
        timeEnd: [
          '08:30',
          '12:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Practic',
          'Lection',
        ],
        duration: [
          1,
          2,
          3,
        ],
      },
      {
        date: '19.05.20',
        name: 'Thusday',
        timeStart: [
          '08:30',
          '12:30',
          '08:30',
        ],
        timeEnd: [
          '08:30',
          '12:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Practic',
          'Lection',
        ],
        duration: [
          1,
          2,
          3,
        ],
      },
      {
        date: '20.05.20',
        name: 'Wensday',
        timeStart: [
          '08:30',
          '12:30',
          '08:30',
        ],
        timeEnd: [
          '08:30',
          '12:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Practic',
          'Lection',
        ],
        duration: [
          1,
          2,
          3,
        ],
      },
      {
        date: '21.05.20',
        name: 'Thusday',
        timeStart: [
          '08:30',
          '12:30',
          '08:30',
        ],
        timeEnd: [
          '08:30',
          '12:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Practic',
          'Lection',
        ],
        duration: [
          1,
          2,
          3,
        ],
      },
      {
        date: '22.05.20',
        name: 'Friday',
        timeStart: [
          '08:30',
          '12:30',
          '08:30',
        ],
        timeEnd: [
          '08:30',
          '12:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Practic',
          'Lection',
        ],
        duration: [
          1,
          2,
          3,
        ],
      },
    ],
    [
      {
        date: '25.05.20',
        name: 'Monday',
        timeStart: [
          '12:30',
          '08:30',
        ],
        timeEnd: [
          '08:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Lection',
        ],
        duration: [
          1,
          2,
        ],
      },
      {
        date: '26.05.20',
        name: 'Thusday',
        timeStart: [
          '08:30',
          '12:30',
        ],
        timeEnd: [
          '08:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Lection',
        ],
        duration: [
          2,
          3,
        ],
      },
      {
        date: '27.05.20',
        name: 'Wensday',
        timeStart: [
          '08:30',
          '08:30',
        ],
        timeEnd: [
          '08:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Lection',
        ],
        duration: [
          1,
          2,
        ],
      },
      {
        date: '28.05.20',
        name: 'Thusday',
        timeStart: [
          '12:30',
          '08:30',
        ],
        timeEnd: [
          '08:30',
          '08:30',
        ],
        type: [
          'Lection',
          'Lection',
        ],
        duration: [
          1,
          2,
        ],
      },
      {
        date: '29.05.20',
        name: 'Friday',
        timeStart: [
          '12:30',
          '08:30',
        ],
        timeEnd: [
          '12:30',
          '08:30',
        ],
        type: [
          'Practic',
          'Lection',
        ],
        duration: [
          1,
          1,
        ],
      },
    ],
  ],
};

export const mockGroups = [
  {
    name: 'Qa',
    id: 1,
  },
  {
    name: 'Js',
    id: 2,
  },
  {
    name: 'Python',
    id: 3,
  },
];