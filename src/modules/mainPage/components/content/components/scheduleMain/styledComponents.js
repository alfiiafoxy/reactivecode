import styled from 'styled-components';

const ScPanelContainerDefault = 'flex-start';

export const ContentWrapper = styled.div`
    width: 100%; 
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row-reverse;
`;

export const ControlPanelContainer = styled.div`
    width: 30%; 
    height: 100%;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center;
    flex-direction:column;
`;

export const SchedulePanelContainer = styled.div`
    width: 70%; 
    height: 100%;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: ${props => props.schPanelContainer ? props.schPanelContainer : ScPanelContainerDefault};
    flex-direction:column;
`;