import styled from 'styled-components';

const statCaptionWrapperBgColorDefault = 'transparent';
const StatItemLastBgDefault = 'none';

export const StatisticWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const Header = styled.div`
  display: flex;
  width: 55%;
  height: 80px;
  justify-content: space-between;
  align-items: center;
  padding-left: 210px;
    padding-right: 481px;
`;

export const DropdownContainer = styled.div`
  width: 180px;
  height: 40px;
  display: flex;
`;

export const TableWrapper = styled.div`
    display: flex;
    width: 100%;
    height:100%;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    margin-top: 70px;
`;

export const Table = styled.div`
  display: flex;
  flex-direction: column;
  width: 800px;
  box-sizing: border-box;
`;

export const CaptionSection = styled.div`
  display: flex;
  height: 50px;
  font-size: 16px;
  font-weight: 400;
  box-sizing: border-box;
  border: ${props => props.captionSectionBorder ? props.captionSectionBorder : null};
  background: ${props => props.statCaptionWrapperBgColor ? props.statCaptionWrapperBgColor : statCaptionWrapperBgColorDefault};
`;
export const Caption = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.widthSize}%;
  box-sizing: border-box;
`;
export const TableBody = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
  font-weight: 500;
`;

export const StatisticRow = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  box-sizing: border-box;
  border-right: ${props => props.borderStatRow ? props.borderStatRow : null};
  border-left: ${props => props.borderStatRow ? props.borderStatRow : null};
  border-bottom: ${props => props.borderStatRow ? props.borderStatRow : null};
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

export const StatisticTotalRow = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  box-sizing: border-box;
  border-right: ${props => props.borderStatRow ? props.borderStatRow : null};
  border-left: ${props => props.borderStatRow ? props.borderStatRow : null};
  border-bottom: ${props => props.borderStatRow ? props.borderStatRow : null};
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

export const StatisricItem = styled.div`
  display: flex;
  width: ${props => props.widthSize}%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  border-right: ${props => !props.last ? `1px solid black` : null};
`;

export const StatisticItemLast = styled.div`
  display: flex;
  width: ${props => props.widthSize}%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  font-size: 16px;
  font-weight: 700;
  background: ${props => props.StatItemLastBg ? props.StatItemLastBg : StatItemLastBgDefault};
`;

export const StatisticItem = styled.div`
  display: flex;
  width: ${props => props.widthSize}%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
`;

export const PanelContainer = styled.div`
    width: 20%; 
    height: 100%;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center;
    flex-direction:column;
`;

export const PanelBlock = styled.div`
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center;
    flex-direction:column;
    
`;

export const LabelWrapper = styled.div`
    display:flex;
    align-items: center;
    align-content: center; 
    justify-content: center;
`;

export const InputWrapper = styled.div`
    min-height: 35px;
    width: auto;
    justify-content: center;
    align-items: center;
`;

export const ButtonBlock = styled.div`
    width:130px;
    height:30px;
    margin-top:15px;
    margin-bottom:10px;
    display: flex;
    flex-direction: column;
    justify-content: center;
`;