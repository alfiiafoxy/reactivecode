import React, { useCallback, useContext } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import Scrollbars from 'react-scrollbars-custom';
import { ThemeContext } from 'styled-components';
import {
    StatisticWrapper,
    Table,
    CaptionSection,
    Caption,
    TableBody,
    StatisticRow,
    StatisticItem,
    TableWrapper,
    PanelContainer,
    PanelBlock,
    LabelWrapper,
    InputWrapper,
    ButtonBlock,
    StatisticItemLast,
    StatisticTotalRow,
} from './styledComponents.js';
import CustomLabel from '../../../../../../libs/customLabel/CustomLabel.jsx';
import CustomInput from '../../../../../../libs/customInput/CustomInput.jsx';
import CustomButton from '../../../../../../libs/customButton/CustomButton.jsx';
import { getDataFromInputs } from '../../../../../../utils/helpers/getDataHelpers';
import actionTypes from '../../../../../../constans/actionTypes';

const AdminStatistic = props => {
    const themeContext = useContext(ThemeContext);
    const adminStatisticColors = themeContext.currentTheme.mainPage;
    const { dictionary, adminStatisticData, config, getSalary, teacherSalary, totalSalary, userRole } = props;

    const refAdminStatistic = {
        dateFrom: React.createRef(),
        dateTo: React.createRef(),
    };

    const getSalaryRequest = () => {
        getSalary(getDataFromInputs(refAdminStatistic));
    };
    const dispatch = useDispatch();
    const createReport = useCallback(
      () => dispatch({ type: actionTypes.DOWNLOAD_EXCEL, payload: { ...getDataFromInputs(refAdminStatistic), getFrom: 'statistic' } })
        [dispatch],
    );

    return (
        <StatisticWrapper>
            <TableWrapper>
            <Table>
                <CaptionSection
                  statCaptionWrapperBgColor={adminStatisticColors.scheduleCaptionBg}
                  captionSectionBorder={`1px solid ${adminStatisticColors.borderColor}`}
                >
                    {adminStatisticData.adminStatisticCaption.map(item => {
                        return (
                            <Caption
                                key={item.idCaption}
                                children={dictionary.resources[item.resourcesKey]}
                                widthSize={item.widthSize}
                                last={item.idCaption === 'adminSalary'}
                            />
                        );
                    })}
                </CaptionSection>
                <TableBody>
                    <Scrollbars style={{ height: '50vh', minHeight: '350px' }}>
                    {teacherSalary.length ? teacherSalary.map((item, i) => {
                        return <StatisticRow
                          key={item.teacherId}
                          borderStatRow={`1px solid ${adminStatisticColors.borderColor}`}
                        >
                            <StatisticItem
                              widthSize={5}
                              children={i + 1}/>
                            <StatisticItem
                                widthSize={19}
                                children={item.name}
                            />
                            <StatisticItem
                                widthSize={19}
                                children={item.surname}
                            />
                            <StatisticItem
                                widthSize={19}
                                children={item.teacherAttend}
                            />
                            <StatisticItem
                                widthSize={19}
                                children={item.duration}
                            />
                            <StatisticItem
                                widthSize={19}
                                children={item.salary}
                                last
                            />
                        </StatisticRow>;
                    }) : null}
                    { totalSalary.length ? totalSalary.map(item => {
                        return < StatisticTotalRow
                          key={item.sumHours}
                          borderStatRow={`1px solid ${adminStatisticColors.borderColor}`}
                        >
                            <StatisticItemLast
                              borderTop={'1px solid black'}
                              widthSize={43}
                              children={dictionary.resources.total}
                            />
                            <StatisticItemLast
                              borderTop={'1px solid black'}
                              widthSize={19}
                              children={`${item.sumAttendance}%`}
                            />
                            <StatisticItemLast
                              borderTop={'1px solid black'}
                              widthSize={19}
                              children={item.sumHours}
                            />
                            <StatisticItemLast
                              borderTop={'1px solid black'}
                              widthSize={19}
                              children={item.sumSalary}
                              last
                            />
                        </StatisticTotalRow>;
                    }) : null}
                    </Scrollbars>
                </TableBody>
            </Table>
                <PanelContainer>
                    {config.adminStatisticInputs.map((item) => {
                        return (
                          <PanelBlock key={item.idInput}>
                              <LabelWrapper key={item.idLabel}>
                              <CustomLabel
                                htmlFor={item.idInput}
                                textColor={'black'}
                                fontWeight={'normal'}
                                fontSize={'16px'}
                                labelName={dictionary.resources[item.resourcesKey]}
                              />
                              </LabelWrapper>
                              <InputWrapper key={item.idInput}>
                              <CustomInput
                                  idInput={item.idInput}
                                  inputRef={refAdminStatistic[item.inputRef]}
                                  inputType={item.inputType}
                                  requiredInput={'required'}
                                  isDisabled={false}
                                  dataAttributeInput={''}//TODO
                                  borderRadius={'none'}
                                  backgroundFocusColor={adminStatisticColors.inputBgColorFocus}
                                  focusColor={adminStatisticColors.mainTextColor}
                                  inputCursor={'pointer'}
                                  colorInput={adminStatisticColors.mainTextColor}
                                  errorMessage={''} //TODO need add error logic
                                  textColor={'transparent'}
                              />
                              </InputWrapper>
                          </PanelBlock>
                        );
                    })}
                    {config.adminStatisticButton.map((item, i) => {
                        if (userRole === 'teacher') {
                            if (i === 1) {
                                return;
                            }
                        }
                        return (
                        <ButtonBlock key={item.idButton}>
                            <CustomButton
                                buttonName={dictionary.resources[item.resourcesKey]}
                                border={'1px solid' + adminStatisticColors.borderBtm}
                                btnMargin={5}
                                borderRadius={4}
                                fontSize={20}
                                fontWeight={'normal'}
                                textTransform={'none'}
                                dataAttribute={item.dataAt}
                                isDisabled={false}//TODO
                                onClickCallback={item.onClickCallback === 'getSalaryRequest' ? getSalaryRequest : createReport} //TODO add function create excel file
                                backgroundColor={adminStatisticColors.btnBgColor}
                            />
                        </ButtonBlock>
                        );
                    })}
                </PanelContainer>
            </TableWrapper>
        </StatisticWrapper>
    );
    };

AdminStatistic.propTypes = {
    config: PropTypes.object,
    dictionary: PropTypes.object,
    adminStatisticData: PropTypes.object,
    adminStatistic: PropTypes.object,
    getSalary: PropTypes.func,
    teacherSalary: PropTypes.array,
    totalSalary: PropTypes.array,
    userRole: PropTypes.string,
};

export default React.memo(AdminStatistic);
