export const mockAdminStatistic = {
    statistic: [
      {
        name: 'Kate',
        surname: 'Johnson',
        attendance: '90%',
        hours: '150',
        salary: '1500$',
      },
      {
        name: 'Kate',
        surname: 'Johnson',
        attendance: '90%',
        hours: '150',
        salary: '1500$',
      },
      {
        name: 'Kate',
        surname: 'Johnson',
        attendance: '90%',
        hours: '150',
        salary: '1500$',
      },
      {
        name: 'Kate',
        surname: 'Johnson',
        attendance: '90%',
        hours: '150',
        salary: '1500$',
      },
      {
        name: 'Kate',
        surname: 'Johnson',
        attendance: '90%',
        hours: '150',
        salary: '1500$',
      },
      {
        name: 'Kate',
        surname: 'Johnson',
        attendance: '90%',
        hours: '150',
        salary: '1500$',
      },
    ],
  total: [
    {
      title: 'Total :',
      attendance: '90%',
      hours: '900',
      salary: '9000$',
    },
  ],
};