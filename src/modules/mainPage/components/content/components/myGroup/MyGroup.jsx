import PropTypes from 'prop-types';
import React, { useCallback, useContext, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Scrollbar from 'react-scrollbars-custom';
import { ThemeContext } from 'styled-components';
import { MainPageContext } from '@/mainPage/MainPage';
import {
  GroupElementContainer,
  MyGroupWrapper,
  GroupElement,
  MyGroupContentRow,
  MyGroupContentTable,
  MyGroupTableWrapper,
  Header,
  DropdownContainer,
  DeleteStudentButton,
  ButtonContainer,
  TeacherContainer,
  TeacherNameElement,
  TeacherButtonContainer,
} from './styledComponents.js';
import CustomDropdown from '../../../../../../libs/CustomDropdown/CustomDropdown.jsx';
import actionTypes from '../../../../../../constans/actionTypes';
import CustomButton from '../../../../../../libs/customButton/CustomButton.jsx';

const MyGroup = props => {
  const contextData = useContext(MainPageContext);
  const { myGroupData, dictionary, students } = contextData;
  const themeContext = useContext(ThemeContext);
  const mainColors = themeContext.currentTheme.mainPage;
  const { config, groupsData, selectedGroup, userRole, toggleModalWindow, attendance, teacherName } = props;

  const dispatch = useDispatch();
  const dropDownRef = React.createRef();

  const changeCurrentGroup = useCallback(
    value => dispatch({ type: actionTypes.SET_SELECTED_GROUP, payload: +value })
      [dispatch],
  );

  const [rowNumber, setRowNumber] = useState(0);

  useEffect(() => {
    if (students.allStudents.length) {
      setRowNumber(students.allStudents[selectedGroup].length);
    }
  });

  const toggleModalWindowGroups = value => {
    toggleModalWindow({ type: value.type, data: value.data });
  };

  const renderActionButtons = () => {
    if (userRole === 'student') {
      return null;
    }
    return userRole ? myGroupData.actionsButton[userRole].map(btn => {
      return (
        <ButtonContainer key={btn.id}>
          <CustomButton
            dataAttribute={btn.dataAt}
            buttonName={dictionary.resources[btn.name]}
            backgroundColor={mainColors.btnAdminGroup}
            borderRadius={4}
            isDisabled={!groupsData.length && btn.id === 'deleteGroup'}
            border={`1px solid ${mainColors.bgBorderButtonAddGroup}`}
            backgroundColorHover={ mainColors.bgHoverButtonAddGroupAdmin}
            onClickCallback={() => toggleModalWindowGroups({ type: btn.type, data: { callbackValue: btn.callbackValue } })}
            fontSize={14}
            textTransform={'none'}
            focusColor={mainColors.dropDownSelectTextColor}
            textColor={ btn.name === 'deleteGroup' ? mainColors.btnAdminGroupDelete : mainColors.btnTextAdminGroup}
            fontWeight={'bold'}
          />
        </ButtonContainer>
      );
    }) : null;
  };

  return (
    <MyGroupWrapper>
      <Header>
        <DropdownContainer>
          <CustomDropdown
            onClickCallback={() => changeCurrentGroup(dropDownRef.current.value)}
            content={groupsData}
            focusColor={mainColors.dropDownSelectTextColor}
            dropdownRef={dropDownRef}
            dataAttributeDropdown={config.dataAttributes.dataAttributeGroupDropdown}
          />
        </DropdownContainer>
        <TeacherContainer widthSchGroupElem={'20%'}>
          <TeacherNameElement
            children={groupsData.length ? `${dictionary.resources.teacher}: ${teacherName}` : null}
            colorSchElem={mainColors.mainTextColor}
            fontSizeSchElem={'20px'}
          />
          {userRole === 'admin' ?
            <TeacherButtonContainer>
              <CustomButton backgroundImage={config.srcButtonDeleteStudents}
                            onClickCallback={() => toggleModalWindowGroups({
                              type: 'confirmModal',
                              data: { callbackValue: 'deleteTeacherFromGroup' },
                            })}/>
            </TeacherButtonContainer> : null
          }
        </TeacherContainer>
        {renderActionButtons()}
      </Header>
      <MyGroupTableWrapper>
        <GroupElementContainer
          groupElContainerBg={mainColors.scheduleCaptionBg}
          groupElContainerBorder={mainColors.borderColor}>
          {myGroupData[userRole !== 'student' ? 'myGroupCaption' : 'myGroupCaptionStudent'].map((item, index) => {
            return (
              <GroupElement
                key={index}
                last={item.idCaption === 'deleteButton'}
                widthSize={item.widthSize}
                children={dictionary.resources[item.resourcesKey]}
                groupElBorder={`1px solid ${mainColors.borderColor}`}
              />
            );
          })}
        </GroupElementContainer>
        <MyGroupContentTable
          marginMinus={rowNumber > 15 ? '-10px' : '0'}
        >
          <Scrollbar
            style={{ height: 400, boxSizing: 'border-box', marginRight: '-20px' }}
          >
            {students.allStudents[selectedGroup] ? students.allStudents[selectedGroup].map((item, index) => {
              let attendanceStudent = attendance && attendance[selectedGroup] ? attendance[selectedGroup].find(el => item.id === el.student_id) : null;
              return (
                <MyGroupContentRow key={index} borderMyGroupRow={`1px solid ${mainColors.borderColor}`}>
                  <GroupElement widthSize={5} children={index + 1}/>
                  <GroupElement widthSize={15} children={item.first_name}/>
                  <GroupElement widthSize={15} children={item.last_name}/>
                  <GroupElement widthSize={15} children={item.phone}/>
                  <GroupElement widthSize={userRole !== 'student' ? 15 : 20} children={item.email}/>
                  <GroupElement widthSize={15} children={item.birth_date}/>
                  <GroupElement widthSize={15} children={`${attendanceStudent ? attendanceStudent.statistic : ''}%`}/>
                  {userRole !== 'student' ? <GroupElement last widthSize={5}>
                    <DeleteStudentButton
                      data-at={config.dataAttributes.dataAttributeLink.dataAttributeForgotPasswordClose}
                    >
                      <CustomButton
                        backgroundImage={config.srcButtonDeleteStudents}
                        dataAttribute={config.dataAttributes.dataAttributeDeleteStudentFromGroup}
                        onClickCallback={() => toggleModalWindowGroups({
                          type: 'confirmModal',
                          data: {
                            callbackValue: 'deleteStudentFromGroup',
                            attribute: { user_id: item.id },
                          },
                        })}
                      />
                    </DeleteStudentButton>
                  </GroupElement> : null}
                </MyGroupContentRow>
              );
            }) : null}
          </Scrollbar>
        </MyGroupContentTable>
      </MyGroupTableWrapper>
    </MyGroupWrapper>
  );
};

MyGroup.propTypes = {
  config: PropTypes.object.isRequired,
  groupsData: PropTypes.array,
  selectedGroup: PropTypes.number,
  userRole: PropTypes.string,
  toggleModalWindow: PropTypes.func,
  attendance: PropTypes.object,
  teacherName: PropTypes.string,
};

export default React.memo(MyGroup);
