import styled from 'styled-components';

const widthGroupElemDefault = '16%';
const borderGroupElemDefault = 'none';
const borderGroupSpanDefault = 'none';
const colorGroupElemDefault = '#000000';
const bgGroupContentRow = 'transparent';
const bgGroupContentRowDefault = 'none';
const groupElementBgDefault = 'transparent';
const marginGroupColumnDefault = '5px 3px';
const borderContentGroupWrapperDefault = 'none';
const groupContentWrapperDefault = 'transparent';
const groupWrapperBgColorDefault = 'transparent';
const borderGroupRowDefault = '1px solid #000000';
const borderGroupWrapperDefault = '1px solid #000000';
const groupCaptionWrapperBgColorDefault = 'transparent';
const borderRightGroupElemDefault = '1px solid #000000';
const borderGroupCaptionWrapperDefault = '1px solid #000000';
const fontSizeTeacherNameElementDefault = '16px';
const groupElContainerBgDefault = 'transparent';
const borderMyGroupRowDefault = 'none';

export const MyGroupWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const Header = styled.div`
  display: flex;
  width: 85%;
  height: 80px;
  justify-content: space-between;
  align-items: center;
`;

export const DropdownContainer = styled.div`
  width: 180px;
  height: 40px;
  display: flex;
`;

export const ButtonContainer = styled.div`
  width: 120px;
  height: 40px;
  display: flex;
`;

export const SearchContainer = styled.div`
  width: 40px;
  display: flex;
  height: 40px;
  justify-content: space-between;
  border-radius: 20px;
  align-items: center;
  background: darkgray;
  overflow: hidden;
  transition: all 350ms ease-in-out;
  outline: none;
  
  &:focus-within {
    width: 500px;
    transition: all 350ms ease-in-out;
  }
  
     &:focus-within input {
      width: 410px;
      margin-left: 5px;
      opacity: 1;
      transition: all 350ms ease-in-out;
      padding-left: 10px;
      outline: none;
      box-sizing: border-box;
  }
  
       &:focus-within #search {
      transform: rotate(360deg);
      transition: all 350ms ease-in-out;
      
      &:hover {
       background-color: #3c3c3c;
     
      }
       &:active {
       background-color: rgb(255,255,255);
       }
  }
  
        &:focus-within #add {
      width: 40px;
      height: 40px;
      opacity: 0.8;
      transform: rotate(180deg);
      transition: all 300ms ease-in-out;
      
      &:hover {
       background-color: #3c3c3c;
     
      }
       &:active {
       background-color: rgb(255,255,255);
       }
  }
  
`;

export const InputSearch = styled.input`
  width: 0;
  height: 30px;
  border-radius: 15px;
  border: none;
  display: flex;
  justify-content: flex-end;
  margin-left: 0;
  opacity: 0;
  transition: all 350ms ease-in-out;
  outline: none;
`;

export const SearchButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  z-index: 1;
  background-color: darkgray;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  background-repeat: no-repeat;
  background-size: 75% 75%;
  background-position: center;
  border-radius: 50%;
  overflow: hidden;
  transition: all 350ms ease-in-out;
  transform: rotate(0);
`;

export const AddStudentButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 0;
  height: 0;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  background-color: darkgray;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  background-repeat: no-repeat;
  background-position: center;
  border-radius: 50%;
  overflow: hidden;
  transition: all 300ms ease-in-out;
  transform: rotate(0);
  
  &:hover {
  opacity: 1;
  }
`;

export const MyGroupTableWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 85%;
`;

export const MyGroupContentTable = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
  font-weight: 500;
  margin-right: ${props => props.marginMinus ? props.marginMinus : '0'};;
`;

export const MyGroupContentRow = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  box-sizing: border-box;
  border-right: ${props => props.borderMyGroupRow ? props.borderMyGroupRow : borderMyGroupRowDefault};
  border-left: ${props => props.borderMyGroupRow ? props.borderMyGroupRow : borderMyGroupRowDefault};
  border-bottom: ${props => props.borderMyGroupRow ? props.borderMyGroupRow : borderMyGroupRowDefault};
  
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

export const GroupElementContainer = styled.div`
  display: flex;
  height: 50px;
  font-size: 16px;
  font-weight: 400;
  box-sizing: border-box;
  border: ${props => props.groupElContainerBorder ? props.groupElContainerBorder : null};
  background-color: ${props => props.groupElContainerBg ? props.groupElContainerBg : groupElContainerBgDefault};
`;

export const GroupElement = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.widthSize}%;
  box-sizing: border-box;
  
`;

export const DeleteStudentButton = styled.div`
  width: 28px;
  height: 28px;
  border: none;
  cursor: pointer;
  opacity: 0.55;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  
  &:hover {
    opacity: 0.8;
  }
`;

export const TeacherContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  width: ${props => props.widthSize}%;
  box-sizing: border-box;
`;

export const TeacherNameElement = styled.span`
  width: 100%;
  margin: 5px 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${props => props.shElementBg ? props.shElementBg : groupElementBgDefault}; 
  border: ${props => props.borderSchElem ? props.borderSchElem : borderContentGroupWrapperDefault};
  color: ${props => props.colorSchElem ? props.colorSchElem : colorGroupElemDefault};
  font-size: ${props => props.fontSizeSchElem ? props.fontSizeSchElem : fontSizeTeacherNameElementDefault};;
`;
export const TeacherButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 28px;
  height: 28px;
  margin-top: 3px;
  box-sizing: border-box;
`;

export const defaultStyles = {
  bgGroupContentRow,
  groupElementBgDefault,
  widthGroupElemDefault,
  colorGroupElemDefault,
  borderGroupRowDefault,
  borderGroupElemDefault,
  borderGroupSpanDefault,
  marginGroupColumnDefault,
  bgGroupContentRowDefault,
  borderGroupWrapperDefault,
  groupContentWrapperDefault,
  groupWrapperBgColorDefault,
  borderRightGroupElemDefault,
  borderContentGroupWrapperDefault,
  borderGroupCaptionWrapperDefault,
  groupCaptionWrapperBgColorDefault,
  TeacherContainer,
  TeacherNameElement,
};

