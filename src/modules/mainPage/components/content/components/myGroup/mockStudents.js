export const mockStudents = {
  students: [
    {
      firstName: 'Vasa',
      lastName: 'Gasa',
      phone: '+380671305050',
      email: 'vasa@gmail.com',
      dataOFBirth: '18.03.2000',
      attendance: '10%',
    },
    {
      firstName: 'Dasa',
      lastName: 'Panasa',
      phone: '+380671305050',
      email: 'dasa@gmail.com',
      dataOFBirth: '18.03.2000',
      attendance: '10%',
    },
    {
      firstName: 'Bread',
      lastName: 'Peat',
      phone: '+380671305050',
      email: 'bread@gmail.com',
      dataOFBirth: '18.03.2000',
      attendance: '10%',
    },
  ],
};