import React, { useCallback, useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import {
    StatisticWrapper,
    Header,
    DropdownContainer,
    ToggleContainer,
    Table,
    CaptionSection,
    Caption,
    TableBody,
    StatisticRow,
    StatisricItem,
    StatisticTotalRow,
    StatisticItemLast,
} from './styledComponent';
import CustomDropdown from '../../../../../../libs/CustomDropdown/CustomDropdown';
import { useDispatch } from 'react-redux';
import actionTypes from '../../../../../../constans/actionTypes';

const Statistic = props => {
    const { config, statistic, dictionary, groupsData } = props;
    const themeContext = useContext(ThemeContext);
    const statisticColor = themeContext.currentTheme.mainPage;

    const dispatch = useDispatch();
    const dropDownRef = React.createRef();

    const changeCurrentGroup = useCallback(
        value => dispatch({ type: actionTypes.SET_SELECTED_GROUP, payload: +value })
            [dispatch],
    );

    return (
        <StatisticWrapper>
            <Header>
                <DropdownContainer>
                    <CustomDropdown
                    onClickCallback={() => changeCurrentGroup(dropDownRef.current.value)}
                    content={groupsData}
                    focusColor={statisticColor.dropDownSelectTextColor}
                    dropdownRef={dropDownRef}
                    />
                </DropdownContainer>
                <ToggleContainer>
                    <div onClick={null} />
                    <div>{`< 30.03.20 - 03.04.20 >`}</div>
                    <div onClick={null} />
                </ToggleContainer>
            </Header>
            <Table>
                <CaptionSection statCaptionWrapperBgColor={statisticColor.statisticCaptionBg}>
                    {config.statisticData.map((item) => {
                        return (
                            <Caption
                            key={item.idCaption}
                            children={dictionary.resources[item.resourcesKey]}
                            widthSize={item.widthSize}
                            last={item.idCaption === 'salary'}
                            />
                        );
                    })}
                </CaptionSection>
                <TableBody>
                    {statistic.statistic.map(item => {
                        return <StatisticRow key={item.group} >
                                <StatisricItem
                                    widthSize={60}
                                    children={item.group}
                                />
                                <StatisricItem
                                    widthSize={20}
                                    children={item.hours}
                                />
                                <StatisricItem
                                    widthSize={20}
                                    children={item.salary}
                                    last
                                />
                                </StatisticRow>;
                    })}
                    {statistic.total.map(item => {
                        return < StatisticTotalRow key={item.title} >
                            <StatisticItemLast
                                widthSize={60}
                                children={item.title}
                            />
                            <StatisticItemLast
                                widthSize={20}
                                children={item.hours}
                            />
                            <StatisticItemLast
                                widthSize={20}
                                children={item.salary}
                                last
                            />
                        </StatisticTotalRow>;
                    })}
                </TableBody>
            </Table>
        </StatisticWrapper>
    );
};

Statistic.propTypes = {
  config: PropTypes.object,
  dictionary: PropTypes.object,
  statistic: PropTypes.object,
  groupsData: PropTypes.array,
};

export default React.memo(Statistic);