export const mockStatistic = {
statistic: [
    {
    idGroup: 'group1',
    group: 'Group 1',
    hours: '20',
    salary: '300$',
    },
    {
    idGroup: 'group2',
    group: 'Group 2',
    hours: '30',
    salary: '400$',
    },
    {
     group: 'Group 3',
     hours: '20',
     salary: '300$',
     },
     {
     group: 'Group 4',
     hours: '30',
     salary: '400$',
     },
],
total: [
    {
    title: 'Total :',
    hours: '200',
    salary: '1400$',
    },
],
dropdown: [
    {
    id: 1,
    name: 'Qa',
    },
    {
    id: 2,
    name: 'JS',
    },
    {
    id: 3,
    name: 'Python',
    },
],
};