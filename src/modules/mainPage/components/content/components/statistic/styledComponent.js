import styled from 'styled-components';

const statCaptionWrapperBgColorDefault = 'transparent';
const borderStatCaptionWrapperDefault = '1px solid #000000';


export const StatisticWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const StatisricItem = styled.div`
  display: flex;
  width: ${props => props.widthSize}%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  border-right: ${props => !props.last ? `1px solid black` : null};
`;

export const StatisticItemLast = styled.div`
  display: flex;
  width: ${props => props.widthSize}%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  font-size: 16px;
  font-weight: 700;
  background: ${props => props.StatItemLastBg ? props.StatItemLastBg : null};
`;

export const Header = styled.div`
  display: flex;
  width: 800px;
  height: 80px;
  justify-content: space-between;
  align-items: center;
`;

export const DropdownContainer = styled.div`
  width: 180px;
  height: 40px;
  display: flex;
`;

export const ToggleContainer = styled.div`
  width: 250px;
  display: flex;
  justify-content: flex-end;
`;

export const Table = styled.div`
  display: flex;
  flex-direction: column;
  width: 800px;
  box-sizing: border-box;
  border: 1px solid black;
`;

export const CaptionSection = styled.div`
  display: flex;
  height: 50px;
  font-size: 16px;
  font-weight: 700;
  box-sizing: border-box;
  background: ${props => props.statCaptionWrapperBgColor ? props.statCaptionWrapperBgColor : statCaptionWrapperBgColorDefault};
  border: ${props => props.borderStatCaptionWrapper ? props.borderStatCaptionWrapper : borderStatCaptionWrapperDefault};border-bottom: 1px solid black;
`;
export const Caption = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.widthSize}%;
  box-sizing: border-box;
  border-right: ${props => !props.last ? `1px solid black` : null};
`;
export const TableBody = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
  font-weight: 500;
`;

export const StatisticRow = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

export const StatisticTotalRow = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

