import styled from 'styled-components';

export const Table = styled.div`
  display: flex;
  flex-direction: column;
  width: 85%;
  box-sizing: border-box;
`;

export const CaptionSection = styled.div`
  width: 100%;
  display: flex;
  height: 50px;
  font-size: 16px;
  font-weight: 700;
  box-sizing: border-box;
  border: ${props => props.captionSectionBorder ? props.captionSectionBorder : null};
  background-color: ${props => props.captionSectionBg ? props.captionSectionBg : null};
`;

export const Caption = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.widthSize}%;
  min-width: ${props => props.first ? null : '75px'};
  box-sizing: border-box;
`;

export const TableBody = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  font-size: 14px;
  font-weight: 500;
`;

export const AttendanceRow = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  box-sizing: border-box;
  border-right: ${props => props.borderAttendanceRow ? props.borderAttendanceRow : null};
  border-left: ${props => props.borderAttendanceRow ? props.borderAttendanceRow : null};
  border-bottom: ${props => props.borderAttendanceRow ? props.borderAttendanceRow : null};
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

export const AttendanceItem = styled.div`
  display: flex;
  width: ${props => props.widthSize}%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  text-align: center;
  min-width: ${props => props.first ? null : '75px'};
`;

export const Header = styled.div`
  display: flex;
  width: 85%;
  height: 80px;
  justify-content: space-between;
  align-items: center;
`;

export const DropdownContainer = styled.div`
  width: 180px;
  height: 40px;
  display: flex;
`;

export const ToggleContainer = styled.div`
  width: 353px;
  display: flex;
  justify-content: flex-end;
`;

export const AttendanceWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const CheckBox = styled.input`
  &:hover {
    cursor: pointer;
  }
`;

export const ButtonWrapper = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex: 1;
    justify-content: flex-end; 
`;

export const ButtonContainer = styled.div`
    width: 100px;
    height: 40px;
    padding-right:10px;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;
    justify-content: center; 
`;
