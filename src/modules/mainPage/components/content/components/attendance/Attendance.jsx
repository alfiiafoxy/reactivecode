import React, { useCallback, useContext } from 'react';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import PropTypes from 'prop-types';
import Scrollbar from 'react-scrollbars-custom';
import { ThemeContext } from 'styled-components';
import { MainPageContext } from '@/mainPage/MainPage.jsx';
import {
  Table,
  Header,
  Caption,
  CheckBox,
  TableBody,
  AttendanceRow,
  AttendanceItem,
  CaptionSection,
  ToggleContainer,
  AttendanceWrapper,
  DropdownContainer,
  ButtonContainer,
  ButtonWrapper,
} from './styledComponents';
import CustomDropdown from '../../../../../../libs/CustomDropdown/CustomDropdown.jsx';
import CustomButton from '../../../../../../libs/customButton/CustomButton';
import actionTypes from '../../../../../../constans/actionTypes';
import FilterDateBlock from '../../../../../../libs/FilterDateBlock/FilterDateBlock';

const Attendance = props => {
  const contextData = useContext(MainPageContext);
  const { config } = contextData;
  const themeContext = useContext(ThemeContext);
  const attendanceColors = themeContext.currentTheme.mainPage;
  const {
    attendance,
    dictionary,
    isChangeButton,
    changeButton,
    groupsData,
    selectedGroup,
    userRole,
    daysOfLessons,
    firstSelectWeekDay,
    lastSelectWeekDay,
    firstSelectWeekDayMilliseconds,
    lastSelectWeekDayMilliseconds,
  } = props;
  const dispatch = useDispatch();

  const dropDownRef = React.createRef();

  const changeCurrentGroup = useCallback(
    value => dispatch({ type: actionTypes.SET_SELECTED_GROUP, payload: +value })
      [dispatch],
  );
  const setAttendance = useCallback(
    value => dispatch({ type: actionTypes.SET_ATTENDANCE, payload: value })
      [dispatch],
  );

  const getSelectCheckMilliseconds = date => {
    const dateSecondsLesson = moment(date, 'DD.MM.YYYY').format('X');
    return dateSecondsLesson >= firstSelectWeekDayMilliseconds && dateSecondsLesson <= lastSelectWeekDayMilliseconds;
  };

  const renderAttendanceButton = () => {
    return (
      < ButtonContainer>
        <CustomButton
          key={isChangeButton ? 'change' : 'save'}
          fontSize={20}
          dataAttribute={''}//TODO
          borderRadius={15}
          fontWeight={'normal'}
          textTransform={'none'}
          isDisabled={false}
          onClickCallback={changeButton}
          backgroundColor={attendanceColors.btnBgColor}
          border={'1px solid' + attendanceColors.borderBtm}
          buttonName={isChangeButton ? dictionary.resources.change : dictionary.resources.save}
          btnMargin={5}
        />
      </ButtonContainer>
    );
  };
  return (
    <AttendanceWrapper>
      <Header>
        <DropdownContainer>
          <CustomDropdown
            onClickCallback={() => changeCurrentGroup(dropDownRef.current.value)}
            content={groupsData}
            focusColor={attendanceColors.dropDownSelectTextColor}
            dropdownRef={dropDownRef}
          />
        </DropdownContainer>
        <ToggleContainer>
          <FilterDateBlock
            config={config}
            firstSelectWeekDay={firstSelectWeekDay}
            lastSelectWeekDay={lastSelectWeekDay}
            colorInput={attendanceColors.mainTextColor}
            errorMessage={''} //TODO need add error logic
            textColor={'transparent'}
          />
        </ToggleContainer>
      </Header>
      <Table>
        <Scrollbar
          style={{ width: '100%', height: 600, boxSizing: 'border-box', marginRight: '-20px' }}
          permanentTrackX
        >
          <CaptionSection
            captionSectionBg={attendanceColors.scheduleCaptionBg}
            captionSectionBorder={attendanceColors.borderColor}
          >
            <Caption widthSize={5} children={'#'} first/>
            <Caption widthSize={25} children={'Student/Data'}/>
            {daysOfLessons && daysOfLessons.hasOwnProperty(selectedGroup) ? daysOfLessons[selectedGroup][0].map(el => {
              if (!getSelectCheckMilliseconds(el.date)) {
                return;
              }
              return <Caption widthSize={15} key={el.id} children={el.date}/>;
            }) : null}
            <Caption last widthSize={20} children={'Attendance (%)'}/>
          </CaptionSection>
          <TableBody>
            {attendance && attendance.hasOwnProperty(selectedGroup) ? attendance[selectedGroup].map((student, i) => {
              return <AttendanceRow
                key={student.student_id}
                borderAttendanceRow={`1px solid ${attendanceColors.borderColor}`}
              >
                <AttendanceItem widthSize={5} first>{i + 1}</AttendanceItem>
                <AttendanceItem widthSize={25}>{student.student_name}</AttendanceItem>
                {daysOfLessons ? daysOfLessons[selectedGroup][0].map(el => {
                  if (!getSelectCheckMilliseconds(el.date)) {
                    return;
                  }
                  return student.lessons.some(lesson => lesson.lesson_id === el.id) ?
                    student.lessons.map(lesson => {
                      if (lesson.lesson_id === el.id) {
                        return <AttendanceItem key={lesson.attendance_id} widthSize={15}>
                          <CheckBox onChange={() => {
                            setAttendance({ attendance_id: lesson.attendance_id, attendancePayload: !lesson.attendance, studentId: student.student_id });
                          }} type='checkbox' value={lesson.attendance_id} defaultChecked={lesson.attendance} disabled={isChangeButton}/>
                        </AttendanceItem>;
                      }
                    })
                    :
                    <AttendanceItem key={Math.random()} widthSize={15}>
                      {dictionary.message.wasntInAGroup}
                    </AttendanceItem>;
                }) : null}
                <AttendanceItem last widthSize={20}>{student.statistic}%</AttendanceItem>
              </AttendanceRow>;
            }) : null}
          </TableBody>
          {userRole !== 'student' ?
            <ButtonWrapper>
              {renderAttendanceButton()}
            </ButtonWrapper>
            : null}
        </Scrollbar>
      </Table>
    </AttendanceWrapper>
  );
};

Attendance.propTypes = {
  attendance: PropTypes.object,
  config: PropTypes.object,
  dictionary: PropTypes.object,
  isChangeButton: PropTypes.bool,
  changeButton: PropTypes.func,
  selectedGroup: PropTypes.number,
  groupsData: PropTypes.array,
  userRole: PropTypes.string,
  daysOfLessons: PropTypes.object,
  lastSelectWeekDay: PropTypes.number,
  firstSelectWeekDay: PropTypes.number,
  lastSelectWeekDayMilliseconds: PropTypes.string,
  firstSelectWeekDayMilliseconds: PropTypes.string,
};

export default React.memo(Attendance);
