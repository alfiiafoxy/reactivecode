import styled from 'styled-components';

export const ContentWrapper = styled.div`
    width: 100%; 
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row-reverse;
`;

export const ControlPanelContainer = styled.div`
    width: 20%; 
    height: 100%;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center;
    flex-direction:column;
`;

export const SchedulePanelContainer = styled.div`
    width: 80%; 
    height: 100%;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center;
    flex-direction:column;
`;