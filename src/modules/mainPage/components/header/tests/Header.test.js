import React from 'react';
import HeaderMemo from '../Header.jsx';
import button from '../../../../../config/buttons.js';
import dictionary from '../../../../../managers/localesManager/dictionary/en.js';
import lightTheme from '../../../../../managers/themeManager/theme/lightTheme.js';

const Header = HeaderMemo.type;

describe('Header react components', () => {
  const props = {
    config: {
      srcButtonSettings: 'url(\'./assets/settings.png\') no-repeat',
      dataAttributes: {
        dataAttributeButton: {
          myAccount: 'at-student-myprofile-button',
          logOut: 'at-student-logout-button',
          settings: 'at-student-settings-button',
        },
      },
      headerButtons: button.headerButtons,
    },
    dictionary: dictionary,
    theme: lightTheme,
  };

  it('snapshot created, should rendered correctly', done => {
    const wrapper = shallow(<Header {...props}/>);

    expect(wrapper).matchSnapshot();

    done();
  });
});
