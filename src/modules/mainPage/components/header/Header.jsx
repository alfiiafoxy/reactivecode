import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';
import {
  HeaderWrapper,
  HeaderContentLogo,
  HeaderContainerLogo,
  HeaderContentContainer,
  HeaderContentToggleButton,
  HeaderToggleButtonContainer,
  HeaderContentNavigationButton } from './styledComponent';
import Logo from '../../../authRegPage/header/components/logo/Logo.jsx';
import { useDispatch } from 'react-redux';
import actionTypes from '../../../../constans/actionTypes';

const Header = (props) => {
  const {
    theme,
    config,
    history,
    userRole,
    dictionary,
    isMyAccount,
    contentName,
    changeContentName,
    clickingNavigationButton,
    firstSelectWeekDay,
    lastSelectWeekDay,
  } = props;

 const getArrayContentButtons = () => {
    switch (userRole) {
      case 'student' :
        return config.contentButtons.studentRole;
      case 'teacher' :
        return config.contentButtons.teacherRole;
      case 'admin' :
        return config.contentButtons.adminRole;
      default :
        return config.contentButtons.studentRole;
    }
  };

  const getArrayPanelButtons = () => {
    return !isMyAccount ? config.headerButtons.headerMainButtons : config.headerButtons.headerMyAccountButtons;
  };

  const dispatch = useDispatch();

  const getLastWeek = useCallback(
    () => dispatch({ type: actionTypes.GET_PREV_WEEK }),
    [firstSelectWeekDay, lastSelectWeekDay],
  );

  const changeContent = name => {
    if (name === 'schedule' || name === 'attendance') {
      getLastWeek();
    }
    changeContentName(name);
  };

  return (
    <HeaderWrapper>
      <HeaderContainerLogo>
        <HeaderContentLogo>
          <Logo config={config}/>
        </HeaderContentLogo>
      </HeaderContainerLogo>
      <HeaderToggleButtonContainer>
        {!isMyAccount ? getArrayContentButtons().map(buttonName =>
          <HeaderContentToggleButton key={Math.random()}>
            <CustomButton
              key={Math.random()}
              buttonName={userRole === 'teacher' && buttonName === 'teachersStatistic' ? dictionary.resources.statistic : dictionary.resources[buttonName]}
              onClickCallback={() => changeContent(buttonName)}
              fontSize={18}
              backgroundColor={contentName === buttonName ? theme.buttonToggleContentPageBgHover : theme.bgButtonAddGroup }
              backgroundColorHover={theme.bgHoverButtonAddGroup}
              dataAttribute={config.dataAttributes.dataAttributeButton[buttonName]}
              borderRadius={4}
              textTransform={'none'}
              border={`1px solid ${theme.buttonToggleContentPageBgHover}`}
              textColor={theme.btnText}
              fontWeight={'none'}
            />
          </HeaderContentToggleButton>) : null}
      </HeaderToggleButtonContainer>
      <HeaderContentContainer>
        {getArrayPanelButtons().map(button =>
          <HeaderContentNavigationButton key={button.name}>
            <CustomButton
              key={button.name}
              onClickCallback={() => clickingNavigationButton({ name: button.name, history })}
              backgroundImage={button.srcButton}
              backgroundColorHover={button.srcButton}
              dataAttribute={config.dataAttributes.dataAttributeButton[button.idButton]}
            />
          </HeaderContentNavigationButton>)}
      </HeaderContentContainer>
    </HeaderWrapper>
  );
};

Header.propTypes = {
  clickingNavigationButton: PropTypes.func,
  changeContentName: PropTypes.func,
  config: PropTypes.object,
  dictionary: PropTypes.object,
  contentName: PropTypes.string,
  userRole: PropTypes.string,
  history: PropTypes.object,
  theme: PropTypes.object,
  isMyAccount: PropTypes.bool,
  firstSelectWeekDay: PropTypes.number,
  lastSelectWeekDay: PropTypes.number,
};

export default React.memo(Header);
