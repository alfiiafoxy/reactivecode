import styled from 'styled-components';

const buttonSettingsHeaderSize = 50;
const defaultSizeWrapper = 100;
const minWidthWrapper = 1366;

export const HeaderWrapper = styled.div`
   width: ${defaultSizeWrapper}%;
   min-width: ${minWidthWrapper}px;
   height: 60px;
   display: flex;  
   justify-content: space-between;   
   align-items: center;
`;

export const HeaderContainerLogo = styled.div`
   height: ${defaultSizeWrapper}%;
   display: flex; 
   flex: 1;
   align-items: center;
   justify-content: space-between;
`;

export const HeaderToggleButtonContainer = styled.div`
   height: ${defaultSizeWrapper}%;
   display: flex; 
   flex: 2;
   align-items: center;
   justify-content: center;
`;

export const HeaderContentLogo = styled.div`
   width: 340px;
   height: 40px;
   display: flex; 
   align-items: center;
   justify-content: center;
   margin-left: 30px;
`;

export const HeaderContentContainer = styled.div`
   height: ${defaultSizeWrapper}%;
   display: flex;
   flex: 1;
   align-items: center;
   justify-content: flex-end;
`;

export const HeaderContentNavigationButton = styled.div`
   width: ${buttonSettingsHeaderSize}px;
   height: ${buttonSettingsHeaderSize}px;
   display: flex;
   align-items: center;
   justify-content: center;
   margin-right: 30px;
   opacity: 1;
`;

export const HeaderContentToggleButton = styled.div`
   height: 30px;
   display: flex;
   align-items: center;
   justify-content: center;
   padding: 0 20px;
   box-sizing: border-box;   
   width: 175px;
`;
