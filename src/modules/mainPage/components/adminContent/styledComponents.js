import styled from 'styled-components';

export const AdminWrapper = styled.div`
    width: 100%; 
    height: 100%;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center;
    flex-direction:column;
`;
