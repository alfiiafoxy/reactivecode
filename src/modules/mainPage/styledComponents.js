import styled from 'styled-components';

export const MainPageContainer = styled.div`
    width:100%;
    height: 100%;
    display: flex;
    position: fixed;
    flex-direction: column; 
    align-content: center;  
`;

export const MainPageWrapper = styled.div`
    width:100%;
    height: 100%;
    display: flex;
    flex-direction: column; 
    align-items: center;  
    background: ${props => props.mainBgColor ? props.mainBgColor : '#ffffff'}
`;