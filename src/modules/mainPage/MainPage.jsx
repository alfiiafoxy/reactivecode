import React from 'react';
import PropTypes from 'prop-types';
import Header from '@/mainPage/components/header/Header.jsx';
import Content from '@/mainPage/components/content/Content.jsx';
import { MainPageWrapper, MainPageContainer } from './styledComponents.js';
import AccountPage from '@/mainPage/components/account/AccountMain.jsx';

export const MainPageContext = React.createContext();

class MainPage extends React.PureComponent {
  constructor(props) {
    super(props);
    const { dateFrom, dateTo } = props;
    props.checkTokenLS();
    props.getGroupsData();
    props.getSchedule();
    props.getStudents();
    props.getAttendance();
    props.getUsersForAdmin();
    props.getUserData();
    props.getSalary({ dateFrom, dateTo, getFrom: 'main' });
  }

  render() {
    const {
      history,
      config,
      theme,
      attendance,
      statistic,
      scheduleData,
      lessons,
      dictionary,
      myGroupData,
      students,
      changeScheduleWeek,
      schedule,
      clickingNavigationButton,
      contentName,
      changeContentName,
      addLesson,
      isChangeButton,
      changeButton,
      currentLesson,
      userRole,
      groupsData,
      currentInputUsers,
      changeInputUser,
      selectedGroup,
      users,
      toggleModalWindow,
      adminStatisticData,
      adminStatistic,
      isAccountButton,
      changeAccountButton,
      isMyAccount,
      teacherName,
      setSelectedGroup,
      daysOfLessons,
      firstSelectWeekDay,
      lastSelectWeekDay,
      firstSelectWeekDayMilliseconds,
      lastSelectWeekDayMilliseconds,
      teacherSalary,
      teacherData,
      updateUserData,
      getSalary,
      totalSalary,
      teachers,
      teacherDropdownData,
    } = this.props;

    return (
      <MainPageContext.Provider value={{ scheduleData, lessons, dictionary, myGroupData, students, config }}>
        <MainPageContainer>
          <Header
            firstSelectWeekDay={firstSelectWeekDay}
            lastSelectWeekDay={lastSelectWeekDay}
            theme={theme.currentTheme.mainPage}
            config={config}
            history={history}
            userRole={userRole}
            dictionary={dictionary}
            contentName={contentName}
            isMyAccount={isMyAccount}
            changeContentName={changeContentName}
            clickingNavigationButton={clickingNavigationButton}
          />
          <MainPageWrapper mainBgColor={theme.currentTheme.authPage.mainBgColor}>
            {isMyAccount ? <AccountPage
              config={config}
              history={history}
              dictionary={dictionary}
              teacherData={teacherData}
              isAccountButton={isAccountButton}
              changeAccountButton={changeAccountButton}
              updateUserData={updateUserData}
            /> : <Content
              firstSelectWeekDayMilliseconds={firstSelectWeekDayMilliseconds}
              lastSelectWeekDayMilliseconds={lastSelectWeekDayMilliseconds}
              firstSelectWeekDay={firstSelectWeekDay}
              lastSelectWeekDay={lastSelectWeekDay}
              schedule={schedule}
              users={users}
              attendance={attendance}
              statistic={statistic}
              contentName={contentName}
              changeScheduleWeek={changeScheduleWeek}
              history={history}
              config={config}
              dictionary={dictionary}
              addLesson={addLesson}
              isChangeButton={isChangeButton}
              changeButton={changeButton}
              currentLesson={currentLesson}
              selectedGroup={selectedGroup}
              groupsData={groupsData}
              userRole={userRole}
              currentInputUsers={currentInputUsers}
              changeInputUser={changeInputUser}
              toggleModalWindow={toggleModalWindow}
              adminStatisticData={adminStatisticData}
              adminStatistic={adminStatistic}
              teacherName={teacherName}
              setSelectedGroup={setSelectedGroup}
              daysOfLessons={daysOfLessons}
              teacherSalary={teacherSalary}
              getSalary={getSalary}
              totalSalary={totalSalary}
              teachers={teachers}
              teacherDropdownData={teacherDropdownData}
            />}
          </MainPageWrapper>
        </MainPageContainer>
      </MainPageContext.Provider>
    );
  }
}

MainPage.propTypes = {
  theme: PropTypes.object,
  users: PropTypes.array,
  config: PropTypes.object,
  groups: PropTypes.array,
  lessons: PropTypes.object,
  history: PropTypes.object,
  schedule: PropTypes.array,
  students: PropTypes.object,
  userRole: PropTypes.string,
  statistic: PropTypes.object,
  addLesson: PropTypes.func,
  groupsData: PropTypes.array,
  attendance: PropTypes.object,
  dictionary: PropTypes.object,
  getStudents: PropTypes.func,
  contentName: PropTypes.string,
  myGroupData: PropTypes.object,
  isMyAccount: PropTypes.bool,
  getSchedule: PropTypes.func,
  scheduleData: PropTypes.object,
  changeButton: PropTypes.func,
  checkTokenLS: PropTypes.func,
  getAttendance: PropTypes.func,
  currentLesson: PropTypes.number,
  selectedGroup: PropTypes.number,
  getGroupsData: PropTypes.func,
  adminStatistic: PropTypes.object,
  isChangeButton: PropTypes.bool,
  isAccountButton: PropTypes.bool,
  changeInputUser: PropTypes.func,
  getUsersForAdmin: PropTypes.func,
  currentInputUsers: PropTypes.number,
  changeContentName: PropTypes.func,
  toggleModalWindow: PropTypes.func,
  adminStatisticData: PropTypes.object,
  changeScheduleWeek: PropTypes.func,
  changeAccountButton: PropTypes.func,
  clickingNavigationButton: PropTypes.func,
  teacherName: PropTypes.string,
  firstSelectWeekDayMilliseconds: PropTypes.string,
  lastSelectWeekDayMilliseconds: PropTypes.string,
  setSelectedGroup: PropTypes.func,
  getUserData: PropTypes.func,
  updateUserData: PropTypes.func,
  teacherData: PropTypes.object,
  daysOfLessons: PropTypes.object,
  firstSelectWeekDay: PropTypes.number,
  lastSelectWeekDay: PropTypes.number,
  getSalary: PropTypes.func,
  teacherSalary: PropTypes.array,
  totalSalary: PropTypes.array,
  teachers: PropTypes.array,
  teacherDropdownData: PropTypes.array,
  dateFrom: PropTypes.string,
  dateTo: PropTypes.string,
};

export default MainPage;