import { connect } from 'react-redux';
import Component from './MainPage.jsx';
import * as selectors from '../../selectors/selectors';
import * as actions from '../../actions/actions';
import { mockLessons } from './components/content/components/scheduleMain/components/schedule/mockLessonsData';
import { mockAdminStatistic } from './components/content/components/adminStatistic/mockAdminStatistic';
import { mockStatistic } from './components/content/components/statistic/mockStatistic.js';

const mapStateToProps = state => ({
  config: selectors.getConfig(state),
  theme: selectors.getTheme(state),
  dictionary: selectors.getTranslate(state),
  scheduleData: selectors.getScheduleCaption(state),
  lessons: mockLessons,
  adminStatistic: mockAdminStatistic,
  attendance: selectors.getAttendance(state),
  statistic: mockStatistic,
  schedule: selectors.getSchedule(state),
  adminStatisticData: selectors.getAdminStatistic(state),
  myGroupData: selectors.getMyGroupCaption(state),
  contentName: selectors.getcontentName(state),
  students: selectors.getStudents(state),
  isChangeButton: selectors.isChangeButton(state),
  currentLesson: selectors.getCurrentLesson(state),
  userRole: selectors.getUserRole(state),
  teacherData: selectors.getTeacherData(state),
  groupsData: selectors.getGroups(state),
  selectedGroup: selectors.getSelectedGroup(state),
  currentInputUsers: selectors.getCurrentInput(state),
  users: selectors.getUsers(state),
  isAccountButton: selectors.getAccountButton(state),
  isMyAccount: selectors.getIsMyAccount(state),
  teacherName: selectors.getCurrentTeacher(state),
  daysOfLessons: selectors.getDaysOfLessons(state),
  firstSelectWeekDay: selectors.getfirstSelectWeekDay(state),
  lastSelectWeekDay: selectors.getlastSelectWeekDay(state),
  firstSelectWeekDayMilliseconds: selectors.getfirstSelectWeekDayMilliseconds(state),
  lastSelectWeekDayMilliseconds: selectors.getlastSelectWeekDayMilliseconds(state),
  teacherSalary: selectors.getSalary(state),
  currentStudent: selectors.getCurrentStudent(state),
  totalSalary: selectors.getTotalSalary(state),
  teachers: selectors.getTeachers(state),
  dateFrom: selectors.getDateFrom(state),
  dateTo: selectors.getDateTo(state),
});

const mapDispatchToProps = dispatch => ({
  changeMainContent: payload => dispatch(actions.changeMainContent(payload)),
  clickingNavigationButton: payload => dispatch(actions.clickingButton(payload)),
  changeScheduleWeek: payload => dispatch(actions.changeScheduleWeek(payload)),
  changeContentName: payload => dispatch(actions.changeContentName(payload)),
  changeButton: () => dispatch(actions.changeButton()),
  addLesson: payload => dispatch(actions.addLesson(payload)),
  getGroupsData: () => dispatch(actions.getGroupsData()),
  checkTokenLS: () => dispatch(actions.checkTokenLS()),
  getSchedule: payload => dispatch(actions.getSchedule(payload)),
  getStudents: payload => dispatch(actions.getStudents(payload)),
  changeInputUser: payload => dispatch(actions.changeInputUser(payload)),
  getAttendance: () => dispatch(actions.getAttendance()),
  getUsersForAdmin: () => dispatch(actions.getUsersForAdmin()),
  toggleModalWindow: payload => dispatch(actions.changeToggleModalWindow(payload)),
  changeAccountButton: () => dispatch(actions.changeAccountButton()),
  changeStateMyAccount: () => dispatch(actions.changeAccountButton()),
  setSelectedGroup: payload => dispatch(actions.setSelectedGroup(payload)),
  setSelectedFirstDay: payload => dispatch(actions.setSelectedFirstDay(payload)),
  setSelectedLastDay: payload => dispatch(actions.setSelectedLastDay(payload)),
  getUserData: () => dispatch(actions.getUserData()),
  updateUserData: payload => dispatch(actions.updateUserData(payload)),
  getSalary: payload => dispatch(actions.getSalary(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
