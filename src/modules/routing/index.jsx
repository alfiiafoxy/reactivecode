import { connect } from 'react-redux';
import Component from './Routing';
import * as selectors from '../../selectors/selectors';
import * as actions from '../../actions/actions';

const mapStateToProps = state => ({
  config: selectors.getConfig(state),
  theme: selectors.getTheme(state),
  dictionary: selectors.getTranslate(state),
  modals: selectors.getModals(state),
  locale: selectors.getlocale(state),
});

const mapDispatchToProps = (dispatch) => ({
  toggleModalWindow: payload => dispatch(actions.changeToggleModalWindow(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
