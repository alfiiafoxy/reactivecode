import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { ToastContainer } from 'react-toastify';
import { GlobalStyles } from './styledComponents.js';

import AuthPage from '../authRegPage/authPage/index.jsx';
import RegPage from '../authRegPage/regPage/index.jsx';
import Modal from '../../managers/modalManager/index.jsx';
import MainPage from '@/mainPage/index.jsx';

export default class Routing extends Component {
  render() {
    const { theme } = this.props;

    return (
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route exact path='/' component={AuthPage}/>
            <Route path='/registration' component={RegPage}/>
            <Route path='/main' component={MainPage} />
            <Redirect to='/'/>
          </Switch>
          <Modal/>
          <GlobalStyles direction={'ltr'}/> {/* ToDo direction must come from store */}
          <ToastContainer
            position='bottom-left'
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
          />
        </Router>
      </ThemeProvider>
    );
  }
}

Routing.propTypes = {
  theme: PropTypes.object.isRequired,
  modals: PropTypes.object,
  config: PropTypes.object,
  dictionary: PropTypes.object,
  toggleModalWindow: PropTypes.func,
};

