import React from 'react';
import HeaderMemo from '../Header.jsx';

const Header = HeaderMemo.type;

describe('Header react components', () => {
    const props = {
        config: {
            srcButtonSettings: 'url(\'./assets/settings.png\') no-repeat',
            dataAttributes: { dataAttributeSetting: 'at-authorization-logo' },
        },
    };

    it('snapshot created, should rendered correctly', done => {
        const wrapper = shallow(<Header {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});

