import styled from 'styled-components';

const buttonSettingsHeaderSize = 50;

export const HeaderWrapper = styled.div`
   width: 100%;
   height: 100%;
   display: flex;  
   padding: 0 30px;
   box-sizing: border-box;
   justify-content: space-between;   
   align-items: center;
`;
export const HeaderContentLogo = styled.div`
   min-width: 300px;
   height: 40px;
   display: flex; 
`;
export const HeaderContentButton = styled.div`
   width: ${buttonSettingsHeaderSize}px;
   height: ${buttonSettingsHeaderSize}px;
   display: flex;
`;

