import React from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../../../libs/customButton/CustomButton';
import { HeaderWrapper, HeaderContentButton, HeaderContentLogo } from './styledComponent';
import Logo from './components/logo/Logo';

const Header = (props) => {
  const getOnOPenModalSettings = () => {
    props.toggleModalWindow({ type: 'settings', data: null });
  };
return (
  <HeaderWrapper>
    <HeaderContentLogo>
      <Logo config={props.config}/>
    </HeaderContentLogo>
    <HeaderContentButton>
      <CustomButton
        onClickCallback={getOnOPenModalSettings}
        backgroundImage={props.config.srcButtonSettings}
        backgroundColorHover={props.config.srcButtonSettings}
        dataAttribute={props.config.dataAttributes.dataAttributeSetting}
      />
    </HeaderContentButton>
  </HeaderWrapper>
);
};

Header.propTypes = {
  config: PropTypes.object,
  toggleModalWindow: PropTypes.func,
};

export default React.memo(Header);
