import React from 'react';
import PropTypes from 'prop-types';

import { LogoContent } from './styledComponent';

const Logo = ({ config, onClick }) =>
        <LogoContent
            src={config.srcLogo}
            alt={config.altLogo}
            onClick={onClick}
            data-at={config.dataAttributes.dataAttributeLogo}
        />
;

Logo.propTypes = {
    onClick: PropTypes.func,
    config: PropTypes.object,
};

export default React.memo(Logo);
