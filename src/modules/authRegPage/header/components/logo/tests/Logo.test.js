import LogoMemo from '../Logo.jsx';
import * as styledComponents from '../styledComponent.js';
import dataAttributes from '../../../../../../config/dataAttributes';

const Logo = LogoMemo.type;

describe('Logo react components', () => {
    const props = {
        config: {
            altLogo: 'Logo',
            srcLogo: './assets/ava.png',
            dataAttributes: dataAttributes,
        },
    };

    it('snapshot created, should rendered correctly', done => {
        const wrapper = shallow(<Logo {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});

