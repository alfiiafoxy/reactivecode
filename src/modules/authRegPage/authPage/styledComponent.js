import styled from 'styled-components';

export const AuthWrapper = styled.div`
    width:100%;
    height: 100vh;
    display: flex;
    flex-direction: column; 
    align-content: center;  
    background: ${props => props.mainBgColor ? props.mainBgColor : '#ffffff'}
`;

export const AuthHeader = styled.div`
    width: 100%;
    height: 60px;
    display: flex;
`;

export const AuthContent = styled.div`
    width: 100%; 
    height: 100%;
    display: flex;
    align-items: center;
    align-content: center; 
    justify-content: center;
    flex-direction:column;
`;
