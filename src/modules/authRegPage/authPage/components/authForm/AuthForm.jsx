import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import { AuthFormWrapper, AuthTitleWrapper, AuthBtnWrapper, AuthLinkWrapper,
        ForgotPasswordContainer, ForgotPasswordLink } from './styledComponent.js';
import AuthRegBlock from '../../../../../libs/AuthRegBlock/AuthRegBlock.jsx';
import CustomButton from '../../../../../libs/customButton/CustomButton.jsx';
import CustomLabel from '../../../../../libs/customLabel/CustomLabel.jsx';
import CustomLink from '../../../../../libs/customLink/CustomLink.jsx';
import * as helpers from '../../../../../utils/helpers/getDataHelpers.js';
import * as validationHelper from '../../../../../utils/validation/validationHelpers.js';

const AuthFrom = props => {
  const themeContext = useContext(ThemeContext);
  const authColors = themeContext.currentTheme.authPage;
  const { config, sendRequestAuth, authBtnState, toggleModalWindow, dictionary, history } = props;

  const refInputs = {
    email: React.createRef(),
    password: React.createRef(),
  };

    const authErrors = {
        emailError: React.createRef(),
        passwordError: React.createRef(),
    };

    const sendDataAuth = () => {
        sendRequestAuth({ data: helpers.getDataFromInputs(refInputs), authErrors: helpers.getRefOfElement(authErrors), history });
    };

    const clearErrorFields = data => {
        data.current.innerText = null;
    };

    const toggleForgotPasswordWindow = () => {
        toggleModalWindow({ type: 'forgotPassword', data: null });
    };

    const renderInputsAut = () => {
        return (
            config.authInputs.map(item => {
                return (
                    <AuthRegBlock
                        key={item.idInput}
                        textColorLabel={authColors.secondaryText}
                        labelName={dictionary.resources[item.resourcesKey]}
                        htmlFor={item.idInput}
                        idInput={item.idInput}
                        inputRef={refInputs[item.idInput]}
                        inputType={item.inputType}
                        onKeyDownCallback={event => validationHelper[item.onKeyDownCallback](event, authErrors[item.errorMessageRef].current,
                                                    dictionary.errorMessage[item.errorMessageRef])}
                        onFocusCallback={() => clearErrorFields(authErrors[item.errorMessageRef])}
                        requiredInput={'required'}
                        placeholderText={dictionary.placeholders[item.placeholderKey]}
                        fontSizeLabel={'18px'}
                        isDisabled={false}
                        dataAttributeInput={config.dataAttributes.dataAttributeInputAuth[item.idInput]}
                        backgroundColor={authColors.inputBgColor}
                        borderRadius={0}
                        backgroundFocusColor={authColors.inputBgColorFocus}
                        focusColor={authColors.mainTextColor}
                        inputCursor={'text'}
                        colorInput={authColors.inputTextColor}
                        textColor={authColors.mainTextColor}
                        textAlign={'center'}
                        marginBlock={'7px 0'}
                        errorRef={authErrors[item.errorMessageRef]}
                        dataAttributeError={config.dataAttributes.dataAttributeInputAuthError}
                    />
                );
            })
        );
    };

    return (
        <AuthFormWrapper bgColor={authColors.formBgColor} borderStyle={'1px solid' + authColors.borderColor}>
            <AuthTitleWrapper>
                <CustomLabel
                    fontSize={'40px'}
                    labelName={dictionary.resources.authorization}
                    textColor={authColors.mainTextColor}
                    dataAttributeLabel={config.dataAttributes.dataAttributeTitlePage.dataAttributeTitle}
                />
            </AuthTitleWrapper>
            {renderInputsAut()}
            <AuthBtnWrapper>
                <CustomButton
                    fontSize={20}
                    dataAttribute={config.dataAttributes.dataAttributeButton.dataAttributeSignIn}
                    borderRadius={4}
                    fontWeight={'normal'}
                    textTransform={ 'none' }
                    isDisabled={authBtnState}
                    onClickCallback={sendDataAuth}
                    backgroundColor={authColors.btnBgColor}
                    border={'1px solid' + authColors.borderBtm}
                    buttonName={dictionary.resources.signIn}
                    textColor={authColors.btnTextColor}
                />
            </AuthBtnWrapper>
        <AuthLinkWrapper>
            <CustomLink
                linkName={dictionary.resources.goToSignUp}
                onClickCallback={() => history.push('/registration')}
                fontSize={18}
                textColor={authColors.linkText}
                dataAttribute={config.dataAttributes.dataAttributeLink.dataAttributeRegistrationLink}
                textColorHover={authColors.linkTextHover}
            />
        </AuthLinkWrapper>
            <ForgotPasswordContainer>
                <ForgotPasswordLink
                    data-at={config.dataAttributes.dataAttributeLink.dataAttributeForgotPasswordLink}
                    onClick={toggleForgotPasswordWindow}
                    textColor={authColors.linkText}
                    textColorHover={authColors.linkTextHover}
                >
                    {dictionary.resources.forgotPassword}
                </ForgotPasswordLink>
            </ForgotPasswordContainer>
        </AuthFormWrapper>
    );
};

AuthFrom.propTypes = {
    config: PropTypes.object,
    dictionary: PropTypes.object,
    sendRequestAuth: PropTypes.func,
    authBtnState: PropTypes.bool,
    history: PropTypes.object,
    toggleModalWindow: PropTypes.func,
};

export default React.memo(AuthFrom);
