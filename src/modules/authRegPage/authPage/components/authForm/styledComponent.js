import styled from 'styled-components';

const defaultTextColor = '#707070';
const defaultTextColorHover = '#a2a2a2';

export const AuthFormWrapper = styled.div`
    width: 350px;
    padding: 0 20px;
    box-sizing: border-box;
    display: flex;   
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background: ${props => props.bgColor ? props.bgColor : '#ffffff'};
    border: ${props => props.borderStyle ? props.borderStyle : 'transparent'};
`;

export const AuthTitleWrapper = styled.div`
    margin: 10px 0;
`;

export const AuthBtnWrapper = styled.div`
    width: 130px;
    height: 30px;
    margin-bottom: 10px;
`;

export const AuthLinkWrapper = styled.div`
    width: 100%;
    height: 30px;
    margin: 10px 0;
    text-align: center;
`;

export const ForgotPasswordContainer = styled.div`
   margin-top: 40px;
   margin-bottom: 25px;
`;

export const ForgotPasswordLink = styled.div`
    font-size: 14px;
    color: ${props => props.textColor ? props.textColor : defaultTextColor};
    text-decoration: underline;
    
    &:hover {
      cursor: pointer;
      color: ${props => props.textColorHover ? props.textColorHover : defaultTextColorHover};
      text-decoration: underline;
    }
`;
