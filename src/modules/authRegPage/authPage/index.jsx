import { connect } from 'react-redux';
import Component from './AuthPage.jsx';
import * as selectors from '../../../selectors/selectors.js';
import * as actions from '../../../actions/actions';

const mapStateToProps = (state) => ({
  config: selectors.getConfig(state),
  theme: selectors.getTheme(state),
  dictionary: selectors.getTranslate(state),
  authBtnState: selectors.getAuthBtnState(state),
});

const mapDispatchToProps = dispatch => ({
  sendRequestAuth: payload => dispatch(actions.sendRequestAuth(payload)),
  toggleModalWindow: payload => dispatch(actions.changeToggleModalWindow(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
