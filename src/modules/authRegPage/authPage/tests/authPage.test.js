import AuthPage from '../AuthPage';
import React from 'react';
import { initialState } from '../../../../managers/themeManager/reducer';
import config from '../../../../config/config';

describe('Authorization page', () => {
    const props = {
        config: config,
        theme: initialState,
    };

    it('snapshot created, should rendered correctly', done => {
        const wrapper = shallow(<AuthPage {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});