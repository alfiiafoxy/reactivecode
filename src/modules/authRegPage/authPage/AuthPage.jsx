import React from 'react';
import PropTypes from 'prop-types';
import Header from '../header/Header.jsx';
import AuthForm from './components/authForm/AuthForm.jsx';
import { AuthWrapper, AuthHeader, AuthContent } from './styledComponent.js';

class AuthPage extends React.PureComponent {
    render() {
        const { config, sendRequestAuth, authBtnState, theme, dictionary, history, toggleModalWindow } = this.props;
        return (
            <AuthWrapper mainBgColor={theme.currentTheme.authPage.mainBgColor}>
                <AuthHeader>
                    <Header
                        toggleModalWindow={toggleModalWindow}
                        config={config}
                    />
                </AuthHeader>
                <AuthContent>
                    <AuthForm
                        config={config}
                        authBtnState={authBtnState}
                        sendRequestAuth={sendRequestAuth}
                        dictionary={dictionary}
                        history={history}
                        toggleModalWindow={toggleModalWindow}
                    />
                </AuthContent>
            </AuthWrapper>
        );
    }
}

AuthPage.propTypes = {
  config: PropTypes.object,
  theme: PropTypes.object,
  dictionary: PropTypes.object,
  history: PropTypes.object,
  sendRequestAuth: PropTypes.func,
  authBtnState: PropTypes.bool,
  toggleModalWindow: PropTypes.func,
};

export default AuthPage;
