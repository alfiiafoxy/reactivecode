import styled from 'styled-components';

export const RegWrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column; 
    align-content: center;  
    background: ${props => props.mainBgColor ? props.mainBgColor : '#ffffff'}
`;

export const RegHeader = styled.div`
    width: 100%;
    height: 60px;
    display: flex;
`;

export const RegContent = styled.div`
    width: 100%; 
    height: 100%;
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;
