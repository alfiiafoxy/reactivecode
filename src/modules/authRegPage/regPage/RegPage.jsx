import React from 'react';
import PropTypes from 'prop-types';
import { RegContent, RegHeader, RegWrapper } from '../regPage/styledComponent.js';
import RegForm from '../regPage/components/regForm/RegForm.jsx';
import Header from '../header/Header.jsx';

export default class RegPage extends React.Component {
  render() {
    const { config, theme, sendRequestReg, dictionary, history, toggleModalWindow } = this.props;
    return (
        <RegWrapper mainBgColor={theme.currentTheme.regPage.mainBgColor}>
            <RegHeader>
                <Header
                    config={config}
                    toggleModalWindow={toggleModalWindow}
                />
            </RegHeader>
            <RegContent>
                <RegForm
                    config={ config }
                    history={history}
                    dictionary={dictionary}
                    sendRequestReg={sendRequestReg}
                />
            </RegContent>
        </RegWrapper>
    );
  }
}

RegPage.propTypes = {
    theme: PropTypes.object,
    config: PropTypes.object,
    history: PropTypes.object,
    dictionary: PropTypes.object,
    sendRequestReg: PropTypes.func,
    toggleModalWindow: PropTypes.func,
};
