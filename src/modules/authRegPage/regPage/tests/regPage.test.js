import RegPage from '../RegPage';
import React from 'react';
import { initialState } from '../../../../managers/themeManager/reducer';
import config from '../../../../config/config';

describe('Registration page', () => {
    const props = {
        config: config,
        theme: initialState,
    };

    it('snapshot created, should rendered correctly', done => {
        const wrapper = shallow(<RegPage {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});