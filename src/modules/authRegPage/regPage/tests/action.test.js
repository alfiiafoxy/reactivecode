import * as action from '../../../../actions/actions';

describe('Testing sendRequestReg: ', () => {
  it('sendRequestReg with type SEND_REG_REQ', () => {
    const expected = { type: 'SEND_REG_REQ', payload: 'aaaaa' };
    const actual = action.sendRequestReg('aaaaa');
    assert.deepEqual(actual, expected);
  });
  it('sendRequestReg with type SEND_FALSE', () => {
    const expected = { type: 'SEND_FALSE', payload: 'aaaaa' };
    const actual = action.sendRequestReg('aaaaa');
    assert.notDeepEqual(actual, expected);
  });
});
