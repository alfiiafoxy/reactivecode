import { connect } from 'react-redux';
import Component from './RegPage.jsx';
import * as selectors from '../../../selectors/selectors.js';
import * as actions from '../../../actions/actions.js';

const mapStateToProps = state => ({
  config: selectors.getConfig(state),
  theme: selectors.getTheme(state),
  dictionary: selectors.getTranslate(state),
});

const mapDispatchToProps = dispatch => ({
  sendRequestReg: payload => dispatch(actions.sendRequestReg(payload)),
  toggleModalWindow: payload => dispatch(actions.changeToggleModalWindow(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
