import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import { RegFormWrapper, RegTitleWrapper, RegBtnWrapper, RegLinkWrapper } from './styledComponent.js';
import * as validationHelper from '../../../../../utils/validation/validationHelpers.js';
import * as helpers from '../../../../../utils/helpers/getDataHelpers.js';
import InputBlock from './components/InputBlock.jsx';
import CustomButton from '../../../../../libs/customButton/CustomButton.jsx';
import CustomLabel from '../../../../../libs/customLabel/CustomLabel.jsx';
import CustomLink from '../../../../../libs/customLink/CustomLink.jsx';

const RegForm = props => {
    const themeContext = useContext(ThemeContext);
    const regColors = themeContext.currentTheme.regPage;
    const { config, sendRequestReg, dictionary } = props;

    const refInputs = {
        firstName: React.createRef(),
        lastName: React.createRef(),
        email: React.createRef(),
        password: React.createRef(),
        repeatPassword: React.createRef(),
        phone: React.createRef(),
        keywords: React.createRef(),
        dateOfBirth: React.createRef(),
    };

    const regErrors = {
        firstNameError: React.createRef(),
        lastNameError: React.createRef(),
        emailError: React.createRef(),
        passwordError: React.createRef(),
        repeatPasswordError: React.createRef(),
        phoneError: React.createRef(),
        dateOfBirthError: React.createRef(),
        keywordsError: React.createRef(),
    };

    const sendRequestAuth = () => {
        sendRequestReg({ data: helpers.getDataFromInputs(refInputs), regErrors: helpers.getRefOfElement(regErrors), history: props.history });
    };

    const clearErrorFields = data => {
        data.current.innerText = null;
    };

    const renderInputsReg = () => {
        return (
            config.regInputs.map((item) => {
                return (
                    <InputBlock
                        key={item.idInput}
                        textColorLabel={regColors.secondaryText}
                        labelName={props.dictionary.resources[item.resourcesKey]}
                        htmlFor={item.idInput}
                        idInput={item.idInput}
                        inputRef={refInputs[item.inputRef]}
                        inputType={item.inputType}
                        onKeyDownCallback={event => validationHelper[item.onKeyDownCallback](event,
                                            regErrors[item.errorMessageRef].current, dictionary.errorMessage[item.errorMessageRef])}
                        onFocusCallback={() => clearErrorFields(regErrors[item.errorMessageRef])}
                        requiredInput={'required'}
                        placeholderText={props.dictionary.placeholders[item.placeholderKey]}
                        widthSizeLabel={150}
                        isDisabled={false}
                        dataAttributeInput={props.config.dataAttributes.dataAttributeInputReg[item.idInput]}
                        backgroundColor={regColors.inputBgColor}
                        borderRadius={0}
                        backgroundFocusColor={regColors.inputBgColorFocus}
                        focusColor={regColors.mainTextColor}
                        inputCursor={'text'}
                        colorInput={regColors.inputTextColor}
                        textAlign={'center'}
                        marginBlock={'7px 0'}
                        errorRef={regErrors[item.errorMessageRef]}
                        dataAttributeError={props.config.dataAttributes.dataAttributeRegError[item.idInput]}
                    />
                );
            })
        );
    };

    return (
        <RegFormWrapper bgColor={regColors.formBgColor} borderStyle={'1px solid' + regColors.borderColor}>
            <RegTitleWrapper>
                <CustomLabel
                    fontSize={'40px'}
                    labelName={props.dictionary.resources.registration}
                    textColor={regColors.mainTextColor}
                    dataAttributeLabel={config.dataAttributes.dataAttributeTitlePage.dataAttributeTitleRegistration}
                />
            </RegTitleWrapper>
            {renderInputsReg()}
            <RegBtnWrapper>
                <CustomButton
                    fontSize={18}
                    borderRadius={4}
                    fontWeight={'normal'}
                    textTransform={'none'}
                    onClickCallback={sendRequestAuth}
                    backgroundColor={regColors.btnBgColor}
                    border={'1px solid' + regColors.borderBtm}
                    buttonName={props.dictionary.resources.signUp}
                    dataAttribute={config.dataAttributes.dataAttributeButton.dataAttributeSignUp}
                />
            </RegBtnWrapper>
            <RegLinkWrapper>
                <CustomLink
                    fontSize={14}
                    linkName={props.dictionary.resources.goToSignIn}
                    textColor={regColors.linkText}
                    dataAttribute={config.dataAttributes.dataAttributeLink.dataAttributeBackToAuthlink}
                    onClickCallback={() => props.history.push('/auth')}
                    textColorHover={regColors.linkTextHover}
                />
            </RegLinkWrapper>
        </RegFormWrapper>
    );
};

RegForm.propTypes = {
    config: PropTypes.object,
    sendRequestReg: PropTypes.func,
    theme: PropTypes.object,
    history: PropTypes.object,
    dictionary: PropTypes.object,
};

export default React.memo(RegForm);
