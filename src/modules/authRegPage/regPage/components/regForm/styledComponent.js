import styled from 'styled-components';

export const RegFormWrapper = styled.div`
    width: 500px;
    padding: 0 20px;
    box-sizing: border-box;
    display: flex;   
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background: ${props => props.bgColor ? props.bgColor : '#ffffff'};
    border: ${props => props.borderStyle ? props.borderStyle : 'transparent'};
`;

export const RegTitleWrapper = styled.div`
    margin: 10px 0;
`;

export const RegBtnWrapper = styled.div`
    width: 130px;
    height: 30px;
    margin-bottom: 10px;
`;

export const RegLinkWrapper = styled.div`
    width: 100%;
    height: 30px;
    margin-bottom: 10px;
    text-align: center;
`;
