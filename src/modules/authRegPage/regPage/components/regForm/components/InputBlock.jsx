import React from 'react';
import PropTypes from 'prop-types';
import { BlockWrapper, InputBlockContainer, InputErrorContainer } from './styledComponents.js';
import CustomLabel from '../../../../../../libs/customLabel/CustomLabel.jsx';
import CustomInput from '../../../../../../libs/customInput/CustomInput.jsx';
import ErrorBlock from '../../../../../../libs/errorBlock/ErrorBlock.jsx';

const InputBlock = props => {
    const {
        fontSizeLabel,
        textColorLabel,
        fontWeightLabel,
        dataAttributeLabel,
        textTransformLabel,
        labelName,
        htmlFor,
        widthSizeLabel,
        idInput,
        inputRef,
        inputType,
        inputCallback,
        onKeyDownCallback,
        onFocusCallback,
        requiredInput,
        placeholderText,
        isDisabled,
        dataAttributeInput,
        backgroundColor,
        borderRadius,
        backgroundFocusColor,
        focusColor,
        inputCursor,
        colorInput,
        errorMessage,
        textColor,
        errorRef,
        marginBlock,
        textAlign,
        dataAttributeError,
    } = props;

    return (
        <BlockWrapper>
            <InputBlockContainer>
                <CustomLabel
                    dataAttributeLabel={dataAttributeLabel}
                    htmlFor={htmlFor}
                    textTransform={textTransformLabel}
                    textColor={textColorLabel}
                    fontWeight={fontWeightLabel}
                    labelName={`${labelName}:`}
                    widthSize={widthSizeLabel}
                    fontSize={fontSizeLabel}
                />
                <CustomInput
                    idInput={idInput}
                    inputRef={inputRef}
                    inputType={inputType}
                    inputCallback={inputCallback}
                    onKeyDownCallback={onKeyDownCallback}
                    onFocusCallback={onFocusCallback}
                    requiredInput={requiredInput}
                    placeholderText={placeholderText}
                    isDisabled={isDisabled}
                    dataAttributeInput={dataAttributeInput}
                    backgroundColor={backgroundColor}
                    borderRadius={borderRadius}
                    backgroundFocusColor={backgroundFocusColor}
                    focusColor={focusColor}
                    intCursor={inputCursor}
                    color={colorInput}
                />
            </InputBlockContainer>
            <InputErrorContainer>
                <ErrorBlock
                    errorRef={errorRef}
                    errorMessage={errorMessage}
                    marginBlock={marginBlock}
                    textAlign={textAlign}
                    textColor={textColor}
                    dataAttributeError={dataAttributeError}
                />
            </InputErrorContainer>
        </BlockWrapper>
    );
};

InputBlock.propTypes = {
    fontSizeLabel: PropTypes.string,
    textColorLabel: PropTypes.string,
    fontWeightLabel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    dataAttributeLabel: PropTypes.string,
    textTransformLabel: PropTypes.string,
    labelName: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    idInput: PropTypes.string,
    inputRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.elementType }),
    ]),
    widthSizeLabel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    inputType: PropTypes.string,
    inputCallback: PropTypes.func,
    requiredInput: PropTypes.string,
    placeholderText: PropTypes.string,
    isDisabled: PropTypes.bool,
    dataAttributeInput: PropTypes.string,
    backgroundColor: PropTypes.string,
    borderRadius: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    backgroundFocusColor: PropTypes.string,
    focusColor: PropTypes.string,
    inputCursor: PropTypes.string,
    colorInput: PropTypes.string,
    errorMessage: PropTypes.string,
    textColor: PropTypes.string,
    errorRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.elementType }),
    ]),
    onKeyDownCallback: PropTypes.func,
    onFocusCallback: PropTypes.func,
    marginBlock: PropTypes.string,
    textAlign: PropTypes.string,
    dataAttributeError: PropTypes.string,
};

export default React.memo(InputBlock);

