import styled from 'styled-components';

export const BlockWrapper = styled.div`
   height: 100%;
   width: 100%;
   display: flex;
   flex-direction: column;
`;

export const InputBlockContainer = styled.div`
   height: 100%;
   display: flex;
   align-items: center;
   justify-content: flex-start;
`;

export const InputErrorContainer = styled.div`
   height: 100%;
   display: flex;
   align-items: center;
   justify-content: flex-start;
`;
