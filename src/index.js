import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';

import rootReducer from './redux/rootReducer';
import rootSaga from './sagas/rootSaga.js';
import Routing from './modules/routing/index';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
process.env.NODE_ENV === 'development' ? window.store = store : null;
sagaMiddleware.run(rootSaga);

ReactDOM.render(
    <Provider store={store}><Routing /></Provider>,
    document.getElementById('root')
);

