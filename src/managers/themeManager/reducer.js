import config from '../../config/config';
import dark from './theme/darkTheme';
import light from './theme/lightTheme';

const themes = { dark, light };

export const getThemeStorage = () => {
    return localStorage.getItem('theme') || config.defaultTheme;
};

const currentTheme = getThemeStorage();

export const initialState = {
    currentTheme: themes[currentTheme],
    nameTheme: currentTheme,
};

export const themeReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_THEME':
            return {
            ...state,
            currentTheme: themes[action.payload],
            nameTheme: action.payload,
        };
        default: return state;
    }
};