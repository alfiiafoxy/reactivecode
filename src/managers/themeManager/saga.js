import { apply, put, takeEvery } from '@redux-saga/core/effects';
import actionTypes from '../../constans/actionTypes';

export function* watchChangeTheme() {
    yield takeEvery(actionTypes.CHANGE_THEME, changeTheme);
}

export function* changeTheme(action) {
    yield put({ type: actionTypes.SET_THEME, payload: action.payload });
    yield apply(localStorage, localStorage.setItem, ['theme', action.payload]);
}
