export default {
    color__01: '#cfeff3;', //main bg light
    color__02: '#202020', //main bg dark
    color__03: 'rgba(255, 255, 255, 0.55);', //form bg light
    color__04: '#333333', //form bg dark
    color__05: '#212121', //main text light
    color__06: '#f4f3f4', //main text dark
    color__07: '#757575', //secondary text light
    color__08: '#dbdadb', //secondary text dark
    color__09: '#2d94f3', //link text light, input border light
    color__10: '#1c62a3', //link text dark, btn bg dark
    color__11: '#D4D9E7', //border dark, border light
    color__12: '#FFC107', //btn bg light
    color__13: '#aee7e3',
    color__14: '#FFFFFF', //input bg light
    color__15: '#707070', //border btm light color link go to registr and back to auth
    color__16: '#e8f0fe', //color focus input
    color__17: '#0081B5', //color focus link forgot password
    color__18: '#115293', //color focus link forgot password
    color__19: '#ffa84b', //btn bg light
    color__20: '#F00000', //btn bg light
    color__22: 'darkgray',
    color__21: '#f6fafe',
};
