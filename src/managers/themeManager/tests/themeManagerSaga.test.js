import { takeEvery } from '@redux-saga/core/effects';
import { watchChangeTheme, changeTheme } from '../saga.js';
import { themeReducer, initialState } from '../reducer';
import darkTheme from '../theme/darkTheme';
import lightTheme from '../theme/lightTheme';

describe('Testing watchChangeTheme: ', () => {
    const generator = watchChangeTheme();

    it('watchChangeTheme takeEvery CHANGE_THEME', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            takeEvery('CHANGE_THEME', changeTheme)
        );
    });

    it('watchChangeTheme done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing reducer themeReducer: ', () => {
    it('Action type SET_THEME, set into dark theme', () => {
        const action = {
            type: 'SET_THEME',
            payload: 'dark',
        };

        assert.deepEqual(themeReducer(initialState, action), {
            ...initialState,
            currentTheme: darkTheme,
            nameTheme: 'dark',
        });
    });

    it('Action type SET_THEME, set into light theme', () => {
        const action = {
            type: 'SET_THEME',
            payload: 'light',
        };

        assert.deepEqual(themeReducer(initialState, action), {
            ...initialState,
            currentTheme: lightTheme,
            nameTheme: 'light',
        });
    });
});