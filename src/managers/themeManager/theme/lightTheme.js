import colorMatrix from '../colorMatrix';

export default {
    regPage: {
        mainBgColor: colorMatrix.color__01,
        formBgColor: colorMatrix.color__03,
        mainTextColor: colorMatrix.color__05,
        secondaryText: colorMatrix.color__07,
        lightText: colorMatrix.color__09,
        btnBgColor: colorMatrix.color__12,
        borderColor: colorMatrix.color__11,
        borderBtm: colorMatrix.color__15,
        inputBgColor: colorMatrix.color__14,
        inputText: colorMatrix.color__06,
        inputBgColorFocus: colorMatrix.color__16,
        linkText: colorMatrix.color__17,
        linkTextHover: colorMatrix.color__18,
    },
    authPage: {
        mainBgColor: colorMatrix.color__01,
        formBgColor: colorMatrix.color__03,
        mainTextColor: colorMatrix.color__05,
        secondaryText: colorMatrix.color__07,
        lightText: colorMatrix.color__09,
        btnBgColor: colorMatrix.color__12,
        borderColor: colorMatrix.color__11,
        borderBtm: colorMatrix.color__15,
        inputBgColor: colorMatrix.color__14,
        inputBgColorFocus: colorMatrix.color__16,
        inputText: colorMatrix.color__06,
        linkText: colorMatrix.color__17,
        modalBgColor: colorMatrix.color__14,
        linkTextHover: colorMatrix.color__18,
        btnTextColor: colorMatrix.color__05,
    },
    schedulePage: {
        mainBgColor: colorMatrix.color__01,
        formBgColor: colorMatrix.color__03,
        mainTextColor: colorMatrix.color__05,
        secondaryText: colorMatrix.color__07,
        lightText: colorMatrix.color__09,
        btnBgColor: colorMatrix.color__14,
        borderColor: colorMatrix.color__11,
        borderBtm: colorMatrix.color__15,
        inputBgColor: colorMatrix.color__14,
        inputBgColorFocus: colorMatrix.color__16,
        inputText: colorMatrix.color__06,
        linkText: colorMatrix.color__15,
        scheduleCaptionBg: colorMatrix.color__09,
        inputsFocusBg: colorMatrix.color__06,
        inputFocusColor: colorMatrix.color__11,
    },
    mainPage: {
        mainBgColor: colorMatrix.color__01,
        mainTextColor: colorMatrix.color__05,
        secondaryText: colorMatrix.color__07,
        lightText: colorMatrix.color__09,
        btnBgColor: colorMatrix.color__12,
        borderColor: colorMatrix.color__22,
        borderBtm: colorMatrix.color__15,
        inputBgColor: colorMatrix.color__13,
        inputBgColorFocus: colorMatrix.color__16,
        inputBgColorDisabled: colorMatrix.color__14,
        buttonToggleContentPageBg: colorMatrix.color__14,
        buttonToggleContentPageBgHover: colorMatrix.color__18,
        bgButtonAddGroup: colorMatrix.color__17,
        bgHoverButtonAddGroup: colorMatrix.color__18,
        bgBorderButtonAddGroup: colorMatrix.color__17,
        inputText: colorMatrix.color__06,
        linkText: colorMatrix.color__15,
        scheduleCaptionBg: colorMatrix.color__09,
        scheduleRowBgOdd: colorMatrix.color__16,
        scheduleRowBgEven: colorMatrix.color__14,
        statisticCaptionBg: colorMatrix.color__09,
        dropDownSelectTextColor: colorMatrix.color__15,
        borderColorInp: colorMatrix.color__09,
        inputBgColorChange: colorMatrix.color__16,
        btnText: colorMatrix.color__14,
        btnTextAdminGroup: colorMatrix.color__17,
        btnAdminGroup: colorMatrix.color__14,
        bgHoverButtonAddGroupAdmin: colorMatrix.color__21,
        btnAdminGroupDelete: colorMatrix.color__20,
    },
    controlPanel: {
        mainBgColor: colorMatrix.color__01,
        formBgColor: colorMatrix.color__03,
        mainTextColor: colorMatrix.color__05,
        btnSecondaryTextColor: colorMatrix.color__20,
        btnMainTextColor: colorMatrix.color__14,
        secondaryText: colorMatrix.color__07,
        lightText: colorMatrix.color__09,
        btnBgColor: colorMatrix.color__14,
        btnMainBgColor: colorMatrix.color__17,
        borderColor: colorMatrix.color__18,
        borderBtm: colorMatrix.color__15,
        inputBgColor: colorMatrix.color__14,
        inputBgColorFocus: colorMatrix.color__16,
        inputText: colorMatrix.color__06,
        linkText: colorMatrix.color__15,
        scheduleCaptionBg: colorMatrix.color__09,
        inputsFocusBg: colorMatrix.color__06,
        inputFocusColor: colorMatrix.color__11,
    },
    accountPage: {
        mainBgColor: colorMatrix.color__01,
        mainTextColor: colorMatrix.color__05,
        mainTextErrorColor: colorMatrix.color__20,
        secondaryText: colorMatrix.color__07,
        lightText: colorMatrix.color__09,
        btnBgColor: colorMatrix.color__12,
        borderColor: colorMatrix.color__11,
        borderBtm: colorMatrix.color__15,
        inputBgColor: colorMatrix.color__13,
        inputBgColorFocus: colorMatrix.color__16,
        inputBgColorDisabled: colorMatrix.color__14,
        buttonToggleContentPageBg: colorMatrix.color__14,
        buttonToggleContentPageBgHover: colorMatrix.color__17,
        inputText: colorMatrix.color__06,
        linkText: colorMatrix.color__15,
        scheduleCaptionBg: colorMatrix.color__09,
        scheduleRowBgOdd: colorMatrix.color__16,
        scheduleRowBgEven: colorMatrix.color__14,
        statisticCaptionBg: colorMatrix.color__09,
        dropDownSelectTextColor: colorMatrix.color__15,
    },
    modalUsers: {
        mainBgColor: colorMatrix.color__01,
        formBgColor: colorMatrix.color__03,
        mainTextColor: colorMatrix.color__05,
        secondaryText: colorMatrix.color__07,
        lightText: colorMatrix.color__09,
        btnBgColor: colorMatrix.color__12,
        borderColor: colorMatrix.color__21,
        borderBtm: colorMatrix.color__15,
        inputBgColor: colorMatrix.color__17,
        inputBgColorFocus: colorMatrix.color__15,
        inputText: colorMatrix.color__14,
        linkText: colorMatrix.color__15,
        btnBgColorHover: colorMatrix.color__19,
    },
    modalConfirm:
      {
          titleColor: colorMatrix.color__02,
          mainBgColor: colorMatrix.color__01,
          formBgColor: colorMatrix.color__03,
          mainTextColor: colorMatrix.color__16,
          secondaryText: colorMatrix.color__20,
          lightText: colorMatrix.color__09,
          btnConfirmBgColor: colorMatrix.color__17,
          btnDeclineBgColor: colorMatrix.color__14,
          borderColor: colorMatrix.color__20,
          borderBtm: colorMatrix.color__15,
          inputBgColor: colorMatrix.color__17,
          inputBgColorFocus: colorMatrix.color__15,
          inputText: colorMatrix.color__14,
          linkText: colorMatrix.color__15,
          btnBgColorHover: colorMatrix.color__18,
          btnBgColorHoverDecline: colorMatrix.color__13,
      },
    modalSettings:
      {
          mainBgColor: colorMatrix.color__14,
          mainTextColor: colorMatrix.color__05,
          secondaryText: colorMatrix.color__07,
      },
};
