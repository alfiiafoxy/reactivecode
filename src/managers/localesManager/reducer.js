import actionTypes from '../../constans/actionTypes.js';
import config from '../../config/config.js';
import en from './dictionary/en.js';
import ru from './dictionary/ru.js';

const dictionary = { en, ru };
export const getLocale = () => {
  return localStorage.getItem('locale') || config.defaultLocale;
};

const currentLocale = getLocale();

export const initialState = {
  locale: currentLocale,
  currentDictionary: dictionary[currentLocale],
};

export const translateReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_LOCALE:
      return {
        ...state,
        locale: action.payload,
        currentDictionary: dictionary[action.payload],
      };
    default:
      return state;
  }
};
