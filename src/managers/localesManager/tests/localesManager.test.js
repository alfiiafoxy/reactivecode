import { apply, put, takeEvery } from '@redux-saga/core/effects';
import { translateReducer, initialState } from '../reducer.js';
import { watchChangeLocale, changeLocale } from '../saga.js';
import actionTypes from '../../../constans/actionTypes.js';
import { getLocale } from '../reducer.js';
import ru from '../dictionary/ru.js';
import en from '../dictionary/en.js';

describe('Testing watchChangeLocale: ', () => {
  const generator = watchChangeLocale();

  it('watchChangeLocale takeEvery CHANGE_LOCALE', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      takeEvery('CHANGE_LOCALE', changeLocale),
    );
  });

  it('watchChangeLocale done equals to true', () => {
    const actual = generator.next();
    assert.isTrue(actual.done);
  });
});

describe('Testing changeLocale worker: ', () => {
  const action = {
    type: 'CHANGE_LOCALE',
    payload: 'ru',
  };
  const generator = changeLocale(action);

  it('changeLocale put SET_LOCALE', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      put({ type: actionTypes.SET_LOCALE, payload: 'ru' }));
  });

  it('changeLocale apply localStorage', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      apply(localStorage, localStorage.setItem, ['locale', action.payload]));
      localStorage.clear();
  });
});

describe('Testing translateReducer: ', () => {
  it('Change locale to en', () => {
    const action = {
      type: 'SET_LOCALE',
      payload: 'en',
    };

    const expected = {
      ...initialState,
      locale: 'en',
      currentDictionary: en,
    };

    const actual = translateReducer(initialState, action);
    assert.deepEqual(actual, expected);
  });

  it('Change locale to ru', () => {
    const action = {
      type: 'SET_LOCALE',
      payload: 'ru',
    };

    const expected = {
      ...initialState,
      locale: 'ru',
      currentDictionary: ru,
    };

    const actual = translateReducer(initialState, action);
    assert.deepEqual(actual, expected);
  });
});

describe('Testing function getLocale: ', () => {
  it('Get en locale', () => {
    localStorage.setItem('locale', 'en');
    const expected = 'en';
    const actual = getLocale();
    assert.deepEqual(actual, expected);
    localStorage.clear();
  });

  it('Get locale ru', () => {
    localStorage.setItem('locale', 'ru');
    const expected = 'ru';
    const actual = getLocale();
    assert.deepEqual(actual, expected);
    localStorage.clear();
  });

  it('Get default locale', () => {
    const expected = 'en';
    const actual = getLocale();
    assert.deepEqual(actual, expected);
    localStorage.clear();
  });
});

