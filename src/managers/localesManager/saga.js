import { apply, put, takeEvery } from '@redux-saga/core/effects';
import actionTypes from '../../constans/actionTypes.js';

export function* watchChangeLocale() {
  yield takeEvery(actionTypes.CHANGE_LOCALE, changeLocale);
}

export function* changeLocale(action) {
  yield put({ type: actionTypes.SET_LOCALE, payload: action.payload });
  yield apply(localStorage, localStorage.setItem, ['locale', action.payload]);
}
