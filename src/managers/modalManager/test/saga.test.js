import { takeEvery } from '@redux-saga/core/effects';
import { watchChangeModal, changeModal } from '../saga.js';

describe('Testing watchChangeTheme: ', () => {
  const generator = watchChangeModal();

  it('watchChangeModal takeEvery SET_TOGGLE_MODAL', () => {
    const actual = generator.next();
    assert.deepEqual(
      actual.value,
      takeEvery('SET_TOGGLE_MODAL', changeModal),
    );
  });

  it('watchChangeModal done equals to true', () => {
    const actual = generator.next();
    assert.isTrue(actual.done);
  });
});