import * as modal from '../modalReducer';

describe('modalReducer', () => {
  const state = {
    settings: {
      type: 'settings',
      data: null,
      isOpen: true,
    },
    forgotPassword: {
      type: 'forgotPassword',
      data: null,
      isOpen: false,
    },
  };

  it('modalReducer settings close', () => {
    const expected = {
      settings: {
        type: 'settings',
        data: null,
        isOpen: false,
      },
      forgotPassword: {
        type: 'forgotPassword',
        data: null,
        isOpen: false,
      },
    };

    const data = {
      type: 'TOGGLE_MODAL_WINDOW',
      payload: {
        type: 'settings',
        data: null,
      },
    };
    const actual = modal.modalReducer(state, data);

    assert.deepEqual(actual, expected);
  });

  it('modalReducer forgotPassword open', () => {
    const expected = {
      settings: {
        type: 'settings',
        data: null,
        isOpen: true,
      },
      forgotPassword: {
        type: 'forgotPassword',
        data: null,
        isOpen: true,
      },
    };

    const data = {
      type: 'TOGGLE_MODAL_WINDOW',
      payload: {
        type: 'forgotPassword',
        data: null,
      },
    };
    const actual = modal.modalReducer(state, data);

    assert.deepEqual(actual, expected);
  });
});