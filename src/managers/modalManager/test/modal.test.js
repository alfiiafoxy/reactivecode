import Modal from '../Modal.jsx';
import React from 'react';
import { initialState } from '../../themeManager/reducer';
import config from '../../../config/config';
import * as modal from '../modalReducer';

describe('Modal manager', () => {
    const props = {
        config: config,
        theme: initialState,
        modals: modal.initialState,
    };

    it('snapshot created, should rendered correctly', done => {
        const wrapper = shallow(<Modal {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});