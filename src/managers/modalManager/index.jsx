import { connect } from 'react-redux';
import Component from './Modal.jsx';
import * as selectors from '../../selectors/selectors.js';
import * as actions from '../../actions/actions';

const mapStateToProps = state => ({
  config: selectors.getConfig(state),
  theme: selectors.getTheme(state),
  dictionary: selectors.getTranslate(state),
  modals: selectors.getModals(state),
  teachers: selectors.getTeachers(state),
  students: selectors.getAllStudents(state),
  addModalData: selectors.getAddModalData(state),
  locale: selectors.getlocale(state),
  confirmModalData: selectors.getConfirmModalData(state),
});

const mapDispatchToProps = (dispatch) => ({
  toggleModalWindow: payload => dispatch(actions.changeToggleModalWindow(payload)),
  createGroup: payload => dispatch(actions.createGroup(payload)),
  setSelectedTeacher: payload => dispatch(actions.setSelectedTeacher(payload)),
  addStudentInGroup: payload => dispatch(actions.addStudentInGroup(payload)),
  addNewTeacherInGroup: payload => dispatch(actions.addNewTeacherInGroup(payload)),
  deleteGroup: payload => dispatch(actions.deleteGroup(payload)),
  deleteUser: payload => dispatch(actions.deleteUser(payload)),
  deleteTeacherFromGroup: payload => dispatch(actions.deleteTeacherFromGroup(payload)),
  deleteStudentFromGroup: payload => dispatch(actions.deleteStudentFromGroup(payload)),
  deleteLesson: payload => dispatch(actions.deleteLesson(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);

