import { put, takeEvery } from '@redux-saga/core/effects';
import actionTypes from '../../constans/actionTypes';

  export function* watchChangeModal() {
      yield takeEvery(actionTypes.SET_TOGGLE_MODAL, changeModal);
  }

  export function* changeModal(action) {
      yield put({ type: actionTypes.TOGGLE_MODAL_WINDOW, payload: action.payload });
  }