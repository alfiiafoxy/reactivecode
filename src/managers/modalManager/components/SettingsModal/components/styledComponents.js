import styled from 'styled-components';

const defaultModalBgColor = '#ffffff';
const defaultMainTextColor = '#000000';

export const MainContainer = styled.div`
   display: flex;
   justify-content: center;
   flex-direction: column;
   width: 100%;
   height: 100%;
   align-items: center;
   background: ${props => props.modalBgColor ? props.modalBgColor : defaultModalBgColor}
`;

export const DropdownContainerLocal = styled.div`
   display: flex;
   flex: 1;
   flex-direction: column;
   justify-content: center;
   align-items: center;
`;

export const DropdownContainerTheme = styled.div`
   display: flex;
   flex: 1;
   flex-direction: column;
   justify-content: start;
   align-items: center;
`;

export const DropDownName = styled.div`
   display: flex;
   justify-content: center;
   align-items: center;
   font-size: 20px;
   color: ${props => props.mainTextColor ? props.mainTextColor : defaultMainTextColor}
`;

export const Dropdown = styled.div`
   display: flex;
   justify-content: center;
   align-items: center;
   width: 90px;
   height: 30px;
`;