import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import {
  MainContainer,
  DropdownContainerLocal,
  DropdownContainerTheme,
  DropDownName,
  Dropdown,
} from './styledComponents.js';
import CustomDropdown from '../../../../../libs/CustomDropdown/CustomDropdown.jsx';
import actionTypes from '../../../../../constans/actionTypes';

const MainSettings = props => {
  const {
    dictionary,
    locale,
    theme,
  } = props;

  const language = [
    { id: 'en', name: dictionary.resources.english },
    { id: 'ru', name: dictionary.resources.russian },
  ];

  const themeContent = [
    { id: 'light', name: dictionary.resources.light },
    { id: 'dark', name: dictionary.resources.dark },
  ];

  const dropDownRef = {
    local: React.createRef(),
    theme: React.createRef(),
  };

  const dispatch = useDispatch();

  const changeLocal = useCallback(
    () => dispatch({ type: actionTypes.CHANGE_LOCALE, payload: dropDownRef.local.current.value })
      [dispatch],
  );

  const changeTheme = useCallback(
    () => dispatch({ type: actionTypes.CHANGE_THEME, payload: dropDownRef.theme.current.value })
      [dispatch],
  );

  return (
    <MainContainer modalBgColor={theme.currentTheme.modalSettings.mainBgColor}>
      <DropdownContainerLocal>
        <DropDownName
          children={dictionary.resources.language}
          mainTextColor={theme.currentTheme.modalSettings.secondaryText}
        />
        <Dropdown>
          <CustomDropdown
            content={language}
            dropdownRef={dropDownRef.local}
            selectedValue={locale}
            onChangeCallback={changeLocal}
          />
        </Dropdown>
      </DropdownContainerLocal>

      <DropdownContainerTheme>
        <DropDownName
          mainTextColor={theme.currentTheme.modalSettings.secondaryText}
          children={dictionary.resources.theme}
        />
        <Dropdown>
          <CustomDropdown
            content={themeContent}
            dropdownRef={dropDownRef.theme}
            selectedValue={theme.nameTheme}
            onChangeCallback={changeTheme}
          />
        </Dropdown>

      </DropdownContainerTheme>
    </MainContainer>
  );
};

MainSettings.propTypes = {
  theme: PropTypes.object,
  locale: PropTypes.string,
  config: PropTypes.object,
  dictionary: PropTypes.object,
};

export default React.memo(MainSettings);
