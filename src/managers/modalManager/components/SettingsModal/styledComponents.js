import styled from 'styled-components';

const defaultModalBgColor = '#ffffff';
const defaultMainTextColor = '#000000';

export const HeaderModalContainer = styled.div`
   display: flex;
   flex-direction: row;
   justify-content: space-between;
   width: 100%;
   align-items: flex-start;
   border-bottom: 3px solid black;
   margin-bottom: 30px;
   background: ${props => props.modalBgColor ? props.modalBgColor : defaultModalBgColor}
`;

export const Title = styled.h2`
   color: ${props => props.mainTextColor ? props.mainTextColor : defaultMainTextColor}
`;

export const ButtonClose = styled.button`
  width: 46px;
  height: 46px;
  background: none;
  border: none;
  cursor: pointer;
  opacity: 0.55;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  background-repeat: no-repeat;
  
  &:hover {
    opacity: 0.8;
  }
`;

export const defaultStyles = { // For Unit Test
    defaultModalBgColor,
};