import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import { Title, HeaderModalContainer, ButtonClose } from './styledComponents.js';
import { getModalCustomStyle } from './helpers/modalCustomStyle';
import MainSettings from './components/MainSettins.jsx';

const Settings = props => {
  const {
    isOpen,
    toggleModalWindow,
    config,
    dictionary,
    theme,
    locale,
  } = props;

  const getOnClose = () => {
    toggleModalWindow({ type: 'settings', data: null });
  };

  return (
    <Modal
      isOpen={isOpen}
      style={getModalCustomStyle(theme.currentTheme.modalSettings.mainBgColor)}
      onRequestClose={getOnClose}
      ariaHideApp={false}
    >
      <HeaderModalContainer
        modalBgColor={theme.currentTheme.modalSettings.mainBgColor}
      >
        <Title
          mainTextColor={theme.currentTheme.modalSettings.mainTextColor}
        >
          {dictionary.resources.settings}
        </Title>
        <ButtonClose
          data-at={''}
          onClick={getOnClose}
          bgImage={config.srcButtonCloseModal}
        />
      </HeaderModalContainer>
      <MainSettings
        theme={theme}
        locale={locale}
        dictionary={dictionary}
        modalBgColor={theme.currentTheme.modalSettings.mainBgColor}
      />
    </Modal>
  );
};

Settings.propTypes = {
  theme: PropTypes.object,
  isOpen: PropTypes.bool,
  config: PropTypes.object,
  locale: PropTypes.string,
  dictionary: PropTypes.object,
  toggleModalWindow: PropTypes.func,
  customStylesModalWindow: PropTypes.object,
};

export default React.memo(Settings);