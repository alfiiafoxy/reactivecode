import styled from 'styled-components';

const defaultModalBgColor = '#ffffff';

export const HeaderModalForgotContainer = styled.div`
   display: flex;
   flex-direction: row;
   justify-content: space-between;
   width: 100%;
   align-items: flex-start;
   border-bottom: 3px solid black;
   margin-bottom: 30px;
    background: ${props => props.modalBgColor ? props.modalBgColor : defaultModalBgColor}
`;

export const MainModalForgotContainer = styled.div`
    width:100%;
    height: 100vh;
    display: flex;
    flex-direction: column; 
    align-content: center;  
    background: ${props => props.modalBgColor ? props.modalBgColor : defaultModalBgColor}
`;

export const TitleForgotPassword = styled.h2`

`;

export const ButtonCloseForgotPassword = styled.button`
  width: 46px;
  height: 46px;
  background: none;
  border: none;
  cursor: pointer;
  opacity: 0.55;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  
  &:hover {
    opacity: 0.8;
  }
`;

export const defaultStyles = { // For Unit Test
    defaultModalBgColor,
};