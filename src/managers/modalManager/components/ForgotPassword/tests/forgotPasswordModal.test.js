import React from 'react';
import ForgotPasswordModalMemo from '../ForgotPasswordModal.jsx';
import { HeaderModalForgotContainer, defaultStyles } from '../styledComponents.js';
import * as theme from '../../../../themeManager/reducer';
import * as modal from '../../../modalReducer';
import resources from '../../../../localesManager/dictionary/en';
import config from '../../../../../config/config';

const ForgotPasswordModal = ForgotPasswordModalMemo.type;

describe('Forgot password modal', () => {
    const props = {
        config: config,
        theme: theme.initialState,
        isOpen: modal.initialState.forgotPassword.isOpen,
        dictionary: resources,
    };

    it('ForgotPasswordModal create a snapshot, should render correctly', done => {
        const wrapper = shallow(<ForgotPasswordModal {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});

describe('ForgotPasswordModal styled components', () => {
    const props = {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        alignItems: 'flex-start',
        borderBottom: '3px solid black',
        marginBottom: '30px',
        modalBgColor: 'modalBgColor',
    };

    it('HeaderModalForgotContainer should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<HeaderModalForgotContainer {...props}/>);

        expect(component).toHaveStyleRule('display', 'flex');
        expect(component).toHaveStyleRule('flex-direction', 'row');
        expect(component).toHaveStyleRule('justify-content', 'space-between');
        expect(component).toHaveStyleRule('width', '100%');
        expect(component).toHaveStyleRule('align-items', 'flex-start');
        expect(component).toHaveStyleRule('border-bottom', '3px solid black');
        expect(component).toHaveStyleRule('margin-bottom', '30px');
        expect(component).toHaveStyleRule('background', 'modalBgColor');
    });
});