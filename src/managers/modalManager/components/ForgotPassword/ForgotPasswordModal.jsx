import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import { TitleForgotPassword, HeaderModalForgotContainer, MainModalForgotContainer, ButtonCloseForgotPassword } from './styledComponents.js';
import { getModalCustomStyle } from './helpers/modalCustomStyle';

const ForgotPasswordModal = props => {
    const { isOpen, toggleModalWindow, config, dictionary, theme } = props;
    const getOnClose = () => {
      toggleModalWindow({ type: 'forgotPassword', data: null });
  };
    return (
      <Modal
          isOpen={isOpen}
          style={getModalCustomStyle(theme.currentTheme.authPage.modalBgColor)}
          onRequestClose={getOnClose}
          ariaHideApp={false}
      >
          <HeaderModalForgotContainer
              modalBgColor={theme.currentTheme.authPage.modalBgColor}
          >
              <TitleForgotPassword>
                  {dictionary.resources.passwordRecovery}
              </TitleForgotPassword>
              <ButtonCloseForgotPassword
                  data-at={config.dataAttributes.dataAttributeLink.dataAttributeForgotPasswordClose}
                  onClick={getOnClose}
                  bgImage={config.srcButtonCloseModal}
              />
          </HeaderModalForgotContainer>
          <MainModalForgotContainer modalBgColor={theme.currentTheme.authPage.modalBgColor}>
                 main            // FIXME
          </MainModalForgotContainer>
      </Modal>
  );
};

ForgotPasswordModal.propTypes = {
    isOpen: PropTypes.bool,
    config: PropTypes.object,
    theme: PropTypes.object,
    dictionary: PropTypes.object,
    toggleModalWindow: PropTypes.func,
    customStylesModalWindow: PropTypes.object,
};

export default React.memo(ForgotPasswordModal);