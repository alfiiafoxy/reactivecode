import styled from 'styled-components';

const defaultModalBgColor = '#ffffff';

export const InputContainerBlock = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
`;

export const HeaderModalContainer = styled.div`
   display: flex;
   flex-direction: row;
   justify-content: space-between;
   width: 100%;
   align-items: flex-start;
   border-bottom: 3px solid black;
   margin-bottom: 30px;
    background: ${props => props.modalBgColor ? props.modalBgColor : defaultModalBgColor}
`;

export const MainModal = styled.div`
    width:100%;
    height: 100vh;
    display: flex;
    flex-direction: column; 
    align-content: center;  
    background: ${props => props.modalBgColor ? props.modalBgColor : defaultModalBgColor}
`;

export const Title = styled.h2`
  color: ${props => props.titleColor ? props.titleColor : '#000000'};
`;

export const ButtonClose = styled.button`
  width: 46px;
  height: 46px;
  border: none;
  cursor: pointer;
  opacity: 0.55;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  background-repeat: no-repeat;
  
  &:hover {
    opacity: 0.8;
  }
`;

export const Header = styled.div`
  display: flex;
  width: 80%;
  height: 80px;
  justify-content: space-between;
  align-items: center;
`;

export const AddButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.btnContainerWidth ? props.btnContainerWidth : '25%'};
  height: ${props => props.btnContainerHeight ? props.btnContainerHeight : '100%'};
  border: none;
  cursor: pointer;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  background-repeat: no-repeat;
  background-size: 75% 75%;
  background-position: center;
  overflow: hidden;
  
  &:hover {
    opacity: .7;
  }
`;

export const MyGroupContentRow = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

export const GroupElementContainer = styled.div`
  display: flex;
  height: 50px;
  font-size: 16px;
  font-weight: 700;
  box-sizing: border-box;
  border-bottom: 1px solid darkgray;
`;

export const GroupElement = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.widthSize}%;
  box-sizing: border-box;
`;

export const InputContainer = styled.div`
  box-sizing: border-box;
  width: ${props => props.inpContWidth ? props.inpContWidth : '100%'};
  height: 60px;
  margin-bottom: 5px;
  padding: ${props => props.inputPadding ? props.inputPadding : '0'};
`;

export const ListContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const LabelContainer = styled.div`
  width: 100px;
  height: 70px;
  display: flex;
  flex-direction: column;
`;

export const defaultStyles = { // For Unit Test
    defaultModalBgColor,
};