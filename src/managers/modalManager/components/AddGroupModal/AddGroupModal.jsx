import React, { useState, useEffect, useCallback } from 'react';
import Modal from 'react-modal';
import Scrollbar from 'react-scrollbars-custom';
import PropTypes from 'prop-types';
import { getModalCustomStyle } from './helpers/modalCustomStyle';
import {
  AddButtonContainer,
  GroupElement,
  GroupElementContainer,
  InputContainer,
  MyGroupContentRow,
  Title,
  HeaderModalContainer,
  MainModal,
  ButtonClose,
  InputContainerBlock,
} from './styledComponents.js';
import CustomInput from '../../../../libs/customInput/CustomInput.jsx';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';
import CustomDropdown from '../../../../libs/CustomDropdown/CustomDropdown';
import LoaderSpinner from '../../../../libs/Loader/Loader.jsx';
import { getDataFromInputs } from '../../../../utils/helpers/getDataHelpers.js';

const AddGroupModal = props => {
  const {
    isOpen,
    toggleModalWindow,
    dictionary,
    theme,
    users,
    modalData,
    onClickGroup,
    onCLickUsers,
    config,
  } = props;

  const {
    modalBgColor,
    inputBgColor,
    inputBgColorFocus,
    mainTextColor,
    inputText,
    lightText,
    btnBgColor,
    btnBgColorHover,
  } = theme.currentTheme.modalUsers;

  const addGroupModalRef = {
    name: React.createRef(),
    level: React.createRef(),
    city: React.createRef(),
  };

  const getOnClose = useCallback(() => {
    toggleModalWindow({ type: 'addGroupModal', data: { callbackValue: null } });
  }, []);

  const [value, setValue] = useState('');
  const [city, setCity] = useState(config.cityDropdown);

  useEffect(() => {
    const cityTranslate = config.cityDropdown.map(el => {
      el.name = dictionary.resources[el.id];
      return el;
    });
    setCity(cityTranslate);
  }, [dictionary]);

  const getModalTitle = () => {
    let title = '';
    switch (modalData) {
      case 'teacher':
        title = dictionary.resources.addNewGroup;
        break;
      case 'student':
        title = dictionary.resources.addStudentInGroup;
        break;
      default:
        title = dictionary.resources.addTeacherInGroup;
        break;
    }
    return title;
  };

  return (
    <Modal
      isOpen={isOpen}
      style={getModalCustomStyle(modalBgColor)}
      onRequestClose={getOnClose}
      ariaHideApp={false}
    >
      <HeaderModalContainer
        modalBgColor={modalBgColor}
      >
        <Title titleColor={mainTextColor}
               children={getModalTitle()}/>
        <ButtonClose
          data-at={config.dataAttributes.dataAttributesAddModal.btnClose}
          onClick={getOnClose}
          bgImage={config.srcButtonCloseModal}
        />
      </HeaderModalContainer>
      <MainModal modalBgColor={theme.currentTheme.authPage.modalBgColor}>
        {modalData === 'teacher' ?
          <InputContainerBlock>
            <InputContainer inpContWidth={'25%'} inputPadding={'5px'}>
              <CustomInput
                inputRef={addGroupModalRef.name}
                fontSizeInp={18}
                color={inputText}
                backgroundColor={inputBgColor}
                backgroundFocusColor={inputBgColorFocus}
                dataAttributeInput={config.dataAttributes.dataAttributesAddModal.groupName}
                placeholderText={dictionary.placeholders.enterNewGroupName}
                requiredInput={'required'}
              />
            </InputContainer>
            <InputContainer inpContWidth={'25%'} inputPadding={'5px'}>
              <CustomDropdown
                backgroundColor={inputBgColor}
                color={inputText}
                focusColor={inputBgColorFocus}
                dropdownRef={addGroupModalRef.level}
                idDropdown={'city'}
                dataAttributeDropdown={config.dataAttributes.dataAttributesAddModal.groupLevel}
                selectedValue={config.englishLevel[0].name}
                content={config.englishLevel}
              />
            </InputContainer>
            <InputContainer inpContWidth={'25%'} inputPadding={'5px'}>
              <CustomDropdown
                backgroundColor={inputBgColor}
                color={inputText}
                focusColor={inputBgColorFocus}
                dropdownRef={addGroupModalRef.city}
                idDropdown={'city'}
                dataAttributeDropdown={config.dataAttributes.dataAttributesAddModal.groupCity}
                selectedValue={city[0].name}
                content={city}
              />
            </InputContainer>
            <AddButtonContainer btnContainerWidth={'25%'}>
              <AddButtonContainer
                btnContainerWidth={'90%'}
                btnContainerHeight={'90%'}
              >
                <CustomButton
                  backgroundColor={btnBgColor}
                  dataAttribute={config.dataAttributes.dataAttributesAddModal.btnCreateGroup}
                  buttonName={dictionary.resources.createGroup}
                  onClickCallback={() => onClickGroup(getDataFromInputs(addGroupModalRef))}
                  backgroundColorHover={btnBgColorHover}
                  borderRadius={5}
                  fontSize={16}
                />
              </AddButtonContainer>
            </AddButtonContainer>
          </InputContainerBlock>
          : null}
        <InputContainer inputPadding={'5px'}>
          <CustomInput
            fontSizeInp={'18px'}
            color={inputText}
            backgroundColor={inputBgColor}
            backgroundFocusColor={inputBgColorFocus}
            dataAttributeInput={config.dataAttributes.dataAttributesAddModal.searchTeacher}
            placeholderText={modalData === 'teacher' || modalData === 'addNewTeacher' ? dictionary.placeholders.searchTeacher : dictionary.placeholders.searchStudent}
            inputCallback={event => setValue(event.target.value)}
          />
        </InputContainer>
        <GroupElementContainer>
          {config.myGroupData.usersCaption.map((item, index) => {
            return (
              <GroupElement
                key={index}
                last={item.idCaption === 'addButton'}
                widthSize={item.widthSize}
                children={dictionary.resources[item.resourcesKey]}
              />
            );
          })}
        </GroupElementContainer>
        <Scrollbar
          style={{ height: 300, boxSizing: 'border-box', marginRight: '-20px' }}
          noScrollX={false}
        >
          {users && users.length ? users.filter(item => item.first_name.startsWith(value) && !item.groups.length).map((el, i) => {
                return (
                  <MyGroupContentRow key={i}>
                    <GroupElement widthSize={25} children={el.first_name}/>
                    <GroupElement widthSize={25} children={el.last_name}/>
                    <GroupElement widthSize={25} children={el.email}/>
                    <AddButtonContainer btnContainerWidth={'25%'}>
                      <CustomButton
                        backgroundImage={config.images.addUser}
                        onClickCallback={() => onCLickUsers(el.user_id)}
                        dataAttribute={config.dataAttributes.dataAttributesAddModal.btnAddStudent}
                      />
                    </AddButtonContainer>
                  </MyGroupContentRow>
                );
              })
              :
              <LoaderSpinner
                loaderType={'Rings'}
                loaderColor={lightText}
                loaderHeight={80}
                loaderWidth={80}
              />
          }
        </Scrollbar>
      </MainModal>
    </Modal>
  );
};

AddGroupModal.propTypes = {
  isOpen: PropTypes.bool,
  config: PropTypes.object,
  theme: PropTypes.object,
  dictionary: PropTypes.object,
  toggleModalWindow: PropTypes.func,
  customStylesModalWindow: PropTypes.object,
  users: PropTypes.array,
  onClickCallback: PropTypes.func,
  modalData: PropTypes.string,
  onClickGroup: PropTypes.func,
  onCLickUsers: PropTypes.func,
};

export default React.memo(AddGroupModal);
