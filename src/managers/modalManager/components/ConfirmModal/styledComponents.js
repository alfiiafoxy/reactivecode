import styled from 'styled-components';

const defaultModalBgColor = '#ffffff';

export const HeaderModalContainer = styled.div`
   display: flex;
   flex-direction: row;
   justify-content: space-between;
   width: 100%;
   align-items: flex-start;
   border-bottom: 3px solid black;
   margin-bottom: 5 px;
   background: ${props => props.modalBgColor ? props.modalBgColor : defaultModalBgColor}
`;

export const MainModal = styled.div`
    width:100%;
    height: 100vh;
    display: flex;
    flex-direction: column; 
    align-content: center;
    justify-content: space-around;  
    background: ${props => props.modalBgColor ? props.modalBgColor : defaultModalBgColor}
`;

export const Title = styled.h2`
  color: ${props => props.titleColor ? props.titleColor : '#000000'};
`;

export const ButtonClose = styled.button`
  width: 46px;
  height: 46px;
  border: none;
  cursor: pointer;
  opacity: 0.55;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  background-repeat: no-repeat;
  
  &:hover {
    opacity: 0.8;
  }
`;

export const Header = styled.div`
  display: flex;
  width: 80%;
  height: 80px;
  justify-content: space-between;
  align-items: center;
`;

export const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  width: ${props => props.btnContainerWidth ? props.btnContainerWidth : '25%'};
  height: ${props => props.btnContainerHeigth ? props.btnContainerHeigth : '100%'};
  border: none;
  cursor: pointer;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  background-repeat: no-repeat;
  background-size: 75% 75%;
  background-position: center;
  overflow: hidden;
  :
`;

export const LabelContainer = styled.div`
  width: auto;
  height: auto;
  display: flex;
  align-content: center;  
  align-items: center;
  justify-content: center;
  font-size: 20px;
`;

export const defaultStyles = {
    defaultModalBgColor,
};
