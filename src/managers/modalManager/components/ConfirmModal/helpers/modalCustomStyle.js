export const getModalCustomStyle = bgColor => {
    return {
       content: {
           display: 'flex',
           flexDirection: 'column',
           alignItems: 'center',
           width: '400px',
           height: '300px',
           top: '50%',
           left: '50%',
           right: 'auto',
           bottom: 'auto',
           marginRight: '-50%',
           transform: 'translate(-50%, -50%)',
           backgroundColor: `${bgColor}`,
       },
   };
};