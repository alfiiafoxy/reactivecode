import React, { useCallback } from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import { getModalCustomStyle } from './helpers/modalCustomStyle';
import {
  ButtonClose,
  HeaderModalContainer,
  Title,
  MainModal,
  LabelContainer,
  ButtonContainer,
} from './styledComponents.js';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';

const ConfirmModal = props => {
  const {
    isOpen,
    toggleModalWindow,
    dictionary,
    theme,
    message,
    functionConfirm,
    config,
    attribute,
  } = props;

  const {
    modalBgColor,
    mainTextColor,
    btnConfirmBgColor,
    btnDeclineBgColor,
    secondaryText,
    borderColor,
    titleColor,
    btnBgColorHover,
    btnBgColorHoverDecline,
  } = theme.currentTheme.modalConfirm;

  const getOnClose = useCallback(() => {
    toggleModalWindow({ type: 'confirmModal', data: { callbackValue: null, data: null } });
  }, []);

  const getConfirm = () => {
    attribute ? functionConfirm(attribute) : functionConfirm();
  };

  return (
    <Modal
      isOpen={isOpen}
      style={getModalCustomStyle(modalBgColor)}
      onRequestClose={getOnClose}
      ariaHideApp={false}
    >
      <HeaderModalContainer
        modalBgColor={modalBgColor}
      >
        <Title titleColor={titleColor}
               children={dictionary.resources.confirmTitle}/>
        <ButtonClose
          data-at={''}
          onClick={getOnClose}
          bgImage={config.srcButtonCloseModal}
        />
      </HeaderModalContainer>
      <MainModal modalBgColor={theme.currentTheme.authPage.modalBgColor}>
        <LabelContainer children={message}/>
        <ButtonContainer btnContainerWidth={'100%'} btnContainerHeigth={'30px'}>
          <ButtonContainer
            btnContainerWidth={'120px'}
          >
            <CustomButton
              backgroundColor={btnConfirmBgColor}
              dataAttribute={''}
              buttonName={dictionary.resources.confirm}
              onClickCallback={getConfirm}
              borderRadius={4}
              fontSize={16}
              textColor={mainTextColor}
              backgroundColorHover={btnBgColorHover}
            />
          </ButtonContainer>
          <ButtonContainer
            btnContainerWidth={'120px'}
          >
            <CustomButton
              backgroundColor={btnDeclineBgColor}
              dataAttribute={''} //todo attribute
              buttonName={dictionary.resources.decline}
              onClickCallback={getOnClose}
              border={`1px solid ${borderColor}`}
              borderRadius={4}
              fontSize={16}
              textColor={secondaryText}
              backgroundColorHover={btnBgColorHoverDecline}
            />
          </ButtonContainer>
        </ButtonContainer>
      </MainModal>
    </Modal>
  );
};

ConfirmModal.propTypes = {
  isOpen: PropTypes.bool,
  config: PropTypes.object,
  theme: PropTypes.object,
  dictionary: PropTypes.object,
  toggleModalWindow: PropTypes.func,
  customStylesModalWindow: PropTypes.object,
  users: PropTypes.array,
  onClickCallback: PropTypes.func,
  modalData: PropTypes.string,
  onClickGroup: PropTypes.func,
  onCLickUsers: PropTypes.func,
  functionConfirm: PropTypes.func,
  message: PropTypes.string,
  attribute: PropTypes.object,
};

export default React.memo(ConfirmModal);
