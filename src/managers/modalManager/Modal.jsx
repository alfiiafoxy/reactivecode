import React from 'react';
import PropTypes from 'prop-types';
import Settings from './components/SettingsModal/Settings.jsx';
import ForgotPasswordModal from './components/ForgotPassword/ForgotPasswordModal.jsx';
import AddGroupModal from './components/AddGroupModal/AddGroupModal.jsx';
import ConfirmModal from './components/ConfirmModal/ConfirmModal';

export default class Modal extends React.PureComponent {
  render() {
    const {
      modals,
      toggleModalWindow,
      config,
      theme,
      dictionary,
      teachers,
      addModalData,
      students,
      createGroup,
      setSelectedTeacher,
      locale,
      addStudentInGroup,
      addNewTeacherInGroup,
      confirmModalData,
      deleteGroup,
      deleteUser,
      deleteTeacherFromGroup,
      deleteStudentFromGroup,
      deleteLesson,
    } = this.props;

    const getAddModalData = () => {
      let users = [];
      let callback = null;
      if (addModalData.callbackValue === 'teacher') {
        users = teachers;
        callback = setSelectedTeacher;
      } else if (addModalData.callbackValue === 'student') {
        users = students;
        callback = addStudentInGroup;
      } else if (addModalData.callbackValue === 'addNewTeacher') {
        users = teachers;
        callback = addNewTeacherInGroup;
    }
      return { users: users, callback: callback };
    };

    const getConfirmModalData = () => {
     const { callbackValue, attribute } = confirmModalData;
      let callback = null;
      let message = null;
      if (callbackValue === 'deleteGroup') {
        callback = deleteGroup;
        message = dictionary.confirmMessage.deleteGroup;
      } else if (callbackValue === 'deleteUser') {
        callback = deleteUser;
        message = dictionary.confirmMessage.deleteUser;
      } else if (callbackValue === 'deleteTeacherFromGroup') {
        callback = deleteTeacherFromGroup;
        message = dictionary.confirmMessage.deleteTeacherFromGroup;
      } else if (callbackValue === 'deleteStudentFromGroup') {
        callback = deleteStudentFromGroup;
        message = dictionary.confirmMessage.deleteStudentFromGroup;
      } else if (callbackValue === 'deleteLesson') {
        callback = deleteLesson;
        message = dictionary.confirmMessage.deleteLesson;
      }
      return { message, callback, attribute };
    };

    return (
      <React.Fragment>
        <Settings
          isOpen={modals.settings.isOpen}
          toggleModalWindow={toggleModalWindow}
          customStylesModalWindow={modals.customStylesModalWindow}
          config={config}
          theme={theme}
          dictionary={dictionary}
          locale={locale}
        />
        <ForgotPasswordModal
          isOpen={modals.forgotPassword.isOpen}
          toggleModalWindow={toggleModalWindow}
          customStylesModalWindow={modals.customStylesModalWindow}
          config={config}
          theme={theme}
          dictionary={dictionary}
        />
        <AddGroupModal
          modalData={addModalData.callbackValue}
          isOpen={modals.addGroupModal.isOpen}
          toggleModalWindow={toggleModalWindow}
          customStylesModalWindow={modals.customStylesModalWindow}
          config={config}
          theme={theme}
          dictionary={dictionary}
          onClickGroup={createGroup}
          onCLickUsers={getAddModalData().callback}
          users={getAddModalData().users}
        />
        <ConfirmModal
          isOpen={modals.confirmModal.isOpen}
          toggleModalWindow={toggleModalWindow}
          customStylesModalWindow={modals.customStylesModalWindow}
          config={config}
          theme={theme}
          dictionary={dictionary}
          functionConfirm={getConfirmModalData().callback}
          message={getConfirmModalData().message}
          attribute={getConfirmModalData().attribute}
        />
      </React.Fragment>
    );
  }
}

Modal.propTypes = {
  modals: PropTypes.object,
  config: PropTypes.object,
  theme: PropTypes.object,
  dictionary: PropTypes.object,
  toggleModalWindow: PropTypes.func,
  teachers: PropTypes.array,
  addModalData: PropTypes.object,
  students: PropTypes.array,
  createGroup: PropTypes.func,
  setSelectedTeacher: PropTypes.func,
  locale: PropTypes.string,
  addStudentInGroup: PropTypes.func,
  addNewTeacherInGroup: PropTypes.func,
  confirmModalData: PropTypes.object,
  deleteGroup: PropTypes.func,
  deleteUser: PropTypes.func,
  deleteTeacherFromGroup: PropTypes.func,
  deleteStudentFromGroup: PropTypes.func,
  deleteLesson: PropTypes.func,
};