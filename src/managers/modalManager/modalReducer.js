import config from '../../config/config';
import actionTypes from '../../constans/actionTypes';

export const initialState = {
  settings: {
    type: 'settings',
    data: null,
    isOpen: config.modalsIsOpen.settings,
  },
  forgotPassword: {
    type: 'forgotPassword',
    data: null,
    isOpen: config.modalsIsOpen.forgotPassword,
  },
  addGroupModal: {
    type: 'addGroupModal',
    data: {
      callbackValue: null,
    },
    isOpen: config.modalsIsOpen.addGroupModal,
  },
  confirmModal: {
    type: 'confirmModal',
    data: {
      callbackValue: null,
      attribute: null,
    },
    isOpen: config.modalsIsOpen.confirmModal,
  },
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_MODAL_WINDOW:
      return {
        ...state,
        [action.payload.type]: {
          ...state[action.payload.type],
          data: action.payload.data,
          isOpen: !state[action.payload.type].isOpen,
        },
      };
    default:
      return state;
  }
};