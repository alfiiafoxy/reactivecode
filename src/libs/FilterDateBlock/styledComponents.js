import styled from 'styled-components';

const widthGroupElemDefault = '100%';
const heightSchGroupElemDefault = '80%';
const groupElementBgDefault = 'transparent';

export const InputWrapper = styled.div`
   height: 100%;
   display: flex;
   `;

export const ButtonWrapper = styled.div`
  width: 20px;
  height: 20px;
  box-sizing: border-box;
`;

export const GroupElement = styled.div`
  width: ${props => props.widthSchGroupElem ? props.widthSchGroupElem : widthGroupElemDefault};
  height: ${props => props.heightSchGroupElem ? props.heightSchGroupElem : heightSchGroupElemDefault};
  margin: 5px 0;
  background: ${props => props.schGroupElementBg ? props.schGroupElementBg : groupElementBgDefault}; 
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
