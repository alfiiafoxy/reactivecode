import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import PropTypes from 'prop-types';
import { InputWrapper, ButtonWrapper, GroupElement } from './styledComponents.js';
import CustomInput from '../customInput/CustomInput.jsx';
import CustomButton from '../customButton/CustomButton.jsx';
import actionTypes from '../../constans/actionTypes';

const FilterDateBlock = props => {
  const {
    backgroundColor,
    backgroundFocusColor,
    colorInput,
    firstSelectWeekDay,
    lastSelectWeekDay,
    config,
  } = props;

  const refControlPanel = {
    searchStartDate: React.createRef(),
    searchEndDate: React.createRef(),
  };

  const dispatch = useDispatch();

  const getFirstDayThisWeek = day => moment().day(day).format('YYYY-MM-DD');

  const getLastWeek = useCallback(
    () => dispatch({ type: actionTypes.GET_PREV_WEEK, payload: { firstSelectWeekDay, lastSelectWeekDay } }),
    [firstSelectWeekDay, lastSelectWeekDay],
  );

  const getNextWeek = useCallback(
    () => dispatch({ type: actionTypes.GET_NEXT_WEEK, payload: { firstSelectWeekDay, lastSelectWeekDay } }),
    [firstSelectWeekDay, lastSelectWeekDay],
  );

  const setSelectedFirstDayGroupUI = useCallback(
    e => dispatch({ type: actionTypes.SET_DATE_GROUP_UI_FROM, payload: e.currentTarget.value }),
    [firstSelectWeekDay, lastSelectWeekDay],
  );

  const setSelectedLastDayGroupUI = useCallback(
    e => dispatch({ type: actionTypes.SET_DATE_GROUP_UI_TO, payload: e.currentTarget.value }),
    [firstSelectWeekDay, lastSelectWeekDay],
  );

  return (
      <GroupElement>
        <ButtonWrapper
          schButtonWrapperRotate={'-135deg'}
          schButtonWrapperTop={'6px'}
          schButtonWrapperLeft={'10px'}
        >
          <CustomButton
            dataAttribute={config.dataAttributes.dataAttributeSchedule.previousWeekBtn}
            onClickCallback={getLastWeek}
            backgroundImage={config.srcButtonArrowLeft}
            backgroundColorHover={config.srcButtonArrowLeft}
          />
        </ButtonWrapper>
        <GroupElement>
          {config.filterDateInputs.map(item =>
            <InputWrapper key={Math.random()}>
            <CustomInput
              inputRef={refControlPanel[item.idInput]}
              inputType={item.inputType}
              inputCallback={item.inputName === 'firstDay' ? setSelectedFirstDayGroupUI : setSelectedLastDayGroupUI}
              requiredInput={'required'}
              dataAttributeInput={''}//TODO
              backgroundColor={backgroundColor}
              borderRadius={4}
              backgroundFocusColor={backgroundFocusColor}
              intCursor={'pointer'}
              inputText={item.inputName === 'firstDay' ? getFirstDayThisWeek(firstSelectWeekDay) : getFirstDayThisWeek(lastSelectWeekDay)}
              color={colorInput}
              textColor={'transparent'}
            />
          </InputWrapper>)}
        </GroupElement>
        <ButtonWrapper
          schButtonWrapperRotate={'45deg'}
          schButtonWrapperTop={'6px'}
          schButtonWrapperLeft={'0px'}
        >
          <CustomButton
            dataAttribute={config.dataAttributes.dataAttributeSchedule.nextWeekBtn}
            onClickCallback={getNextWeek}
            backgroundImage={config.srcButtonArrowRight}
            backgroundColorHover={config.srcButtonArrowRight}
          />
        </ButtonWrapper>
      </GroupElement>
  );
};

FilterDateBlock.propTypes = {
  backgroundColor: PropTypes.string,
  backgroundFocusColor: PropTypes.string,
  colorInput: PropTypes.string,
  firstSelectWeekDay: PropTypes.number,
  lastSelectWeekDay: PropTypes.number,
  config: PropTypes.object,
};

export default React.memo(FilterDateBlock);
