import React from 'react';
import PropTypes from 'prop-types';
import { BlockWrapper } from './styledComponents.js';
import CustomLabel from '../customLabel/CustomLabel.jsx';
import CustomInput from '../customInput/CustomInput.jsx';
import ErrorBlock from '../errorBlock/ErrorBlock.jsx';

const AuthRegBlock = props => {
  const {
    fontSizeLabel,
    textColorLabel,
    fontWeightLabel,
    dataAttributeLabel,
    textTransformLabel,
    labelName,
    htmlFor,
    idInput,
    inputRef,
    inputType,
    inputCallback,
    onKeyDownCallback,
    onFocusCallback,
    requiredInput,
    placeholderText,
    isDisabled,
    dataAttributeInput,
    backgroundColor,
    borderRadius,
    backgroundFocusColor,
    focusColor,
    inputCursor,
    colorInput,
    errorMessage,
    textColor,
    errorRef,
    marginBlock,
    textAlign,
    inputText,
    dataAttributeError,
  } = props;

  return (
    <BlockWrapper>
      <CustomLabel
        dataAttributeLabel={dataAttributeLabel}
        htmlFor={htmlFor}
        textTransform={textTransformLabel}
        textColor={textColorLabel}
        fontWeight={fontWeightLabel}
        labelName={`${labelName}:`}
        fontSize={fontSizeLabel}
      />
      <CustomInput
        idInput={idInput}
        inputRef={inputRef}
        inputType={inputType}
        inputCallback={inputCallback}
        onKeyDownCallback={onKeyDownCallback}
        onFocusCallback={onFocusCallback}
        requiredInput={requiredInput}
        placeholderText={placeholderText}
        isDisabled={isDisabled}
        dataAttributeInput={dataAttributeInput}
        backgroundColor={backgroundColor}
        borderRadius={borderRadius}
        backgroundFocusColor={backgroundFocusColor}
        focusColor={focusColor}
        intCursor={inputCursor}
        color={colorInput}
        inputText={inputText}
      />
      <ErrorBlock
        errorRef={errorRef}
        errorMessage={errorMessage}
        marginBlock={marginBlock}
        textAlign={textAlign}
        textColor={textColor}
        dataAttributeError={dataAttributeError}
      />
    </BlockWrapper>
  );
};

AuthRegBlock.propTypes = {
  fontSizeLabel: PropTypes.string,
  textColorLabel: PropTypes.string,
  fontWeightLabel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  dataAttributeLabel: PropTypes.string,
  textTransformLabel: PropTypes.string,
  labelName: PropTypes.string.isRequired,
  htmlFor: PropTypes.string.isRequired,
  idInput: PropTypes.string,
  inputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.elementType }),
  ]),
  inputType: PropTypes.string,
  inputCallback: PropTypes.func,
  requiredInput: PropTypes.string,
  placeholderText: PropTypes.string,
  isDisabled: PropTypes.bool,
  dataAttributeInput: PropTypes.string,
  backgroundColor: PropTypes.string,
  borderRadius: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backgroundFocusColor: PropTypes.string,
  focusColor: PropTypes.string,
  inputCursor: PropTypes.string,
  colorInput: PropTypes.string,
  errorMessage: PropTypes.string,
  textColor: PropTypes.string,
  errorRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.elementType }),
  ]),
  onKeyDownCallback: PropTypes.func,
  onFocusCallback: PropTypes.func,
  marginBlock: PropTypes.string,
  textAlign: PropTypes.string,
  inputText: PropTypes.string,
  dataAttributeError: PropTypes.string,
};

export default React.memo(AuthRegBlock);

