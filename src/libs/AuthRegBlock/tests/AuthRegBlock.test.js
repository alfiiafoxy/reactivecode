import AuthRegBlockMemo from '../AuthRegBlock.jsx';
import { BlockWrapper } from '../styledComponents.js';

const AuthRegBlock = AuthRegBlockMemo.type;

describe('AuthRegBlock components', () => {
    let props;
    let sandbox;

    before(() => {
        sandbox = sinon.createSandbox();
        props = {
            labelName: 'labelName',
            htmlFor: 'htmlFor',
        };
    });

    after(() => {
        sandbox.restore();
        sandbox.reset();
    });

    it('AuthRegBlock create a snapshot, should render correctly', () => {
        const wrapper = shallow(<AuthRegBlock {...props}/>);

        expect(wrapper).matchSnapshot();
    });
});

describe('BlockWrapper styled components', () => {
    it('BlockWrapper should have flex-box style', () => {
        const component = getTreeSC(<BlockWrapper />);

        expect(component).toHaveStyleRule('display', 'flex');
        expect(component).toHaveStyleRule('flex-direction', 'column');
    });
});
