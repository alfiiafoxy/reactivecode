import CustomLinkMemo from '../CustomLink.jsx';
import { Link, defaultStyles } from '../styledComponents.js';
import React from 'react';

const CustomLink = CustomLinkMemo.type;

describe('CustomLink components', () => {
  let props;
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
    props = {
      dataAttribute: 'dataAttribute',
      backgroundColor: 'backgroundColor',
      onClickCallback: () => true,
      fontSize: 12,
      hrefValue: 'hrefValue',
      targetValue: 'targetValue',
      titleValue: 'titleValue',
      linkName: 'linkName',
      textColor: 'textColor',
      boxShadow: 'boxShadow',
      fontWeight: 700,
      textTransform: 'textTransform',
      backgroundColorHover: 'backgroundColorHover',
    };
  });

  afterEach(() => {
    sandbox.restore();
    sandbox.reset();
  });

  it('CustomLink create a snapshot, should render correctly', () => {
    const wrapper = shallow(<CustomLink {...props}/>);

    expect(wrapper).matchSnapshot();
  });

  it('CustomLink should be disabled', () => {
    const localProps = { ...props, isDisabled: true };
    const component = shallow(<CustomLink {...localProps}/>);

    assert.isTrue(component.find(Link).props().disabled);
  });

  it('CustomLink has href attribute', () => {
    const component = shallow(<CustomLink {...props}/>);

    assert.equal(component.find(Link).props().href, 'hrefValue');
  });

  it('CustomLink has target attribute', () => {
    const component = shallow(<CustomLink {...props}/>);

    assert.equal(component.find(Link).props().target, 'targetValue');
  });

  it('CustomLink should call onClickCallback', () => {
    const localProps = { ...props, onClickCallback: sandbox.stub() };
    const component = shallow(<CustomLink {...localProps}/>);
    component.find(Link).simulate('click');

    sinon.assert.calledOnce(localProps.onClickCallback);
  });
});

describe('CustomLink styled components', () => {
  const props = {
    fntSize: 12,
    bgColor: 'backgroundColor',
    bxShadow: 'boxShadow',
    txtColor: 'textColor',
    fntWeight: 500,
    bgColorHover: 'backgroundColorHover',
    txtColorHover: 'txtColorHover',
    txtColorDisabled: 'txtColorDisabled',
  };

  it('Link should have correct styles when all props were transferred', () => {
    const component = getTreeSC(<Link {...props}/>);

    expect(component).toHaveStyleRule('color', 'textColor');
    expect(component).toHaveStyleRule('font-size', '12px');
    expect(component).toHaveStyleRule('background', 'backgroundColor');
    expect(component).toHaveStyleRule('font-weight', '500');
    expect({
      component,
      modifier: '&:active',
    }).toHaveStyleRule('box-shadow', 'boxShadow');
    expect({
      component,
      modifier: '&:hover',
    }).toHaveStyleRule('color', 'txtColorHover');
    expect({
      component,
      modifier: '&:hover',
    }).toHaveStyleRule('background', 'backgroundColorHover');
    expect({
      component,
      modifier: '&:disabled',
    }).toHaveStyleRule('background', defaultStyles.bgColorDisabled);
    expect({
      component,
      modifier: '&:disabled',
    }).toHaveStyleRule('color', 'txtColorDisabled');
  });

  it('Link should have default styles when all props weren\'t transferred', () => {
    const component = getTreeSC(<Link/>);

    expect(component).toHaveStyleRule('color', defaultStyles.txtColorDefault);
    expect(component).toHaveStyleRule('font-size', `${defaultStyles.fntSizeDefault}px`);
    expect(component).toHaveStyleRule('background', defaultStyles.bgColorDefault);
    expect(component).toHaveStyleRule('font-weight', defaultStyles.fontWeightDefault);
    expect({
      component,
      modifier: '&:active',
    }).toHaveStyleRule('box-shadow', defaultStyles.boxShadowDefault);
    expect({
      component,
      modifier: '&:disabled',
    }).toHaveStyleRule('background', defaultStyles.bgColorDisabled);
  });
});
