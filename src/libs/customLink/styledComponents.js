import styled from 'styled-components';

const bgColorDisabled = 'transparent';
const bgColorDefault = 'transparent';
const fntSizeDefault = 10;
const txtColorDefault = '#ffffff';
const brRadiusDefault = 0;
const fontWeightDefault = 'regular';
const bgColorHoverDefault = 'transparent';
const txtColorHoverDefault = '#a2a2a2';
const txtColorDisabledDefault = '#6f6f6f';
const boxShadowDefault = 'none';
const textDecorationDefault = 'underLine';
const txtTransformDefault = 'none';

export const Link = styled.a`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: ${props => props.bgColor ? props.bgColor : bgColorDefault};
  color: ${props => props.txtColor ? props.txtColor : txtColorDefault};
  font-size: ${props => props.fntSize ? props.fntSize : fntSizeDefault}px;
  font-weight: ${props => props.fntWeight ? props.fntWeight : fontWeightDefault};  
  text-decoration: ${props => props.textDecoration ? props.textDecoration : textDecorationDefault};  
  text-transform: ${props => props.txtTransform ? props.txtTransform : txtTransformDefault};  
    
  &:hover {
    cursor: pointer;
    color: ${props => props.txtColorHover ? props.txtColorHover : txtColorHoverDefault};
    background: ${props => props.bgColorHover ? props.bgColorHover : bgColorHoverDefault};
  }

  &:active {
   box-shadow: ${props => props.bxShadow ? props.bxShadow : boxShadowDefault};
  }
  
  &:disabled {
    opacity: 0.5;
    color: ${props => props.txtColorDisabled ? props.txtColorDisabled : txtColorDisabledDefault};
    cursor: no-drop;
    user-select: none;
    background: ${bgColorDisabled};
    box-shadow: none;
  }
`;

export const defaultStyles = { // For Unit Test
    bgColorDefault,
    fntSizeDefault,
    txtColorDefault,
    brRadiusDefault,
    bgColorDisabled,
    fontWeightDefault,
    bgColorHoverDefault,
    txtColorHoverDefault,
    txtColorDisabledDefault,
    boxShadowDefault,
};
