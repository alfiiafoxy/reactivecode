import React from 'react';
import PropTypes from 'prop-types';
import { Link } from './styledComponents.js';

const CustomLink = props => {
  const {
    linkName,
    fontSize,
    boxShadow,
    textColor,
    hrefValue,
    isDisabled,
    fontWeight,
    titleValue,
    targetValue,
    txtDecoration,
    dataAttribute,
    textTransform,
    textColorHover,
    onClickCallback,
    textColorDisabled,
  } = props;

  return <Link
      data-at={dataAttribute}
      onClick={onClickCallback}
      fntSize={fontSize}
      disabled={isDisabled}
      href={hrefValue}
      target={targetValue}
      title={titleValue}
      children={linkName}
      txtColor={textColor}
      bxShadow={boxShadow}
      fntWeight={fontWeight}
      textDecoration={txtDecoration}
      txtTransform={textTransform}
      txtColorHover={textColorHover}
      txtColorDisabled={textColorDisabled}
    />;
};

CustomLink.propTypes = {
  fontSize: PropTypes.number,
  isDisabled: PropTypes.bool,
  hrefValue: PropTypes.string,
  targetValue: PropTypes.string,
  titleValue: PropTypes.string,
  boxShadow: PropTypes.string,
  textColorHover: PropTypes.string,
  textColorDisabled: PropTypes.string,
  textColor: PropTypes.string,
  fontWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  linkName: PropTypes.string.isRequired,
  dataAttribute: PropTypes.string,
  textTransform: PropTypes.string,
  onClickCallback: PropTypes.func.isRequired,
  txtDecoration: PropTypes.string,
};

CustomLink.defaultProps = {
  isDisabled: false,
};

export default React.memo(CustomLink);
