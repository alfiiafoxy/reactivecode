import styled from 'styled-components';

const bgColorDefault = 'transparent';
const fntSizeDefault = '16px';
const txtColorDefault = '#000000';
const bxShadowDefault = 'none';
const widthSizeDefault = 'auto';
const fontWeightDefault = 'normal';
const txtTransformDefault = 'none';
const bgColorHoverDefault = 'transparent';
const borderLabelDefault = 'none';
const borderLabelHoverDefault = 'none';

export const Label = styled.label`
  margin-bottom: 5px;
  width: ${props => props.widthSize ? `${props.widthSize}px` : widthSizeDefault};
  background: ${props => props.bgColor ? props.bgColor : bgColorDefault};
  color: ${props => props.txtColor ? props.txtColor : txtColorDefault};
  font-size: ${props => props.fntSize ? props.fntSize : fntSizeDefault};
  font-weight: ${props => props.fntWeight ? props.fntWeight : fontWeightDefault};
  text-transform: ${props => props.txtTransform ? props.txtTransform : txtTransformDefault};    
  border: ${props => props.borderLabel ? props.borderLabel : borderLabelDefault};
  
  &:hover {
    cursor: pointer;
    box-shadow: ${props => props.bxShadow ? props.bxShadow : bxShadowDefault};
    background: ${props => props.bgColorHover ? props.bgColorHover : bgColorHoverDefault};
    border: ${props => props.borderLabelHover ? props.borderLabelHover : borderLabelHoverDefault};
  }
`;

export const defaultStyles = { // For Unit Test
    bgColorDefault,
    fntSizeDefault,
    txtColorDefault,
    bxShadowDefault,
    fontWeightDefault,
    borderLabelDefault,
    txtTransformDefault,
    bgColorHoverDefault,
    borderLabelHoverDefault,
};
