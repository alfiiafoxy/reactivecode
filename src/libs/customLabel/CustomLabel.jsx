import React from 'react';
import PropTypes from 'prop-types';
import { Label } from './styledComponents.js';

const CustomLabel = props => {
    const {
        fontSize,
        boxShadow,
        textColor,
        fontWeight,
        dataAttributeLabel,
        textTransform,
        backgroundColor,
        labelName,
        widthSize,
        htmlFor,
        backgroundColorHover,
        borderLabel,
        borderLabelHover,
    } = props;

    return (
        <Label
            data-at={dataAttributeLabel}
            bgColor={backgroundColor}
            htmlFor ={htmlFor}
            fntSize={fontSize}
            txtColor={textColor}
            children={labelName}
            bxShadow={boxShadow}
            fntWeight={fontWeight}
            widthSize={widthSize}
            txtTransform={textTransform}
            bgColorHover={backgroundColorHover}
            borderLabel={borderLabel}
            borderLabelHover={borderLabelHover}
        />
    );
};

CustomLabel.propTypes = {
    widthSize: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    borderLabel: PropTypes.string,
    borderLabelHover: PropTypes.string,
    fontSize: PropTypes.string,
    boxShadow: PropTypes.string,
    textColor: PropTypes.string,
    fontWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    dataAttributeLabel: PropTypes.string,
    textTransform: PropTypes.string,
    backgroundColor: PropTypes.string,
    backgroundColorHover: PropTypes.string,
    htmlFor: PropTypes.string,
    labelName: PropTypes.string,
};

export default React.memo(CustomLabel);
