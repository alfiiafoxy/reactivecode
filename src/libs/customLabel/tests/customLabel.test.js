import CustomLabelMemo from '../CustomLabel.jsx';
import { Label, defaultStyles } from '../styledComponents.js';

const CustomLabel = CustomLabelMemo.type;

describe('CustomLabel components', () => {
    let props;
    let sandbox;

    before(() => {
        sandbox = sinon.createSandbox();
        props = {
            fontSize: '12px',
            boxShadow: 'boxShadow',
            textColor: 'textColor',
            fontWeight: 500,
            dataAttribute: 'dataAttribute',
            textTransform: 'textTransform',
            backgroundColor: 'backgroundColor',
            htmlFor: 'idOfInput',
            backgroundColorHover: 'backgroundColorHover',
        };
    });

    afterEach(() => {
        sandbox.restore();
        sandbox.reset();
    });

    it('CustomLabel create a snapshot, should render correctly', () => {
        const wrapper = shallow(<CustomLabel {...props}/>);

        expect(wrapper).matchSnapshot();
    });
});

describe('CustomLabel styled components', () => {
    const props = {
        fntSize: '12px',
        bgColor: 'backgroundColor',
        bxShadow: 'boxShadow',
        txtColor: 'textColor',
        fntWeight: 500,
        txtTransform: 'textTransform',
        bgColorHover: 'backgroundColorHover',
        borderLabel: 'borderLabel',
        borderLabelHover: 'borderLabelHover',
    };

    it('CustomLabel should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<Label {...props}/>);

        expect(component).toHaveStyleRule('color', 'textColor');
        expect(component).toHaveStyleRule('font-size', '12px');
        expect(component).toHaveStyleRule('background', 'backgroundColor');
        expect(component).toHaveStyleRule('font-weight', '500');
        expect(component).toHaveStyleRule('text-transform', 'textTransform');
        expect(component).toHaveStyleRule('border', 'borderLabel');
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('box-shadow', 'boxShadow');
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('background', 'backgroundColorHover');
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('border', 'borderLabelHover');
    });

    it('CustomLabel should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<Label />);

        expect(component).toHaveStyleRule('color', defaultStyles.txtColorDefault);
        expect(component).toHaveStyleRule('font-size', defaultStyles.fntSizeDefault);
        expect(component).toHaveStyleRule('background', defaultStyles.bgColorDefault);
        expect(component).toHaveStyleRule('font-weight', defaultStyles.fontWeightDefault);
        expect(component).toHaveStyleRule('text-transform', defaultStyles.txtTransformDefault);
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('box-shadow', defaultStyles.bxShadowDefault);
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('background', defaultStyles.bgColorHoverDefault);
    });
});
