import styled from 'styled-components';

export const BlockWrapper = styled.div`
   margin: 7px;
   padding-right: 72px;
   height: 100%;
   width:100%;
   display: flex;
   flex-direction: row;
   justify-content: center;
`;

export const LabelWrapper = styled.div`
   height: 100%;
   width: 82px;
   display: flex;
   justify-content: center;
   `;

export const DropdownWrapper = styled.div`
   height: 100%;
   width: 200px;
   display: flex;
   justify-content: center;
   `;