import React from 'react';
import PropTypes from 'prop-types';
import { BlockWrapper, LabelWrapper, DropdownWrapper } from './styledComponents.js';
import CustomLabel from '../customLabel/CustomLabel.jsx';
import CustomDropdown from '../CustomDropdown/CustomDropdown';

const DropdownBlock = props => {
  const {
    textColorLabel,
    fontWeightLabel,
    dataAttributeLabel,
    textTransformLabel,
    labelName,
    htmlFor,
    idDropdown,
    dropdownRef,
    dataAttributeDropdown,
    backgroundColor,
    borderRadius,
    backgroundFocusColor,
    focusColor,
    color,
    padding,
    content,
    onChangeCallback,
  } = props;

  return (
    <BlockWrapper>
      <LabelWrapper>
      <CustomLabel
        dataAttributeLabel={dataAttributeLabel}
        htmlFor={htmlFor}
        textTransform={textTransformLabel}
        textColor={textColorLabel}
        fontWeight={fontWeightLabel}
        labelName={labelName}
      />
      </LabelWrapper>
      <DropdownWrapper>
      <CustomDropdown
          bgColor={backgroundColor}
          color={color}
          focusColor={focusColor}
          bgFocusColor={backgroundFocusColor}
          padding={padding}
          brRadius={borderRadius}
          id={idDropdown}
          dropdownRef={dropdownRef}
          dataAttributeInput={dataAttributeDropdown}
          content={content}
          onChangeCallback={onChangeCallback}
      />
      </DropdownWrapper>
    </BlockWrapper>
  );
};

DropdownBlock.propTypes = {
  textColorLabel: PropTypes.string,
  fontWeightLabel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  dataAttributeLabel: PropTypes.string,
  textTransformLabel: PropTypes.string,
  labelName: PropTypes.string.isRequired,
  htmlFor: PropTypes.string.isRequired,
  idDropdown: PropTypes.string,
  dropdownRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.elementType }),
  ]),
  requiredInput: PropTypes.string,
  placeholderText: PropTypes.string,
  isDisabled: PropTypes.bool,
  dataAttributeDropdown: PropTypes.string,
  backgroundColor: PropTypes.string,
  borderRadius: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backgroundFocusColor: PropTypes.string,
  focusColor: PropTypes.string,
  onKeyDownCallback: PropTypes.func,
  onFocusCallback: PropTypes.func,
  marginBlock: PropTypes.string,
  textAlign: PropTypes.string,
  color: PropTypes.string,
  padding: PropTypes.string,
  content: PropTypes.array,
  onChangeCallback: PropTypes.func,
};

export default React.memo(DropdownBlock);

