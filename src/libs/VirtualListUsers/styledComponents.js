import styled from 'styled-components';

export const Header = styled.div`
  display: flex;
  width: 80%;
  height: 80px;
  justify-content: space-between;
  align-items: center;
`;

export const AddButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.btnContainerWidth ? props.btnContainerWidth : '25%'};
  height: ${props => props.btnContainerHeigth ? props.btnContainerHeigth : '100%'};
  border: none;
  cursor: pointer;
  background-image: ${props => props.bgImage ? props.bgImage : null};
  background-repeat: no-repeat;
  background-size: 75% 75%;
  background-position: center;
  overflow: hidden;
`;

export const MyGroupContentRow = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  
  &:nth-child(odd) {
    background-color: rgba(74,136,204,0.2);
  }
`;

export const GroupElementContainer = styled.div`
  display: flex;
  height: 50px;
  font-size: 16px;
  font-weight: 700;
  box-sizing: border-box;
  border-bottom: 1px solid darkgray;
`;

export const GroupElement = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.widthSize}%;
  box-sizing: border-box;
  border-right: ${props => !props.last ? `1px solid darkgray` : null};
`;

export const InputContainer = styled.div`
  width: 100%;
  height: 60px;
  margin-bottom: 5px;
`;

export const ListContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const LabelContainer = styled.div`
  width: 100px;
  height: 70px;
  display: flex;
  flex-direction: column;
`;