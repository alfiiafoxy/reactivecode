import React from 'react';
import Scrollbar from 'react-scrollbars-custom';
import PropTypes from 'prop-types';
import {
  GroupElement,
  MyGroupContentRow,
  GroupElementContainer,
  InputContainer,
  AddButtonContainer,
  ListContainer,
  LabelContainer,
} from './styledComponents.js';
import LoaderSpinner from '../Loader/Loader.jsx';
import CustomInput from '../customInput/CustomInput.jsx';
import CustomButton from '../customButton/CustomButton.jsx';
import CustomLabel from '../customLabel/CustomLabel';

const VirtualListUsers = props => {
  const {
    users,
    loaderType,
    loaderColor,
    loaderHeight,
    loaderWidth,
    usersCaption,
    translate,
    bgButtonColor,
    backgroundFocusColor,
    dataAttributeInput,
    placeholderTextGroup,
    placeholderTextSearch,
    onClickCallback,
  } = props;

  return (
    <ListContainer>
      <LabelContainer>
        <CustomLabel
          htmlFor={inputGroupId}
          textColor={labelTextGroup}
        />
      </LabelContainer>
      <InputContainer>
        <CustomInput
          backgroundFocusColor={backgroundFocusColor}
          dataAttributeInput={dataAttributeInput}
          placeholderText={placeholderTextGroup}
        />
      </InputContainer>
      <InputContainer>
        <CustomInput
          backgroundFocusColor={backgroundFocusColor}
          dataAttributeInput={dataAttributeInput}
          placeholderText={placeholderTextSearch}
        />
      </InputContainer>
      <GroupElementContainer>
        {usersCaption.map((item, index) => {
          return (
            <GroupElement
              key={index}
              last={item.idCaption === 'addButton'}
              widthSize={item.widthSize}
              children={translate[item.resourcesKey]}
            />
          );
        })}
      </GroupElementContainer>
      <Scrollbar
        style={{ height: 400, boxSizing: 'border-box' }}
        noScrollX={false}
      >
        {users.length ? users.map(el => {
            return (
              <MyGroupContentRow key={el.email}>
                <GroupElement widthSize={25} children={el.first_name}/>
                <GroupElement widthSize={25} children={el.last_name}/>
                <GroupElement widthSize={25} children={el.email}/>
                <AddButtonContainer btnContainerWidth={'25%'}>
                  <CustomButton
                    backgroundImage={bgButtonColor}
                    onClickCallback={() => onClickCallback(el.id)}
                  />
                </AddButtonContainer>
              </MyGroupContentRow>
            );
          })
          :
          <LoaderSpinner
            loaderType={loaderType}
            loaderColor={loaderColor}
            loaderHeight={loaderHeight}
            loaderWidth={loaderWidth}
          />
        }
      </Scrollbar>
    </ListContainer>
  );
};

VirtualListUsers.propTypes = {
  users: PropTypes.array,
  loaderType: PropTypes.string.isRequired,
  loaderColor: PropTypes.string.isRequired,
  loaderWidth: PropTypes.number.isRequired,
  loaderHeight: PropTypes.number.isRequired,
  index: PropTypes.number,
  key: PropTypes.number,
  usersCaption: PropTypes.array,
  translate: PropTypes.object,
  listWidth: PropTypes.number,
  bgButtonColor: PropTypes.string,
  backgroundFocusColor: PropTypes.string,
  dataAttributeInput: PropTypes.string,
  placeholderTextGroup: PropTypes.string,
  placeholderTextSearch: PropTypes.string,
  onClickCallback: PropTypes.func,
};

export default React.memo(VirtualListUsers);