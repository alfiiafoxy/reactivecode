import styled from 'styled-components';

const defaultColor = '#ff0000';
const defaultFontSize = 12;
const defaultMarginBlock = '3px 0';
const defaultTextAlign = 'center';

export const ErrorBlockWrapper = styled.div`
   width: 100%;
   height: 10px;
   display: flex;
   align-items: center;
   justify-content: flex-start;
   margin: ${props => props.marginBlock ? props.marginBlock : defaultMarginBlock};
   text-align: ${props => props.textAlign ? props.textAlign : defaultTextAlign};
`;

export const MessageText = styled.span`
   color: ${props => props.textColor ? props.textColor : defaultColor};
   font-size: ${props => props.fontSize ? props.fontSize : defaultFontSize}px;
`;

export const defaultStyles = {
  defaultColor,
  defaultFontSize,
  defaultMarginBlock,
  defaultTextAlign,
};
