import React from 'react';
import PropTypes from 'prop-types';
import { ErrorBlockWrapper, MessageText } from './styledComponents.js';

const ErrorBlock = ({ errorMessage, textColor, errorRef, marginBlock, fontSize, textAlign, dataAttributeError }) =>
  <ErrorBlockWrapper marginBlock={marginBlock} textAlign={textAlign}>
    <MessageText fontSize={fontSize} ref={errorRef} textColor={textColor} children={errorMessage} data-at={dataAttributeError}/>
  </ErrorBlockWrapper>
;

ErrorBlock.propTypes = {
  textColor: PropTypes.string,
  errorMessage: PropTypes.string,
  errorRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.elementType }),
  ]),
  marginBlock: PropTypes.string,
  fontSize: PropTypes.number,
  textAlign: PropTypes.string,
  dataAttributeError: PropTypes.string,
};

export default React.memo(ErrorBlock);
