import ErrorBlockMemo from '../ErrorBlock.jsx';
import { defaultStyles, ErrorBlockWrapper, MessageText } from '../styledComponents.js';

const ErrorBlock = ErrorBlockMemo.type;

describe('ErrorBlock react components', () => {
    const props = {
        textColor: '#ffffff',
        errorMessage: 'error message',
        errorRef: null,
        fontSize: 12,
        marginBlock: '5px',
        textAlign: 'textAlign',
    };

    it('snapshot created, should rendered correctly', done => {
        const wrapper = shallow(<ErrorBlock {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});

describe('MessageText styled components', () => {
    const props = {
        textColor: 'textColor',
        fontSize: 12,
    };
    it('MessageText should have correct color, when textColor was transferred', () => {
        const component = getTreeSC(<MessageText {...props}/>);

        expect(component).toHaveStyleRule('color', 'textColor');
        expect(component).toHaveStyleRule('font-size', '12px');
    });

    it('MessageText should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<MessageText/>);

        expect(component).toHaveStyleRule('color', defaultStyles.defaultColor);
        expect(component).toHaveStyleRule('font-size', `${defaultStyles.defaultFontSize}px`);
    });
});

describe('ErrorBlockWrapper styled components', () => {
    const props = {
        marginBlock: '5px',
        textAlign: 'textAlign',
    };

    it('ErrorBlockWrapper should have correct color, when textColor was transferred', () => {
        const component = getTreeSC(<ErrorBlockWrapper {...props}/>);

        expect(component).toHaveStyleRule('margin', '5px');
        expect(component).toHaveStyleRule('text-align', 'textAlign');
    });

    it('ErrorBlockWrapper should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<ErrorBlockWrapper/>);

        expect(component).toHaveStyleRule('margin', defaultStyles.defaultMarginBlock);
        expect(component).toHaveStyleRule('text-align', defaultStyles.defaultTextAlign);
    });
});

