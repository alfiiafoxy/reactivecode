import React from 'react';
import PropTypes from 'prop-types';
import { Input } from './styledComponent';

const CustomInput = props => {
    const {
        idInput,
        inputRef,
        inputType,
        inputCallback,
        onKeyDownCallback,
        onFocusCallback,
        requiredInput,
        placeholderText,
        isDisabled,
        dataAttributeInput,
        backgroundColor,
        borderBottom,
        borderRadius,
        backgroundFocusColor,
        focusColor,
        focusBorderBottom,
        intOutline,
        padding,
        intCursor,
        color,
        inputText,
        borderColorInp,
        inputOnBlur,
        textAlignInput,
    } = props;

    return (
        <Input
            padding={padding}
            brBottom={borderBottom}
            bgColor={backgroundColor}
            intOutline={intOutline}
            bgFocusColor={backgroundFocusColor}
            focusColor={focusColor}
            color={color}
            focusBrBottom={focusBorderBottom}
            data-at={dataAttributeInput}
            type={inputType}
            onChange={inputCallback}
            onKeyDown={onKeyDownCallback}
            onFocus={onFocusCallback}
            placeholder={placeholderText}
            id={idInput}
            required={requiredInput}
            disabled={isDisabled}
            brRadius={borderRadius}
            intCursor={intCursor}
            onBlur={inputOnBlur}
            ref={inputRef}
            defaultValue={inputText}
            borderColorInp={borderColorInp}
            textAlignInput={textAlignInput}
        />
    );
};

CustomInput.propTypes = {
    padding: PropTypes.string,
    borderBottom: PropTypes.string,
    idInput: PropTypes.string,
    inputRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.elementType }),
    ]),
    inputType: PropTypes.string,
    inputCallback: PropTypes.func,
    requiredInput: PropTypes.string,
    placeholderText: PropTypes.string,
    inputContainerClass: PropTypes.string,
    isDisabled: PropTypes.bool,
    dataAttributeInput: PropTypes.string,
    backgroundColor: PropTypes.string,
    backgroundFocusColor: PropTypes.string,
    focusColor: PropTypes.string,
    focusBorderBottom: PropTypes.string,
    intOutline: PropTypes.number,
    borderRadius: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    intCursor: PropTypes.string,
    color: PropTypes.string,
    onKeyDownCallback: PropTypes.func,
    onFocusCallback: PropTypes.func,
    inputOnBlur: PropTypes.func,
    borderColorInp: PropTypes.string,
    inputText: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    textAlignInput: PropTypes.string,
};

CustomInput.defaultProps = {
    placeholderText: 'Enter',
    isDisabled: false,
};

export default React.memo(CustomInput);
