import CustomInputMemo from '../CustomInput';
import { Input, defaultStyles } from '../styledComponent';
import React from 'react';

const CustomInput = CustomInputMemo.type;

describe('CustomInput components', () => {
    let props;
    let sandbox;

    before(() => {
        sandbox = sinon.createSandbox();
        props = {
            inputCallback: () => true,
            dataAttribute: 'dataAttribute',
            backgroundColor: 'backgroundColor',
            border: '2px',
            placeholderText: 'Text',
            borderRadius: '2px',
            padding: '2px',
            color: 'red',
        };
    });

    it('CustomInput create a snapshot, should render correctly', () => {
        const wrapper = shallow(<CustomInput {...props}/>);

        expect(wrapper).matchSnapshot();
    });

    it('CustomInput should be disabled', () => {
        const localProps = { ...props, isDisabled: true };
        const component = shallow(<CustomInput {...localProps}/>);

        assert.isTrue(component.find(Input).props().disabled);
    });

    before(() => {
        sandbox.restore();
    });

    before(() => {
        sandbox.reset();
    });
});

describe('Input styled components', () => {
    const props = {
        bgColor: 'backgroundColor',
        brBottom: 'borderBottom',
        bgFocusColor: 'backgroundFocusColor',
        focusColor: 'focusColor',
        focusBrBottom: 'focusBorderBottom',
        intOutline: 'intOutline',
        brRadius: '5px',
        intCursor: 'intCursor',
        padding: '2px',
        color: 'red',
    };

    it('Input should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<Input {...props}/>);

        expect(component).toHaveStyleRule('background', 'backgroundColor');
        expect(component).toHaveStyleRule('border-radius', '5px');
        expect(component).toHaveStyleRule('padding', '2px');
        expect(component).toHaveStyleRule('outline', 'intOutline');
        expect(component).toHaveStyleRule('cursor', 'intCursor');
        expect(component).toHaveStyleRule('color', 'red');
        expect({
            component,
            modifier: '&:focus',
        }).toHaveStyleRule('background', 'backgroundFocusColor');
        expect({
            component,
            modifier: '&:focus',
        }).toHaveStyleRule('color', 'focusColor');
    });

    it('Input should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<Input/>);

        expect(component).toHaveStyleRule('background', defaultStyles.bgInpColorDefault);
        expect(component).toHaveStyleRule('border-radius', `${defaultStyles.brRadiusDefault}px`);
        expect(component).toHaveStyleRule('outline', defaultStyles.intOutlineDefault);
        expect(component).toHaveStyleRule('cursor', defaultStyles.intCursorDefault);
        expect(component).toHaveStyleRule('color', defaultStyles.colorDefault);
        expect(component).toHaveStyleRule('padding', defaultStyles.paddingDefault);

        expect({
            component,
            modifier: '&:focus',
        }).toHaveStyleRule('background', defaultStyles.bgFocusColorDefault);
        expect({
            component,
            modifier: '&:focus',
        }).toHaveStyleRule('color', defaultStyles.focusColorDefault);
    });
});

