import React from 'react';
import PropTypes from 'prop-types';
import { BlockWrapper, LabelWrapper, InputWrapper } from './styledComponents.js';
import CustomLabel from '../customLabel/CustomLabel.jsx';
import CustomInput from '../customInput/CustomInput.jsx';

const ContrPanelBlock = props => {
  const {
    textColorLabel,
    fontWeightLabel,
    dataAttributeLabel,
    textTransformLabel,
    labelName,
    htmlFor,
    idInput,
    inputRef,
    inputType,
    inputCallback,
    onKeyDownCallback,
    onFocusCallback,
    requiredInput,
    placeholderText,
    isDisabled,
    dataAttributeInput,
    backgroundColor,
    borderRadius,
    backgroundFocusColor,
    focusColor,
    inputCursor,
    colorInput,
  } = props;

  return (
    <BlockWrapper>
      <LabelWrapper>
      <CustomLabel
        dataAttributeLabel={dataAttributeLabel}
        htmlFor={htmlFor}
        textTransform={textTransformLabel}
        textColor={textColorLabel}
        fontWeight={fontWeightLabel}
        labelName={labelName}
      />
      </LabelWrapper>
      <InputWrapper>
      <CustomInput
        idInput={idInput}
        inputRef={inputRef}
        inputType={inputType}
        inputCallback={inputCallback}
        onKeyDownCallback={onKeyDownCallback}
        onFocusCallback={onFocusCallback}
        requiredInput={requiredInput}
        placeholderText={placeholderText}
        isDisabled={isDisabled}
        dataAttributeInput={dataAttributeInput}
        backgroundColor={backgroundColor}
        borderRadius={borderRadius}
        backgroundFocusColor={backgroundFocusColor}
        focusColor={focusColor}
        intCursor={inputCursor}
        color={colorInput}
      />
      </InputWrapper>
    </BlockWrapper>
  );
};

ContrPanelBlock.propTypes = {
  textColorLabel: PropTypes.string,
  fontWeightLabel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  dataAttributeLabel: PropTypes.string,
  textTransformLabel: PropTypes.string,
  labelName: PropTypes.string.isRequired,
  htmlFor: PropTypes.string.isRequired,
  idInput: PropTypes.string,
  inputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.elementType }),
  ]),
  inputType: PropTypes.string,
  inputCallback: PropTypes.func,
  requiredInput: PropTypes.string,
  placeholderText: PropTypes.string,
  isDisabled: PropTypes.bool,
  dataAttributeInput: PropTypes.string,
  backgroundColor: PropTypes.string,
  borderRadius: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backgroundFocusColor: PropTypes.string,
  focusColor: PropTypes.string,
  inputCursor: PropTypes.string,
  colorInput: PropTypes.string,
  textColor: PropTypes.string,
  onKeyDownCallback: PropTypes.func,
  onFocusCallback: PropTypes.func,
  marginBlock: PropTypes.string,
  textAlign: PropTypes.string,
};

export default React.memo(ContrPanelBlock);

