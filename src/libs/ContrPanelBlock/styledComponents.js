import styled from 'styled-components';

export const BlockWrapper = styled.div`
   margin: 7px;
   height: 100%;
   width:100%;
   display: flex;
   flex-direction: row;
   justify-content: center;
`;

export const LabelWrapper = styled.div`
   height: 100%;
   width: 82px;
   display: flex;
   `;

export const InputWrapper = styled.div`
   height: 100%;
   width: 200px;
   display: flex;
   `;
