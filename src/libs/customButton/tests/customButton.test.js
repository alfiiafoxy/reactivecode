import React from 'react';
import CustomButtonMemo from '../CustomButton.jsx';
import { Button, defaultStyles } from '../styledComponents.js';

const CustomButton = CustomButtonMemo.type;

describe('CustomButton components', () => {
    let props;
    let sandbox;

    before(() => {
        sandbox = sinon.createSandbox();
        props = {
            fontSize: 12,
            boxShadow: 'boxShadow',
            textColor: 'textColor',
            fontWeight: 500,
            buttonName: 'default',
            borderRadius: 5,
            dataAttribute: 'dataAttribute',
            textTransform: 'textTransform',
            backgroundColor: 'backgroundColor',
            onClickCallback: () => true,
            border: '1px solid',
            backgroundColorHover: 'backgroundColorHover',
        };
    });

    afterEach(() => {
        sandbox.restore();
        sandbox.reset();
    });

    it('CustomButton create a snapshot, should render correctly', () => {
        const wrapper = shallow(<CustomButton {...props}/>);

        expect(wrapper).matchSnapshot();
    });

    it('CustomButton should be disabled', () => {
        const localProps = { ...props, isDisabled: true };
        const component = shallow(<CustomButton {...localProps}/>);

        assert.isTrue(component.find(Button).props().disabled);
    });

    it('CustomButton should call onClickCallback', () => {
        const localProps = { ...props, onClickCallback: sandbox.stub() };
        const component = shallow(<CustomButton {...localProps}/>);
        component.find(Button).simulate('click');

        sinon.assert.calledOnce(localProps.onClickCallback);
    });
});

describe('CustomButton styled components', () => {
    const props = {
        width: '12px',
        height: '12px',
        margin: '12px',
        fntSize: 12,
        bgColor: 'backgroundColor',
        brRadius: 3,
        bxShadow: 'boxShadow',
        txtColor: 'textColor',
        fntWeight: 500,
        border: '1px solid black',
        txtTransform: 'textTransform',
        bgColorHover: 'backgroundColorHover',
    };

    it('Button should have correct styles when all props were transferred', () => {
        const component = getTreeSC(<Button {...props}/>);

        expect(component).toHaveStyleRule('color', 'textColor');
        expect(component).toHaveStyleRule('font-size', '12px');
        expect(component).toHaveStyleRule('background', 'backgroundColor');
        expect(component).toHaveStyleRule('font-weight', '500');
        expect(component).toHaveStyleRule('border-radius', '3px');
        expect(component).toHaveStyleRule('text-transform', 'textTransform');
        expect(component).toHaveStyleRule('border', '1px solid black');
        expect(component).toHaveStyleRule('box-shadow', 'boxShadow');
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('background', 'backgroundColorHover');
        expect({
            component,
            modifier: '&:disabled',
        }).toHaveStyleRule('background', defaultStyles.bgColorDisabled);
    });

    it('Button should have default styles when all props weren\'t transferred', () => {
        const component = getTreeSC(<Button />);

        expect(component).toHaveStyleRule('color', defaultStyles.txtColorDefault);
        expect(component).toHaveStyleRule('font-size', `${defaultStyles.fntSizeDefault}px`);
        expect(component).toHaveStyleRule('background', defaultStyles.bgColorDefault);
        expect(component).toHaveStyleRule('font-weight', defaultStyles.fontWeightDefault);
        expect(component).toHaveStyleRule('border-radius', `${defaultStyles.brRadiusDefault}px`);
        expect(component).toHaveStyleRule('text-transform', defaultStyles.txtTransformDefault);
        expect(component).toHaveStyleRule('border', defaultStyles.borderDefault);
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('background', defaultStyles.bgColorHoverDefault);
        expect({
            component,
            modifier: '&:disabled',
        }).toHaveStyleRule('background', defaultStyles.bgColorDisabled);
    });
});
