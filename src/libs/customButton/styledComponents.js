import styled from 'styled-components';

const bgColorDisabled = '#79838a';
const bgColorDefault = 'inherit';
const fntSizeDefault = 10;
const txtColorDefault = '#000000';
const brRadiusDefault = 3;
const bxShadowDefault = 'none';
const fontWeightDefault = 'bold';
const txtTransformDefault = 'uppercase';
const borderDefault = 'none';
const btnMarginDefault = 0;

export const Button = styled.button`
  width: 100%;
  height: 100%;
  border: ${props => props.border ? props.border : borderDefault};
  background: ${props => props.bgColor ? props.bgColor : bgColorDefault};
  background-image: ${props => props.backgroundImage ? props.backgroundImage : bgColorDefault};
  border-radius: ${props => props.brRadius ? props.brRadius : brRadiusDefault}px;
  color: ${props => props.txtColor ? props.txtColor : txtColorDefault};
  font-size: ${props => props.fntSize ? props.fntSize : fntSizeDefault}px;
  font-weight: ${props => props.fntWeight ? props.fntWeight : fontWeightDefault};
  text-transform: ${props => props.txtTransform ? props.txtTransform : txtTransformDefault};
  margin:${props => props.btnMargin ? props.btnMargin : btnMarginDefault}px;
  box-shadow: ${props => props.bxShadow ? props.bxShadow : bxShadowDefault};
  background-repeat: no-repeat;
    
  &:hover {
    cursor: pointer;
    background: ${props => props.bgColorHover ? props.bgColorHover : null};
    background-repeat: no-repeat;
    box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12);
  }

  &:active {
    box-shadow: none;
  }
  
  &:disabled {
    opacity: 0.5;
    cursor: no-drop;
    user-select: none;
    background: ${bgColorDisabled};
    box-shadow: none;
  }
`;

export const defaultStyles = { // For Unit Test
    bgColorDisabled,
    bgColorDefault,
    borderDefault,
    fntSizeDefault,
    txtColorDefault,
    brRadiusDefault,
    bxShadowDefault,
    fontWeightDefault,
    txtTransformDefault,
};
