import React from 'react';
import PropTypes from 'prop-types';
import { Button } from './styledComponents.js';

const CustomButton = props => {
    const {
        fontSize,
        boxShadow,
        textColor,
        fontWeight,
        isDisabled,
        buttonName,
        borderRadius,
        dataAttribute,
        textTransform,
        backgroundColor,
        onClickCallback,
        backgroundColorHover,
        backgroundImage,
        border,
        btnMargin,
    } = props;

    return (
        <Button
            data-at={dataAttribute}
            bgColor={backgroundColor}
            onClick={onClickCallback}
            fntSize={fontSize}
            brRadius={borderRadius}
            disabled={isDisabled}
            children={buttonName}
            txtColor={textColor}
            bxShadow={boxShadow}
            fntWeight={fontWeight}
            txtTransform={textTransform}
            bgColorHover={backgroundColorHover}
            backgroundImage={backgroundImage}
            border={border}
            btnMargin={btnMargin}
        />
    );
};

CustomButton.propTypes = {
    fontSize: PropTypes.number,
    isDisabled: PropTypes.bool,
    boxShadow: PropTypes.string,
    textColor: PropTypes.string,
    fontWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    buttonName: PropTypes.string,
    borderRadius: PropTypes.number,
    dataAttribute: PropTypes.string,
    textTransform: PropTypes.string,
    backgroundColor: PropTypes.string,
    bgColorHover: PropTypes.string,
    onClickCallback: PropTypes.func.isRequired,
    backgroundColorHover: PropTypes.string,
    backgroundImage: PropTypes.string,
    border: PropTypes.string,
    btnMargin: PropTypes.number,
};

CustomButton.defaultProps = {
    isDisabled: false,
};

export default React.memo(CustomButton);
