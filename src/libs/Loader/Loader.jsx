import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

const LoaderSpinner = props => {
  const {
    loaderType,
    loaderColor,
    loaderHeight,
    loaderWidth,
  } = props;

  return (
    <React.Fragment>
      <Loader
        type={loaderType}
        color={loaderColor}
        height={loaderHeight}
        width={loaderWidth}
      />
    </React.Fragment>
  );
};

LoaderSpinner.propTypes = {
  loaderType: PropTypes.string.isRequired,
  loaderColor: PropTypes.string.isRequired,
  loaderWidth: PropTypes.number.isRequired,
  loaderHeight: PropTypes.number.isRequired,
};

export default React.memo(LoaderSpinner);
