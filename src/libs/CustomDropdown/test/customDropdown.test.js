import CustomDropdownMemo from '../CustomDropdown.jsx';
import { defaultStyles, Select } from '../styledComponent';
import PropTypes from 'prop-types';

const CustomDropdown = CustomDropdownMemo.type;

describe('CustomDropdown components', () => { //FIXME
    const props = {
        backgroundColor: 'backgroundColor',
        color: 'color',
        focusColor: 'focusColor',
        backgroundFocusColor: 'backgroundFocusColor',
        idDropdown: 'idDropdown',
        dropdownRef: () => {},
        borderRadius: 5,
        dataAttributeDropdown: 'dataAttributeDropdown',
        content: ['first', 'second'],
       };

   it('CustomDropdown create a snapshot, should render correctly', () => {
       const wrapper = shallow(<CustomDropdown {...props}/>);

       expect(wrapper).matchSnapshot();
    });
});

describe('Select style component', () => {
    const props = {
        bgColor: 'backgroundColor',
        color: 'red',
        brRadius: 5,
        dataAttributeDropdown: 'dataAttributeDropdown',
    };

    it('Dropdown should have correct styled when all props were transferred', () => {
       const component = getTreeSC(<Select {...props}/>);

       expect(component).toHaveStyleRule('background', 'backgroundColor');
       expect(component).toHaveStyleRule('border-radius', '5px');
       expect(component).toHaveStyleRule('color', 'red');
    });

    it('Dropdown should have default styles when all props weren\'t transferred', () => {
       const component = getTreeSC(<Select/>);
        expect(component).toHaveStyleRule('background', defaultStyles.bgInpColorDefault);
        expect(component).toHaveStyleRule('border-radius', `${defaultStyles.brRadiusDefault}px`);
        expect(component).toHaveStyleRule('color', defaultStyles.colorDefault);
    });
});