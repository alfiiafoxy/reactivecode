import styled from 'styled-components';

const colorDefault = 'black';
const focusColorDefault = 'black';
const bgInpColorDefault = '#F6F7FA';
const brRadiusDefault = 3;
const borderColorInpDefault = 'none';

export const Select = styled.select`
  width: 100%;
  height: 100%;
  display: flex;
  background: ${props => props.bgColor ? props.bgColor : bgInpColorDefault};
  color: ${props => props.color ? props.color : colorDefault};
  font-size: 14px;
  border: ${props => props.borderColorInp ? props.borderColorInp : borderColorInpDefault};
  border-radius: ${props => props.brRadius ? props.brRadius : brRadiusDefault}px;    
   outline: none;   
   
  :focus {
      color: ${props => props.focusColor ? props.focusColor : focusColorDefault};
      background: ${props => props.bgFocusColor ? props.bgFocusColor : bgInpColorDefault};
  }

  option {
    color: ${props => props.focusColor ? props.focusColor : focusColorDefault};
    background: ${props => props.bgFocusColor ? props.bgFocusColor : bgInpColorDefault};
    display: flex;
    white-space: pre;
    min-height: 20px;
  }`;

export const defaultStyles = {
    colorDefault,
    focusColorDefault,
    bgInpColorDefault,
    brRadiusDefault,
};
