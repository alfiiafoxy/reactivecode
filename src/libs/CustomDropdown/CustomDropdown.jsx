import React from 'react';
import PropTypes from 'prop-types';
import { Select } from './styledComponent';

const CustomDropdown = props => {
    const {
        backgroundColor,
        color,
        focusColor,
        backgroundFocusColor,
        borderRadius,
        idDropdown,
        dropdownRef,
        dataAttributeDropdown,
        content,
        selectedValue,
        isDisabled,
        onClickCallback,
        onChangeCallback,
        borderColorInp,
    } = props;
     return (
            <Select
                bgColor={backgroundColor}
                color={color}
                focusColor={focusColor}
                bgFocusColor={backgroundFocusColor}
                brRadius={borderRadius}
                id={idDropdown}
                ref={dropdownRef}
                data-at={dataAttributeDropdown}
                defaultValue={selectedValue}
                disabled={isDisabled}
                onClick={onClickCallback}
                onChange={onChangeCallback}
                borderColorInp={borderColorInp}
            >
                {content.map((item) => {
                    return <option
                            key={item.id}
                            value = {item.id}
                            >{item.name}</option>
                    ;
                })}
            </Select>
    );
};

CustomDropdown.propTypes = {
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    focusColor: PropTypes.string,
    backgroundFocusColor: PropTypes.string,
    idDropdown: PropTypes.string,
    dropdownRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.elementType }),
    ]),
    borderRadius: PropTypes.number,
    dataAttributeDropdown: PropTypes.string,
    content: PropTypes.array,
    selectedValue: PropTypes.string,
    isDisabled: PropTypes.bool,
    onClickCallback: PropTypes.func,
    onChangeCallback: PropTypes.func,
    borderColorInp: PropTypes.string,
};
export default React.memo(CustomDropdown);
