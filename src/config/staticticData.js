export default {
  statisticCaption: [
   {
      idCaption: 'group',
      resourcesKey: 'admGroups',
       widthSize: 60,
    },
   {
      idCaption: 'hours',
      resourcesKey: 'hours',
      widthSize: 20,
   },
   {
      idCaption: 'salary',
      resourcesKey: 'salary',
      widthSize: 20,
   },
  ],
};