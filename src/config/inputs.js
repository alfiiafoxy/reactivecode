export default {
  authInputs: [
    {
      inputType: 'email',
      idInput: 'email',
      inputRef: 'email',
      resourcesKey: 'email',
      placeholderKey: 'enterEmail',
      errorMessageRef: 'emailError',
      onKeyDownCallback: 'onKeyDownValid',
    },
    {
      inputType: 'password',
      inputRef: 'password',
      idInput: 'password',
      resourcesKey: 'password',
      placeholderKey: 'enterPassword',
      errorMessageRef: 'passwordError',
      onKeyDownCallback: 'onKeyDownValid',
    },
  ],
  regInputs: [
    {
      inputType: 'text',
      idInput: 'firstName',
      inputRef: 'firstName',
      resourcesKey: 'firstName',
      placeholderKey: 'enterFirstName',
      errorMessageRef: 'firstNameError',
      onKeyDownCallback: 'onKeyDownValid',
    },
    {
      inputType: 'text',
      idInput: 'lastName',
      inputRef: 'lastName',
      resourcesKey: 'lastName',
      placeholderKey: 'enterLastName',
      errorMessageRef: 'lastNameError',
      onKeyDownCallback: 'onKeyDownValid',
    },
    {
      inputType: 'email',
      idInput: 'email',
      inputRef: 'email',
      resourcesKey: 'email',
      placeholderKey: 'enterEmail',
      errorMessageRef: 'emailError',
      onKeyDownCallback: 'onKeyDownValid',
    },
    {
      inputType: 'password',
      idInput: 'password',
      inputRef: 'password',
      resourcesKey: 'password',
      placeholderKey: 'enterPassword',
      errorMessageRef: 'passwordError',
      onKeyDownCallback: 'onKeyDownValid',
    },
    {
      inputType: 'password',
      idInput: 'repeatPassword',
      inputRef: 'repeatPassword',
      resourcesKey: 'repeatPassword',
      placeholderKey: 'enterRepeatPassword',
      errorMessageRef: 'repeatPasswordError',
      onKeyDownCallback: 'onKeyDownValid',
    },
    {
      inputType: 'text',
      idInput: 'phone',
      inputRef: 'phone',
      resourcesKey: 'phoneNumber',
      placeholderKey: 'enterPhone',
      errorMessageRef: 'phoneError',
      onKeyDownCallback: 'onKeyDownValid',
    },
    {
      inputType: 'text',
      idInput: 'dateOfBirth',
      inputRef: 'dateOfBirth',
      resourcesKey: 'birthDate',
      placeholderKey: 'enterDateBirth',
      errorMessageRef: 'dateOfBirthError',
      onKeyDownCallback: 'onKeyDownValid',
    },
    {
      inputType: 'text',
      idInput: 'keyword',
      inputRef: 'keywords',
      resourcesKey: 'keyword',
      placeholderKey: 'enterKeyWord',
      errorMessageRef: 'keywordsError',
      onKeyDownCallback: 'onKeyDownValid',
    },
  ],
  controlPanelInputs: [
        {
            inputType: 'date',
            idInput: 'dataOfLesson',
            inputRef: 'dataOfLesson',
            resourcesKey: 'dataOfLesson',
            placeholderKey: 'selectDataOfLesson',
            errorMessageRef: 'errorMessageData',
            onKeyDownCallback: 'onKeyDownValid',
        },
        {
            inputType: 'time',
            idInput: 'startTime',
            inputRef: 'startTime',
            resourcesKey: 'startTime',
            placeholderKey: 'selectTimeStart',
            errorMessageRef: 'errorStartTime',
            onKeyDownCallback: 'onKeyDownValid',
        },
        {
            inputType: 'time',
            idInput: 'endTime',
            inputRef: 'endTime',
            resourcesKey: 'endTime',
            placeholderKey: 'selectTimeEndOfLesson',
            errorMessageRef: 'errorEndTime',
            onKeyDownCallback: 'onKeyDownValid',
        },
    ],
  searchDateInputs: [
    {
      inputType: 'date',
      idInput: 'searchStartDate',
      inputRef: 'searchStartDate',
      resourcesKey: 'searchStartDate',
      placeholderKey: 'searchStartDate',
      defaultValue: 'searchStartDate',
      errorMessageRef: 'errorMessageSearchStartDate',
      callback: 'getStartDate',
    },
    {
      inputType: 'date',
      idInput: 'searchEndDate',
      inputRef: 'searchEndDate',
      resourcesKey: 'searchEndDate',
      placeholderKey: 'searchEndDate',
      defaultValue: 'searchEndDate',
      errorMessageRef: 'errorMessageSearchEndDate',
      callback: 'getEndDate',
    },
  ],
  adminStatisticInputs: [
      {
          inputType: 'date',
          idInput: 'fromDate',
          resourcesKey: 'fromDate',
          idLabel: 'idLabelFrom',
          placeholder: 'fromDate',
          errorMessageRef: 'errorFromDate',
          onKeyDownCallback: 'onKeyDownValid',
          inputRef: 'dateFrom',
      },
      {
          inputType: 'date',
          idInput: 'toDate',
          idLabel: 'idLabelTo',
          resourcesKey: 'toDate',
          placeholder: 'toDate',
          errorMessageRef: 'errorToDate',
          onKeyDownCallback: 'onKeyDownValid',
          inputRef: 'dateTo',
      },
  ],
    accountInputs: [
        {
            inputType: 'text',
            idInput: 'first_name',
            inputRef: 'first_name',
            resourcesKey: 'accountName',
            placeholderText: 'accountName',
            errorMessage: 'errorAccountName',
            onKeyDownCallback: 'onKeyDownValid',
        },
        {
          inputType: 'text',
          idInput: 'last_name',
          inputRef: 'last_name',
          resourcesKey: 'accountSurname',
          placeholderText: 'accountSurname',
          errorMessage: 'errorAccountSurname',
          onKeyDownCallback: 'onKeyDownValid',
        },
        {
            inputType: 'email',
            idInput: 'email',
            inputRef: 'email',
            resourcesKey: 'accountEmail',
            placeholderKey: 'accountEmail',
            errorMessage: 'errorAccountEmail',
            onKeyDownCallback: 'onKeyDownValid',
        },
        {
            inputType: 'text',
            idInput: 'phone',
            inputRef: 'phone',
            resourcesKey: 'accountPhoneNumber',
            placeholderKey: 'accountPhone',
            errorMessage: 'errorAccountPhone',
            onKeyDownCallback: 'onKeyDownValid',
        },
    ],
  filterDateInputs: [
    {
      inputType: 'date',
      idInput: 'searchStartDate',
      inputName: 'firstDay',
    },
    {
      inputType: 'date',
      idInput: 'searchEndDate',
      inputName: 'lastDay',
    },
  ],
};