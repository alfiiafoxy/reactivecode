import inputs from './inputs';
import buttons from './buttons.js';
import dropdown from './dropdown';
import dataAttributes from './dataAttributes';
import scheduleData from './scheduleData';
import myGroupData from './myGroupData.js';
import adminStatisticData from './adminStatisticData';
import statisticData from './staticticData.js';
import images from './images.js';
import modalsSettings from './modalsSettings';

export default {
    defaultLocale: 'en',
    defaultTheme: 'light',
    defaultStateInputs: true,
    authInputs: inputs.authInputs,
    authButtons: buttons.authButtons,
    regInputs: inputs.regInputs,
    regButtons: buttons.regButtons,
    filterDateInputs: inputs.filterDateInputs,
    controlPanelInputs: inputs.controlPanelInputs,
    searchDateInputs: inputs.searchDateInputs,
    controlPanelButton: buttons.controlPanelButton,
    controlPanelDropdown: dropdown.controlPanelDropdown,
    usersRoleDropdown: dropdown.usersRoleDropdown,
    altLogo: 'Logo',
    srcLogo: './assets/logo.png',
    srcLogoMini: './assets/logo2.png',
    dataAttributes,
    srcButtonSettings: 'url(\'./assets/settings.png\')',
    srcButtonCloseModal: 'url(\'./assets/cancel.svg\')',
    srcButtonArrowLeft: 'url(\'./assets/arrow-left.svg\')',
    srcButtonArrowRight: 'url(\'./assets/arrow-right.svg\')',
    srcButtonDeleteStudents: 'url(\'./assets/deleteStudent.svg\')',
    srcButtonSearchStudents: 'url(\'./assets/search.svg\')',
    srcButtonAddStudents: 'url(\'./assets/add.svg\')',
    modalsIsOpen: modalsSettings.modalWindows,
    scheduleData,
    myGroupData,
    statisticData: statisticData.statisticCaption,
    adminStatisticData,
    headerButtons: buttons,
    contentButtons: buttons.contentButtons,
    attendanceButton: buttons.attendanceButton,
    adminStatisticInputs: inputs.adminStatisticInputs,
    adminStatisticButton: buttons.adminStatisticData,
    accountInputs: inputs.accountInputs,
    accountButton: buttons.accountButton,
    images,
    cityDropdown: dropdown.city,
    englishLevel: dropdown.englishLevel,
    cityFilterDropdown: dropdown.filterCity,
};
