export default {
  controlPanelDropdown: [
    {
      idDropdown: 'typeOfLesson',
      dropdownRef: 'typeOfLesson',
      resourcesKey: 'typeOfLesson',
      placeholderKey: 'selectTypeOfLesson',
      errorMessageRef: 'errorMessageLesson',
      onKeyDownCallback: 'onKeyDownValid',
      content: [
        { id: 'Lecture', name: 'Lecture' },
        { id: 'Practice', name: 'Practice' },
        { id: 'Webinar', name: 'Webinar' },
      ],
    },
    {
      idDropdown: 'groups',
      dropdownRef: 'groupsId',
      resourcesKey: 'groupName',
      content: 'groups',
    },
  ],
  usersRoleDropdown: [
    { id: 'student', name: 'Student' },
    { id: 'teacher', name: 'Teacher' },
    { id: 'admin', name: 'Admin' },
  ],
  city: [
    { id: 'Kiev', name: 'Kiev' },
    { id: 'Dnepr', name: 'Dnepr' },
    { id: 'Baku', name: 'Baku' },
    { id: 'TelAviv', name: 'TelAviv' },
  ],
  filterCity: [
    { id: 0, name: 'All city' },
    { id: 'Kiev', name: 'Kiev' },
    { id: 'Dnepr', name: 'Dnepr' },
    { id: 'Baku', name: 'Baku' },
    { id: 'TelAviv', name: 'TelAviv' },
  ],
  englishLevel: [
    { id: 'Beginner', name: 'Beginner' },
    { id: 'Elementary', name: 'Elementary' },
    { id: 'Pre-Intermediate', name: 'Pre-Intermediate' },
    { id: 'Intermediate', name: 'Intermediate' },
    { id: 'Upper Intermediate', name: 'Upper Intermediate' },
    { id: 'Advanced', name: 'Advanced' },
    { id: 'Mastery', name: 'Mastery' },
  ],
};
