export default {
    adminStatisticCaption: [
        {
            idCaption: 'number',
            resourcesKey: 'number',
            widthSize: 5,
        },
        {
            idCaption: 'name',
            resourcesKey: 'name',
            widthSize: 19,
        },
        {
            idCaption: 'surname',
            resourcesKey: 'surname',
            widthSize: 19,
        },
        {
            idCaption: 'attendance',
            resourcesKey: 'attendance',
            widthSize: 19,
        },
        {
            idCaption: 'adminHours',
            resourcesKey: 'adminHours',
            widthSize: 19,
        },
        {
            idCaption: 'adminSalary',
            resourcesKey: 'adminSalary',
            widthSize: 19,
        },
    ],
};