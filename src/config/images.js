export default {
  edit: 'url(\'./assets/edit.png\')',
  cancel: 'url(\'./assets/cancel.png\')',
  delete: 'url(\'./assets/delete.png\')',
  ok: 'url(\'./assets/ok.png\')',
  addUser: 'url(\'./assets/addUser.svg\')',
};