export default {
  scheduleCaption: [
    {
      idCaption: 'day',
      resourcesKey: 'day',
    },
    {
      idCaption: 'date',
      resourcesKey: 'date',
    },
    {
      idCaption: 'group',
      resourcesKey: 'myGroup',
    },
    {
      idCaption: 'lessonStart',
      resourcesKey: 'lessonStart',
    },
    {
      idCaption: 'lessonEnd',
      resourcesKey: 'lessonEnd',
    },
    {
      idCaption: 'lessonType',
      resourcesKey: 'lessonType',
    },
    {
      idCaption: 'controlButtons',
      resourcesKey: 'controlButtons',
    },
  ],
};
