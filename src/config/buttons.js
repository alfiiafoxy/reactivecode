export default {
    authButtons: [
        { idButton: 'authBtn', resourcesKey: 'signIn', onClickCallback: 'authRequestHandler' },
    ],
    regButtons: [
        { idButton: 'regBtn', resourcesKey: 'signUp', onClickCallback: 'regRequestHandler' },
    ],
    headerMainButtons: [
        { idButton: 'myAccount', name: 'myAccount', srcButton: 'url(\'./assets/avatarDefault.png\')' },
        { idButton: 'settings', name: 'settings', srcButton: 'url(\'./assets/settings.png\')' },
        { idButton: 'logOut', name: 'logOut', srcButton: 'url(\'./assets/logOut.png\')' },
    ],
    headerMyAccountButtons: [
        { idButton: 'back', name: 'back', srcButton: 'url(\'./assets/back1.png\')' },
        { idButton: 'settings', name: 'settings', srcButton: 'url(\'./assets/settings.png\')' },
        { idButton: 'logOut', name: 'logOut', srcButton: 'url(\'./assets/logOut.png\')' },
    ],
    contentButtons: {
        studentRole: ['mySchedule', 'myGroup'],
        teacherRole: ['schedule', 'myGroup', 'attendance', 'teachersStatistic'],
        adminRole: ['schedule', 'groups', 'users', 'teachersStatistic'],
    },
    controlPanelButton: [
        { isButton: 'add', resourcesKey: 'add', onClickCallback: 'createLessonHandler' },
        { isButton: 'clear', resourcesKey: 'clear', onClickCallback: 'clearLessonHandler' },
    ],
    attendanceButton: [
        { idButton: 'change', resourcesKey: 'change', onClickCallback: 'change' },
        { idButton: 'save', resourcesKey: 'save', onClickCallback: 'save' },
    ],
    accountButton: [
        { idButton: 'accountChange', resourcesKey: 'accountChange', onClickCallback: 'accountChange' },
        { idButton: 'accountSave', resourcesKey: 'accountSave', onClickCallback: 'accountSave' },
    ],
    adminStatisticData: [
        { idButton: 'takeReport', resourcesKey: 'takeReport', onClickCallback: 'getSalaryRequest', dataAt: 'at-get-statistic' },
        { idButton: 'createReport', resourcesKey: 'createReportFile', onClickCallback: 'createReport', dataAt: 'at-create-csv-file' },
    ],
};
