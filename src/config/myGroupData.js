export default {
  myGroupCaption: [
    {
      idCaption: 'serialNumber',
      resourcesKey: 'serialNumber',
      widthSize: 5,
    },
    {
      idCaption: 'name',
      resourcesKey: 'name',
      widthSize: 15,
    },
    {
      idCaption: 'surname',
      resourcesKey: 'surname',
      widthSize: 15,
    },
    {
      idCaption: 'phone',
      resourcesKey: 'phone',
      widthSize: 15,
    },
    {
      idCaption: 'email',
      resourcesKey: 'email',
      widthSize: 15,
    },
    {
      idCaption: 'birthDate',
      resourcesKey: 'birthDate',
      widthSize: 15,
    },
    {
      idCaption: 'attendance',
      resourcesKey: 'attendance',
      widthSize: 15,
    },
    {
      idCaption: 'deleteButton',
      resourcesKey: 'deleteButton',
      widthSize: 5,
    },
  ],
  myGroupCaptionStudent: [
    {
      idCaption: 'serialNumber',
      resourcesKey: 'serialNumber',
      widthSize: 5,
    },
    {
      idCaption: 'name',
      resourcesKey: 'name',
      widthSize: 15,
    },
    {
      idCaption: 'surname',
      resourcesKey: 'surname',
      widthSize: 15,
    },
    {
      idCaption: 'phone',
      resourcesKey: 'phone',
      widthSize: 15,
    },
    {
      idCaption: 'email',
      resourcesKey: 'email',
      widthSize: 20,
    },
    {
      idCaption: 'birthDate',
      resourcesKey: 'birthDate',
      widthSize: 15,
    },
    {
      idCaption: 'attendance',
      resourcesKey: 'attendance',
      widthSize: 15,
    },
  ],
  usersCaption: [
    {
      idCaption: 'name',
      resourcesKey: 'name',
      widthSize: 25,
    },
    {
      idCaption: 'surname',
      resourcesKey: 'surname',
      widthSize: 25,
    },
    {
      idCaption: 'email',
      resourcesKey: 'email',
      widthSize: 25,
    },
    {
      idCaption: 'addButton',
      resourcesKey: '',
      widthSize: 25,
    },
  ],
  actionsButton: {
    admin: [
      { dataAt: 'at-add-teacher', name: 'buttonAddTeacher', id: 'addTeacher', type: 'addGroupModal', callbackValue: 'addNewTeacher' },
      { dataAt: 'at-add-group', name: 'buttonAddGroup', id: 'addGroup', type: 'addGroupModal', callbackValue: 'teacher' },
      { dataAt: 'at-add-student', name: 'addStudentInGroup', id: 'addStudent', type: 'addGroupModal', callbackValue: 'student' },
      { dataAt: 'at-delete-group', name: 'deleteGroup', id: 'deleteGroup', type: 'confirmModal', callbackValue: 'deleteGroup' },
    ],
    teacher: [
      { dataAt: 'at-add-student', name: 'addStudentInGroup', id: 'addStudent', type: 'addGroupModal', callbackValue: 'student' },
    ],
  },
};