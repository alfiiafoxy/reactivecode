export default {
  modalWindows: {
    settings: false,
    forgotPassword: false,
    addGroupModal: false,
    confirmModal: false,
  },
};