import actionTypes from '../constans/actionTypes.js';

export const sendRequestReg = payload => ({ type: actionTypes.SEND_REG_REQ, payload });
export const changeMainContent = payload => ({ type: actionTypes.CHANGE_MAIN_CONTENT, payload });
export const sendRequestAuth = payload => ({ type: actionTypes.SEND_AUTH_REQ, payload });
export const changeToggleModalWindow = payload => ({ type: actionTypes.TOGGLE_MODAL_WINDOW, payload });
export const changeScheduleWeek = payload => ({ type: actionTypes.SET_SCHEDULE_WEEK, payload });
export const clickingButton = payload => ({ type: actionTypes.CLICK_HEADER_BUTTON, payload });
export const changeContentName = payload => ({ type: actionTypes.CHANGE_NAME_CONTENT, payload });
export const changeInputUser = payload => ({ type: actionTypes.CHANGE_CURRENT_INPUT_USERS, payload });
export const changeButton = () => ({ type: actionTypes.TOGGLE_CHANGE_SAVE_BUTTON });
export const addLesson = payload => ({ type: actionTypes.ADD_LESSON, payload });
export const getGroupsData = () => ({ type: actionTypes.GET_GROUPS_REQ });
export const checkTokenLS = () => ({ type: actionTypes.CHECK_TOKEN });
export const getSchedule = () => ({ type: actionTypes.GET_SCHEDULE });
export const getStudents = payload => ({ type: actionTypes.GET_ALL_STUDENTS, payload });
export const getAttendance = () => ({ type: actionTypes.GET_ATTENDANCE });
export const changeAccountButton = () => ({ type: actionTypes.CHANGE_ACCOUNT_BUTTON });
export const getUsersForAdmin = () => ({ type: actionTypes.GET_ALL_USERS });
export const createGroup = payload => ({ type: actionTypes.CREATE_GROUP, payload });
export const setSelectedTeacher = payload => ({ type: actionTypes.SET_SELECTED_TEACHER, payload });
export const addStudentInGroup = payload => ({ type: actionTypes.ADD_STUDENT_IN_GROUP, payload });
export const setSelectedGroup = payload => ({ type: actionTypes.SET_SELECTED_GROUP, payload });
export const setSelectedFirstDay = payload => ({ type: actionTypes.SET_DATE_GROUP_UI_FROM, payload });
export const setSelectedLastDay = payload => ({ type: actionTypes.SET_DATE_GROUP_UI_TO, payload });
export const getUserData = () => ({ type: actionTypes.GET_USER_DATA });
export const updateUserData = payload => ({ type: actionTypes.SEND_UPDATE_USER_DATA_REQ, payload });
export const addNewTeacherInGroup = payload => ({ type: actionTypes.ADD_NEW_TEACHER_IN_GROUP, payload });
export const getSalary = payload => ({ type: actionTypes.GET_TEACHER_SALARY, payload });
export const deleteGroup = () => ({ type: actionTypes.DELETE_GROUP });
export const deleteUser = payload => ({ type: actionTypes.DELETE_USER, payload });
export const deleteTeacherFromGroup = () => ({ type: actionTypes.DELETE_TEACHER_FROM_GROUP });
export const deleteStudentFromGroup = payload => ({ type: actionTypes.DELETE_STUDENT, payload });
export const deleteLesson = payload => ({ type: actionTypes.DELETE_LESSON, payload });

