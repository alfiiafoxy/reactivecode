import { combineReducers } from 'redux';
import { configReducer } from './reducers/configReducer.js';
import { themeReducer } from '../managers/themeManager/reducer.js';
import { modalReducer } from '../managers/modalManager/modalReducer.js';
import { accountReducer } from './reducers/accountReducer.js';
import { translateReducer } from '../managers/localesManager/reducer.js';
import { dashboardReducer } from './reducers/dashboardReducer.js';
import { scheduleReducer } from './reducers/scheduleReducer.js';
import { attendanceReducer } from './reducers/attendanceReducer.js';
import { groupsReducer } from './reducers/groupsReducer.js';
import { studentsReducer } from './reducers/studentsReducer.js';
import { adminReducer } from './reducers/adminReducer.js';
import { myAccountPageReducer } from './reducers/myAccountPageReducer';

export default combineReducers({
  config: configReducer,
  theme: themeReducer,
  modal: modalReducer,
  account: accountReducer,
  dashboard: dashboardReducer,
  translates: translateReducer,
  schedule: scheduleReducer,
  attendance: attendanceReducer,
  groups: groupsReducer,
  students: studentsReducer,
  admin: adminReducer,
  accountButton: myAccountPageReducer,
});
