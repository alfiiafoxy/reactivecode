import actionTypes from '../../constans/actionTypes';

export const initialState = {
    isAccountButton: true,
};

export const myAccountPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_ACCOUNT_BUTTON:
            return {
                ...state,
                isAccountButton: !state.isAccountButton,
            };
        default:
            return state;
    }
};