import actionTypes from '../../constans/actionTypes';

export const initialState = {
  isChangeButton: true,
  attendance: null,
  daysOfLessons: null,
};

export const attendanceReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_CHANGE_SAVE_BUTTON:
      return {
        ...state,
        isChangeButton: !state.isChangeButton,
      };
    case actionTypes.PUT_ATTENDANCE_IN_STORE:
      return {
        ...state,
        attendance: action.payload,
      };
    case actionTypes.PUT_DAYS_OF_LESSONS:
      return {
        ...state,
        daysOfLessons: action.payload,
      };
    case actionTypes.CLEAR_ATTENDANCE_STORE:
      return {
        ...state,
        attendance: null,
        daysOfLessons: null,
      };
    default:
      return state;
  }
};
