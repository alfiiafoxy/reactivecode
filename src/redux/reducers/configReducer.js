import config from '../../config/config.js';
import actionTypes from '../../constans/actionTypes.js';

export const initialState = {
  ...config,
  isDisabledButtonAuth: false,
  isDisabledButtonReg: false,
};

export const configReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_DISABLE_BUTTON_AUTH:
      return {
        ...state,
        isDisabledButtonAuth: !state.isDisabledButtonAuth,
      };
    case actionTypes.TOGGLE_DISABLE_BUTTON_REG:
      return {
        ...state,
        isDisabledButtonReg: !state.isDisabledButtonReg,
      };
    default:
      return state;
  }
};
