import actionTypes from '../../constans/actionTypes';
import moment from 'moment';

export const initialState = {
  isChangeButton: true,
  users: [],
  teacherSalary: [],
  totalStatistic: [],
  dateFrom: moment().startOf('month').format('YYYY-MM-DD hh:mm'),
  dateTo: moment().endOf('month').format('YYYY-MM-DD hh:mm'),
};

export const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_USERS:
      return {
        ...state,
        users: action.payload,
      };
    case actionTypes.PUT_SALARY_IN_STORE:
      return {
        ...state,
        teacherSalary: action.payload.salary,
        totalStatistic: action.payload.total,
      };
    default:
      return state;
  }
};
