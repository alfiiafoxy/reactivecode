import { configReducer } from '../configReducer.js';

describe('Config reducer', () => {
    it('Config reducer TOGGLE_DISABLE_BUTTON_AUTH', () => {
        const initialState = {
            isDisabledButtonAuth: false,
            isDisabledButtonReg: false,
        };

        const action = {
            type: 'TOGGLE_DISABLE_BUTTON_AUTH',
        };

        const expected = {
            isDisabledButtonAuth: true,
            isDisabledButtonReg: false,
        };

        const actual = configReducer(initialState, action);
        assert.deepEqual(actual, expected);
    });

    it('Config reducer TOGGLE_DISABLE_BUTTON_REG', () => {
        const initialState = {
            isDisabledButtonAuth: false,
            isDisabledButtonReg: false,
        };

        const action = {
            type: 'TOGGLE_DISABLE_BUTTON_REG',
        };

        const expected = {
            isDisabledButtonAuth: false,
            isDisabledButtonReg: true,
        };

        const actual = configReducer(initialState, action);
        assert.deepEqual(actual, expected);
    });

    it('Receive state as object and return state as object ', () => {
        const initialState = {
            isDisabledButtonAuth: false,
            isDisabledButtonReg: false,
        };

        const action = {
            type: 'FAKE_ACTION',
        };

        const expected = {
            isDisabledButtonAuth: false,
            isDisabledButtonReg: false,
        };

        const actual = configReducer(initialState, action);
        assert.deepEqual(actual, expected);
    });
});
