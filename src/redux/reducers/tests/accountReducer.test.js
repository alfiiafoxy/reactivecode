import { accountReducer, initialState } from '../accountReducer.js';

describe('accountReducer', () => {
  it('User logged in', () => {
    const action = {
      type: 'LOG_IN',
      payload: 'token',
    };

    const expected = {
      ...initialState,
      token: 'token',
    };
    const actual = accountReducer(initialState, action);
    assert.deepEqual(actual, expected);
  });

  it('User logged out', () => {
    const action = {
      type: 'LOG_OUT',
      payload: null,
    };

    const expected = {
      ...initialState,
      token: null,
    };
    const actual = accountReducer(initialState, action);
    assert.deepEqual(actual, expected);
  });
});