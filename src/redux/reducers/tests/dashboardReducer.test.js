import { dashboardReducer } from '../dashboardReducer.js';

describe('dashboardReducer reducer', () => {
  it('Config reducer SWITCH_STATE_INPUTS to false', () => {
    const initialState = {
      isDisabledInputs: true,
    };

    const action = {
      type: 'SWITCH_STATE_INPUTS',
    };

    const expected = {
      isDisabledInputs: false,
    };

    const actual = dashboardReducer(initialState, action);
    assert.deepEqual(actual, expected);
  });

  it('Config reducer SWITCH_STATE_INPUTS to true', () => {
    const initialState = {
      isDisabledInputs: false,
    };

    const action = {
      type: 'SWITCH_STATE_INPUTS',
    };

    const expected = {
      isDisabledInputs: true,
    };

    const actual = dashboardReducer(initialState, action);
    assert.deepEqual(actual, expected);
  });

  it('Receive state as object and return state as object ', () => {
    const initialState = {
      isDisabledInputs: false,
    };

    const action = {
      type: 'FAKE_ACTION',
    };

    const expected = {
      isDisabledInputs: false,
    };

    const actual = dashboardReducer(initialState, action);
    assert.deepEqual(actual, expected);
  });
});
