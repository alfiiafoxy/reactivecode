import { scheduleReducer } from '../scheduleReducer.js';

describe('scheduleReducer', () => {
  it('CHANGE_SCHEDULE_WEEK', () => {
    const initialState = {
      currentWeekCount: 0,
    };

    const action = {
      type: 'CHANGE_SCHEDULE_WEEK',
      payload: 1,
    };

    const expected = {
      currentWeekCount: 1,
    };
    const actual = scheduleReducer(initialState, action);
    assert.deepEqual(actual, expected);
  });
});