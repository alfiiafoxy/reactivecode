import actionTypes from '../../constans/actionTypes.js';
import config from '../../config/config.js';

export const initialState = {
  isDisabledInputs: config.defaultStateInputs,
  currentInputUsers: 0,
};

export const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SWITCH_STATE_INPUTS:
      return {
        ...state,
        isDisabledInputs: !state.isDisabledInputs,
      };
    case actionTypes.CHANGE_CURRENT_INPUT_USERS:
      return {
        ...state,
        currentInputUsers: action.payload, //TODO sort logic schedule;
      };
    default:
      return state;
  }
};
