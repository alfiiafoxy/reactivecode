import moment from 'moment';
import actionTypes from '../../constans/actionTypes';

export const initialState = {
  currentDay: moment(new Date()).format('dddd'),
  currentData: moment(new Date()).format('DD.MM.YYYY'),
  currentLesson: 0,
  schedule: [],
  firstSelectWeekDay: 1,
  lastSelectWeekDay: 7,
  firstSelectWeekDayMilliseconds: moment(moment().day(1).format('YYYY-MM-DD'), 'YYYY-MM-DD').format('X'),
  lastSelectWeekDayMilliseconds: moment(moment().day(7).format('YYYY-MM-DD'), 'YYYY-MM-DD').format('X'),
};

export const scheduleReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_SCHEDULE_WEEK:
      return {
        ...state,
        currentWeekCount: action.payload,
      };
    case actionTypes.PUT_SCHEDULE_IN_STORE:
      return {
        ...state,
        schedule: action.payload,
      };
    case actionTypes.ADD_NEW_LESSON:
      return {
        ...state,
        schedule: action.payload, //TODO sort logic schedule;
      };
    case actionTypes.CHANGE_CURRENT_LESSON:
      return {
        ...state,
        currentLesson: action.payload, //TODO sort logic schedule;
      };
    case actionTypes.SET_PREV_WEEK:
      return {
        ...state,
        firstSelectWeekDay: action.payload.firstSelectWeekDay,
        lastSelectWeekDay: action.payload.lastSelectWeekDay,
        firstSelectWeekDayMilliseconds: action.payload.firstSelectWeekDayMilliseconds,
        lastSelectWeekDayMilliseconds: action.payload.lastSelectWeekDayMilliseconds,
      };
    case actionTypes.SET_NEXT_WEEK:
      return {
        ...state,
        firstSelectWeekDay: action.payload.firstSelectWeekDay,
        lastSelectWeekDay: action.payload.lastSelectWeekDay,
        firstSelectWeekDayMilliseconds: action.payload.firstSelectWeekDayMilliseconds,
        lastSelectWeekDayMilliseconds: action.payload.lastSelectWeekDayMilliseconds,
      };
    case actionTypes.SET_DATE_GROUP_UI_FROM_SAGA:
      return {
        ...state,
        firstSelectWeekDayMilliseconds: action.payload,
      };
    case actionTypes.SET_DATE_GROUP_UI_TO_SAGA:
      return {
        ...state,
        lastSelectWeekDayMilliseconds: action.payload,
      };
    default:
      return state;
  }
};