import actionTypes from '../../constans/actionTypes';

export const initialState = {
  groups: [],
  selectedGroup: 0,
  selectedTeacher: 0,
  teacherName: '',
};

export const groupsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_GROUPS:
      return {
        ...state,
        groups: action.payload,
      };
    case actionTypes.PUT_SELECTED_GROUP:
      return {
        ...state,
        selectedGroup: action.payload,
      };
    case actionTypes.PUT_SELECTED_TEACHER:
      return {
        ...state,
        selectedTeacher: action.payload,
      };
    case actionTypes.ADD_NEW_GROUP:
      return {
        ...state,
        groups: [...state.groups, action.payload],
      };
    case actionTypes.PUT_TEACHER_NAME:
      return {
        ...state,
        teacherName: action.payload,
      };
    default:
      return state;
  }
};
