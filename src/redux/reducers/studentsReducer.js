import actionTypes from '../../constans/actionTypes';

export const initialState = {
  allStudents: {},
};

export const studentsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PUT_STUDENTS_IN_STORE:
      return {
        ...state,
        allStudents: action.payload,
      };
    default:
      return state;
  }
};