import actionTypes from '../../constans/actionTypes';

const contentNameStorage = localStorage && localStorage.getItem('contentName');

export const initialState = {
  token: null,
  contentName: contentNameStorage ? contentNameStorage : 'schedule',
  userRole: '',
  isMyAccount: false,
  userData: {},
};

export const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOG_IN:
      return {
        ...state,
        token: action.payload.token,
        userRole: action.payload.userRole,
      };

    case actionTypes.SET_USER_DATA:
      return {
        ...state,
        userData: action.payload,
      };

    case actionTypes.LOG_OUT:
      return {
        ...state,
        token: null,
        contentName: 'schedule',
        userRole: '',
        isMyAccount: false,
        userData: {},
      };

    case actionTypes.CHANGE_NAME_CONTENT:
      return {
        ...state,
        contentName: action.payload,
      };

    case actionTypes.CHANGE_STATE_ISMYACCOUNT:
      return {
        ...state,
        isMyAccount: !state.isMyAccount,
      };

    default:
      return state;
  }
};
