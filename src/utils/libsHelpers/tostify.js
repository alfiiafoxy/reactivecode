import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const notificationSuccess = value => {
  toast.success(`🦖 ${value}`, {
    position: 'bottom-left',
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
};

export const notificationError = value => {
  toast.error(`🙀 ${value}`, {
    position: 'bottom-left',
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
};