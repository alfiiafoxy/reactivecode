import moment from 'moment';

export function validationLesson(data, role) {
  const date = `${data.dataOfLesson} ${data.startTime}`;

  if (data.startTime && data.endTime && data.dataOfLesson && validationData(date, role) && validationTime(data.startTime, data.endTime)) {
    return true;
  }
  return false;
}

function validationData(data, role) {
  if (role === 'admin') {
    return true;
  }
  let lessonData = moment(data, 'YYYY-MM-DD HH:mm').format('X');
  let dataNow = moment().format('X');

  return lessonData >= dataNow;
}

function validationTime(startTime, endTime) {
  return startTime <= endTime;
}