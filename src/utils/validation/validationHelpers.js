import { validationLength } from './validationAuth.js';
import moment from 'moment';

export const onKeyDownValid = (event, errorRef, errorText) => {
  if (validationLength(event.target.value.length, 50) || event.key === '/' || event.key === '<' || event.key === '>') {
    errorRef.innerText = errorText;
    if (event.key !== 'Backspace') {
      event.preventDefault();
    }
  }
};

export const checkIfDateValid = (dateTo, dateFrom) => {
  let isValid = false;
  if (dateTo && dateFrom) {
    if (moment(dateTo).isAfter(dateFrom)) {
      isValid = true;
    }
  }
  return isValid;
};