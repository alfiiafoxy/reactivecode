import * as regExp from './regularExp';

export const namesFieldValid = name => {
  if (!name.length) {
    return false;
  }
  return regExp.nameValid.test(name);
};

export const validEmail = email => {
  if (!email.length || email.length > 50) {
    return false;
  }
  return regExp.emailValid.test(String(email).toLowerCase());
};

export const validPassword = password => {
  if (password.length < 5 || password.length > 50) {
    return false;
  }
  return regExp.onlyWordAndNumbers.test(String(password).toLowerCase());
};

export const checkValidPassword = (password, checkPassword) => {
  if (!checkPassword.length) {
    return false;
  }
  return password === checkPassword;
};

export const validPhone = phone => {
  if (!phone.length || phone.length < 4) {
    return false;
  }
  return regExp.phoneValid.test(String(phone).toLowerCase());
};

export const validData = date => {
  if (!date.length) {
    return false;
  }
  return regExp.dataValid.test(String(date).toLowerCase());
};

export const validKeyword = keyword => {
  if (keyword.length === 0) {
    return true;
  } else if (keyword.length > 50) {
    return false;
  }
  return regExp.onlyWordAndNumbers.test(String(keyword).toLowerCase());
};

export const validAddGroup = groupName => {
   if (!groupName.length || groupName.length > 50) {
    return false;
  }
  return regExp.onlyWordAndNumbers.test(String(groupName).toLowerCase());
};

export const validRatePerHour = ratePerHour => {
  if (ratePerHour < 0) {
    return false;
  }

  return regExp.phoneValid.test(String(ratePerHour).toLowerCase());
};