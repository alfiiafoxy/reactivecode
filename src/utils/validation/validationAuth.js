import * as regExp from './regularExp.js';

export const onKeyDownValid = event => {
  if (validationLength(event.target.value.length, 50) || event.key === '/') {
    if (event.key !== 'Backspace') {
      event.preventDefault();
    } else if (event.key === '<' || event.key === '>') {
      event.key.replace = '';
    }
  }
};

export const checkValidPassword = e => {
  if (e.key.match(/[\<\>]/i) || e.target.value.length >= 15) {
    if (e.key !== 'Backspace') {
      e.preventDefault();
    }
  }
};

export const validOnChange = event => {
  return !validationLength(event.value.length, 50);
};

export const validationLength = (lengthValue, maxLength) => {
  return lengthValue >= maxLength;
};

export const validOnClickAuth = data => {
  if (!data.userName || !data.password) {
    return false;
  }
  return regExp.emailValid.test(String(data.userName).toLowerCase());
};