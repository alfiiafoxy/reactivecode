import * as validation from '../../validation/validationAuth';

describe('validOnChange function', () => {
  const input = document.createElement('input');

  it('validOnChange value is 12', () => {
    input.value = 'adadadadadad';
    const action = validation.validOnChange(input);
    assert.isTrue(action, 'validationLoginSuccess');
    input.value = '';
  });

  it('validOnChange value is 50', () => {
    input.value = 'asasasasasasasasasasasasasasasasasasasas';
    const action = validation.validOnChange(input);
    assert.isTrue(action, 'validationLoginSuccess');
    input.value = '';
  });

  it('validOnChange value is 51', () => {
    input.value = 'asasasasasasasasasasasasasasasasasasasa1s';
    const action = validation.validOnChange(input);
    assert.isTrue(action, 'validationLoginSuccess');
    input.value = '';
  });
});

describe('validationLength function', () => {
  it('validationLength value is 32 and max length 32', () => {
    const value = 32;
    const action = validation.validationLength(value, 32);
    assert.isTrue(action, 'validationLoginSuccess');
  });

  it('validationLength value is 12 and max length 42', () => {
    const value = 'asasasasasas';
    const action = validation.validationLength(value, 42);
    assert.isFalse(action, 'validationLoginError');
  });

  it('validationLength value is 11 and max length 5', () => {
    const value = 11;
    const action = validation.validationLength(value, 5);
    assert.isTrue(action, 'validationLoginSuccess');
  });
});

describe('validOnClickAuth function', () => {
  it('validOnClickAuth two field is empty line', () => {
    const value = {
      userName: '',
      password: '',
    };
    const action = validation.validOnClickAuth(value);
    assert.isFalse(action, 'validationLoginError');
  });

  it('validOnClickAuth second field is empty line ', () => {
    const value = {
      userName: '',
      password: '4444',
    };
    const action = validation.validOnClickAuth(value);
    assert.isFalse(action, 'validationLoginError');
  });

  it('validOnClickAuth first field is empty line ', () => {
    const value = {
      userName: '4444',
      password: '',
    };
    const action = validation.validOnClickAuth(value);
    assert.isFalse(action, 'validationLoginError');
  });

  it('validOnClickAuth check userName qwert@qwert.qwert ', () => {
    const value = {
      userName: 'qwert@qwert.qwert',
      password: '4654654',
    };
    const action = validation.validOnClickAuth(value);
    assert.isTrue(action, 'validationLoginSuccess');
  });

  it('validOnClickAuth check userName qwert123qwert.qwert ', () => {
    const value = {
      userName: 'qwert123qwert.qwert',
      password: '4654654',
    };
    const action = validation.validOnClickAuth(value);
    assert.isFalse(action, 'validationLoginError');
  });

  it('validOnClickAuth check userName q.qwert', () => {
    const value = {
      userName: 'q.qwert',
      password: '4654654',
    };
    const action = validation.validOnClickAuth(value);
    assert.isFalse(action, 'validationLoginError');
  });
});
