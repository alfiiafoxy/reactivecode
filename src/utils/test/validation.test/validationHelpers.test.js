import * as validation from '../../validation/validationHelpers.js';

describe('onKeyDownValid function', () => {
  const event = {
    target: {
      value: 'asdf',
    },
    preventDefault: () => true,
  };

  it('onKeyDownValid key = / and innerText = error', () => {
    event.key = '/';
    const errorRef = {
      innerText: 'no error',
    };

    const errorText = 'error';
    const expected = 'error';
    validation.onKeyDownValid(event, errorRef, errorText);
    const action = errorRef.innerText;

    assert.equal(action, expected);
  });

  it('onKeyDownValid event Prevent default', () => {
    event.key = '/';
    const errorRef = {
      innerText: 'no error',
    };
    const errorText = 'error';

    validation.onKeyDownValid(event, errorRef, errorText);
    const action = event.preventDefault;
    assert.isFunction(action, 'preventDefault is function');
  });
});