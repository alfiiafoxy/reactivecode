import * as validation from '../../validation/validationReg';

describe('namesFieldValid function', () => {
  it('namesFieldValid value ""', () => {
    const value = '';
    const action = validation.namesFieldValid(value);
    assert.isFalse(action, 'namesFieldEmpty');
  });

  it('namesFieldValid value Max', () => {
    const value = 'Max';
    const action = validation.namesFieldValid(value);
    assert.isTrue(action, 'namesFieldValidSuccess');
  });

  it('namesFieldValid value anakonda', () => {
    const value = 'anakonda';
    const action = validation.namesFieldValid(value);
    assert.isFalse(action, 'namesFieldValidError');
  });

  it('namesFieldValid value anak1onda', () => {
    const value = 'anak1onda';
    const action = validation.namesFieldValid(value);
    assert.isFalse(action, 'namesFieldValidError');
  });
});

describe('validEmail function', () => {
  it('validEmail value qwerty@qwerty.qwerty', () => {
    const value = 'qwerty@qwerty.qwerty';
    const action = validation.validEmail(value);
    assert.isTrue(action, 'validationEmailSuccess');
  });

  it('validEmail value qwert.qwerty@aqwe', () => {
    const value = 'qwert.qwerty@aqwe';
    const action = validation.validEmail(value);
    assert.isFalse(action, 'validationEmailError');
  });

  it('validEmail value qwertqw.erty.aqwe', () => {
    const value = 'qwertqw.erty.aqwe';
    const action = validation.validEmail(value);
    assert.isFalse(action, 'validationEmailError');
  });

  it('validEmail value qwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwerty.asfasf@asfasf.asfasf', () => {
    const value = 'qwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwerty.asfasf@asfasf.asfasf';
    const action = validation.validEmail(value);
    assert.isFalse(action, 'validationEmailSuccess');
  });
});

describe('validPassword function', () => {
  it('validPassword value a2345 length is 5', () => {
    const value = 'a23455';
    const action = validation.validPassword(value);
    assert.isTrue(action, 'validPasswordSuccess');
  });

  it('validPassword value qwerty_qwrty', () => {
    const value = 'qwertyqwrty';
    const action = validation.validPassword(value);
    assert.isTrue(action, 'validPasswordSuccess');
  });

  it('validPassword value a234 (length less then 5)', () => {
    const value = 'a234';
    const action = validation.validPassword(value);
    assert.isFalse(action, 'validPasswordError');
  });

  it('validPassword value a2345678910 (length is 11)', () => {
    const value = 'a2345678910';
    const action = validation.validPassword(value);
    assert.isFalse(action, 'validPasswordSuccess');
  });

  it('validPassword value a234567893214567891 (length is 19)', () => {
    const value = 'a234567893214567891';
    const action = validation.validPassword(value);
    assert.isFalse(action, 'validPasswordError');
  });

  it('validPassword value a2345@123@ (password has symbol @)', () => {
    const value = 'a2345@123@';
    const action = validation.validPassword(value);
    assert.isFalse(action, 'validPasswordError');
  });

  it('validPassword value a2345!# (password has symbol !#)', () => {
    const value = 'a2345!#';
    const action = validation.validPassword(value);
    assert.isFalse(action, 'validPasswordError');
  });

  it('validPassword value a2345`* (password has symbol `*)', () => {
    const value = '12345`*';
    const action = validation.validPassword(value);
    assert.isFalse(action, 'validPasswordError');
  });

  it('validPassword value 1234asfasf (password start at number)', () => {
    const value = '1234asfasf';
    const action = validation.validPassword(value);
    assert.isFalse(action, 'validPasswordError');
  });
});

describe('checkValidPassword function', () => {
  it('checkValidPassword checkPassword length is 0', () => {
    const password = 'asdqwer';
    const checkPassword = '';
    const action = validation.checkValidPassword(password, checkPassword);

    assert.isFalse(action, 'checkValidPasswordError');
  });

  it('checkValidPassword checkPassword is similar', () => {
    const password = 'asdqwer';
    const checkPassword = 'asdqwer';
    const action = validation.checkValidPassword(password, checkPassword);

    assert.isTrue(action, 'checkValidPassword');
  });

  it('checkValidPassword checkPassword don\'t similar', () => {
    const password = 'asdqw';
    const checkPassword = 'asdqwer';
    const action = validation.checkValidPassword(password, checkPassword);

    assert.isFalse(action, 'checkValidPasswordError');
  });
});

describe('validPhone function', () => {
  it('validPhone phone length is 0', () => {
    const phone = '';
    const action = validation.validPhone(phone);

    assert.isFalse(action, 'validPhoneError');
  });

  it('validPhone phone +380958787778', () => {
    const phone = '+380958787778';
    const action = validation.validPhone(phone);

    assert.isTrue(action, 'validPhoneSuccess');
  });

  it('validPhone phone 380958787778 without +', () => {
    const phone = '380958787778';
    const action = validation.validPhone(phone);

    assert.isFalse(action, 'validPhoneError');
  });

  it('validPhone phone without *', () => {
    const phone = '3809*8787778';
    const action = validation.validPhone(phone);

    assert.isFalse(action, 'validPhoneError');
  });

  it('validPhone phone without words', () => {
    const phone = '3809sd8787778';
    const action = validation.validPhone(phone);

    assert.isFalse(action, 'validPhoneError');
  });

  it('validPhone phone with -', () => {
    const phone = '380-98787778';
    const action = validation.validPhone(phone);

    assert.isFalse(action, 'validPhoneError');
  });

  it('validPhone phone length bigger then 13', () => {
    const phone = '+3809587877781231414';
    const action = validation.validPhone(phone);

    assert.isFalse(action, 'validPhoneError');
  });
});

describe('validData function', () => {
  it('validData date length is 0', () => {
    const date = '';
    const action = validation.validData(date);

    assert.isFalse(action, 'validDataError');
  });

  it('validData date 12.12.1234', () => {
    const date = '12.12.1234';
    const action = validation.validData(date);

    assert.isTrue(action, 'validDataSuccess');
  });

  it('validData date format 1.12.1234', () => {
    const date = '1.12.1234';
    const action = validation.validData(date);

    assert.isFalse(action, 'validDataError');
  });

  it('validData date format 1.1.1234', () => {
    const date = '12.1.1234';
    const action = validation.validData(date);

    assert.isFalse(action, 'validDataError');
  });

  it('validData date format 1.1.123', () => {
    const date = '12.14.123';
    const action = validation.validData(date);

    assert.isFalse(action, 'validDataError');
  });

  it('validData date format 1.1.12443', () => {
    const date = '12.14.12443';
    const action = validation.validData(date);

    assert.isFalse(action, 'validDataError');
  });
});

describe('validKeyword function', () => {
  it('validData keyword length is 0', () => {
    const keyword = '';
    const action = validation.validKeyword(keyword);

    assert.isFalse(action, 'validKeywordError');
  });

  it('validData keyword length bigger then 50', () => {
    const keyword = 'asdasdasdasdasvdjjjjjjjjjjjjjasdasdasdasdasvdjjjjjjjjjjjjjasdasdasdasdasvdjjjjjjjjjjjjjasdasdasdasdasvdjjjjjjjjjjjjjasdasdasdasdasvdjjjjjjjjjjjjj';
    const action = validation.validKeyword(keyword);

    assert.isFalse(action, 'validKeywordError');
  });

  it('validData keyword start with number', () => {
    const keyword = '1asdasdasdasdasvdj';
    const action = validation.validKeyword(keyword);

    assert.isFalse(action, 'validKeywordError');
  });

  it('validData keyword has symbol @', () => {
    const keyword = 'asda@sdasdasdasvdj';
    const action = validation.validKeyword(keyword);

    assert.isFalse(action, 'validKeywordError');
  });

  it('validData keyword valid', () => {
    const keyword = 'asdasdasdasdasvdj';
    const action = validation.validKeyword(keyword);

    assert.isTrue(action, 'validKeywordError');
  });
});