const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');

const mock = new MockAdapter(axios);

mock.onGet('/reg').reply(200, {
  users: [{ id: 1, name: 'John Smith' }],
});

axios.get('/reg').then(function (response) {
  console.log(response.data);
});