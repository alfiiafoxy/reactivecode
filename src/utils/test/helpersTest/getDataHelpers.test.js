import * as dataHelpers from '../../helpers/getDataHelpers.js';

describe('Testing getDataFromInputs: ', () => {
  it('getDataFromInputs correct', () => {
    const data = {
      name: {
        current: {
          value: 5,
        },
      },
    };
    const expected = { name: 5 };
    const actual = dataHelpers.getDataFromInputs(data);
    assert.deepEqual(actual, expected);
  });

  it('getDataFromInputs value: name, value: qwerty', () => {
    const data = {
      name: {
        current: {
          value: 5,
        },
        qwerty: {
          current: {
            value: 'qwerty',
          },
        },
      },
    };

    const expected = { name: 5, qwerty: 'qwerty' };
    const actual = dataHelpers.getDataFromInputs(data);
    assert.notDeepEqual(actual, expected);
  });
});