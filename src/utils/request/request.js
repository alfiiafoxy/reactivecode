const axios = require('axios');
const constants = require('../../../server/constants/constants');

export const postRequestSender = (path, data, token) => {
  return axios({
    method: 'post',
    url: `${constants.serverPath}${path}`,
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      authorization: `${token}`,
    },
    data,
  });
};

export const deleteRequestSender = (path, data, token) => {
  return axios({
    method: 'delete',
    url: `${constants.serverPath}${path}`,
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      authorization: `${token}`,
    },
    data,
  });
};

export const getRequestSender = (path, token) => {
  return axios({
    method: 'get',
    url: `${constants.serverPath}${path}`,
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      authorization: `${token}`,
    },
  });
};