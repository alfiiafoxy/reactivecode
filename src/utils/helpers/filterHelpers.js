export const getResultSearch = (value, itemValue) => {
  if (!value) {
    return true;
  } else if (value && itemValue && Array.isArray(itemValue)) {
    return itemValue.find((el) => {
      return el.toLowerCase().includes(value.toLowerCase());
    });
  } else if (typeof +value === 'number' && itemValue) {
      return itemValue.toString().includes(value.toString());
  } else if (value && itemValue) {
    return itemValue.toLowerCase().includes(value.toLowerCase());
  } else if (!itemValue && value) {
    return false;
  }
};

export const filterScheduleByCity = (schedule, city, groups) => {
  if (+city.current.value === 0) {
    return schedule;
  }
  let arrayOfGroupId = null;
  const arrayOfCurrentGroup = groups.filter(group => group.city === city.current.value);
  arrayOfGroupId = arrayOfCurrentGroup.map(group => group.id);
  if (!arrayOfGroupId) {
    return;
  }
  return filterScheduleByArrayOfGroupId(schedule, arrayOfGroupId);
};

export const filterScheduleByTeacherId = (schedule, teacherId, groups) => {
  if (+teacherId.current.value === 0) {
    return schedule;
  }
  let arrayOfGroupId = null;
  const arrayOfCurrentGroup = groups.filter(group => group.teacher_id === +teacherId.current.value);
  arrayOfGroupId = arrayOfCurrentGroup.map(group => group.id);
  if (!arrayOfGroupId) {
    return;
  }
  return filterScheduleByArrayOfGroupId(schedule, arrayOfGroupId);
};

export const filterScheduleByArrayOfGroupId = (schedule, arrayOfGroupId) => {
  let newSchedule = schedule.map(day => {
    if (!day) {
      return;
    }
    let lessons = day.lessons.filter(lesson => arrayOfGroupId.includes(lesson.groupsId));
    return { ...day, lessons };
  });

  newSchedule = newSchedule.map(day => {
    if (!day) {
      return;
    }
    if (day.lessons.length === 0) {
      return;
    }
    return day;
  });

  return newSchedule.filter(day => day && day.lessons.length !== 0);
};