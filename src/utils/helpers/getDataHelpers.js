import moment from 'moment';
import XLSX from 'xlsx';
import FileSaver from 'file-saver';

export const getDataFromInputs = data => {
  let result = {};
  for (let key in data) {
    if (data.hasOwnProperty(key)) {
      result = {
        ...result,
        [key]: data[key].current.value,
      };
    }
  }
  return result;
};

export const clearInputs = data => {
  for (let key in data) {
    if (data.hasOwnProperty(key)) {
      data[key].current.value = '';
    }
  }
};

export const getRefOfElement = data => {
  let result = {};
  for (let key in data) {
    if (data.hasOwnProperty(key)) {
      result = {
        ...result,
        [key]: data[key].current,
      };
    }
  }
  return result;
};

export const changeDataToTimestamp = data => {
  const startTime = `${data.dataOfLesson} ${data.startTime}`;
  const endTime = `${data.dataOfLesson} ${data.endTime}`;

  return { startTime, endTime, dataOfLesson: data.dataOfLesson, typeOfLesson: data.typeOfLesson, groupsId: +data.groupsId };
};

export const changeToTimestampForStatistic = data => {
  let result = { ...data };
  for (let key in data) {
    if (data.hasOwnProperty(key)) {
      result = {
        ...result,
        [key]: `${data[key]} 00:00:00`,
      };
    }
  }
  return result;
};

export const changeTimeToTimestamp = data => {
  const currentData = moment(data.data.dataOfLesson.data, 'DD.MM.YYYY').format('YYYY-MM-DD');
  const start_time = `${currentData} ${data.data.startTime}`;
  const finish_time = `${currentData} ${data.data.endTime}`;

  return {
    start_time,
    finish_time,
    lesson_type: data.lesson_type,
    lesson_id: data.lesson_id,
  };
};

export const sumTwoHours = (firstHour, secondHour) => {
  const getCorrectHours = minutes => {
    if (minutes >= 60) {
      if (minutes < 70) {
        sumOfHours += 1;
        return `0${minutes - 60}`;
      }
      sumOfHours += 1;
      return minutes - 60;
    } else {
      return minutes;
    }
  };

  let sumOfHours = +firstHour.split(':')[0] + +secondHour.split(':')[0];
  const sumOfMinutes = +firstHour.split(':')[1] + +secondHour.split(':')[1];
  const checkHours = getCorrectHours(sumOfMinutes);
  const resultHours = `${sumOfHours}:${checkHours}`;
  if (resultHours.length < 5) {
    return `0${resultHours}`;
  }
  return resultHours;
};

export function exportExcel(headers, data, fileName, sheetName) {
  const wb = XLSX.utils.book_new();
  wb.SheetNames.push(sheetName);
  const ws_data = [
    headers,
    ...data,
  ];
  const ws = XLSX.utils.aoa_to_sheet(ws_data);
  wb.Sheets[sheetName] = ws;
  const wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });

  function StringToArrayBuffer(s) {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i < s.length; i++) {
      view[i] = s.charCodeAt(i) & 0xFF;
    }
    return buf;
  }
  FileSaver.saveAs(new Blob([StringToArrayBuffer(wbout)], { type: 'text/plain;charset=utf-8' }), `${fileName}.xlsx`);
}