const morgan = require('morgan');
const express = require('express');
const { Client } = require('pg');
const httpServer = express();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const Server = require('./helpers/helpers.js').Server;
const constants = require('./constants/constants');
const constHelpers = require('./constants/constHelpers');
const ConnectionPostgresQlDB = require('./helpers/helpers.js').ConnectionWithDataBase;
const UserStorage = require('./helpers/userStorageHelper').UserStorage;

const dataBase = new ConnectionPostgresQlDB(Client, constHelpers.connectionString);
const userStorage = new UserStorage();
const server = new Server(httpServer, constants, morgan, bodyParser, constHelpers.accessLogStream, dataBase.client, jwt, userStorage);

server.run(dataBase);