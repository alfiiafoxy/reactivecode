const validation = require('../helpers/serverValidationHelpers');
const User = require('../helpers/userStorageHelper').User;
const queryHelpers = require('./queryHelpser');

class Helper {
  static getUserRegistration = function (req, res, client, jwt, defaultRole, userStorage) {
    const isValid = validation.serverValidation(req.body);
    const { email } = req.body;
    if (isValid) {
      const token = jwt.sign({ email }, defaultRole);
      queryHelpers.getUserEmail(client, email, 'reg')
        .then(checkEmail => {
          if (checkEmail !== 'exist') {
            queryHelpers.createNewUser(client, req.body, defaultRole, token, userStorage, User, checkEmail);
          } else {
            return Promise.resolve(checkEmail);
          }
        })
        .then(checkEmail => {
          if (checkEmail !== 'exist') {
            res.status(200).json({ status: 'OK', email: req.body.email, token, userRole: defaultRole });
          } else {
            res.status(202).send(checkEmail);
          }
        })
        .catch(err => res.status(502).send('SERVER ERROR', err));
    } else {
      res.status(502).send('SERVER ERROR No valid Data');
    }
  };

  static getUserAuthorize = function (req, res, client, jwt, userStorage) {
    const isValid = validation.serverValidation(req.body);
    if (isValid) {
      let sql = `SELECT * FROM "user" WHERE email = '${req.body.email}' AND password = '${req.body.password}' ORDER BY id ASC; `;
      client.query(sql,
        (error, response) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else if (response.rows.length) {
            const { email, password, user_role, id } = response.rows[0];
            const token = jwt.sign({ email }, password);
            const userData = new User(user_role, token, id);
            userStorage.addUser(userData);
            res.status(200).json({ status: 'OK', user_role, token, password });
          } else {
            res.status(500).send('SERVER ERROR');
          }
        });
    } else {
      res.status(502).send('SERVER ERROR No valid Data');
    }
  };

  static getUserData = function (req, res, client, userStorage) {
    const { authorization } = req.headers;
    const userId = userStorage.getIdByUserToken(authorization);
    let sql = `SELECT first_name, last_name, email, phone FROM "user" WHERE id = '${userId}'`;
    client.query(sql,
      (error, response) => {
        if (error) {
          res.status(500).send('SERVER ERROR');
        } else {
          res.status(200).json(response.rows[0]);
        }
      });
  };

  static getUserGetGroups = function (req, res, client, userStorage) {
    const { authorization } = req.headers;
    const haveAccess = userStorage.getTokenRoleByUserToken(authorization);
    const userId = userStorage.getIdByUserToken(authorization);

    if (haveAccess === 'teacher') {
      let sql = `select groups.id, groups.name, groups.level, groups.teacher_id, groups.city, "user".first_name, "user".last_name 
from "groups" inner join "user" on groups.teacher_id = "user".id WHERE groups.teacher_id = '${userId}'; `;
      client.query(sql,
        (error, response) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send(response.rows);
          }
        });
    } else if (haveAccess === 'admin') {
      let sql = `SELECT groups.id, groups.name, groups.level, groups.teacher_id, groups.city, "user".first_name, "user".last_name 
FROM "groups" INNER JOIN "user" ON groups.teacher_id = "user".id;`;
      client.query(sql,
        (error, response) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send(response.rows);
          }
        });
    } else if (haveAccess === 'student') {
      queryHelpers.selectGroupId(client, userId, haveAccess)
        .then((groups) => queryHelpers.selectDataFromTable(client, groups, 'groups', 'id', 'id'))
        .then(data => {
          res.status(200).json(data);
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static getSchedule = function (req, res, client, userStorage) {
    const { authorization } = req.headers;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    const userId = userStorage.getIdByUserToken(authorization);
    if (role === 'admin') {
      queryHelpers.selectScheduleAdmin(client)
        .then(data => {
          res.status(200).json({ status: 'OK', data });
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else if (role === 'teacher' || role === 'student') {
      queryHelpers.selectGroupId(client, userId, role)
        .then((groups) => queryHelpers.selectDataFromTable(client, groups, 'lessons', 'group_id', 'date'))
        .then(data => {
          res.status(200).json({ status: 'OK', data });
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static createLessonInDb = function (req, res, client, userStorage) {
    const { token } = req.body;
    const role = userStorage.getTokenRoleByUserToken(token);
    if (role !== 'student') {
      queryHelpers.addLesson(client, req.body)
        .then(lessonId => queryHelpers.getUsersByGroup(client, req.body, lessonId))
        .then(data => queryHelpers.setAttendance(client, data))
        .then(data => {
          res.status(200).json({ status: 'OK', data });
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static getStudents = function (req, res, client, userStorage) {
    const { authorization } = req.headers;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    const userId = userStorage.getIdByUserToken(authorization);
    if (role === 'admin') {
      let sql = `SELECT users.id, users.first_name, users.last_name, users.birth_date, users.phone, 
                        users.user_role, users.email, groups.name, groups.level, groups_detail.group_id 
                        FROM "user" as users INNER JOIN groups_detail on users.id = groups_detail.student_id
                        INNER JOIN groups on groups_detail.group_id = groups.id 
                        WHERE groups_detail.group_id in (select groups_detail.group_id
                        FROM groups_detail                        
                        GROUP BY groups_detail.group_id)`;
      client.query(sql,
        (error, response) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send(response.rows);
          }
        });
    }
    if (role === 'teacher') {
      let sql = `SELECT students.id, students.first_name, students.last_name, students.email, students.birth_date, students.phone, 
                         groups_detail.group_id, groups.name
                         FROM "user" as students 
                         INNER JOIN groups_detail on students.id = groups_detail.student_id
                         INNER JOIN groups on groups_detail.group_id = groups.id 
                         WHERE groups_detail.group_id in (SELECT groups.id
                         FROM "user" as teachers INNER JOIN groups on teachers.id = groups.teacher_id
                         WHERE teachers.id = ${userId})`;
      client.query(sql,
        (error, response) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send(response.rows);
          }
        });
    } else if (role === 'student') {
      let sql = `SELECT students.id, students.first_name, students.last_name, students.birth_date, students.email, students.phone, students.user_role, groups_detail.group_id, groups.name, groups.level 
                        FROM "user" as students inner join groups_detail on students.id = groups_detail.student_id
                        INNER JOIN groups on groups_detail.group_id = groups.id 
                        WHERE groups_detail.group_id in (SELECT groups_detail.group_id
                            FROM groups_detail
                            WHERE student_id = '${userId}'
                            GROUP BY groups_detail.group_id);`;
      client.query(sql,
        (error, response) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send(response.rows);
          }
        });
    }
  };

  static getAttendance = function (req, res, client, userStorage) {
    const { authorization } = req.headers;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    const userId = userStorage.getIdByUserToken(authorization);
    let sql;
    let lessonsDate = [];
    if (role === 'admin') {
      queryHelpers.getDateOfLessonByGroup(client, userId, role)
        .then(date => {
          lessonsDate = date;
        });
      sql = `select attendance.id as attendance_id, attendance.lesson_id, attendance.student_id, attendance.attendance, lessons.group_id, lessons.start_time, "user".first_name, "user".last_name 
                from attendance inner join lessons on attendance.lesson_id = lessons.id
                inner join "user" on attendance.student_id = "user".id;`;
    } else if (role === 'student') {
      queryHelpers.getDateOfLessonByGroup(client, userId, role)
        .then(date => {
          lessonsDate = date;
        });
      sql = `select attendance.id as attendance_id, attendance.lesson_id, attendance.student_id, attendance.attendance, lessons.group_id, lessons.start_time, "user".first_name, "user".last_name 
                from attendance inner join lessons on attendance.lesson_id = lessons.id
                inner join "user" on attendance.student_id = "user".id
                where lessons.group_id in (select groups_detail.group_id
                from groups_detail
                where student_id = '${userId}'
                group by groups_detail.group_id);`;
    } else if (role === 'teacher') {
      queryHelpers.getDateOfLessonByGroup(client, userId, role)
        .then(date => {
          lessonsDate = date;
        });
      sql = `select attendance.id as attendance_id, attendance.lesson_id, attendance.student_id, attendance.attendance, lessons.group_id, lessons.start_time, "user".first_name, "user".last_name 
                from attendance inner join lessons on attendance.lesson_id = lessons.id
                inner join "user" on attendance.student_id = "user".id;`;
    }//TODO load groups only teacher
    client.query(sql,
      (error, response) => {
        if (error) {
          res.status(500).send(error);
        } else {
          res.status(200).send({ attendance: response.rows, date: lessonsDate });
        }
      });
  };

  static deleteLesson = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { lesson_id } = req.body;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role !== 'student') {
      queryHelpers.deleteAttendance(client, lesson_id)
        .then(lessonId => queryHelpers.deleteLesson(client, lessonId))
        .then(groupId => {
          res.status(200).json({ status: 'ok', groupId });
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static deleteStudent = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { user_id } = req.body;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role !== 'student') {
      queryHelpers.deleteStudentFromGroupsDetail(client, user_id)
        .then(() => queryHelpers.deleteAttendanceByUserID(client, user_id))
        .then(data => {
          res.status(200).json({ status: 'ok', data });
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static setAttendance = (req, res, client, userStorage) => {
    const { attendance_id, attendancePayload } = req.body;
    const { authorization } = req.headers;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role !== 'student') {
      let sql = `UPDATE attendance SET attendance=${attendancePayload} WHERE id=${attendance_id}; `;
      client.query(sql,
        (error) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send('Ok');
          }
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static getAllUserData = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role !== 'student') {
      queryHelpers.getUsersData(client, role)
        .then(users => queryHelpers.getGroupDetail(client, users, role))
        .then(data => {
          res.status(200).json({ status: 'OK', data });
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static updateCurrentUser = (req, res, client, userStorage) => {
      const { authorization } = req.headers;
      const { userId, email } = req.body;
      const role = userStorage.getTokenRoleByUserToken(authorization);
      if (role === 'admin') {
        queryHelpers.getUserEmail(client, email, 'users', userId)
          .then(checkEmail => {
            if (checkEmail !== 'exist') {
              queryHelpers.updateCurrentUserHelper(client, req.body, userId);
            } else {
              return Promise.resolve(checkEmail);
            }
          })
          .then(checkEmail => {
            if (checkEmail !== 'exist') {
              res.status(200).send(checkEmail);
            } else {
              res.status(202).send(checkEmail);
            }
          })
          .catch(err => res.status(502).send('SERVER ERROR', err));
      } else {
        res.status(502).send('SERVER ERROR No Access');
      }
  };

  static updateUserData = (req, res, client, userStorage) => {
    const isValid = validation.serverValidation(req.body);
    if (isValid) {
      const { authorization } = req.headers;
      const { email } = req.body;
      const userId = userStorage.getIdByUserToken(authorization);
      queryHelpers.getUserEmail(client, email, 'account', userId)
        .then(checkEmail => {
          if (checkEmail !== 'exist') {
            queryHelpers.updateUserDataHelper(client, req.body, userId);
          } else {
            return Promise.resolve(checkEmail);
          }
        })
        .then(checkEmail => {
          if (checkEmail !== 'exist') {
            res.status(200).send(checkEmail);
          } else {
            res.status(202).send(checkEmail);
          }
        })
        .catch(err => res.status(502).send('SERVER ERROR', err));
    } else {
      res.status(502).send('SERVER ERROR No valid Data');
    }
  };

  static setLessonData = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { lesson_id, lesson_type, finish_time, start_time } = req.body;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role !== 'student') {
      let sql = `UPDATE lessons SET start_time='${start_time}', finish_time='${finish_time}', lesson_type='${lesson_type}' WHERE  id=${lesson_id};`;
      client.query(sql,
        (error) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send('Ok');
          }
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static createGroup = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    const { name, level, selectedTeacher, city } = req.body;
    if (role === 'admin') {
      let sql = `INSERT INTO "groups" (name, level, teacher_id, city)
                VALUES ('${name}', '${level}', ${selectedTeacher}, '${city}') RETURNING id;`; // TODO fix city from client
      client.query(sql,
        (error, response) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send(response.rows[0]);
          }
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static addStudentInGroup = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { groupId, studentId } = req.body;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role !== 'student') {
      queryHelpers.addStudentInDb(client, groupId, studentId)
        .then(data => {
          res.status(200).json(data);
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static updateTeacherInGroup = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { id, selectedGroup } = req.body;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role === 'admin') {
      queryHelpers.setTeacherIdInGroup(client, id, selectedGroup)
        .then(data => {
          res.status(200).json(data);
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static getTeacherSalary = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { dateFrom, dateTo } = req.body;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    const userId = userStorage.getIdByUserToken(authorization);
    if (role !== 'student') {
      queryHelpers.getStatistic(client, userId, role, dateFrom, dateTo)
        .then(data => queryHelpers.getStatisticAttendance(client, userId, role, dateFrom, dateTo, data))
        .then(data => res.status(200).send(data))
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static deleteUser = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { user_id, user_role } = req.body;
    const adminId = userStorage.getIdByUserToken(authorization);
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role === 'admin') {
      if (user_role === 'student') {
        queryHelpers.deleteStudentFromGroupsDetail(client, user_id)
          .then(() => queryHelpers.deleteAttendanceByUserID(client, user_id))
          .then(() => queryHelpers.deleteUser(client, user_id))
          .then(() => {
            res.status(200).json({ status: 'ok' });
          })
          .catch(error => {
            res.status(500).send(`SERVER ERROR ${error}`);
          });
      } else if (user_id !== adminId) {
        queryHelpers.setTeacherIdByTeacherId(client, adminId, user_id)
          .then(() => queryHelpers.deleteUser(client, user_id))
          .then(() => {
            res.status(200).json({ status: 'Ok' });
          })
          .catch(error => {
            res.status(500).send(`SERVER ERROR ${error}`);
          });
      } else {
        res.status(502).send('SERVER ERROR No Access');
      }
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static deleteGroup = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { group_id } = req.body;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    if (role === 'admin') {
      queryHelpers.deleteAttendanceByGroupId(client, group_id)
        .then(() => queryHelpers.deleteLessonByGroupId(client, group_id))
        .then(() => queryHelpers.deleteGroupsDetailByGroupId(client, group_id))
        .then(() => queryHelpers.deleteGroupByGroupId(client, group_id))
        .then(() => {
          res.status(200).json({ status: 'Ok' });
        })
        .catch(error => {
          res.status(500).send(`SERVER ERROR ${error}`);
        });
    } else {
      res.status(502).send('SERVER ERROR No Access');
    }
  };

  static deleteTeacherFromGroup = (req, res, client, userStorage) => {
    const { authorization } = req.headers;
    const { groupId } = req.body;
    const role = userStorage.getTokenRoleByUserToken(authorization);
    const userId = userStorage.getIdByUserToken(authorization);
    if (role === 'admin') {
      let sql = `UPDATE public.groups SET teacher_id=${userId} WHERE id=${groupId};`;
      client.query(sql,
        (error) => {
          if (error) {
            res.status(500).send('SERVER ERROR');
          } else {
            res.status(200).send({ userId });
          }
        });
    }
  };

  static deleteUserFromUserStorage = (req, res, userStorage) => {
    const { authorization } = req.headers;
    if (authorization) {
      const isUserDelete = userStorage.deleteUser(authorization);
      if (!isUserDelete) {
        res.status(500).send('User isn\'t exist');
      } else {
        res.status(200).send('User delete from server');
      }
    } else {
      res.status(500).send('User isn\'t exist');
    }
  };
}

module.exports = {
  Helper,
};
