class User {
  id = null;
  role = null;
  token = null;

  constructor(role, token, id) {
    this.role = role;
    this.token = token;
    this.id = id;
  }

  getRole() {
    return this.role;
  }

  getId() {
    return this.id;
  }

  getToken() {
    return this.token;
  }
}

class UserStorage {
  users = {};

  constructor() {
  }

  addUser(user) {
    this.users[user.getToken()] = {
      role: user.getRole(),
      id: user.getId(),
    };
  }

  deleteUser(token) {
    if (!token) {
      return false;
    }
    delete this.users[`${token}`];
    return true;
  }

  getTokenRoleByUserToken(token) {
    if (!token) {
      return;
    }
    return this.users[token].role;
  }

  getIdByUserToken(token) {
    if (!token) {
      return;
    }
    return this.users[token].id;
  }
}

module.exports = {
  UserStorage,
  User,
};