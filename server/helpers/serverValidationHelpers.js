const serverValidation = data => {
  for (let key in data) {
    if (data.hasOwnProperty(key)) {
      if (data[key].includes('<')) {
        return false;
      }
    }
  }
  return true;
};

module.exports = {
  serverValidation,
};