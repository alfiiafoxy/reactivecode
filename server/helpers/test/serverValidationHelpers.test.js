const validation = require('../serverValidationHelpers');

describe('serverValidation', () => {
  it('serverValidation is valid', () => {
    const data = {
      email: 'email',
      password: 'password',
    };

    const action = validation.serverValidation(data);

    assert.isTrue(action, 'Data is valid');
  });

  it('serverValidation is invalid', () => {
    const data = {
      email: 'email',
      password: '<password',
    };

    const action = validation.serverValidation(data);

    assert.isFalse(action, 'Data is invalid');
  });
});