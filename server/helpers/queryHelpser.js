const selectGroupId = (client, userId, role) => {
  let sql = null;
  if (role === 'student') {
    sql = `SELECT * FROM "groups_detail" WHERE student_id = ${userId} ORDER BY id ASC;`;
  } else if (role === 'teacher') {
    sql = `SELECT * FROM "groups" WHERE teacher_id = ${userId} ORDER BY id ASC;`;
  }
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve(response.rows);
        } else {
          resolve(null);
        }
      });
  });
};

const selectScheduleAdmin = client => {
  return new Promise((resolve, reject) => {
    client.query('SELECT * FROM "lessons" ORDER BY date ASC',
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve(response.rows);
        } else {
          resolve([]);
        }
      });
  });
};

const selectDataFromTable = (client, groups, tableName, idName, sortBy) => {
  const conditionQuery = lessonsConstructorQuery(groups, tableName, idName, sortBy);
  return new Promise((resolve, reject) => {
    if (!groups) {
      resolve(null);
    }
    client.query(conditionQuery,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve(response.rows);
        } else {
          resolve([]);
        }
      });
  });
};

const lessonsConstructorQuery = (groups, tableName, idName, sortBy) => {
  if (!groups) {
    return;
  }
  let condition = '';
  let count = 0;
  const isGroupId = groups[0].hasOwnProperty('group_id');
  if (isGroupId) {
    groups.forEach(group => {
      condition += `${group.group_id}, `;
      if (count === groups.length - 1) {
        condition += group.group_id;
      }
      count++;
    });
  } else {
    groups.forEach(group => {
      condition += `${group.id}, `;
      if (count === groups.length - 1) {
        condition += group.id;
      }
      count++;
    });
  }
  if (tableName === 'lessons') {
    return `SELECT * FROM "${tableName}" WHERE "${idName}" IN (${condition}) ORDER BY ${sortBy} ASC`;
  } else {
    return `SELECT groups.id, groups.name, groups.level, groups.teacher_id, groups.city, "user".first_name, "user".last_name
            FROM "groups" INNER JOIN "user" ON groups.teacher_id = "user".id WHERE groups.id IN (${condition});`;
  }
};

const addLesson = (client, data) => {
  const { groupsId, startTime, endTime, dataOfLesson, typeOfLesson } = data;
  const sql = `INSERT INTO "lessons" (group_id, start_time, finish_time, date, lesson_type)
                    VALUES ('${groupsId}', '${startTime}', '${endTime}', '${dataOfLesson}', 
                    '${typeOfLesson}') RETURNING id;`;

  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve(response.rows[0].id);
        } else {
          resolve(null);
        }
      });
  });
};

const getUsersByGroup = (client, data, lessonId) => {
  const { groupsId } = data;
  sql = `SELECT * FROM groups_detail WHERE group_id = ${groupsId} ORDER BY id ASC;`;

  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve({ lessonId, users: response.rows });
        } else {
          resolve(null);
        }
      });
  });
};

const setAttendance = (client, data) => {
  const { lessonId, users } = data;
  let resString = '';
  for (let i = 0; i < users.length; i++) {
    if (users.length - 1 !== i) {
      resString += `(${lessonId}, ${users[i].student_id}, false),`;
    } else {
      resString += `(${lessonId}, ${users[i].student_id}, false)`;
    }
  }
  sql = `INSERT INTO attendance ( lesson_id, student_id, attendance) VALUES ${resString} RETURNING id, student_id;`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve({ lessonId, attendanceId: response.rows });
        } else {
          resolve(lessonId);
        }
      });
  });
};

const getUsersData = (client, role) => {
  let sql = '';
  if (role === 'admin') {
    sql = `SELECT "user".id AS user_id, "user".first_name, "user".last_name, "user".email, "user".birth_date, "user".user_role, "user".phone, "user".rate_per_hour FROM "user"; `;
  } else {
    sql = `SELECT "user".id AS user_id, "user".first_name, "user".last_name, "user".email, "user".birth_date, "user".user_role, "user".phone FROM "user" WHERE user_role = 'student'; `;
  }
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve(response.rows);
        } else {
          resolve([]);
        }
      });
  });
};

const getGroupDetail = (client, users) => {
  let sql = `SELECT groups_detail.group_id, groups_detail.student_id, groups.name AS group_name FROM groups_detail INNER JOIN groups ON groups_detail.group_id =  groups.id; `;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve({ users: users, group_detail: response.rows });
        } else {
          resolve({ users: users, group_detail: [] });
        }
      });
  });
};

const addStudentInDb = (client, groupId, studentId) => {
  let sql = `INSERT INTO "groups_detail" (group_id, student_id) VALUES (${groupId}, ${studentId}) RETURNING id`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else {
          resolve(response.rows[0]);
        }
      });
  });
};

const deleteAttendance = (client, lesson_id) => {
  let sql = `DELETE FROM public.attendance WHERE lesson_id=${lesson_id};`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve(lesson_id);
        }
      });
  });
};

const deleteLesson = (client, lesson_id) => {
  let sql = `DELETE FROM lessons WHERE id=${lesson_id} RETURNING group_id;`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve('ok');
        } else {
          resolve('ok');
        }
      });
  });
};

const deleteStudentFromGroupsDetail = (client, user_id) => {
  let sql = `DELETE FROM "groups_detail" WHERE student_id=${user_id}`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const deleteAttendanceByUserID = (client, student_id) => {
  let sql = `DELETE FROM public.attendance WHERE student_id=${student_id};`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const getUserEmail = (client, email, scenario, userId) => {
  let sql = '';
  if (scenario === 'reg') {
    sql = `SELECT email FROM "user" WHERE email = '${email}';`;
  } else {
    sql = `SELECT id FROM "user" WHERE email = '${email}';`;
  }
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (scenario === 'reg' && response.rows.length) {
          resolve('exist');
        } else if (scenario !== 'reg' && response.rows[0].id !== userId) {
          resolve('exist');
        } else {
          resolve('notExist');
        }
      });
  });
};

const updateUserDataHelper = (client, data, userId) => {
  const { first_name, last_name, email, phone } = data;
  let sql = `UPDATE "user" SET first_name='${first_name}',
                       last_name='${last_name}',
                       email='${email}',
                       phone='${phone}'
                       WHERE id='${userId}';`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve('Ok');
        }
      });
  });
};

const updateCurrentUserHelper = (client, data, userId) => {
  const { userRole, firstName, lastName, ratePerHour, phone, birthDate, email } = data;
  let sql = `UPDATE "user" SET  
      user_role='${userRole}', 
      first_name='${firstName}',
      last_name='${lastName}',
      phone='${phone}',
      birth_date='${birthDate}',
      rate_per_hour='${ratePerHour}',
      email='${email}' WHERE id='${userId}';`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else if (response.rows.length) {
          resolve('Ok');
        }
      });
  });
};

const createNewUser = (client, data, defaultRole, token, userStorage, User, checkEmail) => {
  return new Promise((resolve, reject) => {
    const sql1 = `INSERT INTO "user" (first_name, last_name, email, password, birth_date, user_role, token, phone, keywords)
                    VALUES ('${data.firstName}', '${data.lastName}', '${data.email}', '${data.password}', '${data.dateOfBirth}', 
                    '${defaultRole}', '${token}', '${data.phone}', '${data.keywords}') RETURNING id, user_role;`;
    client.query(sql1,
      (err, response) => {
        if (err) {
          reject(err);
        } else {
          const userData = new User(defaultRole, token, response.rows[0].id);
          userStorage.addUser(userData);
          resolve(checkEmail);
        }
      });
  });
};

const getDateOfLessonByGroup = (client, userId, role) => {
  let sql = '';
  if (role === 'student') {
    sql = `select lessons.date, lessons.group_id, lessons.id from lessons where lessons.group_id in (select groups_detail.group_id
                from groups_detail where student_id = ${userId} group by groups_detail.group_id) order by lessons.group_id ASC;`;
  } else if (role === 'teacher') {
    sql = `select lessons.date, lessons.group_id, lessons.id from lessons where lessons.group_id in (select groups.id
    from groups where groups.teacher_id = ${userId} group by groups.id) order by lessons.group_id ASC;`;
  } else {
    sql = `select lessons.date, lessons.group_id, lessons.id from lessons where lessons.group_id in (select groups.id
    from groups group by groups.id) order by lessons.group_id ASC;`;
  }
  return new Promise((resolve, reject) => {
    client.query(sql,
      (err, response) => {
        if (err) {
          reject(err);
        } else if (response.rows.length) {
          resolve(response.rows);
        } else {
          resolve([]);
        }
      });
  });
};

const deleteUser = (client, user_id) => {
  let sql = `DELETE FROM "user" WHERE id = ${user_id};`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const setTeacherIdInGroup = (client, teacher_id, id) => {
  let sql = `UPDATE "groups" SET teacher_id = ${teacher_id} WHERE  id = ${id};`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const setTeacherIdByTeacherId = (client, new_teacher_id, teacher_id) => {
  let sql = `UPDATE "groups" SET teacher_id = ${new_teacher_id} WHERE  teacher_id = ${teacher_id};`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const deleteAttendanceByGroupId = (client, group_id) => {
  let sql = `DELETE FROM attendance WHERE lesson_id IN (SELECT id FROM lessons WHERE group_id=${group_id} GROUP BY id);`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const deleteLessonByGroupId = (client, group_id) => {
  let sql = `DELETE FROM lessons WHERE group_id=${group_id};`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const deleteGroupsDetailByGroupId = (client, group_id) => {
  let sql = `DELETE FROM groups_detail WHERE group_id=${group_id};`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const deleteGroupByGroupId = (client, group_id) => {
  let sql = `DELETE FROM groups WHERE id=${group_id};`;
  return new Promise((resolve, reject) => {
    client.query(sql,
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve('Ok');
        }
      });
  });
};

const getStatistic = (client, userId, role, dateFrom, dateTo) => {
  return new Promise((resolve, reject) => {
    if (role === 'admin') {
      sql = `SELECT users.id as "teacherId", users.first_name, users.last_name, groups.name as "groupName", groups.id as "groupId", lessons.date, groups.city,
                 EXTRACT(EPOCH FROM (lessons.finish_time - lessons.start_time)/3600)*users.rate_per_hour AS "salary",
                 EXTRACT(EPOCH FROM ((lessons.finish_time - lessons.start_time))) AS "duration"
                 FROM "user" AS users 
                 INNER JOIN groups ON users.id = groups.teacher_id     
                 INNER JOIN lessons ON lessons.group_id = groups.id
                 WHERE groups.teacher_id = users.id and lessons.date >= '${dateFrom}' and  lessons.date <= '${dateTo}' ORDER by groups.teacher_id;`;
    } else if (role === 'teacher') {
      sql = `SELECT users.id as "teacherId", users.first_name, users.last_name, groups.name as "groupName", groups.id as "groupId", lessons.date, groups.city,
                 EXTRACT(EPOCH FROM (lessons.finish_time - lessons.start_time)/3600)*users.rate_per_hour AS "salary",
                 EXTRACT(EPOCH FROM (lessons.finish_time - lessons.start_time)) AS "duration"
                 FROM "user" AS users 
                 INNER JOIN groups ON users.id = groups.teacher_id     
                 INNER JOIN lessons ON lessons.group_id = groups.id
                 WHERE groups.teacher_id = ${userId} and lessons.date >= '${dateFrom}' and  lessons.date <= '${dateTo}' ORDER by groups.teacher_id;`;
    }
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else {
          resolve(response.rows);
        }
      });
  });
};

const getStatisticAttendance = (client, userId, role, dateFrom, dateTo, data) => {
  return new Promise((resolve, reject) => {
    if (role === 'admin') {
      sql = `SELECT DISTINCT users.id as "teacherId", groups.name as "groupName", groups.id as "groupId",
                 100 * (SELECT sum(attendance::int) from "attendance" where lesson_id in (select id from "lessons" where group_id = groups.id)) / (SELECT count(*) from "attendance" where lesson_id in (select id from "lessons" where group_id = groups.id))::real AS "attendance"
                 FROM "user" AS users
                 INNER JOIN groups ON users.id = groups.teacher_id     
                 INNER JOIN lessons ON lessons.group_id = groups.id
                 WHERE groups.teacher_id = users.id and lessons.date >= '${dateFrom}' and  lessons.date <= '${dateTo}';`;
    } else if (role === 'teacher') {
      sql = `SELECT DISTINCT users.id as "teacherId", groups.name as "groupName", groups.id as "groupId",
                 100 * (SELECT sum(attendance::int) from "attendance" where lesson_id in (select id from "lessons" where group_id = groups.id)) / (SELECT count(*) from "attendance" where lesson_id in (select id from "lessons" where group_id = groups.id))::real AS "attendance"
                 FROM "user" AS users
                 INNER JOIN groups ON users.id = groups.teacher_id     
                 INNER JOIN lessons ON lessons.group_id = groups.id
                 WHERE groups.teacher_id = ${userId} and lessons.date >= '${dateFrom}' and  lessons.date <= '${dateTo}';`;
    }
    client.query(sql,
      (error, response) => {
        if (error) {
          reject(error);
        } else {
          resolve({ attendance: response.rows, statistic: data });
        }
      });
  });
};

module.exports = {
  selectGroupId,
  selectDataFromTable,
  selectScheduleAdmin,
  addLesson,
  getUsersByGroup,
  setAttendance,
  getUsersData,
  getGroupDetail,
  addStudentInDb,
  deleteAttendance,
  deleteLesson,
  deleteStudentFromGroupsDetail,
  getUserEmail,
  createNewUser,
  getDateOfLessonByGroup,
  deleteAttendanceByUserID,
  setTeacherIdInGroup,
  deleteUser,
  setTeacherIdByTeacherId,
  getStatistic,
  getStatisticAttendance,
  deleteAttendanceByGroupId,
  deleteLessonByGroupId,
  deleteGroupsDetailByGroupId,
  deleteGroupByGroupId,
  updateUserDataHelper,
  updateCurrentUserHelper,
};
