const Helper = require('./requestsHelpers').Helper;
const constants = require('../constants/constants');

class ConnectionWithDataBase {
    constructor(Client, connectionString) {
        this.client = new Client({
            connectionString: connectionString,
        });
    }

    connect() {
        this.client.connect();
    }
}

class Server {
    constructor(httpServer, constants, morgan, bodyParser, accessLogStream, client, jwt, userStorage) {
        this.httpServer = httpServer;
        this.port = constants.port;
        this.defaultRole = constants.defaultRole;
        this.morgan = morgan;
        this.bodyParser = bodyParser;
        this.accessLogStream = accessLogStream;
        this.client = client;
        this.jwt = jwt;
        this.userStorage = userStorage;
    }

    run(dataBase) {
        const { httpServer, port, morgan, bodyParser, accessLogStream, client, defaultRole, jwt, userStorage } = this;

        httpServer.use(bodyParser.json());
        httpServer.use(bodyParser.urlencoded({ extended: true }));
        httpServer.use(morgan('combined', { stream: accessLogStream }));
        httpServer.use(function (req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, authorization');
            next();
        });

        httpServer.listen(port, () => {
            dataBase.connect();
            console.log(`httpServer running on port ${port}`);
            console.log(`connect with BD`);
        });

        httpServer.post(constants.path.reg, (req, res) => Helper.getUserRegistration(req, res, client, jwt, defaultRole, userStorage));
        httpServer.post(constants.path.auth, (req, res) => Helper.getUserAuthorize(req, res, client, jwt, userStorage));
        httpServer.get(constants.path.groups, (req, res) => Helper.getUserGetGroups(req, res, client, userStorage));
        httpServer.get(constants.path.getUserData, (req, res) => Helper.getUserData(req, res, client, userStorage));
        httpServer.get(constants.path.schedule, (req, res) => Helper.getSchedule(req, res, client, userStorage));
        httpServer.post(constants.path.createLesson, (req, res) => Helper.createLessonInDb(req, res, client, userStorage));
        httpServer.get(constants.path.getStudents, (req, res) => Helper.getStudents(req, res, client, userStorage));
        httpServer.get(constants.path.getAttendance, (req, res) => Helper.getAttendance(req, res, client, userStorage));
        httpServer.post(constants.path.deleteLesson, (req, res) => Helper.deleteLesson(req, res, client, userStorage));
        httpServer.post(constants.path.setAttendance, (req, res) => Helper.setAttendance(req, res, client, userStorage));
        httpServer.get(constants.path.getAllUsersData, (req, res) => Helper.getAllUserData(req, res, client, userStorage));
        httpServer.post(constants.path.updateCurrentUser, (req, res) => Helper.updateCurrentUser(req, res, client, userStorage));
        httpServer.post(constants.path.updateUserData, (req, res) => Helper.updateUserData(req, res, client, userStorage));
        httpServer.post(constants.path.setLessonData, (req, res) => Helper.setLessonData(req, res, client, userStorage));
        httpServer.post(constants.path.createGroup, (req, res) => Helper.createGroup(req, res, client, userStorage));
        httpServer.post(constants.path.addStudentInGroup, (req, res) => Helper.addStudentInGroup(req, res, client, userStorage));
        httpServer.post(constants.path.updateTeacher, (req, res) => Helper.updateTeacherInGroup(req, res, client, userStorage));
        httpServer.post(constants.path.getTeacherSalary, (req, res) => Helper.getTeacherSalary(req, res, client, userStorage));
        httpServer.post(constants.path.deleteTeacherFromGroup, (req, res) => Helper.deleteTeacherFromGroup(req, res, client, userStorage));
        httpServer.post(constants.path.deleteStudent, (req, res) => Helper.deleteStudent(req, res, client, userStorage));
        httpServer.post(constants.path.deleteUser, (req, res) => Helper.deleteUser(req, res, client, userStorage));
        httpServer.post(constants.path.deleteGroup, (req, res) => Helper.deleteGroup(req, res, client, userStorage));
        httpServer.post(constants.path.userLogout, (req, res) => Helper.deleteUserFromUserStorage(req, res, userStorage));
    }
}

module.exports = {
    ConnectionWithDataBase,
    Server,
};
