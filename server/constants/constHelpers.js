const fs = require('fs');
const path = require('path');

module.exports = {
  connectionString: fs.readFileSync('resources.txt', 'utf8'),
  accessLogStream: fs.createWriteStream(path.join(__dirname, '../loggerInfo/access.log'), { flags: 'a' }),
};