const rout = require('./rout');

module.exports = {
    port: 3773,
    path: rout,
    serverPath: process.env.NODE_ENV === 'development' ? `http://localhost:3773` : 'http://35.233.224.244/server',
    defaultRole: 'student',
};
